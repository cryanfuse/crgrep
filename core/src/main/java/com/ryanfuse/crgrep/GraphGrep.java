/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.ryanfuse.crgrep.db.NeoGraphParser;
import com.ryanfuse.crgrep.util.GrepMatcher;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Grep a graph database for matches against node:label names and optionally
 * properties (content).
 * 
 * @author Craig Ryan
 */
public class GraphGrep extends AbstractGrep {

	private NeoGraphParser parser;

	public GraphGrep(Switches switches) {
		super(switches);
	}

	public String displayName() {
		return "Graph database grep";
	}

	@Override
	public void execute() throws GrepException {
		trace("Executing GraphGrep");
		if (parser == null) {
			parser = new NeoGraphParser(this, getUri(), getUser(), getPw());
			// if (parser == null) {
			// getDisplay().error("crgrep: unknown/unsupported graph database
			// with URI " + getUri());
			// return;
			// }
		}
		parser.setDisplay(getDisplay());
		parser.setResourceList(getResourceList());
		parser.setIgnoreCase(getSwitches().isIgnoreCase());
		parser.setAllContent(getSwitches().isAllcontent());
		try {
			grepGraph();
			parser.cleanup();
		} catch (Exception e) {
			getDisplay().error("crgrep: failed graph database grep [" + e.getLocalizedMessage() + "]");
			if (e instanceof GrepException) {
				throw (GrepException) e;
			}
			throw new GrepException("crgrep: failed graph database grep", e);
		}
	}

	/*
	 * Generic entry point for all graph database parser implementations.
	 * 
	 * Examples of valid graph patterns and their effect on results ('entries' refer to both nodes and
	 * relationships):
	 * 
     *    crgrep  "*"     "FOO.*"    match all entries (*) against all properties (.*) in entries labeled/named FOO
     *    crgrep  "BAH"   "FOO.*"    match entries containing BAH against all properties (.*) in labeled/named FOO
     *    crgrep  "*"     "*.PROP"   match all labels/names against PROP attributes in all entries
     *    crgrep  "*"     "LAB.C*"   match all properties against property names beginning with 'C' in entries with label/name LAB
     *    crgrep  "BAH"   "T*.*"     match properties containing BAH against all properties (.*) in entries with label/name matching 'T*'
	 */
	private void grepGraph() throws Exception {
		// key:label -> list (node id's + properties)
		if (!parser.openConnection()) {
			// The REST endpoint doesn't implement a method for determining the exact cause 
			// of the failure to connect (bad URI or user/pw).
			StringBuilder sb = new StringBuilder();
			sb.append("Graph database not connected at '").append(parser.getUri());
			sb.append("' as user [").append(parser.getUser()).append("].\n");
			sb.append("The Neo4j server may not be running at the given address, or ");
			sb.append("the supplied user/password may not be valid for this server.");
			getDisplay().error(sb.toString());
			return;
		}
		grepNodes();
		grepRelationships();
	}

	private void grepNodes() throws Exception {
		Map<String, List<Map.Entry<String, List<String>>>> nodeMap = parser.getMatchingNodeMap();
		if (nodeMap == null || nodeMap.isEmpty()) {
			return;
		}
		Set<Entry<String, List<Entry<String, List<String>>>>> matchedNodes = nodeMap.entrySet();
		for (Entry<String, List<Entry<String, List<String>>>> e : matchedNodes) {
			String label = e.getKey();
			List<Entry<String, List<String>>> nodes = e.getValue();
			// Match each node independently. Each node instance may vary by
			// what properties it defines.
			// Each node is reported on its own line.
			for (Entry<String, List<String>> node : nodes) {
				String nodeId = node.getKey();
				List<String> includedProps = node.getValue();
				List<String> matchingProps = resourceListMatch(includedProps);
				if (matchingProps.isEmpty()) {
					// <node>.<prop> pattern must match this entry by
					// name otherwise ignore the whole node.
					continue;
				}
				// Grep for pattern against included label/properties
				if (isContent() || isAllcontent()) {
					matchNodeContents(label, nodeId, matchingProps);
				} else {
					matchNodeListing(label, nodeId, matchingProps);
				}
			}
		}
	}

	private void grepRelationships() throws Exception {
		Map<String, List<Map.Entry<String, List<String>>>> relMap = parser.getMatchingRelationshipMap();
		if (relMap == null || relMap.isEmpty()) {
			//getDisplay().warn("Graph database has no relationships with matching name!");
			return;
		}
		Set<Entry<String, List<Entry<String, List<String>>>>> matchedRels = relMap.entrySet();
		for (Entry<String, List<Entry<String, List<String>>>> e : matchedRels) {
			String relName = e.getKey();
			List<Entry<String, List<String>>> rels = e.getValue();
			// Match each relationship independently. Each relationship instance
			// may vary by what properties it defines.
			// Each relationship is reported on its own line.
			for (Entry<String, List<String>> rel : rels) {
				String relId = rel.getKey();
				List<String> includedProps = rel.getValue();
				List<String> matchingProps = resourceListMatch(includedProps);
				if (matchingProps.isEmpty()) {
					// <rel>.<prop> pattern must match this relationship by
					// name otherwise ignore the whole relationship.
					continue;
				}
				// Grep for pattern against included name/properties
				if (isContent() || isAllcontent()) {
					matchRelationshipContents(relName, relId, matchingProps);
				} else {
					matchRelationshipListing(relName, relId, matchingProps);
				}
			}
		}
	}

	/*
	 * Subset of a node/relationship's properties which match required resource
	 * list pattern by name.
	 */
	public List<String> resourceListMatch(List<String> includedProps) throws Exception {
		List<String> matchedProps = new ArrayList<String>();
		trace("Included properties test against pattern [" + getResourceList().lastPart() + "]");
		if (!getResourceList().isLastPartWild()) {
			matchedProps = new ArrayList<String>();
			for (String prop : includedProps) {
				trace("Included property [" + prop + "] test (ignore case)");
				if (getResourceList().lastPartContainsWild()) {
					GrepMatcher pm = GrepMatcher.getMatcher(prop, getResourceList().lastPartResourcePattern(),
							getSwitches(), true);
					if (!pm.matches()) {
						continue;
					}
				} else {
					if (!GrepMatcher.matchingStrings(prop, getResourceList().lastPart(), getSwitches(), true)) {
						continue;
					}
				}
				trace("  -> matched!");
				matchedProps.add(prop);
			}
		} else {
			// all properties match
			matchedProps = includedProps;
		}
		if (matchedProps.size() > 0) {
			debug("Included properties count " + includedProps.size() + ", matching subset count " + matchedProps.size()
					+ " for pattern " + getResourceList().lastPart());
		}
		return matchedProps;
	}

	/*
	 * Match by node label and property names only.
	 */
	private void matchNodeListing(String label, String nodeId, List<String> includedProps) {
		// Of the filtered resources, attempt a match against <pattern>
		debug("GraphGrep match node listing (case always ignored)");
		StringBuilder match = new StringBuilder();
		// get case insensitive pattern matcher
		GrepMatcher pm = GrepMatcher.getMatcher(label, getResourcePattern(), getSwitches(), true);
		boolean hit = pm.matches();
		match.append("Node[").append(nodeId).append("]:").append(label);
		match.append(" {");
		boolean propNameMatch = matchesEntryPropertyName(includedProps, pm, match);
		hit = hit ? hit : propNameMatch;
		match.append("}");
		if (hit) {
			getDisplay().result(match.toString());
		}
	}

	/*
	 * Match by relationship name and property names only.
	 */
	private void matchRelationshipListing(String relName, String relId, List<String> includedProps) {
		// Of the filtered resources, attempt a match against <pattern>
		debug("GraphGrep match relationship listing (case always ignored)");
		StringBuilder match = new StringBuilder();
		// get case insensitive pattern matcher
		GrepMatcher pm = GrepMatcher.getMatcher(relName, getResourcePattern(), getSwitches(), true);
		boolean hit = pm.matches();
		long[] nodes = parser.getRelationshipNodeIds(relId);
		if (nodes != null && nodes[0] != -1) {
			match.append("(Node[").append(nodes[0]).append("])-");
		}
		match.append("Relationship[").append(relId).append("]");
		match.append(":").append(relName).append(" {");
		boolean propNameMatch = matchesEntryPropertyName(includedProps, pm, match);
		hit = hit ? hit : propNameMatch;
		match.append("}");
		if (nodes != null && nodes[1] != -1) {
			match.append("-(Node[").append(nodes[1]).append("])");
		}
		if (hit) {
			getDisplay().result(match.toString());
		}
	}

	/*
	 * Match by node label/properties name or property values (content).
	 * Properties matching by name only display name, by value display both
	 * name/value. If no property matches nor node label match, nothing is
	 * displayed.
	 */
	private void matchNodeContents(String label, String nodeId, List<String> includedProps) {
		// Of the filtered resources, attempt a match against <pattern>
		StringBuilder match = new StringBuilder();
		// get case insensitive pattern matcher
		GrepMatcher pm = GrepMatcher.getMatcher(label, getResourcePattern(), getSwitches(), true);
		boolean hit = false;
		hit = pm.matches();
		// format: Node[1234]: { key : val, ... }
		match.append("Node[").append(nodeId).append("]:").append(label);
		match.append(" {");
		boolean propNameOrValueMatch = matchesEntryPropertyNameOrValue(nodeId, true, includedProps, pm, match);
		// include prop name/value result in whether to report a 'hit' or not.
		hit = hit ? hit : propNameOrValueMatch;
		match.append("}");
		if (hit) {
			getDisplay().result(match.toString());
		} else {
			debug("No node content match for [" + match.toString() + "]");
		}
	}


	/*
	 * Match by relationship name/properties name or property values (content).
	 * Properties matching by name only display name, by value display both
	 * name/value. If no property matches nor node label match, nothing is
	 * displayed.
	 * 
	 * Surrounding start/end node IDs are included in matches to show which nodes
	 * the relationships belongs to.
	 */
	private void matchRelationshipContents(String relName, String relId, List<String> includedProps) {
		// Of the filtered resources, attempt a match against <pattern>
		StringBuilder match = new StringBuilder();
		// get case insensitive pattern matcher
		GrepMatcher pm = GrepMatcher.getMatcher(relName, getResourcePattern(), getSwitches(), true);
		boolean hit = false;
		hit = pm.matches();
		// format: (Node[1])-Relationship[222]: { key : val, ... }-(Node[2])
		long[] nodes = parser.getRelationshipNodeIds(relId);
		if (nodes != null && nodes[0] != -1) {
			match.append("(Node[").append(nodes[0]).append("])-");
		}
		match.append("Relationship[").append(relId).append("]");
		match.append(":").append(relName).append(" {");
		boolean propNameOrValueMatch = matchesEntryPropertyNameOrValue(relId, false, includedProps, pm, match);
		// include prop name/value result in whether to report a 'hit' or not.
		hit = hit ? hit : propNameOrValueMatch;
		match.append("}");
		if (nodes != null && nodes[1] != -1) {
			match.append("-(Node[").append(nodes[1]).append("])");
		}
		if (hit) {
			getDisplay().result(match.toString());
		} else {
			debug("No rel content match for [" + match.toString() + "]");
		}
	}

	/*
	 * Node/relationship property name only match (i.e. listing only search)
	 */
	private boolean matchesEntryPropertyName(List<String> includedProps, GrepMatcher pm, StringBuilder match) {
		boolean hit = false;
		boolean first = true;
		for (String p : includedProps) {
			pm.setText(p);
			if (pm.matches()) {
				hit = true;
				if (!first) {
					match.append(",");
				}
				match.append(pm.toString());
				first = false;
			}
		}
		return hit;
	}

	/*
	 * Node/relationship property name/value match
	 */
	private boolean matchesEntryPropertyNameOrValue(String entryId, boolean isNode, List<String> includedProps,
			GrepMatcher pm, StringBuilder match) {
		boolean hit = false;
		boolean first = true;
		for (String propKey : includedProps) {
			pm.setText(propKey);
			boolean nameMatched = pm.matches();
			StringBuilder valueMatch = new StringBuilder();
			boolean valueMatched = parser
					.matchesPropertyValue(propKey,
							isNode ? parser.nodePropertyResult(entryId, propKey)
									: parser.relationshipPropertyResult(entryId, propKey),
							valueMatch, getResourcePattern());
			if (nameMatched || valueMatched) {
				hit = true;
				if (!first) {
					match.append(",");
				}
				match.append(nameMatched ? pm.toString(false) : propKey);
				first = false;
				if (valueMatched) {
					match.append(valueMatch);
				}
				debug("Prop [" + propKey + ":" + valueMatch.toString() + "] matches pattern ["
						+ getResourcePattern().getPatternString() + "]");
			}
		}
		return hit;
	}

	// For testing, allow mock injection
	public void setParser(NeoGraphParser parser) {
		this.parser = parser;
	}
}
