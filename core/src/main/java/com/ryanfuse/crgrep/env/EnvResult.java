/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.env;

import java.util.ArrayList;
import java.util.List;

/*
 * An environmental status result to capture a status and related message text 
 * to report back to the user.
 */
public class EnvResult {

    private String module;
    private EnvStatus status;
    private List<String> errors;
    private List<String> warnings;
    
    public EnvResult(String module) {
        this(module, null);
    }

    public EnvResult(String module, EnvStatus status) {
        this.module = module;
        if (status != null) {
            setStatus(status);
        }
        errors = new ArrayList<String>();
        warnings = new ArrayList<String>();
    }

    public List<String> getErrors() {
        return errors;
    }

    public List<String> getWarnings() {
        return warnings;
    }

    public String getModule() {
        return module;
    }

    public EnvStatus getStatus() {
        return status;
    }

    public void setStatus(EnvStatus status) {
        this.status = status;
    }

    public boolean hasErrors() {
        return status == EnvStatus.ERROR || status == EnvStatus.ERROR_WARN;
    }

    public boolean hasWarnings() {
        return status == EnvStatus.WARN || status == EnvStatus.ERROR_WARN;
    }
    
    public boolean isOK() {
        return status == EnvStatus.OK;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (status == EnvStatus.OK) {
            sb.append("Environment check OK for '").append(module).append("'\n");
        } else {
            sb.append("Environment issues exist for '").append(module).append("'\n");
            if (status == EnvStatus.ERROR || status == EnvStatus.ERROR_WARN) {
                sb.append("> Errors detected:\n").append(getMessages(errors));
            }
            if (status == EnvStatus.WARN || status == EnvStatus.ERROR_WARN) {
                sb.append("> Warnings detected:\n").append(getMessages(warnings));
            }
        }
        return sb.toString();
    }

    private String getMessages(List<String> messages) {
        StringBuilder sb = new StringBuilder();
        for (String msg: messages) {
            sb.append(msg);
        }
        return sb.toString();
    }
}
