/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.lang.StringUtils;

public interface PathUtils {

    /*
     * Does the given path point to a valid directory on disk?
     */
    static boolean validDirPath(String dirpath) {
        if (StringUtils.isEmpty(dirpath)) {
            return false;
        }
        File dir = new File(dirpath);
        return dir.isDirectory();
    }

    /*
     * Does the given path point to a valid file on disk?
     */
    static boolean validFilePath(String filepath) {
        if (StringUtils.isEmpty(filepath)) {
            return false;
        }
        File dir = new File(filepath);
        return dir.isFile();
    }

	/*
	 * Strip out any '..' etc in paths to make them more readable when displayed
	 */
	static String normalisedHomeDir() {
		String home = System.getProperty("crgrep.home");
		if (home != null) {
			try {
				Path p = Paths.get(home);
				home = p.normalize().toString();
			} catch (Exception e) {
				// abort attempt to normalise, use home as-is.
			}
		}
		return home;
	}
}
