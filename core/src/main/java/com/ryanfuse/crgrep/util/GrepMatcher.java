/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.ResultText;

/**
 * Provides a matcher class to control which lines match the search pattern.
 *
 * Supports the -v option to reverse the match to lines which don't match the pattern.
 * 
 * @author 'Craig Ryan'
 */
public class GrepMatcher {

    private boolean isReverseMatch = false;
    private Matcher matcher;
    private String string;
    private Switches switches;
    private boolean ignoreCase;
    private ResourcePattern resourcePattern;

    // Convenience call for file grep
    public static GrepMatcher getMatcher(String text, FileGrep fileGrep) {
        return new GrepMatcher(text, fileGrep.getResourcePattern(), fileGrep.getSwitches(), fileGrep.getSwitches().isIgnoreCase());
    }

    // Not ready with text until setText() call
    public static GrepMatcher getMatcher(ResourcePattern pattern, Switches switches) {
        return new GrepMatcher(null, pattern, switches, switches.isIgnoreCase());
    }

    // Generic call
    public static GrepMatcher getMatcher(String text, ResourcePattern pattern, Switches switches) {
        return new GrepMatcher(text, pattern, switches, switches.isIgnoreCase());
    }

    // Override ignoreCase setting on the command line
    public static GrepMatcher getMatcher(String text, ResourcePattern pattern, Switches switches, boolean ignoreCase) {
        return new GrepMatcher(text, pattern, switches, ignoreCase);
    }

    // Do the strings match?
    public static boolean matchingStrings(String s1, String s2, Switches switches, boolean ignoreCase) {
        // TODO switches check for is reverse test and if true return !same
        boolean same = ignoreCase ? s1.toUpperCase().equals(s2.toUpperCase()) : s1.equals(s2);
        return same;
    }

    // Does the string match the pattern (regular expression, so '.' instead of '?' etc).
    public static boolean matchingPattern(String string, String pattern, Switches switches, boolean ignoreCase) {
        // TODO switches check for is reverse test and if true return !same
        Pattern p = ignoreCase ? Pattern.compile(pattern, Pattern.CASE_INSENSITIVE) : Pattern.compile(pattern);
        return p.matcher(string).matches();
    }

    private GrepMatcher(String text, ResourcePattern resourcePattern, Switches switches, boolean ignoreCase) {
        this.resourcePattern = resourcePattern;
        this.switches = switches;
        this.ignoreCase = ignoreCase;
        if (text != null) {
            this.matcher = resourcePattern.getPattern(ignoreCase).matcher(text);
            setText(text);
        } else {
            this.string = null;
        }
        // TODO isReverseMatch = switches.isReverseMatch();
    }

    // This matcher can be re-used with existing settings by specifying a new string.
    public void setText(String text) {
        this.string = text;
        this.matcher = resourcePattern.getPattern(ignoreCase).matcher(text);
    }

    /* Determine if the pattern matches the given sequence */
    public boolean matches() {
        // TODO: whole line match could be selected here 
        if (isReverseMatch) {
            return !matcher.find();
        }
        return matcher.find();
    }
    
    public Matcher getMatcher() {
        if (matcher != null) {
            // Reset the find() state. TODO: if we introduce boundary limits on matching (eg between col M:N) then this code
            // will also need to include a call to matcher.region(M, N)
            matcher.reset();
        }
        return matcher;
    }
    
    @Override
    public String toString() {
        return toString(this.string, true);
    }

    public String toString(boolean applyTrimming) {
        return toString(this.string, applyTrimming);
    }

    public String toString(String theText) {
        return toString(theText, true);
    }

    // Allow the displayed text be different from the text used to test for a match.
    //
    public String toString(String theText, boolean applyTrimming) {
        ResultText rt = new ResultText(resourcePattern, switches);
        // Could be trimmed/colour highlighted etc
        return rt.result(theText, applyTrimming);
    }

    public Pattern getPattern() {
        return resourcePattern.getPattern();
    }
}
