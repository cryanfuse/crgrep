/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.lang.StringUtils;

import com.ryanfuse.crgrep.nlp.Sentiment;

/**
 * Switches (toggles) specified by the command line option list. Also holds a reference
 * to Properties loaded from ~/.crgrep.
 * 
 * @author Craig Ryan
 */
public class Switches {

    private static final int DEFAULT_MAX_SENTENCE_WORDS = 20;
    private static final int DEFAULT_SENTENCE_LINES_OF_CONTEXT = 2;
	
    private boolean verbose;
    private boolean debug;
    private boolean trace;

    private boolean recurse;
    private boolean content;
    private boolean allcontent;
    private boolean listing;
    private boolean database;
    private boolean maven;
    private boolean ocr; // optical char recognition
    private Properties userProperties;
	private Integer maxResultLength;
	private Integer maxSentenceWords = DEFAULT_MAX_SENTENCE_WORDS;
	private Integer maxSentenceLinesOfContext = DEFAULT_SENTENCE_LINES_OF_CONTEXT;
    private boolean ignoreCase;
    private boolean removeLineBreaks;
    private Highlight highlight;
    private String highlightWhen;
    private Sentiment sentiment;
    
	public Switches() {
    }

    public Switches(CommandLine line) {
        ignoreCase = line.hasOption('i');
        verbose = line.hasOption("warn");
        recurse = line.hasOption('r');
        listing = true;
        content = !line.hasOption('l');
        maven = line.hasOption('m');
        allcontent = line.hasOption('a');
        if (line.hasOption("color")) {
            highlightWhen = line.getOptionValue("color"); 
        } else if (line.hasOption("colour")) {
            highlightWhen = line.getOptionValue("colour"); 
        }

        // Can we force --colour=xx ? instead?
        setOcr(line.hasOption("ocr"));
        if (!content && allcontent) {
            content = true;
        }
        database = line.hasOption('d');
        maxResultLength = Integer.MAX_VALUE;

        // exclusions - TODO
        /*
        if (line.hasOption('e')) {
            String[] exs = line.getOptionValues('e');
            for (String ex : exs) {
                if (ex.equals("l")) {
                    listing = false;
                }
            }
        }
        */
        
    	sentiment = Sentiment.IGNORE; 
        if (line.hasOption("mood")) {
        	String m = line.getOptionValue("mood");
        	if (!StringUtils.isEmpty(m)) {
        		m = m.toLowerCase();
        		if (m.equals("1") || m.startsWith("pos")) {
        			sentiment = Sentiment.POSITIVE;
        		} else if (m.equals("0") || m.startsWith("neu")) {
        			sentiment = Sentiment.NEUTRAL;
        		} else if (m.equals("-1") || m.startsWith("neg")) {
        			sentiment = Sentiment.NEGATIVE;
        		} else {
        			System.err.println("--mood value '" + m + "' unknown, mood filtering will be disabled.");
        		}
        	}
        }
    }
    
    /**
     * Extensions such as debug, trace
     *  
     * @param extensions
     */
    public void processExtensions(String extensions) {
        if (extensions == null) {
            return;
        }
        for (String extension : extensions.split(",")) {
            if (extension.equalsIgnoreCase("debug")) {
                debug = true;
            } else if (extension.equalsIgnoreCase("trace")) {
                trace = true;
            }
        }
    }

    /**
     * Verbose output?
     * @return
     */
    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    /**
     * Debug output?  
     * 
     * @return
     */
    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    /**
     * Trace output?
     * @return
     */
    public boolean isTrace() {
        return trace;
    }

    public void setTrace(boolean trace) {
        this.trace = trace;
    }

    /**
     * Recurse into sub directories?
     * 
     * @return
     */
    public boolean isRecurse() {
        return recurse;
    }

    public void setRecurse(boolean recurse) {
        this.recurse = recurse;
    }

    /**
     * Include content in search (instead of just resource name matches)
     *  
     * @return
     */
    public boolean isContent() {
        return content;
    }

    public void setContent(boolean content) {
        this.content = content;
    }

    /**
     * Include all content types in search rather than just text related resources? 
     * 
     * @return
     */
    public boolean isAllcontent() {
        return allcontent;
    }

    public void setAllcontent(boolean allcontent) {
        this.allcontent = allcontent;
    }

    /**
     * Are Listings included in the search?
     * 
     * @return
     */
    public boolean isListing() {
        return listing;
    }

    public void setListing(boolean listing) {
        this.listing = listing;
    }

    /**
     * Is this a Database search?
     * 
     * @return
     */
    public boolean isDatabase() {
        return database;
    }

    public void setDatabase(boolean database) {
        this.database = database;
    }

    /**
     * Should the search expand POM file(s) and include those dependencies (artifacts) in the search? 
     * @return
     */
    public boolean isMaven() {
        return maven;
    }

    public void setMaven(boolean maven) {
        this.maven = maven;
    }

    /**
     * Is OCR text extraction from image files enabled?
     * 
     * This option will require a pre-existing Win/Mac/Linux installation of 'tesseract' and it associated language
     * data libraries. See the README for instructions.
     * 
     * @return
     */
	public boolean isOcr() {
		return ocr;
	}

	public void setOcr(boolean ocr) {
		this.ocr = ocr;
	}

	/**
     * Store a reference to the user properties (~/.crgrep) as a convenience in here for 
     * any implementation classes to lookup their own properties.
     * 
     * @param userProperties
     */
    public void setUserProperties(Properties userProperties) {
        this.userProperties = userProperties;
    }

    public Properties getUserProperties() {
        return userProperties;
    }

    /**
     * Max length of characters in a matched line of text that will be displayed.
     * @param maxResultLen max length, if positive integer its value is used otherwise set to unlimited
     */
	public void setMaxResultLength(Integer maxResultLen) {
        if (maxResultLen == null) {
            maxResultLength = null;
            return;
        }
        maxResultLength = maxResultLen <= 0 ? Integer.MAX_VALUE : maxResultLen;
	}

	public Integer getMaxResultLength() {
		return maxResultLength;
	}

	public boolean isIgnoreCase() {
		return ignoreCase;
	}

	public void setIgnoreCase(boolean ignoreCase) {
		this.ignoreCase = ignoreCase;
	}

    public boolean isRemoveLineBreaks() {
        return removeLineBreaks;
    }

    public void setRemoveLineBreaks(boolean removeLineBreaks) {
        this.removeLineBreaks = removeLineBreaks;
    }

    public boolean isHighlighting() {
        if (highlight == null) {
            return false;
        }
        return highlight.isHighlighting();
    }

    // Are we filtering by mood
    public boolean isMood() {
		return sentiment != Sentiment.IGNORE;
	}

    public Sentiment getSentiment() {
		return sentiment;
	}

	public void setSentiment(Sentiment sentiment) {
		this.sentiment = sentiment;
	}


    /*
     * Options and user preferences have been loaded. Time for any
     * post-loading initialisations.
     */
    public void userPropertiesLoaded() {
        String col = null;
        String sdelim = null, edelim = null;
        boolean ansi = false; 
        if (userProperties != null) {
            if (highlightWhen == null) {
                highlightWhen = userProperties.getProperty("colour");
                if (highlightWhen == null) {
                    // U.S. spelling also supported
                    highlightWhen = userProperties.getProperty("color");
                }
            }
            col = userProperties.getProperty("colour.code");
            if (col == null) {
                col = userProperties.getProperty("color.code");
            }
            sdelim = userProperties.getProperty("colour.code.start");
            if (sdelim == null) {
                sdelim = userProperties.getProperty("color.code.start");
            }
            edelim = userProperties.getProperty("colour.code.end");
            if (edelim == null) {
                edelim = userProperties.getProperty("color.code.end");
            }
            String forceAnsi= userProperties.getProperty("colour.ansi");
            if (forceAnsi == null) {
                forceAnsi = userProperties.getProperty("color.ansi");
            }
            if (forceAnsi != null) {
                try {
                    ansi = Boolean.parseBoolean(forceAnsi);
                } catch (Exception e) {
                    // not a boolean, remains as false
                }
            }
            if (this.trace) {
                System.err.println("Highlight ~/.crgrep: color=" + (highlightWhen == null ? "<empty>" : highlightWhen)
                        + ", code=" + col + ", start=" + (sdelim==null?"<empty>":sdelim) + ", end=" 
                        + (edelim==null?"<empty>":edelim));
            }
        }

        // Validation check
        if (highlightWhen != null) {
            highlightWhen = highlightWhen.toLowerCase();
            switch (highlightWhen) {
            case "auto": 
            case "always": 
            case "never": 
                break;
            default:
                System.err.println("--colour value '" + highlightWhen + "' unknown, highlighting will be disabled.");
            }
        }

        highlight = new Highlight(highlightWhen, col, ansi);
        if (sdelim != null) {
            highlight.setStart(sdelim);
        }
        if (edelim != null) {
            highlight.setEnd(edelim);
            if (sdelim == null) {
                // clear the default colour control code
                highlight.setStart(null);
            }
        }
    }

    public Highlight getHighlight() {
        return highlight;
    }

    public void setHighlight(Highlight highlight) {
        this.highlight = highlight;
    }

	public Integer getMaxSentenceWords() {
		return maxSentenceWords;
	}

	public void setMaxSentenceWords(Integer maxSentenceWords) {
		if (maxSentenceWords <= 0) {
			System.err.println("Warning: nlp.maxSentenceWords value '" + maxSentenceWords + "' in <HOME>/.crgrep cannot be less than 1. Using default " + DEFAULT_MAX_SENTENCE_WORDS);
			return;
		}
		this.maxSentenceWords = maxSentenceWords;
	}

	public Integer getMaxSentenceLinesOfContext() {
		return maxSentenceLinesOfContext;
	}

	public void setMaxSentenceLinesOfContext(Integer maxSentenceLinesOfContext) {
		if (maxSentenceLinesOfContext < 0) {
			System.err.println("Warning: nlp.maxSentenceLinesOfContext value '" + maxSentenceLinesOfContext + "' in <HOME>/.crgrep cannot be less than 0. Using default " + DEFAULT_SENTENCE_LINES_OF_CONTEXT);
			return;
		}
		this.maxSentenceLinesOfContext = maxSentenceLinesOfContext;
	}
}
