/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.util.ArrayList;
import java.util.List;

public class ParameterUtils {

    // Strip surrounding quotes (don't handle mismatched quotes)
    public static String stripQuotes(String param) {
        if (param == null) {
            return null;
        }
        int strLen = param.length();
        if (strLen <= 1) {
            return param;
        }
        if (param.startsWith("\'")) {
            if (param.endsWith("\'")) {
                param = param.substring(1, strLen-1);
            }
        } else if (param.startsWith("\"")) {
            if (param.endsWith("\"")) {
                param = param.substring(1, strLen-1);
            }
        }
        return param;
    }

    // Strip surrounding quotes off each list element (don't handle mismatched quotes)
    public static List<String> stripQuotes(List<String> params) {
        if (params == null || params.isEmpty()) {
            return params;
        }
        // Too bad if the original list contains no strings needing stripping.
        // Element by element check and replace would be just as slow.
        List<String> strippedList = new ArrayList<String>(params.size());
        for (String p : params) {
            strippedList.add(stripQuotes(p));
        }
        return strippedList;
    }
}
