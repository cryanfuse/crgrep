/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import static org.fusesource.jansi.internal.CLibrary.STDOUT_FILENO;
import static org.fusesource.jansi.internal.CLibrary.isatty;

import java.io.PrintStream;

import org.fusesource.jansi.WindowsAnsiOutputStream;

import com.ryanfuse.crgrep.ResultText;
import com.ryanfuse.crgrep.util.Highlight.Colouring;

/**
 * Class to handle all output displayed
 * 
 * @author Craig Ryan
 */
public class Display {
    private boolean debugOn;
	private boolean traceOn;
	private boolean warnOn;
	// Prefix for log messages
    private String logPrefix = null;
	// Prefix for displaying results
    private String prefix = null;
    private PrintStream outStream = System.out;
    private PrintStream errStream = System.err;
    private boolean winCheck = false;
    private Highlight highlight;
    
    public Display() {
        String os = System.getProperty("os.name");
        winCheck = os.startsWith("Windows");
    }

	/*
	 * Display a result (grep match)
	 * TODO remove and use print methods instead
     */
	public Display result(String msg) {
        if (getPrefix() != null) {
            outStream.print(getPrefix());
        }
        outStream.println(msg);
        return this;
	}

    public Display print(ResultText resultText) {
        if (getPrefix() != null) {
            outStream.print(getPrefix());
        }
        outStream.println(resultText.toString());
        return this;
    }
    
    // bypass result text, unconditional print
    public void print(String text) {
        if (getPrefix() != null) {
            outStream.print(getPrefix());
        }
        outStream.print(text);
    }

    public void println(String text) {
        if (getPrefix() != null) {
            outStream.print(getPrefix());
        }
        outStream.println(text);
    }

    public String getPrefix() {
        return prefix;
    }

    public void unsetPrefix() {
        this.prefix = null;
    }
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Display warn(String msg) {
    	if (isWarnOn()) {
    		outStream.println(msg);
    	}
		return this;
	}

	public Display error(String msg) {
		errStream.println(msg);
		return this;
	}

	public Display trace(String msg) {
		if (isTraceOn()) {
			prefix(outStream);
			outStream.println(msg);
		}
		return this;
	}

	public Display debug(String msg) {
		if (isDebugOn()) {
			prefix(outStream);
			outStream.println(msg);
		}
		return this;
	}

	private Display prefix(PrintStream ps) {
		if (logPrefix == null) {
			return this;
		}
		ps.print("(" + logPrefix + ") ");
		return this;
	}

	public String getLogPrefix() {
		return logPrefix;
	}

	/**
	 * Set a prefix
	 * @param prefix
	 */
	public void setLogPrefix(String prefix) {
		this.logPrefix = prefix;
	}

	public boolean isDebugOn() {
		return debugOn;
	}

	public void setDebugOn(boolean debugOn) {
		this.debugOn = debugOn;
	}

	public boolean isTraceOn() {
		return traceOn;
	}

	public void setTraceOn(boolean traceOn) {
		this.traceOn = traceOn;
	}

	public boolean isWarnOn() {
		return warnOn;
	}

	public void setWarnOn(boolean warnOn) {
		this.warnOn = warnOn;
	}

    public PrintStream getOutStream() {
        return outStream;
    }

    public void setOutStream(PrintStream outStream) {
        this.outStream = outStream;
    }

    public PrintStream getErrStream() {
        return errStream;
    }

    public void setErrStream(PrintStream errStream) {
        this.errStream = errStream;
    }

    public Highlight getHighlight() {
        return highlight;
    }

    public void setHighlight(Highlight highlight) {
        this.highlight = highlight;
        if (highlight != null && highlight.isHighlighting()) {
            if (winCheck) {
                // We're on windows and the configured 'colour.ansi' flag might be enabled which forces us
                // to bypass normal Windows native highlighting and assume a Posix console (eg emacs/cygwin etc).
                //
                if (highlight.isAnsi()) {
                    // If AUTO mode and not attached to a tty then disable highlighting
                    // TODO this doesn't work, need to skip the check and allow highlighting
                    /*
                    if (highlight.getColouring() == Highlight.Colouring.AUTO) {
                        checkForTty();
                    }
                    */
                } else {
                    // On windows and the console does not interpret ANSI escape codes, wrap in a stream
                    // which will use native Windows highlighting via JNI/JNA.
                    try {
                        //debug("Windows native highlighting enabled.");
                        this.outStream = new PrintStream(new WindowsAnsiOutputStream(System.out));
                        return;
                    } catch (Throwable ignore) {
                        // this happens when JNA is not in the path.. or
                        // this happens when the stdout is being redirected to a file.
                        //debug("Unable to load Windows native console support, colour highlighting may not work.");
                    }
                    this.highlight.setColouring(Colouring.NONE);
                }
            } else {
                // Linux based. If AUTO mode and not attached to a tty then disable highlighting
                if (highlight.getColouring() == Highlight.Colouring.AUTO) {
                    checkForTty();
                }
            }
        }
    }

    // Check if we're attached to a TTY, if not disable highlighting.
    // Not being attached means stdout is directed to a pipe or file.
    //
    /*
    */
    private void checkForTty() {
        boolean disabled = true;
        try {
            // Just doesn't work on Posix terminals (i.e not cmd.exe)
            if (isatty(STDOUT_FILENO) != 0) {
                // Attached to a TTY, leave enabled. 
                disabled = false;
            }
            // These erros happen if the JNI lib is not available for your platform.
        } catch (NoClassDefFoundError ignore) {
        } catch (UnsatisfiedLinkError ignore) {
        }
        if (disabled) {
            debug("Not attached to a TTY, colour highlighting disabled.");
            this.highlight.setColouring(Colouring.NONE);
        }
    }
}