/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

/*
 * Represents a resource path on disk.
 * 
 * A path consists of real directories and/or wild-carded components which can be used
 * to search and discover multiple physical resources.
 * 
 * This class is implemented using java 7 Paths/Path behaviour for splitting 
 * paths and handles embedded wild cards and '**' ant style strings by treating
 * the parent part from the wild part separately and therefore without throwing
 * an illegal path exception. The 'parentPath' should be a valid path by Paths.get(path).    
 */
public class ResourcePath {
    private static final CharSequence ANT_GLOB_PATTERN = "**";
    private String fullPath;
    private List<String> dirParts; // path parts that exist on disk 
    private List<String> parentParts; // path parts that exist on disk 
    private String rootPart; // prefix to the dirParts
    private String parentPath;
    private String wildPath; // trailing path that cannot be found on disk (likely contain wild cards)
    private boolean validParent;
    private boolean wildStartsWithNonGlob;
    private Path javaPath;
    private int javaPathNameCount = 0;
    private Display display;
    private int pathDepth = 0;
    private int wildDepth = 0;
    
    public ResourcePath(String path, Display display) {
        this.fullPath = ParameterUtils.stripQuotes(path);
        this.display = display;
        parentPath = null;
        wildPath = null;
        validParent = false;
        wildStartsWithNonGlob = false;
        dirParts = new ArrayList<String>();
        parentParts = new ArrayList<String>();
        
        if (StringUtils.isEmpty(path)) {
            return;
        }
        path = path.trim();
        
        boolean isFile = PathUtils.validFilePath(path);
        if (isFile || PathUtils.validDirPath(path)) {
            parentPath = path;
            validParent = true;
            javaPath = Paths.get(path);
            javaPathNameCount = javaPath.getNameCount() - (isFile ? 1 : 0);
            return;
        }

        // Divide the path string into its components based on standard path separators.
        convertToParts();
        
        // Separate out the deepest base directory (real directory path on disk) from
        // the trailing wild carded path. 
        calculateSplit();
        
        // Calculate how deep do search in the directory tree
        calculatePathDepth();
    }

    /*
     * Parent directory path (real path on disk). This is the longest part
     * of the original path that maps to a real directory path on disk.
     */
    public String getParentPath() {
        if (rootPart == null) {
            return parentPath;
        }
        return rootPart + parentPath;
    }

    /*
     * Number of name parts in the real parent path (excludes Root)
     * For '/a/b/c/foo.txt' with parent path '/a/b' will return 2.
     */
    public int getParentNameCount() {
        if (javaPath != null) {
            return javaPathNameCount;
        }
        return parentParts.size();
    }
    
    /*
     * Is there a wild path?
     */
    public boolean hasValidWildPath() {
        return javaPath == null ? StringUtils.isNotEmpty(wildPath) : false;
    }

    /*
     * Wild path. That part of the path following the parent path.
     */
    public String getWildPath() {
        if (javaPath != null) {
            return null;
        }
        return wildPath;
    }

    /*
     * Wild path contains '**'?
     */
    public boolean wildPathContainsAntglob() {
        if (javaPath != null) {
            return false;
        }
        return wildPath != null && wildPath.contains(ANT_GLOB_PATTERN);
    }

    /*
     * Does this path have a real parent (basedir) on disk?
     * If not, the path is relative, wild or non-existant, and anchored to '.'
     */
    public boolean hasValidParent() {
        return validParent;
    }

    public void setDisplay(Display display) {
        this.display = display;
    }

    /*
     * Path contains something valid.
     */
    public boolean isValidPath() {
        if (wildStartsWithNonGlob) {
            return false;
        }
        if (validParent) {
            return true;
        }
        return StringUtils.isNotEmpty(wildPath);
    }

    /*
     * The original path unchanged with the exception that surrounding
     * quotes are stripped. The path /a/b/foo.txt and '/a/b/foo.txt' 
     * both return /a/b/foo.txt (same for a double quoted path).
     */
    public String getFullPath() {
        return fullPath;
    }

    public int getMaxPathDepth() {
        return pathDepth;
    }

    /*
     * Determine the directory from which to start the search.
     */
    public Path getStartDirectory() {
        if (hasValidParent()) {
            return Paths.get(getParentPath());
        }
        return Paths.get(".");
    }

    @Override
    public String toString() {
        return fullPath;
    }
    
    private void convertToParts() {
        String path = fullPath.trim();
        display.debug("Full path: [" + fullPath + "]");
        int strLen = path.length();
        // Check for prefix such as '/', '~/', 'C:\' etc
        rootPart = extractRoot(path);
        if (rootPart != null) {
            int pl = rootPart.length();
            path = path.substring(pl > path.length() ? pl-1 : pl);
            display.trace("Path stripped of prefix: '" + path + "'");
        }
        StringBuilder sb = new StringBuilder(strLen);
        boolean escaping = false;
        for (char currentChar : path.toCharArray()) {
            switch (currentChar) {
            case '\\':
                if (escaping) {
                    escaping = false;
                } else {
                    escaping = true;
                    addPart(sb);
                }
                break;
            case '/':
                if (escaping) {
                    escaping = false;
                    sb.append(currentChar);
                } else {
                    addPart(sb);
                }
                break;
            default:
                sb.append(currentChar);
                if (escaping) {
                    escaping = false;
                }
            }
        }
        addPart(sb); // flush any trailing path
    }

    // Split real directory path from the trailing wild-carded path
    private void calculateSplit() {
        if (dirParts.isEmpty()) {
            return;
        }
        StringBuilder dirs = new StringBuilder();
        StringBuilder wild = new StringBuilder();
        boolean start = true;
        boolean prevSep = false;
        boolean inWild = false;
        for (String p : dirParts) {
            if (inWild) {
                // eat up trailing paths
                if (wild.length() > 0) {
                    // Use forward path sep for 'glob:<pattern>' searching since it will work 
                    // without change on any platform. Otherwise we'll need to convert glob patterns like '*\*.txt'
                    // to '*\\*.txt' in order for glob to not treat the slash as an escaped char.
                    wild.append("/");
                }
                wild.append(p);
                wildDepth++;
                continue;
            }
            int pLen = dirs.length();
            if (start) {
                start = false;
                if (pLen == 0 && p.length() == 1) {
                    if (p.equals("/") || p.equals("\\")) {
                        dirs.append(File.separatorChar);
                        validParent = true;
                        prevSep = true;
                        continue;
                    }
                }
            }
            if (validDirPath(dirs, p)) {
                if (pLen > 0) {
                    if (!prevSep) {
                        dirs.append(File.separatorChar);
                    }
                }
                dirs.append(p);
                parentParts.add(p);
                prevSep = false;
            } else {
                // no more valid dirs, begin wild part capture. This must contain a glob pattern otherwise
                // it's the beginning of a non-existant dir or file path.
                inWild = true;
                wild.append(p);
                wildDepth++;
                if (!pathContainsGlobPattern(p)) {
                    wildStartsWithNonGlob = true;
                    // continue in order to complete the capture of all path parts.
                }
            }
        }
        parentPath = dirs.toString();
        wildPath = wild.toString();
        display.debug("Path split:  Root [" + rootPart + "], parent [" + parentPath + "], wild [" + wildPath + "]");
        if (display.isTraceOn()) {
            display.trace("Parent parts for '" + fullPath + "' with parent name count " + getParentNameCount());
            for (int i = 0; i < parentParts.size(); i++) {
                display.trace("   part[" + i + "] = '" + parentParts.get(i) + "'");
            }
        }
    }

    /*
     * Path depth is calculated based on the number of path components, including standard wild-cards.
     * The ant wild-card '**' returns an infinite depth.
     */
    private void calculatePathDepth() {
        if (wildPathContainsAntglob()) {
        	display.trace("Calculate path depth, ant glob present so depth is unlimited.");
            pathDepth = Integer.MAX_VALUE;
            return;
        }
        pathDepth += wildDepth;
    	display.trace("Calculate path depth, adding wild path depth " + wildDepth + " for total path depth of " + pathDepth);
    }

    private boolean pathContainsGlobPattern(String p) {
        if (
            p.contains("*")
            || p.contains("?")
            || (p.contains("{") && p.contains("}"))
            || (p.contains("[") && p.contains("]"))
        ) {
            return true;
        }
        return false;
    }

    // Extract the path root or prefix ('C:\', '/', '~/' etc)
    private String extractRoot(String line) {
        if (StringUtils.isEmpty(line)) {
            return null;
        }
        // One difference with apache call to Path handling is that paths starting with ~ and ~/ and ~\ 
        // return this string as root since apache is handling both Win/Unix paths. 
        // In these cases we go with Path and return null, treating ~ as the first parent path part.
        String pr = FilenameUtils.getPrefix(line);
        if (StringUtils.isNotEmpty(pr)) {
            if (pr.equals("~/") || pr.equals("~\\")) {
                return null;
            }
            validParent = true;
            display.debug("Root of '" + line + "' is '" + pr + "'");
            return pr;
        }
        if (line.startsWith("/")) { // handles leading '/' on windows
            validParent = true;
            return File.separatorChar == '/' ? "/" : "\\";
        }
        return null;
    }

    // Add a directory path component to the path list
    private void addPart(StringBuilder sb) {
        display.trace("Add part: '" + sb.toString() + "'");
        if (sb.length() == 0) {
            return;
        }
        dirParts.add(sb.toString());
        sb.setLength(0);
    }

    // Determine if the combined parent+subdir would result in a real
    // path on disk.
    //
    private boolean validDirPath(StringBuilder parent, String subdir) {
        String basedir = null;
        if (rootPart == null) {
            basedir = parent.toString();
        } else {
            basedir = rootPart + parent.toString();
        }
        if (!basedir.isEmpty() && !basedir.endsWith(File.separator)) {
            basedir += File.separator;
        }
        File dir = new File(basedir+subdir);
        if (dir.isDirectory()) {
            display.trace("Parent '" + parent.toString() + "' + child '" + subdir + "' is a valid directory. ValidParent set to true.");
            validParent = true;
            return true;
        }
        return false;
    }
}
