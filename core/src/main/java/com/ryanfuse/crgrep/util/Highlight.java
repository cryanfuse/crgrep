/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/*
 * Colour-highlighted settings which determine if highlighting is applied
 * and, if so, what colour is used.
 *
 * Highlighting is controlled by the --colour (--color) command line options and/or
 * the ~/.crgrep 'colour' ('color') and 'colour.code' ('color.code') settings. 
 */
public class Highlight {

    // ANSI escape code sequence surrounding highlighted text
    public static final String HIGHLIGHT_START = "\33[%sm\33[K";
    public static final String HIGHLIGHT_END = "\33[m\33[K";

    // Colour codes
    public static final int FG_BLACK = 30;     /* black */
    public static final int FG_RED = 31;       /* red */
    public static final int FG_GREEN = 32;     /* green */
    public static final int FG_YELLOW = 33;    /* yellow */
    public static final int FG_BLUE = 34;      /* green */
    public static final int FG_MAGENTA = 35;   /* magenta */
    public static final int FG_CYAN = 36;      /* cyan */
    public static final int FG_WHITE = 37;     /* white */

    public static final int BG_ADD = 10;       // add to FG_ code to get BG code

    public static final int ATTR_RESET = 0;
    public static final int ATTR_BRIGHT = 1;
    public static final int ATTR_DIM = 2;
    public static final int ATTR_UNDERLINE = 3;
    public static final int ATTR_BLINK = 5;
    public static final int ATTR_REVERSE = 7;
    public static final int ATTR_HIDDEN = 8;
    
    private static Pattern numericCodePattern = Pattern.compile("[0-9]+([0-9]|;)*");
    
    private static final Map<String, Integer> colourMap;
    static
    {
        colourMap = new HashMap<String, Integer>();
        colourMap.put("black", FG_BLACK);
        colourMap.put("red", FG_RED);
        colourMap.put("green", FG_GREEN);
        colourMap.put("yellow", FG_YELLOW);
        colourMap.put("blue", FG_BLUE);
        colourMap.put("magenta", FG_MAGENTA);
        colourMap.put("cyan", FG_CYAN);
        colourMap.put("white", FG_WHITE);
    }

    private static final Map<String, Integer> attrMap;
    static
    {
        attrMap = new HashMap<String, Integer>();
        attrMap.put("reset", ATTR_RESET);
        attrMap.put("bright", ATTR_BRIGHT);
        attrMap.put("bold", ATTR_BRIGHT);
        attrMap.put("dim", ATTR_DIM);
        attrMap.put("underline", ATTR_UNDERLINE);
        attrMap.put("blink", ATTR_BLINK);
        attrMap.put("reverse", ATTR_REVERSE);
        attrMap.put("hidden", ATTR_HIDDEN);
    }

    private String start;
    private String end;
    private String colour;
    private Colouring colouring;
    private boolean ansi;

    public Highlight(String when, String colour) {
    	this(when, colour, false);
    }

    public Highlight(String when, String colour, boolean ansi) {
        this.setAnsi(ansi);
        Colouring toCol = Colouring.fromString(when);
        this.colouring = toCol == null ? Colouring.NONE : toCol;
        if (this.colouring == Colouring.AUTO) {
            try {
                // disabling colouring if stdout is a dumb terminal
                String t = System.getenv("TERM");
                if (t != null && t.toLowerCase().equals("dumb")) {
                    this.colouring = Colouring.NONE;
                }
            } catch (Exception e) {
                // prob security exception, assume dumb
                this.colouring = Colouring.NONE;
            }
        }
        setColour(colour);
        setStart(HIGHLIGHT_START);
        setEnd(HIGHLIGHT_END);
    }

    public enum Colouring {
        ALWAYS("always"),
        AUTO("auto"),
        NONE("never");

        private String text;
        Colouring(String text) {
          this.text = text;
        }
        public String getText() {
          return this.text;
        }
        public static Colouring fromString(String text) {
            if (text != null) {
                for (Colouring b : Colouring.values()) {
                    if (text.equalsIgnoreCase(b.text)) {
                        return b;
                    }
                }
            }
            return null;
        }
    }

    public static String colourCode(Integer attr, Integer colour) {
        return attr + ";" + colour;
    }

    /*
     * Get the start prefix which includes the start control code with embedded colour code.
     */
    public String getStart() {
        return start == null ? "" : start;
    }

    /*
     * Set highlighting start (could be anything such as '[' if colouring isn't wanted)
     */
    public void setStart(String start) {
        if (start == null) {
            this.start = null;
            return;
        }
        this.start = String.format(start, getColour());
    }

    public String getEnd() {
        return end == null ? "" : end;
    }

    /*
     * Set highlighting end (could be anything such as ']' if colouring isn't wanted)
     */
    public void setEnd(String end) {
        this.end = end;
    }

    public String getColour() {
        return colour;
    }

    // Map a value to a colour, first by name and then by assuming the value is
    // an actual colour control code.
    // 
    // If no value is specified, default to 'red'
    //
    public void setColour(String c) {
        String defCol = colourCode(ATTR_BRIGHT, FG_RED);
        if (c == null) {
            this.colour = defCol;
        } else {
            String[] col_words = c.toLowerCase().trim().split(" ");
            Integer attr = null, col = null;
            if (col_words.length == 1) {
                c = col_words[0];
                col = colourMap.get(c);
                if (col != null) {
                    this.colour = Integer.toString(col);
                } else {
                    if (numericCodePattern.matcher(c).matches()) {
                        this.colour = c;
                    } else {
                        this.colour = defCol;
                    }
                }
            } else if (col_words.length > 1) {
                attr = attrMap.get(col_words[0]);
                col = colourMap.get(col_words[1]);
                if (attr != null && col != null) {
                    this.colour = colourCode(attr, col);
                } else {
                    this.colour = defCol;
                }
            }
        }
    }

    public Colouring getColouring() {
        return colouring;
    }

    public void setColouring(Colouring colouring) {
        this.colouring = colouring;
    };
    
    public boolean isHighlighting() {
        return colouring != Colouring.NONE;
    }
    
    @Override
    public String toString() {
        return "[start:" + getStart() + "] [colour: " + getColour() + "] [end:" + getEnd() + "]";  
    }

    public boolean isAnsi() {
        return ansi;
    }

    public void setAnsi(boolean ansi) {
        this.ansi = ansi;
    }
}
