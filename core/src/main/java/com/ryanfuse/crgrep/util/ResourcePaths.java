/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.net.URI;

/*
 * Manages ResourcePath instances.
 */
public class ResourcePaths {

	private Switches switches;
    private Display display;

    public ResourcePaths(Switches switches, Display display) {
        this.switches = switches;
        this.display = display;
	}

    public ResourcePath get(String path) {
        ResourcePath rp = new ResourcePath(path, display);
        return rp;
    }

    public static ResourcePath get(URI path) {
        //ResourcePath rp = new ResourcePath(path);
        // future..
        return null;
    }
}
