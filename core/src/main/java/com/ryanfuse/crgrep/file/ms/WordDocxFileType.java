/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 * 
 * Adapted from an existing Apache Tika java class
 *    org.apache.tika.parser.microsoft.ooxml.XWPFWordExtractorDecorator
 * With the following preserved license file header:
 * 
 * --------------------------------------------------------------------
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ryanfuse.crgrep.file.ms;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.xwpf.model.XWPFCommentsDecorator;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.IBody;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.ICell;
import org.apache.poi.xwpf.usermodel.IRunElement;
import org.apache.poi.xwpf.usermodel.ISDTContent;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFHeaderFooter;
import org.apache.poi.xwpf.usermodel.XWPFHyperlink;
import org.apache.poi.xwpf.usermodel.XWPFHyperlinkRun;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFPicture;
import org.apache.poi.xwpf.usermodel.XWPFPictureData;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFSDT;
import org.apache.poi.xwpf.usermodel.XWPFSDTCell;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.xml.sax.SAXException;

/**
 * File type for grep'ing MS OOXML Word documents (.docx). 
 *
 * The logic is significantly different to .doc parsing to warrant its own file type.
 *
 * Supports the new 2007+ .docx format. Earlier formats use WordFileType instead.
 *
 * @author Craig Ryan
 */
public class WordDocxFileType {

    private WordFileType wordType;

    public WordDocxFileType(WordFileType wordType) {
        this.wordType = wordType;
    }

    // Word 2007+ format
    public void grepDocument(XWPFDocument xdoc, String entryName, String entryLabel) {
        try {
            XWPFHeaderFooterPolicy hfPolicy = xdoc.getHeaderFooterPolicy();
            // headers
            if (hfPolicy != null) {
                grepHeaders(hfPolicy, entryName);
            }

            // process text in the order that it occurs in
            grepBodyText(xdoc, entryName, ":P:");

            // then all document tables
            if (hfPolicy!=null) {
                grepFooters(hfPolicy, entryName);
            }
        } catch (SAXException e) {
            wordType.ignoreResource(entryName, e.getLocalizedMessage());
        } catch (XmlException e) {
            wordType.ignoreResource(entryName, e.getLocalizedMessage());
        } catch (Exception e) {
            wordType.ignoreResource(entryName, e.getLocalizedMessage());
        }
    }

    private void grepBodyText(IBody bodyElement, String entryName, String successLabel)
            throws SAXException, XmlException, IOException {
       for (IBodyElement element : bodyElement.getBodyElements()) {
          if (element instanceof XWPFParagraph) {
             XWPFParagraph paragraph = (XWPFParagraph)element;
             grepParagraph(paragraph, entryName, successLabel);
          }
          if (element instanceof XWPFTable) {
             XWPFTable table = (XWPFTable)element;
             grepTable(table, entryName, ":T:");
          }
          if (element instanceof XWPFSDT){
             extractSDT((XWPFSDT) element, entryName, successLabel);
          }
      }
    }
    
    // Structured Document Tag elements. Whatever they are.
    private void extractSDT(XWPFSDT element, String entryName, String successLabel) throws SAXException, 
        XmlException, IOException {
       ISDTContent content = element.getContent();
       if (content != null) {
           wordType.grepMultiLine(entryName, successLabel, content.getText());
       }
    }
    
    private void grepParagraph(XWPFParagraph paragraph, String entryName, String successLabel)
            throws SAXException, XmlException, IOException {
       // If this paragraph is actually a whole new section, then
       //  it could have its own headers and footers
       // Check and handle if so
       XWPFHeaderFooterPolicy headerFooterPolicy = null;
       if (paragraph.getCTP().getPPr() != null) {
           CTSectPr ctSectPr = paragraph.getCTP().getPPr().getSectPr();
           if (ctSectPr != null) {
              headerFooterPolicy =
                  new XWPFHeaderFooterPolicy(paragraph.getDocument(), ctSectPr);
              grepHeaders(headerFooterPolicy, entryName);
           }
       }
       
       // Attach bookmarks for the paragraph
       // (In future, we might put them in the right place, for now
       //  we just put them in the correct paragraph)
       for (int i = 0; i < paragraph.getCTP().sizeOfBookmarkStartArray(); i++) {
          CTBookmark bookmark = paragraph.getCTP().getBookmarkStartArray(i);
          wordType.grepMultiLine(entryName, ":BM:", bookmark.getName());
       }
       
       wordType.grepMultiLine(entryName, successLabel, paragraph.getText()); 
       
       // Do the iruns for embedded links or pictures
       for (IRunElement run : paragraph.getIRuns()) {
          if (run instanceof XWPFSDT){
             processSDTRun((XWPFSDT)run, entryName, successLabel);
          } else {
             processRun((XWPFRun)run, paragraph, entryName, successLabel);
          }
       }
       
       // Now do any comments for the paragraph
       XWPFCommentsDecorator comments = new XWPFCommentsDecorator(paragraph, null);
       String commentText = comments.getCommentText();
       if (commentText != null && commentText.length() > 0) {
           wordType.grepMultiLine(entryName, ":C:", commentText);
       }

       String footnoteText = paragraph.getFootnoteText();
       if (footnoteText != null && footnoteText.length() > 0) {
           wordType.grepMultiLine(entryName, ":N:", footnoteText);
       }

       // Also extract any paragraphs embedded in text boxes:
       for (XmlObject embeddedParagraph : paragraph.getCTP().selectPath("declare namespace w='http://schemas.openxmlformats.org/wordprocessingml/2006/main' declare namespace wps='http://schemas.microsoft.com/office/word/2010/wordprocessingShape' .//*/wps:txbx/w:txbxContent/w:p")) {
           grepParagraph(new XWPFParagraph(CTP.Factory.parse(embeddedParagraph.xmlText()), paragraph.getBody()), entryName, ":TB:");
       }

       // Finish this paragraph
       if (headerFooterPolicy != null) {
           grepFooters(headerFooterPolicy, entryName);
       }
    }

    private void processRun(XWPFRun run, XWPFParagraph paragraph, String entryName, String entryLabel) 
          throws SAXException, XmlException, IOException{
       if (run instanceof XWPFHyperlinkRun) {
          XWPFHyperlinkRun linkRun = (XWPFHyperlinkRun)run;
          XWPFHyperlink link = linkRun.getHyperlink(paragraph.getDocument());
          if (link != null && StringUtils.isNotBlank(link.getURL())) {
              wordType.grepMultiLine(entryName, ":URL:", link.getURL());
          } else if (StringUtils.isNotBlank(linkRun.getAnchor())) {
              wordType.grepMultiLine(entryName, ":URL:", linkRun.getAnchor());
          }
       }

       // If we have any pictures, output them
       for (XWPFPicture picture : run.getEmbeddedPictures()) {
          if (paragraph.getDocument() != null) {
             XWPFPictureData data = picture.getPictureData();
             if (data != null) {
                String pic = data.getFileName();
                if (StringUtils.isNotBlank(pic)) {
                    if (StringUtils.isNotBlank(picture.getDescription())) {
                        pic += ": " + picture.getDescription();
                    }
                    wordType.grepMultiLine(entryName, ":PIC:", pic);
                }
             }
          }
       }
    }

    private void processSDTRun(XWPFSDT run, String entryName, String successLabel)
          throws SAXException, XmlException, IOException{
        if (run.getContent() != null) {
            wordType.grepMultiLine(entryName, successLabel, run.getContent().getText());
        }
    }

    private void grepTable(XWPFTable table, String entryName, String successLabel)
            throws SAXException, XmlException, IOException {
       for (XWPFTableRow row : table.getRows()) {
          for (ICell cell : row.getTableICells()){
              if (cell instanceof XWPFTableCell) {
                  grepBodyText((XWPFTableCell)cell, entryName, successLabel);
              } else if (cell instanceof XWPFSDTCell) {
                  wordType.grepMultiLine(entryName, successLabel, ((XWPFSDTCell)cell).getContent().getText()); 
              }
          }
       }
    }
    
    private void grepFooters(XWPFHeaderFooterPolicy hfPolicy, String entryName)
            throws SAXException, XmlException, IOException {
        // footers
        if (hfPolicy.getFirstPageFooter() != null) {
            grepHeaderFooterText(hfPolicy.getFirstPageFooter(), entryName, ":F:");
        }
        if (hfPolicy.getEvenPageFooter() != null) {
            grepHeaderFooterText(hfPolicy.getEvenPageFooter(), entryName, "F:even:");
        }
        if (hfPolicy.getDefaultFooter() != null) {
            grepHeaderFooterText(hfPolicy.getDefaultFooter(), entryName, ":F:");
        }
    }

    private void grepHeaders(XWPFHeaderFooterPolicy hfPolicy, String entryName)
            throws SAXException, XmlException, IOException {
        if (hfPolicy == null) {
            return;
        }
        if (hfPolicy.getFirstPageHeader() != null) {
            grepHeaderFooterText(hfPolicy.getFirstPageHeader(), entryName, ":H:");
        }
        if (hfPolicy.getEvenPageHeader() != null) {
            grepHeaderFooterText(hfPolicy.getEvenPageHeader(), entryName, ":H:even:");
        }
        if (hfPolicy.getDefaultHeader() != null) {
            grepHeaderFooterText(hfPolicy.getDefaultHeader(), entryName, ":H:");
        }
    }

    private void grepHeaderFooterText(XWPFHeaderFooter headerFooter, String entryName, String successLabel) 
            throws SAXException, XmlException, IOException {
        for (IBodyElement e : headerFooter.getBodyElements()){
            if (e instanceof XWPFParagraph){
                grepParagraph((XWPFParagraph)e, entryName, successLabel);
            } else if (e instanceof XWPFTable){
                // :H:T: or :F:T: to indicate table in header/footer
                grepTable((XWPFTable)e, entryName, successLabel + "T:"); 
            } else if (e instanceof XWPFSDT){
                extractSDT((XWPFSDT)e, entryName, successLabel);
            }
        }
    }
}
