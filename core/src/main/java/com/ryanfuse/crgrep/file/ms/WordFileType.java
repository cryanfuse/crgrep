/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file.ms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.POIXMLException;
import org.apache.poi.hssf.record.crypto.Biff8EncryptionKey;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.HWPFOldDocument;
import org.apache.poi.hwpf.OldWordFileFormatException;
import org.apache.poi.hwpf.extractor.Word6Extractor;
import org.apache.poi.hwpf.usermodel.HeaderStories;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.file.AbstractFileType;

/**
 * File type for grep'ing MS Word documents (.doc).
 * 
 * Supports 97-2007 format. Limited 95 and 6.0 support.
 * 
 * @author Craig Ryan
 */
public class WordFileType extends AbstractFileType {

    private static final String SUPPORTED_WORD_TYPES = ".*\\.(doc|docx)";
    
    private WordDocFileType docType;
    private WordDocxFileType docxType;
    
	public WordFileType(FileGrep fileGrep) {
		super(fileGrep);
	}

    @Override
	public boolean matchesSupportedType(File entry) {
        Pattern supportedArchiveExtent = Pattern.compile(SUPPORTED_WORD_TYPES);
        boolean matches = supportedArchiveExtent.matcher(entry.getName().toLowerCase()).matches();
        getFileGrep().trace("WordFileType (.doc[x]): '" + entry.getPath() + "' supported type? " + (matches ? "true" : " false "));
        return matches;
	}

	@Override
	public void grepEntry(File file) {
        String entryName = file.getPath();
        String err = null;
        try {
            String pw = getFileGrep().getPw();
            WordDoc doc = WordFactory.create(file, pw);
            if (doc.isXDoc()) {
                XWPFDocument xdoc = doc.getXdoc();
                getDocxType().grepDocument(xdoc, entryName, "");
            } else if (doc.isDoc()) {
                HWPFDocument hdoc = doc.getWdoc();
                getDocType().grepDocument(hdoc, entryName, "");
            } else {
                ignoreResource(entryName, "Unknown Word format");
            }
        } catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
            err = e.getLocalizedMessage();
            ignoreResource(entryName, err);
        } catch (OldWordFileFormatException e) {
            // Old format - try straight text extractor
            InputStream is = null;
            try {
                is = new FileInputStream(file);
                grepOldWordEntryStream(is, entryName, "");
            } catch (IOException e1) {
                ignoreResource(entryName, e.getLocalizedMessage());
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException fe) { // ignore
                    }
                }
            }
        } catch (Exception e) {
            err = e.getLocalizedMessage();
            ignoreResource(entryName, err);
        }
        if (err != null) {
            detectErrors(err);
        }
	}

    @Override
    public void grepEntryStream(InputStream entryStream, String entryName, String entryLabel) {
        String err = null;
        try {
            String pw = getFileGrep().getPw();
            WordDoc doc = WordFactory.create(entryStream, pw);
            if (doc.isXDoc()) {
                XWPFDocument xdoc = doc.getXdoc();
                getDocxType().grepDocument(xdoc, entryName, "");
            } else if (doc.isDoc()) {
                HWPFDocument hdoc = doc.getWdoc();
                getDocType().grepDocument(hdoc, entryName, "");
            } else {
                ignoreResource(entryName, "Unknown Word format");
            }
        } catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
            err = e.getLocalizedMessage();
            ignoreResource(entryName, err);
        } catch (OldWordFileFormatException e) {
            ignoreResource(entryName, "Can't read a pre-Word97 format document nested within another resource.");
        } catch (Exception e) {
            err = e.getLocalizedMessage();
            ignoreResource(entryName, err);
        }
        if (err != null) {
            detectErrors(err);
        }
    }

    // Word 95 or earlier.
    private void grepOldWordEntryStream(InputStream entryStream, String entryName, String entryLabel) throws IOException {
        HWPFOldDocument doc = new HWPFOldDocument(new POIFSFileSystem(POIFSFileSystem.createNonClosingInputStream(entryStream)));
        Word6Extractor extractor = new Word6Extractor(doc);
        int p = 1;
        // A paragraph per string is returned
        for (String para : extractor.getParagraphText()) {
            grepMultiLine(entryName, ":" + p + ":" , para);
            p++;
        }
    }

    /*
     * Attempt to advice on specific error conditions.
     */
    private void detectErrors(String err) {
        err = err.toLowerCase();
        if (err.contains("export restriction")) {
            getDisplay().warn("The problem suggests a modification is needed to your Java installation. Refer to the 'Java Cryptography Extensions' section of INSTALL.txt");
        } else if (err.contains("password")) {
            getDisplay().warn("Specify a valid password using the -p <password> option if you wish to search into a password protected document.");                    
        }
    }

    private WordDocFileType getDocType() {
        if (docType == null) {
            docType = new WordDocFileType(this);
        }
        return docType;
    }

    private WordDocxFileType getDocxType() {
        if (docxType == null) {
            docxType = new WordDocxFileType(this);
        }
        return docxType;
    }
}
