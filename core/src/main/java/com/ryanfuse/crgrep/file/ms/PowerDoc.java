/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file.ms;

import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.xslf.usermodel.XMLSlideShow;


/*
 * Wrap either SlideShow / XMLSlideShow 
 */
public class PowerDoc {
    private SlideShow ss;
    private XMLSlideShow xss;

    public PowerDoc(SlideShow ss) {
        this.ss = ss;
    }
    public PowerDoc(XMLSlideShow xss) {
        this.xss = xss;
    }
    public boolean isXss() {
        return xss != null;
    }
    public boolean isSs() {
        return ss != null;
    }
    public SlideShow getSs() {
        return ss;
    }
    public XMLSlideShow getXss() {
        return xss;
    }
}
