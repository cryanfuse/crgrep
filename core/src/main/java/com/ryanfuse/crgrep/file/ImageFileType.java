/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.TagDescriptor;
import com.drew.metadata.adobe.AdobeJpegDescriptor;
import com.drew.metadata.adobe.AdobeJpegDirectory;
import com.drew.metadata.exif.ExifIFD0Descriptor;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDescriptor;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.ExifThumbnailDescriptor;
import com.drew.metadata.exif.ExifThumbnailDirectory;
import com.drew.metadata.exif.GpsDescriptor;
import com.drew.metadata.exif.GpsDirectory;
import com.drew.metadata.exif.makernotes.CanonMakernoteDescriptor;
import com.drew.metadata.exif.makernotes.CanonMakernoteDirectory;
import com.drew.metadata.exif.makernotes.CasioType1MakernoteDescriptor;
import com.drew.metadata.exif.makernotes.CasioType1MakernoteDirectory;
import com.drew.metadata.exif.makernotes.CasioType2MakernoteDescriptor;
import com.drew.metadata.exif.makernotes.CasioType2MakernoteDirectory;
import com.drew.metadata.exif.makernotes.NikonType1MakernoteDescriptor;
import com.drew.metadata.exif.makernotes.NikonType1MakernoteDirectory;
import com.drew.metadata.exif.makernotes.NikonType2MakernoteDescriptor;
import com.drew.metadata.exif.makernotes.NikonType2MakernoteDirectory;
import com.drew.metadata.exif.makernotes.OlympusMakernoteDescriptor;
import com.drew.metadata.exif.makernotes.OlympusMakernoteDirectory;
import com.drew.metadata.gif.GifHeaderDescriptor;
import com.drew.metadata.gif.GifHeaderDirectory;
import com.drew.metadata.icc.IccDescriptor;
import com.drew.metadata.icc.IccDirectory;
import com.drew.metadata.iptc.IptcDescriptor;
import com.drew.metadata.iptc.IptcDirectory;
import com.drew.metadata.jfif.JfifDescriptor;
import com.drew.metadata.jfif.JfifDirectory;
import com.drew.metadata.jpeg.JpegCommentDescriptor;
import com.drew.metadata.jpeg.JpegCommentDirectory;
import com.drew.metadata.photoshop.PhotoshopDescriptor;
import com.drew.metadata.photoshop.PhotoshopDirectory;
import com.drew.metadata.xmp.XmpDescriptor;
import com.drew.metadata.xmp.XmpDirectory;
import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.env.EnvResult;
import com.ryanfuse.crgrep.env.EnvStatus;
import com.ryanfuse.crgrep.util.GrepMatcher;
import com.ryanfuse.crgrep.util.PathUtils;

/**
 * An Image file type to handle and process image meta data and contents (if OCR ie enabled).
 * 
 * Images may contain various meta data encoded inside the binary image file.
 * This data typically includes image attributes (size, camera settings etc) and
 * even user defined comments/titles/descriptions and location information.  
 * 
 * Extract any known meta data from the image and include in the grep.
 * 
 * The image contents may optionally be extracted using OCR and any resulting text identified
 * in the content included in the grep.
 * 
 * @author Craig Ryan
 */
public class ImageFileType extends AbstractFileType {

    private static final String IMAGE_FILE_EXTENTS = ".*\\.(png|bmp|jpeg|jpg|gif|tif|tiff)";
    private static final String META_DELIMITER = ": @";
    private static final String OCR_DELIMITER = ":";
    private static final String TESSDATA_PREFIX_KEY = "crgrep.tessdata";
    
    private String fileName;
    private boolean listedFile;
    private List<String> duplicates = new ArrayList<String>();
    private Tesseract ocrReader = null;
    
    public ImageFileType(FileGrep fileGrep) {
        super(fileGrep);
    }

    @Override
    public boolean matchesSupportedType(File entry) {
        boolean isImage = isImageFile(entry.getName());
    	trace("ImageFileType '" + entry.getPath() + "' supported type? isImage=" + isImage);
    	return isImage;
    }
    
    @Override
    public boolean matchesSupportedFileName(String entryName) {
        boolean isImage = isImageFile(entryName);
    	trace("ImageFileType '" + entryName + "' supported stream name? isImage=" + isImage);
        return isImage;
    }
    
    @Override
    public void grepEntry(File file) throws GrepException {
    	fileName = file.getPath();
		try {
	    	Metadata metadata = ImageMetadataReader.readMetadata(file);
	        grepImage(metadata, fileName);
		} catch (ImageProcessingException e) {
            ignoreResource(fileName, e.getLocalizedMessage());
		} catch (IOException e) {
            ignoreResource(fileName, e.getLocalizedMessage());
		}
		
		if (getFileGrep().getSwitches().isOcr()) {
			BufferedImage bi;
			try {
				bi = ImageIO.read(file);
				if (bi != null) {
				    getFileGrep().debug("ImageFileType grep file '" + file.getPath() + "' for OCR processing.");
					grepOCRText(bi, fileName);
				} else {
					ignoreResource(fileName, "Unable to read image file (for OCR processing)");
				}
			} catch (IOException e) {
	            ignoreResource(fileName, e.getLocalizedMessage());
			}
		}
    }
    
    @Override
    public void grepEntryStream(InputStream entryStream, String entryName, String entryPath) throws GrepException {
    	fileName = streamEntryName(entryPath, entryName);
    	BufferedInputStream bis = entryStream instanceof BufferedInputStream ? (BufferedInputStream) entryStream : new BufferedInputStream(entryStream); 
    	try {
            if (!getFileGrep().getSwitches().isOcr()) {
                // Can't re-read the entire stream easily for both meta-data and OCR.
                // OCR will take priority.
                Metadata metadata = ImageMetadataReader.readMetadata(bis);
                grepImage(metadata, entryName);
            } else {
                getDisplay().warn("Image stream '" + fileName + "' meta data search skipped for OCR search.");
            }
		} catch (ImageProcessingException e) {
            ignoreResource(fileName, e.getLocalizedMessage());
		} catch (IOException e) {
            ignoreResource(fileName, e.getLocalizedMessage());
		}
        if (getFileGrep().getSwitches().isOcr()) {
            String fullName = streamEntryName(entryPath, entryName);
            getFileGrep().trace("ImageFileType Grep stream '" + fullName + "' (for OCR processing)");
			try {
	            BufferedImage bi = ImageIO.read(entryStream);
				if (bi != null) {
					grepOCRText(bi, fileName);
				} else {
					ignoreResource(fileName, "Unable to read image stream (for OCR processing)");
				}
            } catch (GrepException ge) {
                throw ge;
            } catch (Exception e) {
				ignoreResource(fileName, e.getLocalizedMessage());
			}
		}
    }
    
    /*
     * If its a supported image file type (jpeg etc).
     */
    private boolean isImageFile(String entryName) {
        Pattern archiveExtent = Pattern.compile(IMAGE_FILE_EXTENTS, Pattern.CASE_INSENSITIVE);
        if (archiveExtent.matcher(entryName).matches()) {
            return true;
        }
        return false;
    }
    
    /*
     * Extract text from the actual image using OCR techniques and grep the result.
     */
    protected void grepOCRText(BufferedImage bi, String entryName) throws GrepException {
        getFileGrep().debug("ImageFileType grep OCR extracted text for image '" + entryName + "'");
		
		try {
			String extractedText = getTesseractReader().doOCR(bi);
	    	getFileGrep().debug("ImageFileType grep OCR extracted text is [" + extractedText + "]");
	    	grepExtractedText(extractedText);
        } catch (TesseractException e) {
			ignoreResource(fileName, "Unable to OCR extract data: " + e.getLocalizedMessage());
		} catch (UnsatisfiedLinkError|NoClassDefFoundError le) {
            EnvResult res = new EnvResult("OCR image grep", EnvStatus.ERROR);
            res.getErrors().add(missingInstallationError());
            getFileGrep().debug("Tesseract OCR library load error: " + le.getLocalizedMessage());
            throw new GrepException(res);
		}
    }

	private String missingInstallationError() {
	    StringBuilder sb = new StringBuilder();
        sb.append("Optical Character Recognition (--ocr) cannot load the required third party TesseractOCR library.\n");
	    sb.append("Refer to the crgrep document 'INSTALL.txt' for installation details.\n");
	    sb.append("   (located in the crgrep home directory '");
	    sb.append(PathUtils.normalisedHomeDir()).append("')\n");
	    sb.append("Also refer to https://code.google.com/p/tesseract-ocr for steps to obtain Tesseract and its language files.\n");
	    return sb.toString();
    }
    
    private void grepImage(Metadata metadata, String entryName) {
        getFileGrep().debug("ImageFileType grep meta-data for image '" + entryName + "'");
    	if (getFileGrep().getSwitches().isTrace()) {
    		debugDirList(metadata);
    	}
    	duplicates.clear();
    	listedFile = false;
    	// grep various dir types
    	// see http://javadoc.metadata-extractor.googlecode.com/git/2.7.0/index.html
    	if (metadata.containsDirectoryOfType(ExifSubIFDDirectory.class)) {
            ExifSubIFDDirectory directory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
            grepDirectoryTags(directory, new ExifSubIFDDescriptor(directory));
    	}
    	if (metadata.containsDirectoryOfType(ExifIFD0Directory.class)) {
            ExifIFD0Directory dir = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
            grepDirectoryTags(dir, new ExifIFD0Descriptor(dir));
    	}
        if (metadata.containsDirectoryOfType(JpegCommentDirectory.class)) {
            JpegCommentDirectory dir = metadata.getFirstDirectoryOfType(JpegCommentDirectory.class);
            grepDirectoryTags(dir, new JpegCommentDescriptor(dir));
        }
        if (metadata.containsDirectoryOfType(GifHeaderDirectory.class)) {
            GifHeaderDirectory dir = metadata.getFirstDirectoryOfType(GifHeaderDirectory.class);
            grepDirectoryTags(dir, new GifHeaderDescriptor(dir));
        }
    	if (metadata.containsDirectoryOfType(JfifDirectory.class)) {
    	    JfifDirectory dir = metadata.getFirstDirectoryOfType(JfifDirectory.class);
    	    grepDirectoryTags(dir, new JfifDescriptor(dir));
    	}
    	if (metadata.containsDirectoryOfType(GpsDirectory.class)) {
    	    GpsDirectory dir = metadata.getFirstDirectoryOfType(GpsDirectory.class);
            grepDirectoryTags(dir, new GpsDescriptor(dir));
    	}
    	if (metadata.containsDirectoryOfType(IccDirectory.class)) {
            IccDirectory dir = metadata.getFirstDirectoryOfType(IccDirectory.class);
            grepDirectoryTags(dir, new IccDescriptor(dir));
    	}
    	if (metadata.containsDirectoryOfType(PhotoshopDirectory.class)) {
    	    PhotoshopDirectory directory = metadata.getFirstDirectoryOfType(PhotoshopDirectory.class);
    	    grepDirectoryTags(directory, new PhotoshopDescriptor(directory));
    	}
    	if (metadata.containsDirectoryOfType(AdobeJpegDirectory.class)) {
            AdobeJpegDirectory directory = metadata.getFirstDirectoryOfType(AdobeJpegDirectory.class);
            grepDirectoryTags(directory, new AdobeJpegDescriptor(directory));
    	}
    	if (metadata.containsDirectoryOfType(ExifThumbnailDirectory.class)) {
    	    grepExifThumbnailDirectory(metadata);
    	}
    	if (metadata.containsDirectoryOfType(XmpDirectory.class)) {
    		grepXmpDirectory(metadata);
    	}
    	if (metadata.containsDirectoryOfType(CanonMakernoteDirectory.class)) {
    		grepCanonMakernoteDirectory(metadata);
    	}
    	if (metadata.containsDirectoryOfType(OlympusMakernoteDirectory.class)) {
    		grepOlympusMakernoteDirectory(metadata);
    	}
    	if (metadata.containsDirectoryOfType(IptcDirectory.class)) {
    		grepIptcDirectory(metadata);
    	}
    	if (metadata.containsDirectoryOfType(NikonType1MakernoteDirectory.class)) {
    		grepNikonType1MakernoteDirectory(metadata);
    	}
    	if (metadata.containsDirectoryOfType(NikonType2MakernoteDirectory.class)) {
    		grepNikonType2MakernoteDirectory(metadata);
    	}
    	if (metadata.containsDirectoryOfType(CasioType1MakernoteDirectory.class)) {
    		grepCasioType1MakernoteDirectory(metadata);
    	}
    	if (metadata.containsDirectoryOfType(CasioType2MakernoteDirectory.class)) {
    		grepCasioType2MakernoteDirectory(metadata);
    	}
    }
    
    private void grepDirectoryTags(Directory dir, TagDescriptor<? extends Directory> desc) {
        getFileGrep().debug("Directory tag directory " + dir.getClass().getName() + " grep>");
        Collection<Tag> tags = dir.getTags();
        List<String> tl = new ArrayList<String>();
        for (Tag t : tags) {
            tl.add(t.getTagName());
            tl.add(desc.getDescription(t.getTagType()));
        }
        grepMetaData(tl.toArray(new String[]{}));
    }

    private void grepCasioType1MakernoteDirectory(Metadata metadata) {
    	CasioType1MakernoteDirectory dir = metadata.getFirstDirectoryOfType(CasioType1MakernoteDirectory.class);
    	CasioType1MakernoteDescriptor desc = new CasioType1MakernoteDescriptor(dir);
    	trace("CasioType1Makernote grep>");
    	grepMetaData(new String[] {
   			"CcdSensitivity", desc.getCcdSensitivityDescription(),
			"contrast", desc.getContrastDescription(), 
			"DigitalZoom", desc.getDigitalZoomDescription(), 
			"FlashIntensity", desc.getFlashIntensityDescription(), 
			"FlashMode", desc.getFlashModeDescription(), 
			"FocusingMode", desc.getFocusingModeDescription(), 
			"ObjectDistance", desc.getObjectDistanceDescription(), 
			"Quality", desc.getQualityDescription(), 
			"RecordingMode", desc.getRecordingModeDescription(), 
			"Saturation", desc.getSaturationDescription(), 
			"Sharpness", desc.getSharpnessDescription(), 
			"WhiteBalance", desc.getWhiteBalanceDescription(), 
    	});
	}
    
    private void grepCasioType2MakernoteDirectory(Metadata metadata) {
    	CasioType2MakernoteDirectory dir = metadata.getFirstDirectoryOfType(CasioType2MakernoteDirectory.class);
    	CasioType2MakernoteDescriptor desc = new CasioType2MakernoteDescriptor(dir);
    	trace("CasioType2Makernote grep>");
    	// complete
    	grepMetaData(new String[] {
			"CasioPreviewThumbnail", desc.getCasioPreviewThumbnailDescription(),
			"CcdSensitivity", desc.getCcdIsoSensitivityDescription(),
			"ColourMode", desc.getColourModeDescription(),
			"Contrast", desc.getContrastDescription(),
			"Enhancement", desc.getEnhancementDescription(),
			"Filter", desc.getFilterDescription(),
			"FlashDistance", desc.getFlashDistanceDescription(),
			"FocalLength", desc.getFocalLengthDescription(),
			"FocusMode1", desc.getFocusMode1Description(),
			"FocusMode2", desc.getFocusMode2Description(),
			"ImageSize", desc.getImageSizeDescription(),
			"IsoSensitivity", desc.getIsoSensitivityDescription(),
			"ObjectDistance", desc.getObjectDistanceDescription(),
			"PrintImageMatchingInfo", desc.getPrintImageMatchingInfoDescription(),
			"Quality", desc.getQualityDescription(),
			"QualityMode", desc.getQualityModeDescription(),
			"RecordMode", desc.getRecordModeDescription(),
			"Saturation", desc.getSaturationDescription(),
			"SelfTimer", desc.getSelfTimerDescription(),
			"Sharpness", desc.getSharpnessDescription(),
			"ThumbnailDimensions", desc.getThumbnailDimensionsDescription(),
			"ThumbnailOffset", desc.getThumbnailOffsetDescription(),
			"ThumbnailSize", desc.getThumbnailSizeDescription(),
			"Timezone", desc.getTimeZoneDescription(),
			"WhiteBalance1", desc.getWhiteBalance1Description(),
			"WhiteBalance2", desc.getWhiteBalance2Description(),
			"WhiteBalanceBias", desc.getWhiteBalanceBiasDescription()
    	});
	}

    private void grepExifThumbnailDirectory(Metadata metadata) {
        ExifThumbnailDirectory directory = metadata.getFirstDirectoryOfType(ExifThumbnailDirectory.class);
        ExifThumbnailDescriptor descriptor = new ExifThumbnailDescriptor(directory);
        trace("ExifThumbnailDirectory grep>");
        // complete
        grepMetaData(new String[] {
        	"BitsPerSample", descriptor.getBitsPerSampleDescription(),
        	"Compression", descriptor.getCompressionDescription(),
        	"Orientation", descriptor.getOrientationDescription(),
        	"PhotometricInterpretation", descriptor.getPhotometricInterpretationDescription(),
        	"PlanarConfiguration", descriptor.getPlanarConfigurationDescription(),
        	"ReferenceBlackWhite", descriptor.getReferenceBlackWhiteDescription(),
        	"Resolution", descriptor.getResolutionDescription(),
        	"RowsPerStrip", descriptor.getRowsPerStripDescription(),
        	"SamplesPerPixel", descriptor.getSamplesPerPixelDescription(),
        	"StripByteCounts", descriptor.getStripByteCountsDescription(),
        	//"ThumbnailImageHeight", descriptor.getThumbnailImageHeightDescription(),
        	//"ThumbnailImageWidth", descriptor.getThumbnailImageWidthDescription(),
        	"ThumbnailLength", descriptor.getThumbnailLengthDescription(),
        	"ThumbnailOffset", descriptor.getThumbnailOffsetDescription(),
        	"XResolution", descriptor.getXResolutionDescription(),
        	"YCbCrPositioning", descriptor.getYCbCrPositioningDescription(),
        	"YCbCrSubsampling", descriptor.getYCbCrSubsamplingDescription(),
        	"YResolution", descriptor.getYResolutionDescription()
		});
    }

    private void grepNikonType1MakernoteDirectory(Metadata metadata) {
        NikonType1MakernoteDirectory directory = metadata.getFirstDirectoryOfType(NikonType1MakernoteDirectory.class);
        NikonType1MakernoteDescriptor descriptor = new NikonType1MakernoteDescriptor(directory);
        trace("NikonType1MakernoteDir grep>");
        // complete
        grepMetaData(new String[] {
			"CcdSensitivity", descriptor.getCcdSensitivityDescription(),
			"ColorMode", descriptor.getColorModeDescription(),
			"Converter", descriptor.getConverterDescription(),
			"DigitalZoom", descriptor.getDigitalZoomDescription(),
			"Focus", descriptor.getFocusDescription(),
			"ImageAdjustment", descriptor.getImageAdjustmentDescription(),
			"Quality", descriptor.getQualityDescription(),
			"WhiteBalance", descriptor.getWhiteBalanceDescription()
		});
    }

    private void grepNikonType2MakernoteDirectory(Metadata metadata) {
        NikonType2MakernoteDirectory directory = metadata.getFirstDirectoryOfType(NikonType2MakernoteDirectory.class);
        NikonType2MakernoteDescriptor descriptor = new NikonType2MakernoteDescriptor(directory);
        trace("NikonType1MakernoteDir grep>");
        // complete
        grepMetaData(new String[] {
			"ActiveDLighting", descriptor.getActiveDLightingDescription(),
			"AutoFlashCompensation", descriptor.getAutoFlashCompensationDescription(),
			"AutoFocusPosition", descriptor.getAutoFocusPositionDescription(),
			"ColorMode", descriptor.getColorModeDescription(),
			"ColorSpace", descriptor.getColorSpaceDescription(),
			"DigitalZoom", descriptor.getDigitalZoomDescription(),
			"ExposureDifference", descriptor.getExposureDifferenceDescription(),
			"ExposureTuning", descriptor.getExposureTuningDescription(),
			"FirmwareVersion", descriptor.getFirmwareVersionDescription(),
			"FlashBracketCompensation", descriptor.getFlashBracketCompensationDescription(),
			"FlashExposureCompensation", descriptor.getFlashExposureCompensationDescription(),
			"FlashUsed", descriptor.getFlashUsedDescription(),
			"HighISONoiseReduction", descriptor.getHighISONoiseReductionDescription(),
			"HueAdjustment", descriptor.getHueAdjustmentDescription(),
			"IsoSetting", descriptor.getIsoSettingDescription(),
			"Lens", descriptor.getLensDescription(),
			"LensStops", descriptor.getLensStopsDescription(),
			"LensType", descriptor.getLensTypeDescription(),
			"NEFCompression", descriptor.getNEFCompressionDescription(),
			"PowerUpTime", descriptor.getPowerUpTimeDescription(),
			"ProgramShift", descriptor.getProgramShiftDescription(),
			"ShootingMode", descriptor.getShootingModeDescription(),
			"VignetteControl", descriptor.getVignetteControlDescription()
		});
    }

    private void grepIptcDirectory(Metadata metadata) {
        IptcDirectory directory = metadata.getFirstDirectoryOfType(IptcDirectory.class);
        IptcDescriptor descriptor = new IptcDescriptor(directory);
        trace("IptcDir grep>");
        // complete
        grepMetaData(new String[] {
			"ByLine", descriptor.getByLineDescription(),
			"ByLineTitle", descriptor.getByLineTitleDescription(),
			"Caption", descriptor.getCaptionDescription(),
			"Category", descriptor.getCategoryDescription(),
			"City", descriptor.getCityDescription(),
			"CopyrightNotice", descriptor.getCopyrightNoticeDescription(),
			"CountryOrPrimaryLocation", descriptor.getCountryOrPrimaryLocationDescription(),
			"Credit", descriptor.getCreditDescription(),
			"DateCreated", descriptor.getDateCreatedDescription(),
			"FileFormat", descriptor.getFileFormatDescription(),
			"Headline", descriptor.getHeadlineDescription(),
			"Keywords", descriptor.getKeywordsDescription(),
			"ObjectName", descriptor.getObjectNameDescription(),
			"OriginalTransmissionReference", descriptor.getOriginalTransmissionReferenceDescription(),
			"OriginatingProgram", descriptor.getOriginatingProgramDescription(),
			"ProvinceOrState", descriptor.getProvinceOrStateDescription(),
			"RecordVersion", descriptor.getRecordVersionDescription(),
			"ReleaseDate", descriptor.getReleaseDateDescription(),
			"ReleaseTime", descriptor.getReleaseTimeDescription(),
			"Source", descriptor.getSourceDescription(),
			"SpecialInstructions", descriptor.getSpecialInstructionsDescription(),
			"SupplementalCategories", descriptor.getSupplementalCategoriesDescription(),
			"TimeCreated", descriptor.getTimeCreatedDescription(),
			"Urgency", descriptor.getUrgencyDescription(),
			"Writer", descriptor.getWriterDescription()
		});
    }

    private void grepOlympusMakernoteDirectory(Metadata metadata) {
        OlympusMakernoteDirectory directory = metadata.getFirstDirectoryOfType(OlympusMakernoteDirectory.class);
        OlympusMakernoteDescriptor descriptor = new OlympusMakernoteDescriptor(directory);
        trace("OlympusMakernoteDir grep>");
        // complete
        grepMetaData(new String[] {
			"MacroMode", descriptor.getMacroModeDescription(),
			"DigiZoomRatio", descriptor.getDigiZoomRatioDescription(),
			"JpegQuality", descriptor.getJpegQualityDescription(),
			"SpecialMode", descriptor.getSpecialModeDescription()
		});
    }

    private void grepXmpDirectory(Metadata metadata) {
        XmpDirectory directory = metadata.getFirstDirectoryOfType(XmpDirectory.class);
        XmpDescriptor descriptor = new XmpDescriptor(directory);
        trace("XmpDir grep>");
        // complete
        grepMetaData(new String[] {
			"ApertureValue", descriptor.getApertureValueDescription(),
			"ExposureProg", descriptor.getExposureProgramDescription(),
			"ExposureTime", descriptor.getExposureTimeDescription(),
			"FNumber", descriptor.getFNumberDescription(),
			"FocalLength", descriptor.getFocalLengthDescription(),
			"ShutterSpeed", descriptor.getShutterSpeedDescription()
		});
    }

    private void grepCanonMakernoteDirectory(Metadata metadata) {
        CanonMakernoteDirectory directory = metadata.getFirstDirectoryOfType(CanonMakernoteDirectory.class);
        CanonMakernoteDescriptor descriptor = new CanonMakernoteDescriptor(directory);
        trace("CanonMakernoteDir grep>");
        // complete
        grepMetaData(new String[] {
			"AfPointSelected", descriptor.getAfPointSelectedDescription(),
			"AfPointUsed", descriptor.getAfPointUsedDescription(),
			"ContinuousDriveMode", descriptor.getContinuousDriveModeDescription(),
			"Contrast", descriptor.getContrastDescription(),
			"DigitalZoom", descriptor.getDigitalZoomDescription(),
			"EasyShootingMode", descriptor.getEasyShootingModeDescription(),
			"ExposureMode", descriptor.getExposureModeDescription(),
			"FlashActivity", descriptor.getFlashActivityDescription(),
			"FlashBias", descriptor.getFlashBiasDescription(),
			"FlashDetails", descriptor.getFlashDetailsDescription(),
			"FlashMode", descriptor.getFlashModeDescription(),
			"FlashMode", descriptor.getFlashModeDescription(),
			"FocalUnitsPerMillimetre", descriptor.getFocalUnitsPerMillimetreDescription(),
			"FocusMode1", descriptor.getFocusMode1Description(),
			"FocusMode2", descriptor.getFocusMode2Description(),
			"FocusType", descriptor.getFocusTypeDescription(),
			"ImageSize", descriptor.getImageSizeDescription(),
			"LongFocalLength", descriptor.getLongFocalLengthDescription(),
			"MacroMode", descriptor.getMacroModeDescription(),
			"MeteringMode", descriptor.getMeteringModeDescription(),
			"Quality", descriptor.getQualityDescription(),
			"Saturation", descriptor.getSaturationDescription(),
			"SelfTimerDelay", descriptor.getSelfTimerDelayDescription(),
			"SerialNumber", descriptor.getSerialNumberDescription(),
			"Sharpness", descriptor.getSharpnessDescription(),
			"ShortFocalLength", descriptor.getShortFocalLengthDescription(),
			"WhiteBalance", descriptor.getWhiteBalanceDescription()
		});
    }

    private void grepMetaData(String[] meta) {
        if (!getFileGrep().isContent()) {
        	if (listedFile) {
        		// Already listed the file after a match. Avoid duplicate reporting.
        		return;
        	}
        }
        //Pattern pattern = getResourcePattern().getPattern();
        GrepMatcher gm = GrepMatcher.getMatcher(null, getFileGrep());
        StringBuilder sb = new StringBuilder();
        boolean haveMatch = false;
        for (int i = 0; i < meta.length; i += 2) {
            String label = meta[i];
            String data = meta[i+1];
            if (getFileGrep().getSwitches().isTrace()) {
            	trace("    -> label [" + label + "] data [" + data + "] against [" + gm.getPattern() + "]");
            }
            if (data == null || data.trim().length() == 0) {
                continue;
            }
            String key = label + META_DELIMITER + data;
            if (duplicates.contains(key)) {
            	// Avoid reporting on duplicate data from different Directory types for this image.
            	trace("Ignore duplicate label '" + label + "' data '" + data + "'");
            	continue;
            }
            duplicates.add(key);
            gm.setText(data);
            if (gm.matches()) {
                if (sb.length() == 0) {
                    sb.append("{");
                } else {
                    sb.append(", ");
                }
                sb.append(label).append("=").append(gm.toString());
                haveMatch = true;
            }
        }

        if (haveMatch) {
            sb.append("}");
            getDisplay().result(fileName + (getFileGrep().isContent() ? META_DELIMITER + sb.toString() : ""));
            listedFile = true;
        }
    }

    private void grepExtractedText(String data) {
        Pattern pattern = getResourcePattern().getPattern();
        if (getFileGrep().getSwitches().isTrace()) {
        	trace("    -> OCR data [" + data + "] against [" + pattern + "]");
        }
        if (data == null || data.trim().length() == 0) {
        	return;
        }
        String[] lines = data.split("\\n");
        for (int i = 0; i < lines.length; i++) {
        	String line = lines[i];
            GrepMatcher gm = GrepMatcher.getMatcher(line, getFileGrep());
            if (gm.matches()) {
        		getDisplay().result(fileName + OCR_DELIMITER + i + OCR_DELIMITER + " " + gm.toString());
        	}
        }
    }

    /*
     * Load the native Tesseract DLL/shared lib. Report any failures.
     */
    protected Tesseract getTesseractReader() throws GrepException {
        if (ocrReader == null) {
            try {
                ocrReader = Tesseract.getInstance();
                // suppress info/debug output from Tesseract
                ocrReader.setTessVariable("debug_file", "/dev/null");
                if (System.getenv("TESSDATA_PREFIX") == null) {
                    String datapath = System.getProperty(TESSDATA_PREFIX_KEY);
                    ocrReader.setDatapath(datapath);
                    getFileGrep().debug("Using OCR TESSDATA_PREFIX: '" + datapath + "'");
                } else {
                    getFileGrep().debug("OCR TESSDATA_PREFIX env var is set to: '" + System.getenv("TESSDATA_PREFIX") + "'");
                    ocrReader.setDatapath(System.getenv("TESSDATA_PREFIX"));
                }
    		} catch (UnsatisfiedLinkError|NoClassDefFoundError le) {
                EnvResult res = new EnvResult("OCR image grep", EnvStatus.ERROR);
                res.getErrors().add(missingInstallationError());
                throw new GrepException(res);
            }
        }
        return ocrReader;
    }
    
    protected void setTesseractReader(Tesseract ocrReader) {
        this.ocrReader = ocrReader;
    }

    private void trace(String msg) {
        getFileGrep().trace(msg);
    }

    private void debugDirList(Metadata metadata) {
        Iterable<Directory> dirs = metadata.getDirectories();
        trace("Directory List for [" + fileName + "]");
        trace("-----------------------------------------------");
        StringBuilder sb = new StringBuilder();
        for (Directory d : dirs) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(d.getName());
        }
        trace(sb.toString());
    }
}
