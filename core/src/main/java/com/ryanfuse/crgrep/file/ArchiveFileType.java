
/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.StreamingNotSupportedException;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResultText;
import com.ryanfuse.crgrep.util.GrepMatcher;

/**
 * File type for various supported types of archives.
 * 
 * @author Craig Ryan
 */
public class ArchiveFileType extends AbstractFileType {

    private static final String SUPPORTED_ARCHIVE_TYPES = ".*\\.(jar|ear|war|zip|tar|gz|7z)";

    private Pattern autoExtentPattern = Pattern.compile("(jar|zip|tar|7z)");
    
    public ArchiveFileType(FileGrep fileGrep) {
        super(fileGrep);
    }

    @Override
    public boolean matchesSupportedType(File entry) {
        boolean supported = isSupportedArchiveType(entry.getName());
    	getFileGrep().trace("ArchiveFileType '" + entry.getPath() + "' supported type? " + supported);
    	return supported;
    }

    @Override
    public boolean matchesSupportedFileName(String entryName) {
    	boolean supported = isSupportedArchiveType(entryName);
    	getFileGrep().trace("ArchiveFileType '" + entryName + "' supported stream type? " + supported);
    	return supported;
    }

    @Override
    public void grepEntry(File file) throws GrepException {
    	getFileGrep().debug("ArchiveFileType Grep file '" + file.getPath() + "'");
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(file));
            ArchiveInputStream input = createArchiveInputStream(file.getName(), bis);
            grepArchiveGeneric(input, null, file.getPath());
        } catch (FileNotFoundException e) {
            ignoreResource(file.getPath(), e.getLocalizedMessage());
        } catch (ArchiveException e) {
        	getFileGrep().debug("ArchiveFileType file exception: " + e.getLocalizedMessage());
			if (e instanceof StreamingNotSupportedException) {
				// Attempt file based grep
				grepUnstreamableArchive(file, ((StreamingNotSupportedException)e).getFormat());
			} else {
				ignoreResource(file.getPath(), e.getLocalizedMessage());
			}
        } catch (IOException e) {
            ignoreResource(file.getPath(), e.getLocalizedMessage());
		} finally {
            getFileGrep().closeResource(bis);
        }
    }

	@Override
    public void grepEntryStream(InputStream entryStream, String entryName, String entryPath) throws GrepException {
    	String fullName = streamEntryName(entryPath, entryName);
    	getFileGrep().debug("ArchiveFileType Grep stream " + fullName);
    	ArchiveInputStream nestedArchiveStream = null;
		try {
			nestedArchiveStream = createArchiveInputStream(entryName, entryStream);
		} catch (ArchiveException e) {
			getFileGrep().debug("ArchiveFileType exception: " + e.getLocalizedMessage());
            ignoreResource(fullName, e.getLocalizedMessage());
            return;
        } catch (IOException e) {
            ignoreResource(fullName, e.getLocalizedMessage());
            return;
        }
		grepArchiveGeneric(nestedArchiveStream, entryName, fullName);
    }
    
    private boolean isSupportedArchiveType(String fileName) {
        Pattern supportedArchiveExtent = Pattern.compile(SUPPORTED_ARCHIVE_TYPES);
        return supportedArchiveExtent.matcher(fileName).matches();
    }
    
    /*
     * Process a stream we know is a supported archive type. 
     * Not all archive types support streaming and may still through an exception.
     */
    private ArchiveInputStream createArchiveInputStream(String fileName, InputStream bis) throws ArchiveException, IOException {
    	String ext = getExtent(fileName);
    	if (ext == null) {
    		// shouldn't happen since we already determined this to be a supported archive stream.
    		// Let the factory try guess the content type.
    		getFileGrep().debug("ArchiveFileType create archive i/p stream '" + fileName + "' no extent, call factory to guess stream type.");
    		return new ArchiveStreamFactory().createArchiveInputStream(bis);
    	}
    	ArchiveStreamFactory streamFactory = new ArchiveStreamFactory();
        if (autoExtentPattern.matcher(ext).matches()) {
    		getFileGrep().debug("ArchiveFileType create archive i/p stream '" + fileName + "' pass type '" + ext + "' factory to create stream.");
        	return streamFactory.createArchiveInputStream(ext, bis);
        }
        // special case - multiple extents .tar.gz
        if (fileName.toLowerCase().endsWith(".tar.gz")) {
        	return streamFactory.createArchiveInputStream(ArchiveStreamFactory.TAR, new GzipCompressorInputStream(bis));
        }
        // factory doesn't automatically recognise extents like ear/war, tell it to use ZIP 
        getFileGrep().debug("ArchiveFileType create archive i/p stream '" + fileName + "' pass ZIP to factory to create stream.");
        return streamFactory.createArchiveInputStream(ArchiveStreamFactory.ZIP, bis);
    }

    private String getExtent(String fileName) {
    	int extIdx = fileName.lastIndexOf('.');
    	if (extIdx == -1) {
    		return null;
    	}
    	return fileName.substring(extIdx + 1 < fileName.length() ? extIdx + 1 : extIdx).toLowerCase();
    }
    
    // Grep an archive and it's contents which doesn't support streaming.
    // Wrap each entry from the file in an archive stream.
    private void grepUnstreamableArchive(File file, String format) throws GrepException {
    	String entryPath = file.getPath();
    	grepByName(entryPath, file.getName());
    	if (format == null) {
    		// give up.. can't determine extent
			ignoreResource(entryPath, "cant determine file type.");
    		return;
    	}
    	if (format.equals(ArchiveStreamFactory.SEVEN_Z)) {
    		getFileGrep().debug("ArchiveFileType grep 7z file " + entryPath);
    		try {
    		    String pw = getFileGrep().getPw();
    			SevenZFile sevenZFile = pw == null ? new SevenZFile(file) 
    			    : new SevenZFile(file, pw.getBytes());
    			SevenZWrapper zis = new SevenZWrapper(sevenZFile); 
    			ArchiveEntry ze = null;
    			while ((ze = zis.getNextEntry()) != null) {
                    getFileGrep().trace("ArchiveFileType Grep archive entry content " + streamEntryName(entryPath , ze.getName()));
                	grepArchiveEntryListing(entryPath, ze.getName());
                    if (!ze.isDirectory() && getFileGrep().isContent()) {
                        grepArchiveEntryContent(entryPath, ze, zis);
                    } else {
                        getFileGrep().trace("ArchiveFileType 7z entry " + streamEntryName(entryPath , ze.getName()) + " ignored. (" + (getFileGrep().isContent() ? "isDir" : "noContent") + ")");
                    }
    			}
    			sevenZFile.close();
    			zis.close();
    		} catch (IOException e) {
    			ignoreResource(entryPath, e.getLocalizedMessage());
    		} catch (ArchiveException e) {
                ignoreResource(entryPath, e.getLocalizedMessage());
            }
    	}
    }

    // Examine an archive and grep its contents.
    private void grepArchiveGeneric(ArchiveInputStream zis, String entryName, String entryPath) throws GrepException {
        getFileGrep().debug("ArchiveFileType process stream " + entryPath);
        try {
            ArchiveEntry ze = null;
            while ((ze = zis.getNextEntry()) != null) {
                grepArchiveEntryListing(entryPath, ze.getName());
                if (!zis.canReadEntryData(ze)) {
                    ignoreResource(streamEntryName(entryPath , ze.getName()), "unable to read archive entry (possibly unsupported encryption?)");
                    continue;
                }
                if (!ze.isDirectory() && getFileGrep().isContent()) {
                    getFileGrep().trace("ArchiveFileType Grep archive entry content " + streamEntryName(entryPath , ze.getName()));
                    grepArchiveEntryContent(entryPath, ze, zis);
                } else {
                    getFileGrep().trace("ArchiveFileType ZIP entry " + streamEntryName(entryPath , ze.getName()) + " ignored. (" + (getFileGrep().isContent() ? "isDir" : "noContent") + ")");
                }
            }
        } catch (IOException e) {
            ignoreResource(entryPath, e.getLocalizedMessage());
        } catch (ArchiveException e) {
            ignoreResource(entryPath, e.getLocalizedMessage());
        }
    }
        
    /*
     * Grep the archive entry by name (listing).
     */
    private void grepArchiveEntryListing(String archivePath, String entryName) {
        //getFileGrep().trace("Grep ["+archiveName+"["+entryName+"]] against ["+ getResourcePattern().getPattern()+"]");
        GrepMatcher gm = GrepMatcher.getMatcher(entryName, getFileGrep());
        if (gm.matches()) {
            ResultText rt = new ResultText(getResourcePattern(), getFileGrep().getSwitches());
            // avoid text trimming when displaying the entry name
            String entry = streamEntryName(rt.result(archivePath, false), gm.toString(false));
            getDisplay().result(entry);
        }
    }

    /*
     * Grep archive entries, including supported inner archive types (jars within wars etc). 
     */
    private void grepArchiveEntryContent(String entryPath, ArchiveEntry entry, ArchiveInputStream ais) throws ArchiveException, GrepException {
        String entryName = entry.getName();
        getFileGrep().grepByContentsStream(ais, entryName, entryPath);
    }

    private static class SevenZWrapper extends ArchiveInputStream {
    	private SevenZFile file;
    	private SevenZWrapper(SevenZFile file) {
    		this.file = file;
    	}
    	@Override
    	public int read() throws IOException {
    		return file.read();
    	}
    	@Override
    	public int read(byte[] b) throws IOException {
    		return file.read(b);
    	}
    	@Override
    	public int read(byte[] b, int off, int len) throws IOException {
    		return file.read(b, off, len);
    	}
    	@Override
    	public ArchiveEntry getNextEntry() throws IOException {
    		return file.getNextEntry();
    	}
    }
}
