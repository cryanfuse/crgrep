/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.BadSecurityHandlerException;
import org.apache.pdfbox.pdmodel.encryption.StandardDecryptionMaterial;
import org.apache.pdfbox.util.PDFTextStripper;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.util.GrepMatcher;

/**
 * File type for grep'ing PDF documents.
 * 
 * @author Craig Ryan
 */
public class PdfFileType extends AbstractFileType {

	private static final String PDF_EXTENT = ".pdf";

	static {
	    System.setProperty("org.apache.commons.logging.Log",
	            "org.apache.commons.logging.impl.NoOpLog");
	}
	
	public PdfFileType(FileGrep fileGrep) {
		super(fileGrep);
		java.util.logging.Logger.getLogger("org.apache.pdfbox").setLevel(java.util.logging.Level.OFF);
	}

	@Override
	public boolean matchesSupportedType(File entry) {
		return entry.getName().toLowerCase().endsWith(PDF_EXTENT);
	}

	@Override
	public void grepEntry(File file) {
		try {
			grepEntryStream(new FileInputStream(file), file.getName(), file.getPath());
		} catch (FileNotFoundException e) {
			ignoreResource(file.getPath(), e.getLocalizedMessage());
		} 
	}

	public void grepEntryStream(InputStream entryStream, String entryName, String entryPath) {
	    grepByName(entryPath, entryName);
	    if (!getFileGrep().isContent()) {
	        return;
	    }
		PDDocument doc = null;
		try {
			doc = PDDocument.load(entryStream);
			if (!documentIsDecrypted(doc)) {
				ignoreResource(entryPath, "Unable to decrypt file (is -p password correct?)");
				return;
			}
			int pages = doc.getNumberOfPages();
			getDisplay().debug("PDF file '" + entryPath + "' number of pages: " + pages);
			PDFTextStripper ps = new PDFTextStripper();
			for (int page = 1; page <= pages; page++){
				grepPdfPage(entryPath, doc, page, ps);
			}
		} catch (IOException e) {
			ignoreResource(entryPath, e.getLocalizedMessage());
		} finally {
			if (doc != null) {
				try {
					doc.close();
				} catch (IOException e) {
					// ignore
				}
			}
		}
    }

    private boolean documentIsDecrypted(PDDocument doc) {
    	if (!doc.isEncrypted()) {
    		getDisplay().debug("PDF file is not encrypted");
    		return true;
    	}
    	String password = getFileGrep().getPw();
    	if (password == null) {
    		password = "";
    	}
		getDisplay().debug("PDF file is encrypted, attempted to decrypt.");
    	StandardDecryptionMaterial sdm = new StandardDecryptionMaterial(password);
        try {
			doc.openProtection(sdm);
		} catch (BadSecurityHandlerException e) {
    		return false;
		} catch (IOException e) {
    		return false;
		} catch (CryptographyException e) {
    		return false;
		}
        AccessPermission ap = doc.getCurrentAccessPermission();
        return ap.canExtractContent();
	}

	private void grepPdfPage(String entryPath, PDDocument doc, int page, PDFTextStripper ps) throws IOException {
		getDisplay().trace("PDF file '" + entryPath + "' grep page: " + page);
		ps.setStartPage(page);
		ps.setEndPage(page);
		StringWriter sw = new StringWriter();
		ps.writeText(doc, sw);
		BufferedReader br = new BufferedReader(new StringReader(sw.toString()));
		String line = null;
		int linenum = 1;
		GrepMatcher gm = GrepMatcher.getMatcher(null, getFileGrep());
		while ((line = br.readLine()) != null) {
		    gm.setText(line);
		    if (gm.matches()) {
		        // report as <name>:<page>:<line>:text 
		        getDisplay().result(entryPath + ":" + page + ":" + linenum + ":" + gm.toString());
		    }
			linenum++;
		}
	}
}
