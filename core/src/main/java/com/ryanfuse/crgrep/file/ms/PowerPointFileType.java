/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file.ms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hslf.HSLFSlideShow;
import org.apache.poi.hslf.model.Comment;
import org.apache.poi.hslf.model.HeadersFooters;
import org.apache.poi.hslf.model.Notes;
import org.apache.poi.hslf.model.Slide;
import org.apache.poi.hslf.model.TextRun;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.hssf.record.crypto.Biff8EncryptionKey;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.OldWordFileFormatException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.crypt.Decryptor;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFTextShape;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.file.AbstractFileType;

/**
 * File type for grep'ing MS Power Point documents.
 * 
 * Supports 97-2007 (.ppt) and 2007+ (OOXML/.pptx) formats.
 * 
 * @author Craig Ryan
 */
public class PowerPointFileType extends AbstractFileType {

    private static final String PP_EXTENT = ".ppt";
	private static final String SUPPORTED_PP_TYPES = ".*\\.(ppt|pptx)";

	public PowerPointFileType(FileGrep fileGrep) {
		super(fileGrep);
	}

	@Override
	public boolean matchesSupportedType(File entry) {
        Pattern supportedArchiveExtent = Pattern.compile(SUPPORTED_PP_TYPES);
        boolean matches = supportedArchiveExtent.matcher(entry.getName().toLowerCase()).matches();
        getFileGrep().trace("PowerPointFileType '" + entry.getPath() + "' supported type? " + (matches ? "true" : " false "));
        return matches;
	}

	@Override
	public void grepEntry(File file) {
	    String entryName = file.getPath();
	    String err = null;
	    try {
	        String pw = getFileGrep().getPw();
	        PowerDoc doc = PowerPointFactory.create(file, pw);
	        if (doc.isXss()) {
	            XMLSlideShow xss = doc.getXss();
                grepXMLSlideShow(xss, entryName, "");
	        } else if (doc.isSs()) {
	            SlideShow ss = doc.getSs();
                grepSlideShow(ss, entryName, "");
	        } else {
	            ignoreResource(entryName, "Unknown PowerPoint format");
	        }
	    } catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
	        err = e.getLocalizedMessage();
	        ignoreResource(entryName, err);
	    } catch (Exception e) {
	        err = e.getLocalizedMessage();
	        ignoreResource(entryName, err);
	    }
	    if (err != null) {
	        detectErrors(err);
	    }
	}

    public void grepEntryStream(InputStream entryStream, String entryName, String entryLabel) {
        String err = null;
        try {
            String pw = getFileGrep().getPw();
            PowerDoc doc = PowerPointFactory.create(entryStream, pw);
            if (doc.isXss()) {
                XMLSlideShow xss = doc.getXss();
                grepXMLSlideShow(xss, entryName, "");
            } else if (doc.isSs()) {
                SlideShow ss = doc.getSs();
                grepSlideShow(ss, entryName, "");
            } else {
                ignoreResource(entryName, "Unknown PowerPoint format");
            }
        } catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
            err = e.getLocalizedMessage();
            ignoreResource(entryName, err);
        } catch (Exception e) {
            err = e.getLocalizedMessage();
            ignoreResource(entryName, err);
        }
        if (err != null) {
            detectErrors(err);
        }
    }

    // PowerPoint 2007+ format
    public void grepXMLSlideShow(XMLSlideShow ppt, String entryName, String entryLabel) throws IOException, GeneralSecurityException {
        getDisplay().debug("PowerPoint (.pptx) entry [" + entryName + "]");
        XSLFSlide[] slides = ppt.getSlides();
        if (slides == null) {
            return;
        }
        for (int s = 0; s < slides.length; s++){
            XSLFSlide slide = slides[s];
            for (XSLFShape shape : slide.getShapes()) {
                if (shape instanceof XSLFTextShape) {
                    XSLFTextShape txShape = (XSLFTextShape)shape;
                    grepMultiLine(entryName, ":" + (s+1) + ":", txShape.getText());
                }
            }
        }
    }
    
    // PowerPoint 97-07 format
    public void grepSlideShow(SlideShow ss, String entryName, String entryLabel) throws IOException {
        getDisplay().debug("PowerPoint (.ppt) entry [" + entryName + "]");
        Slide[] slides = ss.getSlides();
        if (slides == null) {
            return;
        }
        for (int s = 0; s < slides.length; s++){
            Slide slide = slides[s]; 
            TextRun[] runs = slide.getTextRuns();
            if (runs != null) {
                for (TextRun r : runs) {
                    grepMultiLine(entryName, ":" + (s+1) + ":", r.getText());
                }
            }
            HeadersFooters hf = slide.getHeadersFooters();
            if (hf != null) {
                // report using label ': "H|F" :' depending on whether header or footer.
                grepMultiLine(entryName, ":H:", hf.getHeaderText());
                grepMultiLine(entryName, ":F:", hf.getFooterText());
            }
            Comment[] comments = slide.getComments();
            if (comments != null) {
                for (int j = 0; j < comments.length; j++) {
                    String commentLine = comments[j].getText();
                    grepMultiLine(entryName, ":C:", commentLine);
                }
            }
            Notes notes = slide.getNotesSheet();
            if (notes != null) {
                TextRun[] noteRuns = notes.getTextRuns();
                for (TextRun nr : noteRuns) {
                    grepMultiLine(entryName, ":" + (s+1) + ":", nr.getText());
                }
            }
        }
    }

    /*
     * Attempt to advice on specific error conditions.
     */
    private void detectErrors(String err) {
        err = err.toLowerCase();
        if (err.contains("export restriction")) {
            getDisplay().warn("The problem suggests a modification is needed to your Java installation. Refer to the 'Java Cryptography Extensions' section of INSTALL.txt");
        } else if (err.contains("password")) {
            getDisplay().warn("Specify a valid password using the -p <password> option if you wish to search into a password protected document.");                    
        }
    }
}

