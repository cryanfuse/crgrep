/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file.ms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.security.GeneralSecurityException;

import org.apache.poi.EmptyFileException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.record.crypto.Biff8EncryptionKey;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.crypt.Decryptor;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

/*
 * Similar to org.apache.poi.ss.usermodel.WorkbookFactory, but for Word documents.
 * Handles creation of both format HWPFDocument / XWPFDocument document instances.
 */
public class WordFactory {
    /**
     * Creates a HWPFDocument from the given POIFSFileSystem
     * <p>Note that in order to properly release resources the 
     *  WordDoc should be closed after use.
     */
    public static WordDoc create(POIFSFileSystem fs) throws IOException {
        return new WordDoc(new HWPFDocument(fs));
    }
    
    /**
     * Creates a HWPFDocument from the given NPOIFSFileSystem
     * <p>Note that in order to properly release resources the 
     *  WordDoc should be closed after use.
     */
    public static WordDoc create(NPOIFSFileSystem fs) throws IOException {
        try {
            return create(fs, null);
        } catch (InvalidFormatException e) {
            // Special case of OOXML-in-POIFS which is broken
            throw new IOException(e);
        }
    }
    
    /**
     * Creates a WordDoc from the given NPOIFSFileSystem, which may
     *  be password protected
     */
    private static WordDoc create(NPOIFSFileSystem fs, String password) throws IOException, InvalidFormatException {
        DirectoryNode root = fs.getRoot();
        
        // Encrypted OOXML files go inside OLE2 containers, is this one?
        if (root.hasEntry(Decryptor.DEFAULT_POIFS_ENTRY)) {
            EncryptionInfo info = new EncryptionInfo(fs);
            Decryptor d = Decryptor.getInstance(info);
            
            boolean passwordCorrect = false;
            InputStream stream = null;
            try {
                if (password != null && d.verifyPassword(password)) {
                    passwordCorrect = true;
                }
                if (!passwordCorrect && d.verifyPassword(Decryptor.DEFAULT_PASSWORD)) {
                    passwordCorrect = true;
                }
                if (passwordCorrect) {
                    stream = d.getDataStream(root);
                }
            } catch (GeneralSecurityException e) {
                throw new IOException(e);
            }

            if (!passwordCorrect) {
                if (password != null) {
                    throw new EncryptedDocumentException("Word document is protected, but an incorrect password was supplied");
                } else {
                    throw new EncryptedDocumentException("Word document is protected, but no password was supplied");
                }
            }
            OPCPackage pkg = OPCPackage.open(stream);
            return create(pkg);
        }
        
        // If we get here, it isn't an encrypted DOCX file
        // So, treat it as a regular HWPF Word one
        if (password != null) {
            Biff8EncryptionKey.setCurrentUserPassword(password);
        }
        WordDoc wb = null;
        // May throw OldWordFileFormatException for older formats
        wb = new WordDoc(new HWPFDocument(root));
        Biff8EncryptionKey.setCurrentUserPassword(null);
        return wb;
    }

    /**
     * Creates a XWPFDocument from the given OOXML Package
     * <p>Note that in order to properly release resources the 
     *  WordDoc should be closed after use.
     */
    public static WordDoc create(OPCPackage pkg) throws IOException {
        return new WordDoc(new XWPFDocument(pkg));
    }

    /**
     * Creates the appropriate HWPFDocument / XWPFDocument from
     *  the given InputStream, which may be password protected.
     * <p>Your input stream MUST either support mark/reset, or
     *  be wrapped as a {@link PushbackInputStream}! Note that 
     *  using an {@link InputStream} has a higher memory footprint 
     *  than using a {@link File}.</p> 
     * <p>Note that in order to properly release resources the 
     *  Document should be closed after use. Note also that loading
     *  from an InputStream requires more memory than loading
     *  from a File, so prefer {@link #create(File)} where possible.
     * @throws EncryptedDocumentException If the wrong password is given for a protected file
     * @throws EmptyFileException If an empty stream is given
     */
    public static WordDoc create(InputStream inp, String password) throws IOException, InvalidFormatException, EncryptedDocumentException {
        // If clearly doesn't do mark/reset, wrap up
        if (! inp.markSupported()) {
            inp = new PushbackInputStream(inp, 8);
        }
        
        // Ensure that there is at least some data there
        byte[] header8 = IOUtils.peekFirst8Bytes(inp);

        // Try to create
        if (POIFSFileSystem.hasPOIFSHeader(header8)) {
            NPOIFSFileSystem fs = new NPOIFSFileSystem(inp);
            return create(fs, password);
        }
        if (POIXMLDocument.hasOOXMLHeader(inp)) {
            return create(OPCPackage.open(inp));
        }
        //throw new IllegalArgumentException("Your InputStream was neither an OLE2 stream, nor an OOXML stream");
        throw new IllegalArgumentException("Word document is in an unsupported format");
    }
    
    /**
     * Creates the appropriate HWPFDocument / XWPFDocument from
     *  the given File, which must exist and be readable, and
     *  may be password protected
     * <p>Note that in order to properly release resources the 
     *  Document should be closed after use.
     * @throws EncryptedDocumentException If the wrong password is given for a protected file
     * @throws EmptyFileException If an empty stream is given
     */
    public static WordDoc create(File file, String password) throws IOException, InvalidFormatException, EncryptedDocumentException {
        if (! file.exists()) {
            throw new FileNotFoundException(file.toString());
        }
        try {
            NPOIFSFileSystem fs = new NPOIFSFileSystem(file);
            return create(fs, password);
        } catch (OfficeXmlFileException e) {
            // opening as .doc failed => try opening as .docx
            OPCPackage pkg = OPCPackage.open(file);
            try {
                return create(pkg);
            } catch (IOException ioe) {
                // ensure that file handles are closed (use revert() to not re-write the file)
                pkg.revert();
                //pkg.close();
                
                // rethrow exception
                throw ioe;
            } catch (IllegalArgumentException ioe) {
                // ensure that file handles are closed (use revert() to not re-write the file) 
                pkg.revert();
                //pkg.close();
                
                // rethrow exception
                throw ioe;
            }
        }
    }

    /**
     * Creates the appropriate HWPFDocument / XWPFDocument from
     *  the given File, which must exist and be readable.
     * <p>Note that in order to properly release resources the 
     *  Document should be closed after use.
     * @throws EncryptedDocumentException If the document given is password protected
     */
    public static WordDoc create(File file) throws IOException, InvalidFormatException, EncryptedDocumentException {
        return create(file, null);
    }
}
