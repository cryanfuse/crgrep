/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file.pom;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.repository.ArtifactRepositoryPolicy;
import org.apache.maven.artifact.repository.MavenArtifactRepository;
import org.apache.maven.artifact.repository.layout.DefaultRepositoryLayout;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Proxy;
import org.apache.maven.settings.Settings;
import org.apache.maven.settings.io.DefaultSettingsReader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.slf4j.impl.SimpleLogger;
import org.sonatype.aether.artifact.Artifact;
import org.sonatype.aether.resolution.DependencyResolutionException;
import org.sonatype.aether.util.artifact.DefaultArtifact;
import org.sonatype.aether.util.artifact.JavaScopes;

import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.util.Display;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Navigate a maven POM dependency list looking for resources (artifacts) to include in the search.
 *
 * This parser is not limited to maven local repository lookup, it may also result in remote
 * repository access when resolving dependencies which result in a delayed response.
 *
 * If a settings.xml exists (typically in HOME/.m2/settings.xml) then proxy configuration
 * will be used.
 * 
 * TODO: could define an option to follow <modules> and process those POM files also 
 * TODO: full use of settings.xml based authentication/repository lists
 *  
 * @author Craig Ryan
 */
public class PomParser {
    
    private static final String DEFAULT_REMOTE_REPO = "http://repo1.maven.org/maven2/";
    private static final String POM_FILE_PREFIX = "->";
    private List<File> artifacts = new ArrayList<File>();
    private String remoteRepoList;  // syntax is 'urlRepo1;urlRepo2;..'
    private List<ArtifactRepository> remoteRepos = new ArrayList<ArtifactRepository>(); 
    private File localRepoFolder;
    
    private File projectPom;
    private Display display;
    private Switches switches;
    private String prefixName;
    private Proxy proxy;
    private Settings settings;

    public PomParser(Switches switches, Display display) {
        this.display = display;
        this.switches = switches;
        loadSettingsFile();
        Properties userProperties = switches.getUserProperties();
        if (userProperties != null) {
        	setRemoteRepoList(userProperties.getProperty("maven.remoteRepos"));
        	setLocalRepo(userProperties.getProperty("maven.localRepo"));
        } else {
        	setRemoteRepoList(null);
        	setLocalRepo(null);
        }

        // Turn off painful INFO logging
        System.setProperty(SimpleLogger.LOG_KEY_PREFIX + "com.ning.http.client", "warn");
    }
    
    /**
     * Main entry point to initiate processing of a new POM file.
     * 
     * Extract all artifacts and store as a list of File resources.
     * 
     * @param pomFile the project POM file 
     * @throws XmlPullParserException 
     * @throws IOException 
     * @throws DependencyResolutionException 
     */
    public void parsePomFile(File pomFile) throws DependencyResolutionException, IOException, XmlPullParserException, GrepException {
        this.projectPom = pomFile;
        prefixName = projectPom.getPath();
        display.debug("PomParser parse file '" + pomFile.getPath() + "'");
        reset();
        saveArtifactsFromMavenProjectFile();
    }

    /**
     * Alternative to {@link #parsePomFile(File)} which processes a POM as an input stream.
     * 
     * @param pomStream POM stream
     * @param pomFileName name of POM file associated with this stream
     * @throws XmlPullParserException 
     * @throws IOException 
     * @throws DependencyResolutionException 
     */
    public void parsePomFileStream(InputStream pomStream, String pomFileName) throws DependencyResolutionException, IOException, XmlPullParserException, GrepException {
        reset();
        prefixName = pomFileName;
        saveArtifactsFromMavenProjectStream(pomStream);
    }

    /**
     * Get POM file name prefix for displaying results. This includes the POM file name
     * plus a delimiter.
     * 
     * @return the name and delimiter string. 
     */
    public String getPrefix() {
        return prefixName + POM_FILE_PREFIX;
    }
    
    /**
     * Get a list of artifacts (resources) from the current POM. 
     * 
     * @return a list (possibly empty) of artifacts
     */
    public List<File> getDependencyList() {
        return artifacts;
    }

    /*
     * Load the standard settings.xml maven configuration.
     */
    private void loadSettingsFile() {
        File userSettingsFile = new File(System.getProperty("user.home"), ".m2" + File.separator + "settings.xml");
        DefaultSettingsReader r = new DefaultSettingsReader();
        try {
            settings = r.read(userSettingsFile, null);
        } catch (IOException e) {
            display.debug("Failed to read maven settings.xml with error: " + e.getLocalizedMessage());
            return;
        }
        proxy = settings.getActiveProxy();
    }

    /*
     * Process the POM and extract artifacts (resources) that will be included in the search list.
     */
    private void saveArtifactsFromMavenProjectFile() throws DependencyResolutionException, IOException, XmlPullParserException, GrepException {
        MavenProject proj = readMavenProject(projectPom);
        processMavenProject(proj);
    }
    
    private void saveArtifactsFromMavenProjectStream(InputStream pomStream) throws DependencyResolutionException, IOException, XmlPullParserException, GrepException {
        MavenProject proj = readMavenProjectFromStream(pomStream);
        processMavenProject(proj);
    }
    
    /*
     * Navigate the POM tree extracting dependency artifact paths
     */
    private void processMavenProject(MavenProject proj) throws DependencyResolutionException, IOException, XmlPullParserException, GrepException {
    	List<Dependency> dependencyList = proj.getDependencies();
    	Iterator<Dependency> dependencies = dependencyList.iterator();
    	Properties properties = proj.getProperties();
    	Map<String, String> propMap = new HashMap(properties);
    	proj.setRemoteArtifactRepositories(remoteRepos);
        PomResolver pomResolver = new PomResolver(proj, localRepoFolder, display);
        while (dependencies.hasNext()) {
    		Dependency dependency = dependencies.next();
    		String scope = dependency.getScope();
    		if (scope == null) {
    			// default is COMPILE if <scope> isn't specified. 
    			scope = JavaScopes.SYSTEM;
    		}
    		String grpId = sub(dependency.getGroupId(), properties);
    		String artId = sub(dependency.getArtifactId(), properties);
    		String classifier = sub(dependency.getClassifier(), properties);
    		String typ = sub(dependency.getType(), properties);
    		String ver = sub(dependency.getVersion(), properties);
    		display.debug("Pom: resolving dependency Group[" + grpId + "] Artifact[" + artId + "] Scope[" + scope + "] Ver[" + ver + "]");
    		Artifact root = new DefaultArtifact(
    				grpId, 
    				artId, 
    				classifier, 
    				typ,
    				ver, 
    				propMap, 
    				(File)null);
    		Collection<Artifact> artifactList = pomResolver.resolve(
    				root,
    				scope
    				);
    		 Iterator<Artifact> artifactIter = artifactList.iterator();
             while (artifactIter.hasNext()) {
                 Artifact artifact = artifactIter.next();
                 artifacts.add(artifact.getFile());
             }
        }
    }

    /*
     * Substitute variables in dependency data
     */
    private String sub(String v, Properties p) throws GrepException {
        if (v == null) {
            return v;
        }
        int i = v.indexOf("${");
        while (i != -1) {
            int j = v.indexOf("}");
            display.trace("pom var substitution [" + i + ":" + j + "] on [" + v + "]");
            if (j < i) {
                break; // invalid format
            }
            String var = v.substring(i + 2, j);
            String val = p.getProperty(var);
            if (val != null) {
                String re = Pattern.quote("${" + var + "}");
                v = v.replaceAll(re, val);
            } else {
            	throw new GrepException("Failed to substitute for pom file variable [" + v + "]");
            }
            int i2 = v.indexOf("${");
            if (i2 == i) {
            	// sitting on the same var (no replacement made).. break
            	break;
            }
        }
        return v;
    }

    /*
     * Read the POM into a MavenProject container
     */
    private MavenProject readMavenProject(File pomFile) throws IOException, XmlPullParserException {
        MavenXpp3Reader mavenReader = new MavenXpp3Reader();
        MavenProject ret = null;
        if (pomFile != null && pomFile.exists()) {
            FileReader reader = null;
            try {
                reader = new FileReader(pomFile);
                Model model = mavenReader.read(reader);
                model.setPomFile(pomFile);
                ret = new MavenProject(model);
            } finally {
                reader.close();
            }
        }
        return ret;
    }

    private MavenProject readMavenProjectFromStream(InputStream pomFile) throws IOException, XmlPullParserException {
        MavenXpp3Reader mavenReader = new MavenXpp3Reader();
        MavenProject ret = null;
        if (pomFile != null) {
            try {
                Model model = mavenReader.read(pomFile);
                ret = new MavenProject(model);
            } finally {
            }
        }
        return ret;
    }
    
    /*
     * Setup a list of remote repos, either configured or default
     */
    private void setRemoteRepoList(String repoList) {
        this.remoteRepoList = repoList == null ? DEFAULT_REMOTE_REPO : repoList;
        display.debug("Pom: remote repo list [" + remoteRepoList + "]");
        String[] dirs = remoteRepoList.split(";");
        org.apache.maven.repository.Proxy repoProxy = proxy == null ? null : asRepoProxy(proxy); 
        for (String dir : dirs) {
            try {
                new URL(dir); // valid url, should be OK
                String id = dir;
                if (dir.equals(DEFAULT_REMOTE_REPO)) {
                    id = "maven-central";
                }
                ArtifactRepository repo = new MavenArtifactRepository(
                        id,
                        dir,
                        new DefaultRepositoryLayout(),
                        new ArtifactRepositoryPolicy(), 
                        new ArtifactRepositoryPolicy()
                        );
                if (repoProxy != null) {            
                    repo.setProxy(repoProxy);
                }
                remoteRepos.add(repo);
                display.debug("Pom: include remote repo [" + dir + "]");
            } catch (MalformedURLException e) {
                display.warn("Warning: the maven remote repository path '" + dir + "' is invalid and will be ignored.");
                // ignore
            }
        }
    }

    /*
     * Convert settings Proxy to repository Proxy
     */
    private org.apache.maven.repository.Proxy asRepoProxy(Proxy proxy) {
        org.apache.maven.repository.Proxy rProxy = new org.apache.maven.repository.Proxy();
        rProxy.setHost(proxy.getHost());
        rProxy.setPort(proxy.getPort());
        rProxy.setNonProxyHosts(proxy.getNonProxyHosts());
        rProxy.setProtocol(proxy.getProtocol());
        rProxy.setUserName(proxy.getUsername());
        rProxy.setPassword(proxy.getPassword());
        display.debug("Pom: proxy entry in settings.xml used, host:port [" + rProxy.getHost() + ":" + rProxy.getPort() + "]" + " user [" + rProxy.getUserName() + "]");
        return rProxy;
    }

    /*
     * Setup the local repo path from config or default.
     */
    private void setLocalRepo(String locRepo) {
        File repoFile;
        if (locRepo != null) {
            repoFile = new File(locRepo);
            if (repoFile.exists() && repoFile.isDirectory()) {
                localRepoFolder = repoFile;
                display.debug("Pom: include local repo [" + locRepo + "]");
                return;
            }
            display.warn("Warning: the maven local repository path '" + locRepo + "' is invalid and will be ignored.");
            // fall-through, try the default..
        } 
        String home = System.getProperty("user.home").replaceAll("\\\\", "/");;
        locRepo = home +  "/.m2/repository/"; // use unix separator.
        repoFile = new File(locRepo);
        if (repoFile.exists() && repoFile.isDirectory()) {
            localRepoFolder = repoFile;
            display.debug("Pom: include default local repo [" + locRepo + "]");
            // The configured user property is invalid, update with the corrected default.
            if (switches.getUserProperties() != null) {
                switches.getUserProperties().setProperty("maven.localRepo", locRepo);
            }
        }
    }

    /*
     * Clear the artifacts from the last POM. Leave startup config unchanged.
     */
    private void reset() {
        artifacts.clear();
    }
}
