/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file.ms;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

/*
 * Wrap either HWPFDocument / XWPFDocument 
 */
public class WordDoc {
    private HWPFDocument wdoc;
    private XWPFDocument xdoc;

    public WordDoc(HWPFDocument wdoc) {
        this.wdoc = wdoc;
    }
    public WordDoc(XWPFDocument xdoc) {
        this.xdoc = xdoc;
    }
    public boolean isXDoc() {
        return xdoc != null;
    }
    public boolean isDoc() {
        return wdoc != null;
    }
    public HWPFDocument getWdoc() {
        return wdoc;
    }
    public XWPFDocument getXdoc() {
        return xdoc;
    }
}
