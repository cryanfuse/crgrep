/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file.ms;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.HeaderStories;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Range;

/**
 * File type for grep'ing MS Word documents (.doc).
 * 
 * Supports 97-2007 format. Limited 95 and 6.0 support.
 * 
 * @author Craig Ryan
 */
public class WordDocFileType {

    private WordFileType wordType;
    
	public WordDocFileType(WordFileType wordType) {
	    this.wordType = wordType;
	}

    // Word 97-07 format, possibly also Word 6 and 95.
	public void grepDocument(HWPFDocument doc, String entryName, String entryLabel) {
        try {
            // Streams are exhausted (eof) or closed if this call fails. Can't
            // re-use the stream regardless of the outcome.
            //doc = new HWPFDocument(entryStream);
            grepHeaders(doc, entryName);
            // no label, will use the paragraph ordinal
            grepParagraph(doc.getRange(), null, entryName);
            grepCommentsAndNotes(doc, entryName);
            grepTextBoxes(doc, entryName);
            grepFooters(doc, entryName);
        } catch (EncryptedDocumentException e) {
            wordType.ignoreResource(entryName, e.getLocalizedMessage());
        } catch (Exception e) {
            wordType.ignoreResource(entryName, e.getLocalizedMessage());
        }
    }

    private void grepTextBoxes(HWPFDocument doc, String entryName) {
        Range textboxRange = doc.getMainTextboxRange();
        int numTextboxes = textboxRange.numParagraphs();
        for (int i = 0; i < numTextboxes; i++) {
            String textbox = textboxRange.getParagraph(i).text();
            wordType.grepMultiLine(entryName, ":TB:", textbox);
        }
    }

    private void grepCommentsAndNotes(HWPFDocument doc, String entryName) {
        Range commentsRange = doc.getCommentsRange();
        int numComments = commentsRange.numParagraphs();
        for (int i = 0; i < numComments; i++) {
            String comment = commentsRange.getParagraph(i).text();
            wordType.grepMultiLine(entryName, ":C:", comment);
        }
        Range notesRange = doc.getEndnoteRange();
        int numNotes = notesRange.numParagraphs();
        for (int j = 0; j < numNotes; j++) {
            String note = notesRange.getParagraph(j).text();
            wordType.grepMultiLine(entryName, ":N:", note);
        }
    }

    private void grepHeaders(HWPFDocument doc, String entryName) {
        HeaderStories headerFooter = new HeaderStories(doc);
        grepParagraph(headerFooter.getFirstHeaderSubrange(), ":H:", entryName);
        grepParagraph(headerFooter.getEvenHeaderSubrange(), ":H:even:", entryName);
        grepParagraph(headerFooter.getOddHeaderSubrange(), ":H:odd:", entryName);
    }

    private void grepFooters(HWPFDocument doc, String entryName) {
        HeaderStories headerFooter = new HeaderStories(doc);
        grepParagraph(headerFooter.getFirstFooterSubrange(), ":F:", entryName);
        grepParagraph(headerFooter.getEvenFooterSubrange(), ":F:even:", entryName);
        grepParagraph(headerFooter.getOddFooterSubrange(), ":F:odd:", entryName);
    }

    private void grepParagraph(Range r, String successLabel, String entryName) {
        if (r == null) {
            return;
        }
        for (int i = 0; i < r.numParagraphs(); i++) {
            Paragraph p = r.getParagraph(i);
            if (p != null) {
                String label = successLabel == null ? ":" + (i+1) + ":" : successLabel;
                // report as <name>:<paragraph>:text 
                wordType.grepMultiLine(entryName, label, p.text());
            }
        }
    }
}
