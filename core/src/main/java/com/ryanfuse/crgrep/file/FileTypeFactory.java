/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.file.ms.ExcelFileType;
import com.ryanfuse.crgrep.file.ms.PowerPointFileType;
import com.ryanfuse.crgrep.file.ms.WordFileType;

/**
 * Manage all file type handlers.
 * 
 * As each resource (File or stream) is discovered, the factory is consulted for a suitable
 * FileType to handler that resource type.
 * 
 * @author Craig Ryan
 */
public class FileTypeFactory {

    private List<AbstractFileType> fileTypeGrepers;
    private AbstractFileType defaultFileType;

    public FileTypeFactory(FileGrep fileGrep) {
        
        // Do NOT add DefaultFileType to fileTypeGrepers list!
        fileTypeGrepers = new ArrayList<AbstractFileType>();
        fileTypeGrepers.add(new DirectoryFileType(fileGrep));
        fileTypeGrepers.add(new ArchiveFileType(fileGrep));
        fileTypeGrepers.add(new ImageFileType(fileGrep));
        fileTypeGrepers.add(new AudioFileType(fileGrep));
        fileTypeGrepers.add(new PomFileType(fileGrep));
        fileTypeGrepers.add(new PdfFileType(fileGrep));
        fileTypeGrepers.add(new WordFileType(fileGrep));
        fileTypeGrepers.add(new PowerPointFileType(fileGrep));
        fileTypeGrepers.add(new ExcelFileType(fileGrep));

        defaultFileType = new DefaultFileType(fileGrep);
    }

    public AbstractFileType getFileType(Path entry) {
        return getFileType(entry.toFile());
    }
    
    public AbstractFileType getFileType(File entry) {
        for (AbstractFileType aft : fileTypeGrepers) {
            if (aft.matchesSupportedType(entry)) {
                return aft;
            }
        }
        return defaultFileType;
    }
    
    public AbstractFileType getFileTypeByName(String entryPath) {
        for (AbstractFileType aft : fileTypeGrepers) {
            if (aft.matchesSupportedFileName(entryPath)) {
                return aft;
            }
        }
        return defaultFileType;
    }
}
