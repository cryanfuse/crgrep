/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file.ms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.crypt.Decryptor;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.Header;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.file.AbstractFileType;

/**
 * File type for grep'ing MS Excel spreadsheet documents.
 * 
 * Supports 97-2007 (.xls) and 2007+ (OOXML/.xlsx) formats. Pre-97 is not supported.
 * 
 * @author Craig Ryan
 */
public class ExcelFileType extends AbstractFileType {

    private static final String SUPPORTED_EXCEL_TYPES = ".*\\.(xls|xlsx)";

	public ExcelFileType(FileGrep fileGrep) {
		super(fileGrep);
	}

	@Override
	public boolean matchesSupportedType(File entry) {
        Pattern supportedArchiveExtent = Pattern.compile(SUPPORTED_EXCEL_TYPES);
        boolean matches = supportedArchiveExtent.matcher(entry.getName().toLowerCase()).matches();
        getFileGrep().trace("ExcelFileType '" + entry.getPath() + "' supported type? " + (matches ? "true" : " false "));
        return matches;
	}

	@Override
	public void grepEntry(File file) {
		try {
			grepExcelEntryStream(new FileInputStream(file), file.getPath(), "");
		} catch (FileNotFoundException e) {
			ignoreResource(file.getPath(), e.getLocalizedMessage());
		} 
	}

    public void grepEntryStream(InputStream entryStream, String entryName, String entryLabel) {
        grepExcelEntryStream(entryStream, entryName, entryLabel);
    }

    public void grepExcelEntryStream(InputStream entryStream, String entryName, String entryLabel) {
        String pw = getFileGrep().getPw();
        List<String> errs = new ArrayList<String>();
        Workbook wb = getWorkbook(entryStream, entryName, pw, errs);
        /* wont work, stream is closed
        if (wb == null && pw != null) {
            // re-try without using the configured password
            wb = getWorkbook(entryStream, entryName, null, null);
        }
        */
        if (wb == null) {
            if (!errs.isEmpty()) {
                String err = errs.get(0).toLowerCase();
                ignoreResource(entryName, err);
                if (err.contains("export restriction")) {
                    getDisplay().warn("The problem suggests a modification is needed to your Java installation. Refer to the 'Java Cryptography Extensions' section of INSTALL.txt");
                } else if (err.contains("must be decrypted before use") || err.contains("is protected")) {
                    getDisplay().warn("Specify a valid password using the -p <password> option if you wish to search into a password protected document.");                    
                } else if (err.contains("support") && err.contains("encryption")) {
                    getDisplay().warn("Unable to decrypt this format of password protected document");                    
                }
            }
            return;
        }
        try {
            grepExcelSheets(wb, entryName, entryLabel);
        } catch (Exception e) {
            ignoreResource(entryName, e.getLocalizedMessage());
        } finally {
            if (wb != null) {
                try {
                    wb.close();
                } catch (IOException e) {
                }
            }
        }
    }

    // 'errs' param allows multiple error conditions to be captured and analysed in case Hints can be offered. 
    private Workbook getWorkbook(InputStream entryStream, String entryName, String pw, List<String> errs) {
        Workbook wb = null;
        String err = null;
        try {
            if (pw == null || pw.isEmpty()) {
                wb = WorkbookFactory.create(entryStream);
            } else {
                wb = WorkbookFactory.create(entryStream, pw);
            }
        } catch (IOException e) {
            ignoreResource(entryName, e.getLocalizedMessage());
        } catch (InvalidFormatException e) {
            ignoreResource(entryName, e.getLocalizedMessage());
        } catch (EncryptedDocumentException e) {
            getDisplay().debug("Spreadsheet " + entryName + " failed with Encrypted Document error.");
            if (errs != null) {
                err = e.getLocalizedMessage();
                errs.add(err);
            } else {
                ignoreResource(entryName, e.getLocalizedMessage());
            }
        }
        return wb;
    }

    /*
     * TODO: implement old format extraction.
     * import org.apache.poi.hssf.extractor.OldExcelExtractor;
    public void grepOldExcelEntryStream(InputStream entryStream, String entryName, String entryLabel) {
        OldExcelExtractor oee = ??
    }
     */

    private void grepExcelSheets(Workbook wb, String entryName, String entryLabel) {
        int numSheets = wb.getNumberOfSheets();
        getDisplay().debug("Spreadsheet " + entryName + " has " + numSheets + " sheets.");
        for (int s = 0; s < numSheets; s++) {
            Sheet sheet = wb.getSheetAt(s);
            if (sheet == null) {
                continue;
            }
            int numRows = sheet.getLastRowNum(); //sheet.getPhysicalNumberOfRows();
            getDisplay().debug("sheet " + s + " with " + numRows + " sheet rows.");
            for (int r = sheet.getFirstRowNum(); r < numRows; r++) {
                Row row = sheet.getRow(r);
                if (row == null) {
                    continue;
                }
                int numCells = row.getLastCellNum();//row.getPhysicalNumberOfCells();
                for (int c = row.getFirstCellNum(); c < numCells; c++) {
                    Cell cell = row.getCell(c);
                    grepCell(s, r, c, cell, entryName);
                }
            }
            Header h = sheet.getHeader();
            if (h != null) {
                grepHeaderFooter(":H:" + s + ":", h.getLeft(), h.getCenter(), h.getRight(), entryName);
            }
            Footer f = sheet.getFooter();
            if (f != null) {
                grepHeaderFooter(":F:" + s + ":", f.getLeft(), f.getCenter(), f.getRight(), entryName);
            }
        }
    }

    // Headers and footers on each sheet can have left, center and/or right text areas.
    private void grepHeaderFooter(String successLabel, String left, String center, String right, String entryName) {
        if (left == null && center == null && right == null) {
            return;
        }
        grepMultiLine(entryName, successLabel, left);
        grepMultiLine(entryName, successLabel, center);
        grepMultiLine(entryName, successLabel, right);
    }

    // Grep the cell content based on cell type
    private void grepCell(int sheet, int rowNum, int cellNum, Cell cell, String entryName) {
        if (cell == null) {
            return;
        }
        String line = null;
        switch (cell.getCellType()) {
        case Cell.CELL_TYPE_STRING:
            line = cell.getStringCellValue();
            break;
        case Cell.CELL_TYPE_NUMERIC:
            line = Double.toString(cell.getNumericCellValue());
            break;
        case Cell.CELL_TYPE_BOOLEAN:
            line = Boolean.toString(cell.getBooleanCellValue());
            break;
        case Cell.CELL_TYPE_FORMULA:
            line = cell.getCellFormula();
            break;
        }
        // report as <sheet>:<row>:<cell>
        grepMultiLine(entryName, ":" + sheet + ":" + rowNum + ":" + cellNum + ":", line);

        // Cell comment
        Comment comment = cell.getCellComment();
        if (comment != null) {
            line = comment.getString().getString();
            grepMultiLine(entryName, ":C:" + sheet + ":" + rowNum + ":" + cellNum + ":", line);
        }
    }
}
