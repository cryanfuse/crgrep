/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.sonatype.aether.resolution.DependencyResolutionException;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.file.pom.PomParser;

/**
 * A Project Object Model (maven POM) file type implementation.
 * 
 * A POM may be discovered in the search list or inside another resource
 * (say within a JAR artifact). If -m is enabled then we will extract
 * artifact dependencies from the POM and include those in the search list.
 *  
 * @author Craig Ryan
 */
public class PomFileType extends AbstractFileType {

    private static final String POM_FILE = "pom.xml";
    private static final int MAX_DEFAULT_DEPTH = 1;
    
    private PomParser pomParser;
    private int depth;
    private int maxDepth = MAX_DEFAULT_DEPTH;

    public PomFileType(FileGrep fileGrep) {
        super(fileGrep);
        depth = 0;
        Properties userProperties = getFileGrep().getSwitches().getUserProperties();
        if (userProperties != null) {
            String max = userProperties.getProperty("maven.depth");
            try {
                if (max != null && !max.isEmpty()) {
                    maxDepth = Integer.valueOf(max);
                }
            } catch (NumberFormatException e) {
            	getDisplay().warn("PomFileType user property 'maven.depth' in HOME/.crgrep with value '" + max + "' can't be converted to an integer. Defaulting to 1.");
                // ignore
            }
        }
    }

    /**
     * Accept the POM file for parsing if its a valid '*pom.xml' name and 
     * -m (maven) option is enabled. POMs are not processed by default.
     */
    @Override
    public boolean matchesSupportedType(File entry) {
        boolean matches = isPomFile(entry.getName()) && getFileGrep().isMaven();
    	getFileGrep().trace("PomFileType '" + entry.getPath() + "' supported type? " + (matches ? "true" : " false " + (getFileGrep().isMaven() ? "(ignoreMaven)" : "(not POM)")));
    	return matches;
    }

    @Override
    public boolean matchesSupportedFileName(String entryName) {
        boolean matches = isPomFile(entryName) && getFileGrep().isMaven();
    	getFileGrep().trace("PomFileType '" + entryName + "' supported type? " + (matches ? "true" : " false " + (getFileGrep().isMaven() ? "(ignoreMaven)" : "(not POM)")));
    	return matches;
    }
    
    /**
     * Grep a POM file. The depth limit is enforced to prevent unlimited nested, 
     * for example a POM file which contains a JAR artifact which in turn contains
     * a POM file and so on, which could potentially lead to cycles. 
     * @throws GrepException 
     */
    @Override
    public void grepEntry(File file) throws GrepException {
    	if (!withinDepth(file.getPath())) {
    		return;
    	}
        try {
        	getFileGrep().debug("PomFileType grep entry '" + file.getPath() + "' (depth now " + depth + " of max " + getMaxDepth() + ")");
            grepPomEntry(file);
        } finally {
            depth--;
        }
    }

    @Override
    public void grepEntryStream(InputStream entryStream, String entryName, String entryPath) throws GrepException {
    	String fullName = streamEntryName(entryPath, entryName);
    	if (!withinDepth(fullName)) {
    		return;
    	}
        try {
        	getFileGrep().debug("PomFileType grep entry '" + fullName + "' (depth now " + depth + " of max " + getMaxDepth() + ")");
            grepPomEntryStream(entryStream, entryName, entryPath);
        } finally {
            depth--;
        }
    }

    private boolean withinDepth(String entryName) {
        if (depth + 1 > getMaxDepth()) {
        	getFileGrep().trace("PomFileType grep entry '" + entryName + "' ignored, reached maxDepth of " + getMaxDepth());
            return false;
        }
        ++depth;
        return true;
    }
    
    private boolean isPomFile(String fileName) {
        return fileName.endsWith(POM_FILE);
    }
    
    /*
     * Parse the POM and extract artifacts and include in the search path
     */
    private void grepPomEntry(File pomFile) throws GrepException {
        String fullPath = pomFile.getPath();
        grepByName(fullPath, pomFile.getName());
        getPomParser();
        try {
            pomParser.parsePomFile(pomFile);
        } catch (DependencyResolutionException e) {
            getDisplay().debug("POM file '" + fullPath + "' failed parsing with Dependency Resolution error, cause: " + e.getLocalizedMessage());
            ignoreResource(fullPath, e.getLocalizedMessage());
            return;
        } catch (IOException e) {
            getDisplay().debug("POM file '" + fullPath + "' failed parsing with IO error, cause: " + e.getLocalizedMessage());
            ignoreResource(fullPath, e.getLocalizedMessage());
            return;
        } catch (XmlPullParserException e) {
            getDisplay().debug("POM file '" + fullPath + "' failed parsing with XML error, cause: " + e.getLocalizedMessage());
            ignoreResource(fullPath, e.getLocalizedMessage());
            return;
        } catch (GrepException e) {
            getDisplay().debug("POM file '" + fullPath + "' failed parsing with cause: " + e.getLocalizedMessage());
            ignoreResource(fullPath, e.getLocalizedMessage());
            throw e;
        } 
        grepDependencyList();
    }

    /*
     * Parse the POM from a stream and extract artifacts and include in the search path
     */
    private void grepPomEntryStream(InputStream entryStream, String entryName, String entryPath) throws GrepException {
    	String fullPath = streamEntryName(entryPath, entryName);
        getPomParser();
        try {
            pomParser.parsePomFileStream(entryStream, fullPath);
        } catch (DependencyResolutionException e) {
            getDisplay().debug("POM file stream '" + fullPath + "' failed parsing with Dependency Resolution error, cause: " + e.getLocalizedMessage());
            ignoreResource(fullPath, e.getLocalizedMessage());
            return;
        } catch (IOException e) {
            getDisplay().debug("POM file stream '" + fullPath + "' failed parsing with IO error, cause: " + e.getLocalizedMessage());
            ignoreResource(fullPath, e.getLocalizedMessage());
            return;
        } catch (XmlPullParserException e) {
            getDisplay().debug("POM file stream '" + fullPath + "' failed parsing with XML error, cause: " + e.getLocalizedMessage());
            ignoreResource(fullPath, e.getLocalizedMessage());
            return;
        }
        grepDependencyList();
    }

    /*
     * Grep the dependency list extracted from the POM
     */
    private void grepDependencyList() throws GrepException {
        List<File> depList = pomParser.getDependencyList();
        if (depList.isEmpty()) {
            return;
        }
        getDisplay().setPrefix(pomParser.getPrefix());
        for (File artifact : depList) {
            grepFileEntry(artifact);
        }
        getDisplay().unsetPrefix();

    }

    /**
     * Max depth to drill into POM dependencies. 
     * @return
     */
    public int getMaxDepth() {
        return maxDepth;
    }

    /**
     * @return the pomParser
     */
    public PomParser getPomParser() {
        if (pomParser == null) {
            pomParser = new PomParser(getFileGrep().getSwitches(), getDisplay());
        }
        return pomParser;
    }

    /**
     * @param pomParser the pomParser to set
     */
    public void setPomParser(PomParser pomParser) {
        this.pomParser = pomParser;
    }
}
