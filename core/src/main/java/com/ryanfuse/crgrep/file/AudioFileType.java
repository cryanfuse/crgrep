/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

import com.beaglebuddy.id3.enums.PictureType;
import com.beaglebuddy.id3.pojo.AttachedPicture;
import com.beaglebuddy.mp3.MP3;
import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.util.GrepMatcher;

/**
 * An Audio file type to handle and process meta data and contents.
 * 
 * Extract any known meta data from the audio file include album, title, 
 * artist and track information. 
 * 
 * @author Craig Ryan
 */
public class AudioFileType extends AbstractFileType {

    private static final String AUDIO_FILE_EXTENTS = ".*\\.(mp3)";
    private static final String META_DELIMITER = ": @";
    private static Pattern archiveExtent = Pattern.compile(AUDIO_FILE_EXTENTS, Pattern.CASE_INSENSITIVE);
    
    private boolean listedFile;

    public AudioFileType(FileGrep fileGrep) {
        super(fileGrep);
    }

    @Override
    public boolean matchesSupportedType(File entry) {
        boolean isAudio = isAudioFile(entry.getName());
    	trace("AudioFileType '" + entry.getPath() + "' supported type? isAudio=" + isAudio);
    	return isAudio;
    }
    
    @Override
    public boolean matchesSupportedFileName(String entryName) {
        boolean isAudio = isAudioFile(entryName);
    	trace("AudioFileType '" + entryName + "' supported stream name? isAudio=" + isAudio);
        return isAudio;
    }
    
    @Override
    public void grepEntry(File file) throws GrepException {
        String entryPath = file.getPath();
        grepByName(entryPath, file.getName());
        if (!getFileGrep().isContent()) {
            return;
        }
		try {
		    MP3 mp3 = new MP3(file);
            grepAudio(mp3, entryPath);
		} catch (IOException e) {
            ignoreResource(entryPath, e.getLocalizedMessage());
        }
    }
    
    @Override
    public void grepEntryStream(InputStream entryStream, String entryName, String entryPath) throws GrepException {
    	String streamPath = streamEntryName(entryPath, entryName);
    	try {
    	    MP3 mp3 = new MP3(entryStream);
            grepAudio(mp3, streamPath);
		} catch (IOException e) {
            ignoreResource(streamPath, e.getLocalizedMessage());
		}
    }
    
    /*
     * If its a supported audio file type.
     */
    private boolean isAudioFile(String entryName) {
        if (archiveExtent.matcher(entryName).matches()) {
            return true;
        }
        return false;
    }
    
    private void grepAudio(MP3 mp3, String entryPath) throws GrepException {
        getFileGrep().debug("AudioFileType grep tag-data for audio '" + entryPath + "'");
        if (getFileGrep().getSwitches().isTrace()) {
            traceEntry(entryPath, mp3);
        }
        listedFile = false;
        AttachedPicture frontPic = mp3.getPicture(PictureType.FRONT_COVER);
        AttachedPicture backPic = mp3.getPicture(PictureType.BACK_COVER);
        grepTagData(new String[] {
                "Album", mp3.getAlbum(),
                "Artist", mp3.getBand(),
                "Year", Integer.toString(mp3.getYear()),
                "TrackTitle", mp3.getTitle(),
                "LeadArtist", mp3.getLeadPerformer(),
                "LyricsBy", mp3.getLyricsBy(),
                "MusicBy", mp3.getMusicBy(),
                "Duration", Integer.toString(mp3.getAudioDuration()),
                "Lyrics", mp3.getLyrics(),
                "TrackNo", Integer.toString(mp3.getTrack()),
                "Publisher", mp3.getPublisher(),
                "Size", Integer.toString(mp3.getAudioSize()),
                "BitRate", Integer.toString(mp3.getBitrate()),
                "BitRateType", mp3.getBitrateType().toString(),
                "Frequency", Integer.toString(mp3.getFrequency()),
                "Rating", Integer.toString(mp3.getRating()),
                "FrontCoverDescription", (frontPic == null ? null : frontPic.getDescription()),
                "BackCoverDescription", (backPic == null ? null : backPic.getDescription())
            },
            entryPath
        );
        grepAttachedImage(frontPic, entryPath);
        grepAttachedImage(backPic, entryPath);
    }
    
    /*
     * Pass an attached image up for processing.
     */
    private void grepAttachedImage(AttachedPicture pic, String entryPath) throws GrepException {
        if (pic == null) {
            return;
        }
        String entryName = imageEntryName(pic.getPictureType(), pic.getMimeType());
        ByteArrayInputStream image = new ByteArrayInputStream(pic.getImage());
        getFileGrep().grepByContentsStream(image, entryName, entryPath);
    }

    /* 
     * Generate an image name by picture and mime type to include a name and extent
     * A FRONT_COVER image with type 'image/jpg' will result in an entry name
     * of FrontCover-image.jpg so that the ImageFileType might be able to handle it.
     */
    private String imageEntryName(PictureType pictureType, String mimeType) {
        StringBuilder sb = new StringBuilder();
        sb.append(pictureType == PictureType.FRONT_COVER ? "FrontCover-" : "BackCover-");
        sb.append(mimeType.replaceAll("/", "."));
        return sb.toString();
    }

    private void grepTagData(String[] tags, String entryPath) {
        if (!getFileGrep().isContent()) {
        	if (listedFile) {
        		// Already listed the file after a match. Avoid duplicate reporting.
        		return;
        	}
        }
        GrepMatcher gm = GrepMatcher.getMatcher(null, getFileGrep());
        StringBuilder sb = new StringBuilder();
        boolean haveMatch = false;
        for (int i = 0; i < tags.length; i += 2) {
            String label = tags[i];
            String data = tags[i+1];
            if (getFileGrep().getSwitches().isTrace()) {
            	trace("    -> tag [" + label + "] data [" + data + "]");
            }
            if (data == null || data.trim().length() == 0) {
                continue;
            }
            gm.setText(data);
            if (gm.matches()) {
                if (sb.length() == 0) {
                    sb.append("{");
                } else {
                    sb.append(", ");
                }
                sb.append(label).append("=").append(gm.toString());
                haveMatch = true;
            }
        }

        if (haveMatch) {
            sb.append("}");
            getDisplay().result(entryPath + (getFileGrep().isContent() ? META_DELIMITER + sb.toString() : ""));
            listedFile = true;
        }
    }

    private void trace(String msg) {
        getFileGrep().trace(msg);
    }

    private void traceEntry(String entryPath, MP3 mp3) {
        StringBuilder sb = new StringBuilder();
        sb.append("MP3 entry [" + entryPath + "]:\n" +
            "codec..............: " + mp3.getCodec()                          + "\n"         +
            "bit rate...........: " + mp3.getBitrate()                        + " kbits/s\n" +
            "bit rate type......: " + mp3.getBitrateType()                    + "\n"         +
            "frequency..........: " + mp3.getFrequency()                      + " hz\n"      +
            "audio duration.....: " + mp3.getAudioDuration()                  + " s\n"       +
            "audio size.........: " + mp3.getAudioSize()                      + " bytes\n"   +
            "album..............: " + mp3.getAlbum()                          + "\n"         +
            "artist.............: " + mp3.getBand()                           + "\n"         +
            "contributing artist: " + mp3.getLeadPerformer()                  + "\n"         +
            "lyrics by..........: " + mp3.getLyricsBy()                       + "\n"         +
            "music by...........: " + mp3.getMusicBy()                        + "\n"         +
            "picture............: " + mp3.getPicture(PictureType.FRONT_COVER) + "\n"         +
            "publisher..........: " + mp3.getPublisher()                      + "\n"         +
            "rating.............: " + mp3.getRating()                         + "\n"         +
            "title..............: " + mp3.getTitle()                          + "\n"         +
            "track #............: " + mp3.getTrack()                          + "\n"         +
            "year recorded......: " + mp3.getYear()                           + "\n"         +
            "lyrics.............: " + mp3.getLyrics()                         + "\n");
        trace(sb.toString());
    }
}
