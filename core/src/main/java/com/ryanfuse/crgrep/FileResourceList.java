/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import com.ryanfuse.crgrep.util.ParameterUtils;


/**
 * List of all file resources to be grep'ed, represented as a wildcard pattern.
 * 
 * @author Craig Ryan
 */
public class FileResourceList implements ResourceList<String> {

	List<String> listPattern;
	int idx = 0;
	boolean wild = false;
	String first, last, path;

	public FileResourceList(String file) {
		this.listPattern = new ArrayList<String>();
		this.listPattern.add(ParameterUtils.stripQuotes(file));
	}

	public FileResourceList(List<String> fileList) {
		this.listPattern = ParameterUtils.stripQuotes(fileList);
	}

	public int size() {
		return listPattern.size();
	}

	@Override
	public String get(int index) {
		return listPattern.get(index);
	}

	@Override
	public boolean hasNext() {
		return idx < listPattern.size();
	}

	@Override
	public String next() {
		setIndicators();
		return listPattern.get(idx++);
	}

	@Override
	public void remove() {
		// no-op
	}

	private void setIndicators() {
		String currentResItem = listPattern.get(idx);
		if (currentResItem.equals("*") || currentResItem.equals(".") 
			|| currentResItem.equals("*.*") || currentResItem.equals("/")
			|| currentResItem.equals("*/*") || currentResItem.equals("*/*.*")) {
			wild = true;
		} else {
			wild = false;
		}

		// path, less name
		path = FilenameUtils.getFullPathNoEndSeparator(currentResItem);
		
		// File part - name and extent
		first = FilenameUtils.getBaseName(currentResItem);
		last = FilenameUtils.getExtension(currentResItem);
	}
	
	/**
	 * File name, less path and less extension. ie 'foo' from 'a/b/foo.txt'
	 */
	public String firstPart() {
		return first;
	}
	
	/**
	 * First part as a R.E.
	 */
	public String firstPartPattern() {
		return ResourcePattern.convertWildcardToRegex(first);
	}

    public ResourcePattern firstPartResourcePattern() {
        return new ResourcePattern(first);
    }
	
	/**
	 * File name extension, less path and less base name. ie 'txt' from 'a/b/foo.txt'
	 */
	public String lastPart() {
		return last;
	}

	/**
	 * Last part as a R.E.
	 */
	public String lastPartPattern() {
		return ResourcePattern.convertWildcardToRegex(last);
	}

	public ResourcePattern lastPartResourcePattern() {
	    return new ResourcePattern(last);
	}

	public boolean isWild() {
		return wild;
	}
	
	public String pathPart() {
		return path;
	}

	public boolean isFirstPartWild() {
		return wild ? true : empty(first) ? true : first.equals("*");
	}

	public boolean isLastPartWild() {
		return wild ? true : empty(last) ? true : last.equals("*");
	}

	public boolean isPathWild() {
		return wild ? true : empty(path) ? true : path.equals("*");
	}

	public boolean pathContainsWild() {
		return wild ? true : empty(path) ? false : (path.contains("*") || path.contains("?")) ;
	}

	public boolean firstPartContainsWild() {
		return wild ? true : empty(first) ? false : (first.contains("*") || first.contains("?")) ;
	}

	public boolean lastPartContainsWild() {
		return wild ? true : empty(last) ? false : (last.contains("*") || last.contains("?")) ;
	}
	
	// FilenameUtils returns "" and/or null, check for both. 
	private boolean empty(String val) {
		return val == null || val.isEmpty();
	}
}
