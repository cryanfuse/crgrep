/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/*
 * Reads lines of text whilst maintaining a specific number of lines
 * before and after the current line, as context 'around' the current line.
 * 
 * The actual character reading is delegated to the supplied BufferedReader. 
 */
public class ContextualReader {

	private static final int DEFAULT_CONTEXT_SIZE = 10;
	
	private BufferedReader reader;
	private Stack<String> preStack;
	private LinkedList<String> postStack;
	private int contextSize = DEFAULT_CONTEXT_SIZE;
	private String currentLine;

	public ContextualReader(BufferedReader reader) {
		this(reader, DEFAULT_CONTEXT_SIZE);
	}

	public ContextualReader(BufferedReader reader, int contextSize) {
		this.reader = reader;
		preStack = new Stack<String>();
		postStack = new LinkedList<String>();
	}
	
	/*
	 * Read the next line of text. A 'null' value indicates
	 * no data is left to read.
	 */
	public String readLine() throws IOException {
		// save current line
		if (currentLine != null) {
			preStack.push(currentLine);
		}
		// read ahead for more lines of text.
		if (postStack.isEmpty()) {
			topupPostStack();
		}
		if (!postStack.isEmpty()) {
			// add() to end, removeFirst() from front
			currentLine = postStack.removeFirst();
			topupPostStack();
		} else {
			currentLine = null;
		}
		// trim the number of old saved lines
		if (currentLine != null) {
			trimPreStack();
		}
		return currentLine;
	}

	public List<String> linesBefore() {
		if (!preStack.isEmpty()) {
			return Collections.list(preStack.elements());
		}
		return Collections.EMPTY_LIST;
	}

	public List<String> linesAfter() {
		return postStack;
	}
	
	private void trimPreStack() {
		while (preStack.size() > contextSize) {
			preStack.remove(0);
		}
	}

	private void topupPostStack() throws IOException {
		while (postStack.size() < contextSize) {
			String line = reader.readLine();
			if (line == null) {
				break;
			}
			postStack.add(line);
		}
	}

	public int getContextSize() {
		return contextSize;
	}

	public void setContextSize(int contextSize) {
		this.contextSize = contextSize;
	}
}
