/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.util.regex.Matcher;

import com.ryanfuse.crgrep.util.Switches;

/*
 * A class representing text matched by the grep search.
 */
public class ResultText {

    public static final String HIDDEN_TEXT_MARKER = "...";

    // Number of leading chars in a match before we include those in the
    // displayed text results
    public static final int DEFAULT_MIN_DATA_VALUE_PREFIX = 10;
    // Default max matched text length after which we trim the value displayed in
    // results
    public static final int DEFAULT_MAX_DATA_VALUE_LENGTH = 250;

    private int minPrefixIndex;
    private int maxResultLength;
    private boolean removeLineBreaks = Boolean.FALSE;
    private Switches switches;
    private ResourcePattern resourcePattern;

    public ResultText(ResourcePattern pattern, Switches switches) {
        setMinPrefixIndex(DEFAULT_MIN_DATA_VALUE_PREFIX);
        setMaxResultLength(switches.getMaxResultLength());
        this.switches = switches;
        this.resourcePattern = pattern;
    }

    public String result(String val) {
        return result(val, "", true);
    }

    public String result(String val, boolean applyTrimming) {
        return result(val, "", applyTrimming);
    }

    public String result(String val, String defaultVal) {
        return result(val, defaultVal, true);
    }

    /**
     * Get the result string from the supplied text.
     *
     * @param val
     *            the input text from which to construct a result string.
     * @param defaultVal
     *            if val is null, return this default value
     * @param applyTrimming
     *            used to avoid trimming text result displayed in specific cases
     *            such as when displaying resource paths.  
     * @return result string, possibly trimmed
     */
    public String result(String val, String defaultVal, boolean applyTrimming) {
        if (val == null) {
            return defaultVal;
        }
        String res = val;
        // Trim results, before highlighting so that we don't chop text which
        // might be wrapped with start and end highlighting.
        if (applyTrimming && (val.length() >= getMaxResultLength())) {
            res = trimmedValue(val);
        }
        // Turn line breaks into whitespace to keep text on a single line.
        if (removeLineBreaks) {
            res = filterLineBreaks(res);
        }
        // Highlight result using colour.
        if (switches.isHighlighting()) {
            res = highlightMatchingText(res);
        }
        return res;
    }

    private String highlightMatchingText(String text) {
        if (resourcePattern.isRegularExpressionWildcard()) {
            // skip highlighting if full wildcard matching
            return text;
        }
        Matcher pm = resourcePattern.getPattern().matcher(text);
        int lastEnd = 0;
        StringBuilder sb = new StringBuilder();
        while (pm.find()) {
            int s = pm.start(), e = pm.end();
            if (s > lastEnd) {
                sb.append(text.substring(lastEnd, s));
            }
            sb.append(switches.getHighlight().getStart())
                .append(text.substring(s, e))
                .append(switches.getHighlight().getEnd());
            lastEnd = e;
        }
        if (lastEnd < text.length()) {
            sb.append(text.substring(lastEnd));
        }
        return sb.toString();
    }

    /*
     * Process result string and limit the length of the string displayed.
     * 
     * The rules to determine trimming behaviour are: - if the string is short
     * then return it in full - if the string contains a pattern match then
     * return a subset of characters 'around' the match - if the string doesn't
     * contain a pattern match then return a subset starting from the beginning
     * of the text The characters '...' are used to identified text trimmed (not
     * displayed).
     */
    private String trimmedValue(String val) {
        String trimmedVal;
        if (resourcePattern != null) {
            Matcher pm = resourcePattern.getPattern().matcher(val);
            if (pm.find()) {
                trimmedVal = stringRange(pm.start(), pm.end(), val);
            } else {
                trimmedVal = stringRange(0, 0, val);
            }
        } else {
            trimmedVal = stringRange(0, 0, val);
        }
        return trimmedVal;
    }

    private String filterLineBreaks(String trimmedVal) {
        if (trimmedVal == null) {
            return null;
        }
        return trimmedVal.replaceAll("\\r\\n|\\r|\\n", "\\\\n");
    }

    /*
     * Extract a string 'around' the start:end index range.
     * 
     * @param start index into the string
     * 
     * @param val the string to be trimmed.
     * 
     * @return a subset of the string within the range
     */
    private String stringRange(int startx, int endx, String val) {
        int valLength = val.length();
        int maxl = getMaxResultLength();
        
        int start = startx - minPrefixIndex;
        if (start < 0) {
            start = 0;
        }
        int end = start + maxl;
        if (end > valLength) {
            end = valLength;
        } else {
            // try to right shift if chars are chopped off the matching text
            // ...s....sx...e........ex...   or
            // ...s....sx...ex....e.......
            int post = endx - end;    // # chopped
            int pre = startx - start; // # available to shift right
            if (post > 0 && pre > 0) {
                int shift = Math.min(pre, post);
                start += shift;
                end += shift;
            }
        }

        StringBuilder sb = new StringBuilder();
        if (start > 0) {
            sb.append(HIDDEN_TEXT_MARKER); // indicate leading chars not shown
        }
        sb.append(val.substring(start, end));
        if (end < valLength) {
            sb.append(HIDDEN_TEXT_MARKER); // indicate trailing chars not
            // displayed
        }
        return sb.toString();
    }

    /*
     * The aim is to show the matched text in the context of characters before
     * and after it, if possible. This setting determines how many characters
     * must exist before the match text before we include those characters in
     * the displayed text.
     */
    public int getMinPrefixIndex() {
        return minPrefixIndex;
    }

    public void setMinPrefixIndex(int minPrefixIndex) {
        if (minPrefixIndex < 0) {
            minPrefixIndex = 0;
        }
        this.minPrefixIndex = minPrefixIndex;
    }

    public int getMaxResultLength() {
        return maxResultLength;
    }

    // < 1 means unlimited. Null means use the default (50).
    public void setMaxResultLength(Integer maxIndex) {
        if (maxIndex == null) {
            this.maxResultLength = DEFAULT_MAX_DATA_VALUE_LENGTH;
            return;
        } else if (maxIndex < 1) {
            this.maxResultLength = Integer.MAX_VALUE;
            return;
        }
        this.maxResultLength = maxIndex;
    }

    /*
     * Determines if we want to display line breaks as '\n' instead of breaking
     * displayed text across multiple lines.
     */
    public boolean isRemoveLineBreaks() {
        return removeLineBreaks;
    }

    public void setRemoveLineBreaks(boolean stripLineBreaks) {
        this.removeLineBreaks = stripLineBreaks;
    }

    @Override
    public String toString() {
        return "[min:" + getMinPrefixIndex() + ", end:" + getMaxResultLength() + "] delLineBreaks?:" + removeLineBreaks;  
    }
}
