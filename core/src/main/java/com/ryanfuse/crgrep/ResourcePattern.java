/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.util.regex.Pattern;

import com.ryanfuse.crgrep.util.ParameterUtils;


/**
 * An interface for resource patterns.
 * 
 * Supports the conversion between RE and Database patterns.
 * 
 * @author Craig Ryan
 */
public class ResourcePattern {

	private String pattern; // the command line pattern (wildcard string)
	private String rePattern; 
	private String dbPattern;
	private boolean ignoreCase;

	public ResourcePattern(String searchPattern) {
		pattern = ParameterUtils.stripQuotes(searchPattern);
		// convert to DB wildcarded string
		dbPattern = pattern.replace('*', '%').replace('?', '_');
		// convert from wildcard to valid RE
		rePattern = convertWildcardToRegex(pattern);
	}

	public String getPatternString() {
		return pattern;
	}
	
	/**
	 * A compile regular expression for the command line, possibly wildcarded, search string.
	 * The -i option will also make the compiled pattern case-insensitive.
	 *  
	 * @return compiled RE
	 */
    public Pattern getPattern() {
        return getPatternCase(ignoreCase);
    }

    /**
     * Explicitly pass the ignoreCase choice, regardless of the -i setting.
     * 
     * @param ignoreCase
     * @return
     */
    public Pattern getPattern(boolean ignoreCase) {
        return getPatternCase(ignoreCase);
    }

    private Pattern getPatternCase(boolean ignoreCase) {
		if (ignoreCase) {
			return Pattern.compile(rePattern, Pattern.CASE_INSENSITIVE);
		}
		return Pattern.compile(rePattern);
	}

	/**
	 * Get the regular expression, converted from the command line args wildcard.
	 * The RE is not anchored to start/end of the line.
	 * 
	 * @return
	 */
	public String getRegularExpression() {
		return rePattern;
	}
	
	public String getDatabaseExpression() {
		return dbPattern;
	}

	public String getDatabaseWildcardExpression() {
		String wcPat = dbPattern;
		if (!dbPattern.startsWith("%")) {
			wcPat = "%" + dbPattern;
		}
		if (!dbPattern.endsWith("%")) {
			wcPat = wcPat + "%";
		}
		return wcPat;
	}

	public boolean isDatabaseExpressionWildcard() {
		return dbPattern.equals("%");
	}
	public boolean isRegularExpressionWildcard() {
		return rePattern.equals(".*");
	}
	public boolean databaseExpressionContainsWildcard() {
		return dbPattern.contains("%") || dbPattern.contains("_");
	}
	public boolean regularExpressionContainsWildcard() {
		return rePattern.contains(".") || rePattern.contains(".*");
	}
	
	public static String convertWildcardToRegex(String wildcard) {
		StringBuffer s = new StringBuffer( wildcard.length() );
		//s.append('^');
		for( int i = 0, is = wildcard.length(); i < is; i++ ) {
			char c = wildcard.charAt( i );
			switch (c) {
			case '*': s.append(".*");	break;
			case '?': s.append("."); break;
			case '(':
			case ')':
			case '[':
			case ']':
			case '$':
			case '^':
			case '.':
			case '{':
			case '}':
			case '|':
			case '\\':
				s.append("\\");
				s.append(c);
				break;
			default: s.append( c );	break;
			}
		}
		//s.append('$');
		return (s.toString());
	}

	public void setIgnoreCase(boolean ignoreCase) {
		this.ignoreCase = ignoreCase;
	}
}
