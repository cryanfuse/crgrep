/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.env.EnvResult;
import com.ryanfuse.crgrep.env.EnvStatus;

/**
 * Parser class to query and parse data from an H2 database.
 * 
 * @author Craig Ryan
 */
public class H2Parser extends DatabaseParser {
	
    protected static final String H2_DEFAULT_DRIVER = "org.h2.Driver";
    protected static final String H2_DEF_USER = "sa";
	protected static final String H2_DEF_PW = "";
	private static final String LOG_PREFIX = "H2";

	private static final String ALL_TABLES = "INFORMATION_SCHEMA.TABLES t, INFORMATION_SCHEMA.COLUMNS c WHERE c.TABLE_NAME = t.TABLE_NAME AND t.TABLE_TYPE = 'TABLE'";
	private static final Object GREPABLE_TYPES = "c.TYPE_NAME LIKE '%VARCHAR%' OR c.TYPE_NAME LIKE '%INT' "
	        + "OR c.TYPE_NAME = 'TIMESTAMP' OR c.TYPE_NAME = 'TIME' OR c.TYPE_NAME = 'DATE' "
	        + "OR c.TYPE_NAME = 'CHAR' OR c.TYPE_NAME = 'INTEGER' OR c.TYPE_NAME = 'DECIMAL' OR c.TYPE_NAME = 'REAL' OR c.TYPE_NAME = 'DOUBLE' OR c.TYPE_NAME = 'BOOLEAN' "
	        + "OR c.TYPE_NAME = 'UUID' OR c.TYPE_NAME = 'IDENTITY' ";

	private Connection conn = null;
	private String schemaName;
	Map<String, List<String[]>> tableColumnTypeMap = new HashMap<String, List<String[]>>();
			
	public H2Parser(String uri, String user, String password) {
		super(uri, user, password);
	}

	@Override
	public String getDefaultUser() {
		return H2_DEF_USER;
	}

	@Override
	public String getDefaultPassword() {
		return H2_DEF_PW;
	}

	public String getLogPrefix() {
		return LOG_PREFIX;
	}
	
	@Override
	public Connection dbConnection() throws Exception {
		if (conn != null && !conn.isClosed()) {
			return conn;
		}
		if (getDbDriver() == null) {
		    Class<?> driver = null;
	        String driverClassName = getDriverClass(DatabaseFactory.H2_DRIVER_KEY, H2_DEFAULT_DRIVER);
		    try {
                driver = Class.forName(driverClassName);
            } catch (ClassNotFoundException e) {
                EnvResult res = new EnvResult("H2 database grep", EnvStatus.ERROR);
                missingDriver(res, driverClassName, DatabaseFactory.H2_DRIVER_KEY, H2_DEFAULT_DRIVER, "h2-<version>.jar");
                throw new GrepException(res);
            }
			setDbDriver((Driver) driver.newInstance());
		}
		debug("H2 database connection, uri ["+getUri()+"] u ["+getUser()+"] p ["+getPassword()+"]");
		conn = openConnection();
		return conn;
	}

	public void dbClose() throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}
	
	/*
	 * A map with key = table name and values a list of columns for that table.
	 * 
	 * @see com.ryanfuse.crgrep.db.DatabaseParser#getFullTableMap()
	 */
	@Override
	public Map<String, List<String>> getFullTableMap() throws Exception {
		StringBuilder allColsQuery = new StringBuilder("select t.TABLE_NAME, c.COLUMN_NAME from ");
		allColsQuery.append(ALL_TABLES);
		if (schemaName != null) {
			allColsQuery.append(" AND t.TABLE_SCHEMA = '").append(schemaName).append("'");
		}

		/*
		 * Table/column syntax:
		 *    table.col  	exact column
		 *    .col 			all tables with specific column
		 *    table 		all columns from table
		 *    table*.*col? - limited wildcarded table name(s) and column(s)
		 */
		String tabFilter = dbResourceListFilter();
		if (tabFilter != null) {
			allColsQuery.append(" AND ").append(tabFilter);
		}
		String allCols = allColsQuery.toString();
		debug("DB TABLE COLUMNS QUERY: " + allCols);
		ResultSet rs = queryDb(dbConnection(), allCols);
		Map<String, List<String>> tableMap = mapTableColumns(rs);
		return tableMap;
	}

	/*
	 * Subset of a tables columns which match required data types. Allows the grep 
	 * to search on text or date data etc and ignore binary values.
	 * 
	 * @see com.ryanfuse.crgrep.db.DatabaseParser#getColumnSubset(java.lang.String)
	 */
	@Override
	public List<String> getColumnSubset(String table) throws Exception {
		StringBuilder colsQuery = new StringBuilder("select c.COLUMN_NAME from ");
		colsQuery.append(ALL_TABLES).append(" AND t.TABLE_NAME = '" + table + "'");
		if (schemaName != null) {
			colsQuery.append(" AND t.TABLE_SCHEMA = '").append(schemaName).append("'");
		}
		colsQuery.append(" AND (").append(GREPABLE_TYPES).append(") ") ;
		debug("DB CONTENT SUBSET QUERY> " + colsQuery.toString());
		ResultSet rs = queryDb(dbConnection(), colsQuery.toString()); // non numeric/blob columns
		return listColumns(rs);
	}

	/*
	 * All rows whose data matches the regular expression, returned as a list of row data.
	 * 
	 * @see com.ryanfuse.crgrep.db.DatabaseParser#getMatchingColumns(java.util.List, java.lang.String, com.ryanfuse.crgrep.ResourcePattern)
	 */
	@Override
	public List<List<String>> getMatchingColumns(List<String> columnList, String table, ResourcePattern pattern) throws Exception {
		String dbPattern = pattern.getDatabaseWildcardExpression();
		String colCondition = buildColumnWhere(dbPattern, columnList);
		if (colCondition == null || colCondition.isEmpty()) {
			// no columns of the required type
			return null;
		}
		StringBuilder dataQuery = new StringBuilder();
		String colSelect = buildColumnSelect(columnList);
		dataQuery.append("SELECT ").append(colSelect).append(" from ").append(table);
		dataQuery.append(colCondition);
		debug("DB CONTENT QUERY> " + dataQuery.toString());
		ResultSet rs = queryDb(dbConnection(), dataQuery.toString());
		return resultSetToList(rs);
	}
	
	/*
	 * Build a map keyed by table name, and all columns in the result set (value=list of column names)
	 */
	private Map<String, List<String>> mapTableColumns(ResultSet rs) throws SQLException {
		Map<String, List<String>> tabs = new HashMap<String, List<String>>();
		while (rs.next()) {
			String tab = rs.getString("TABLE_NAME");
			String col = rs.getString("COLUMN_NAME");
			if (!tabs.containsKey(tab)) {
				debug("Add table [" + tab + "] to map.");
				tabs.put(tab, new ArrayList<String>());
			}
			debug("Map table [" + tab + "] column [" + col + "]");
			tabs.get(tab).add(col);
		}
		rs.close();
		return tabs;
	}

	/*
	 * Build a list of all columns in the result set
	 */
	private List<String> listColumns(ResultSet rs) throws SQLException {
		List<String> cols = new ArrayList<String>();
		while (rs.next()) {
			String col = rs.getString("COLUMN_NAME");
			if (!cols.contains(col)) {
				cols.add(col);
			}
		}
		rs.close();
		return cols;
	}
	
	/*
	 * Filter of tables and or columns based on limited RE.
	 *  table.col  		exact column
	 *  .col 			all tables with specific column
	 *  table 			all columns from table
	 *  table*.*col? 	limited wildcarded table name(s) and column(s)
	 */
	private String dbResourceListFilter() {
		String tab = null;
		if (getResourceList() == null) {
		    return null;
		}
		debug("Resource list: " + getResourceList().get(0));
		if (!getResourceList().isWild()) {
			if (!getResourceList().isFirstPartWild()) {
				tab = getResourceList().firstPartPattern();
			}
		}
		StringBuilder q = new StringBuilder();
		if (tab != null) {
			if (getResourceList().firstPartContainsWild()) {
				q.append("t.TABLE_NAME LIKE '").append(tab).append("'");
			} else {
				q.append("t.TABLE_NAME = '").append(tab).append("'");
			}
		}
		debug("TABLE LIST FILTER: " + q.toString());
		return q.length() == 0 ? null : q.toString();
	}

	/**
	 * How to represent a column name in a WHERE clause considering the current case sensitivity
	 * and DB's identifier quoting convention.
	 */
	@Override
	public String columnIdentifier(String col, boolean ignoreCase, boolean isSelect) {
		StringBuilder q = new StringBuilder();
        if (ignoreCase) {
			q.append(" UPPER(");
		}
		q.append(IDENTIFIER_DOUBLE_QUOTE).append(col).append(IDENTIFIER_DOUBLE_QUOTE);
		if (ignoreCase) {
			q.append(") ");
		}
		return q.toString();
	}
}
