/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.IteratorUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.rest.graphdb.RestGraphDatabase;
import org.neo4j.rest.graphdb.entity.RestNode;
import org.neo4j.rest.graphdb.query.RestCypherQueryEngine;
import org.neo4j.rest.graphdb.util.QueryResult;

import com.ryanfuse.crgrep.GraphGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceList;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.util.Display;
import com.ryanfuse.crgrep.util.GrepMatcher;

/**
 * Parser class to query and parse data from a Neo4J graph database.
 * 
 * This parser uses a java wrapper for the Neo4J REST API so that 
 * calls are made to the remote server.
 * 
 * @author Craig Ryan
 */
public class NeoGraphParser {
	
	protected static final String NEO_DEF_USER = null;
	protected static final String NEO_DEF_PW = null;
	private static final String LOG_PREFIX = "Neo4j";
    private static final String REST_CONTEXT = "db/data";

    protected String uri;
    protected String user;
    protected String password;
    private ResourceList<String> resourceList;
    private boolean ignoreCase;
    private boolean verbose;
    private Display display;
    
    private RestCypherQueryEngine engine;
    private GraphDatabaseService graphdb;
    private boolean allContent;
    private GraphGrep graphGrep;
    
	public NeoGraphParser(GraphGrep graphGrep, String uri, String user, String password) {
        this.graphGrep = graphGrep;
        this.uri = uri;
        this.user = user == null ? getDefaultUser() : user;
        this.password = password == null ? getDefaultPassword() : password;
	}

	/*
	 * Ensure we can connect to the database, either with the supplied URI or 
	 * modified to use the default RESTful URI.
	 */
    public boolean openConnection() {
        if (uri == null) {
            return false;
        }
        if (graphdb == null) {
            resetEngine();
        }
        if (pingDatabase()) {
            registerShutdownHook(graphdb);
            return true;
        }
        shutdownGraph();
        // 2nd attempt, try ensure a RESTful URI was specified and not just
        // the database browser URI.
        if (!uri.endsWith(REST_CONTEXT)) {
            if (uri.endsWith("/")) {
                uri += REST_CONTEXT;
            } else {
                uri += "/" + REST_CONTEXT;
            }
        }
        resetEngine();
        if (pingDatabase()) {
            registerShutdownHook(graphdb);
            return true;
        }
        return false;
    }

    /*
     * Initialise the RESTful wrapper and query engine.
     */
    private void resetEngine() {
    	debug("Graph database reset for URI [" + uri + "] user [" + user + "]");
        graphdb = user == null ? new RestGraphDatabase(uri) : new RestGraphDatabase(uri, user, password);
        engine = new RestCypherQueryEngine(((RestGraphDatabase)graphdb).getRestAPI());
    }
    
    /*
     * Ping ensures the DB is reachable and can be queried.
     */
    private boolean pingDatabase() {
        if (graphdb == null || !graphdb.isAvailable(10)) {
            return false;
        }
        try {
            engine.query("MATCH (n) RETURN n LIMIT 0", null);
        } catch (Exception e) {
            debug("Graph database ping operation failed: " + e.getLocalizedMessage());
            return false; 
        }
        return true;
    }

	/**
	 * The map contains all nodes matched by label, and the complete properties list for those nodes.
	 */
	public Map<String, List<Map.Entry<String, List<String>>>> getMatchingNodeMap() throws Exception {
        Collection<String> labels = ((RestGraphDatabase)graphdb).getAllLabelNames();
        Transaction tx = graphdb.beginTx();
        Map<String, List<Map.Entry<String, List<String>>>> nodeMap = new HashMap<String, List<Map.Entry<String, List<String>>>>();
        try {
			for (String label : labels) {
			    // Does the label match?
				if (!resourceNameMatch(label)) {
					continue;
				}
			    debug("Node label '" + label + "' included in search for matching pattern [" + resourceList.firstPart() + "]");
			    
			    // Get the properties for the matched <label>
			    Iterable<RestNode> nodeIterable = ((RestGraphDatabase)graphdb).getRestAPI().getNodesByLabel(label);
			    Iterator<RestNode> nodeIter = nodeIterable.iterator();
			    while (nodeIter.hasNext()) {
			        RestNode node = nodeIter.next();
			        Iterable<String> props = node.getPropertyKeys();
	                List<String> matchedProps = IteratorUtils.toList(props.iterator());
	                
	                mapNodeOrRelationshipEntry(node.getId(), "node", label, matchedProps, nodeMap);
			    }
			    if (isDebugOn()) {
			        List<Entry<String, List<String>>> l = nodeMap.get(label);
			        debug("Number of nodes mapped for label '" + label + "' is " + (l==null?0:l.size()));
			    }
			}
			tx.success();
        } finally {
        	tx.close();
        }
		return nodeMap;
	}

	/**
	 * The map contains all relationships matched by name, and the complete properties list for those relationships.
	 */
	public Map<String, List<Map.Entry<String, List<String>>>> getMatchingRelationshipMap() throws Exception {
        Map<String, List<Map.Entry<String, List<String>>>> relMap = new HashMap<String, List<Map.Entry<String, List<String>>>>();
        Transaction tx = graphdb.beginTx();
        try {
        	QueryResult<Map<String, Object>> allRels = engine.query("MATCH ()-[r]-() RETURN DISTINCT r", null);
        	for (Map<String, Object> allRelMap : emptyIfNull(allRels)) {
        		for (String relKey : allRelMap.keySet()) { // just one key 'r'
        			Relationship rship = (Relationship) allRelMap.get(relKey);
        			String relTypeName = rship.getType().name(); // eg 'IS_A' or 'LIKES'
        			if (!resourceNameMatch(relTypeName)) {
        				continue;
        			}
        			debug("Relationship name '" + relTypeName + "' included in search for matching pattern [" + resourceList.firstPart() + "]");
        			Iterable<String> relPropKeys = rship.getPropertyKeys();
                    List<String> matchedProps = IteratorUtils.toList(relPropKeys.iterator());
                    
                    mapNodeOrRelationshipEntry(rship.getId(), "relationship", relTypeName, matchedProps, relMap);
    			    if (isDebugOn()) {
    			        List<Entry<String, List<String>>> l = relMap.get(relTypeName);
    			        debug("Number of relationships mapped for name '" + relTypeName + "' is " + (l==null?0:l.size()));
    			    }
        		}
        	}
        	tx.success();
        } finally {
		    tx.close();
		}
		return relMap;
	}

	// Node IDs joined by this relationship
	public long[] getRelationshipNodeIds(String relId) {
        Transaction tx = graphdb.beginTx();
        long nodes[] = new long[2];
        nodes[0] = -1;
        nodes[1] = -1;
        Map<String,Object> params = new HashMap<String, Object>(); 
    	try {
			params.put("id", Integer.parseInt(relId));
		} catch (NumberFormatException e) {
	    	warning("Find nodes for relationship query failed, ID ["+relId+"] is not numeric.");
			return nodes;
		} 
        String byId = "START r=relationship({id}) return r as r";
        debug("Relationship content match query: " + byId + "  (id:"+relId+")");
        QueryResult<Map<String, Object>> result = null;
        try {
        	result = engine.query(byId, params);
         	tx.success();
        } finally {
        	tx.close();
        }
        for (Map<String, Object> allRelMap : emptyIfNull(result)) {
        	Relationship rship = (Relationship) allRelMap.get("r");
        	if (rship != null) {
        		Node sn = rship.getStartNode();
        		nodes[0] = sn == null ? -1 : sn.getId();
        		Node en = rship.getEndNode();
        		nodes[1] = en == null ? -1 : en.getId();
        	}
        }
        return nodes;
	}
	
	private void mapNodeOrRelationshipEntry(long id, String entryType, String entryName, List<String> matchedProps,
			Map<String, List<Entry<String, List<String>>>> entryMap) {
		String relId = String.valueOf(id);
        List<Map.Entry<String, List<String>>> entryList = entryMap.get(entryName);
        if (entryList == null) {
            debug("no " + entryType + " map entry yet for " + entryName);
            entryList = new ArrayList<Map.Entry<String, List<String>>>();
            entryMap.put(entryName, entryList);
        }
        if (!matchedProps.isEmpty()) {
		    if (isDebugOn()) {
		    	debug("Properties for " + entryType + " with id: " + relId + " count: " + matchedProps.size());
		    	debug("  Props: " + matchedProps.toString());
		    }
            // map entry node/relationship ID -> list of node/relationship properties
            Map.Entry<String, List<String>> entry = new AbstractMap.SimpleEntry(relId, matchedProps);
            entryList.add(entry);
        } else {
            debug("Empty Properties for " + entryType + " with id: " + relId);
            // Node/relationship has no properties, map only node/relationship ID
            entryList.add(new AbstractMap.SimpleEntry(relId, new ArrayList<String>()));
        }
	}

	boolean resourceNameMatch(String labelOrName) {
	    if (!resourceList.isFirstPartWild()) {
	        if (resourceList.firstPartContainsWild()) {
                GrepMatcher pm = GrepMatcher.getMatcher(
                        labelOrName, resourceList.firstPartResourcePattern(), graphGrep.getSwitches(), true);
	            if (!pm.matches()) {
	            	trace("Label '" + labelOrName + "' does not match first part pattern [" + resourceList.firstPartResourcePattern() + "]");
	        		return false;
	            }
	        } else {
	            if (!GrepMatcher.matchingStrings(labelOrName, resourceList.firstPart(), graphGrep.getSwitches(), true)) {
	            	trace("Label '" + labelOrName + "' does not match first part [" + resourceList.firstPart() + "]");
	        		return false;
	            }
	        }
	    }
		return true;
	}

    public Map<String, Object> nodePropertyResult(String nodeId, String propName) {
    	Transaction tx = graphdb.beginTx();
    	Map<String,Object> params = new HashMap<String, Object>(); 
    	try {
			params.put("id", Integer.parseInt(nodeId));
		} catch (NumberFormatException e) {
	    	warning("Node content match query failed, ID ["+nodeId+"] is not numeric.");
			return null;
		} 
    	String byId = "START n=node({id}) return n." + propName + " as val";
    	debug("Node content match query: " + byId + "  (id:"+nodeId+")");
    	QueryResult<Map<String, Object>> result = null;
        try {
        	result = engine.query(byId, params);
         	tx.success();
        } finally {
        	tx.close();
        }
    	for (Map<String, Object> row : emptyIfNull(result)) { // only one row will be returned
    		return row;
    	}
		return null;
    }

    public Map<String, Object> relationshipPropertyResult(String relId, String propName) {
    	Transaction tx = graphdb.beginTx();
    	Map<String,Object> params = new HashMap<String, Object>(); 
    	try {
			params.put("id", Integer.parseInt(relId));
		} catch (NumberFormatException e) {
	    	warning("Relationship content match query failed, ID ["+relId+"] is not numeric.");
			return null;
		} 
    	String byId = "START r=relationship({id}) return r." + propName + " as val";
    	debug("Relationship content match query: " + byId + "  (id:"+relId+")");
    	QueryResult<Map<String, Object>> result = null; 
        try {
        	result = engine.query(byId, params);
        	tx.success();
        } finally {
        	tx.close();
        }
    	for (Map<String, Object> row : emptyIfNull(result)) { // only one row will be returned
    		return row;
    	}
    	return null;
    }
        
	/**
	 * Scan node/relationship properties and attempt to match by value.
	 * 
	 * A property is either a primitive type or an array of primitive types.
	 * 
	 * @param prop the property name
	 * @param result result of query for property value 
	 * @param match buffer carrying result of the match
	 * @param pattern the pattern to match value against
	 * @return true if a property value match was found
	 */
    public boolean matchesPropertyValue(String prop, Map<String, Object> result, StringBuilder match, ResourcePattern pattern) {
        boolean hit = false;
        Object propVal = result.get("val");
        if (propVal == null) {
        	match.append(":null");
        } else {
        	StringBuilder valueMatch = new StringBuilder(":");
        	if (propVal instanceof List) {
        		hit = matchArray(((List)propVal).toArray(), valueMatch, pattern);
        	} else {
        		hit = matchPrimitive(propVal, valueMatch, pattern);
        	}
        	if (hit) {
        		if (isTraceOn()) {
        			trace("property match for {" + prop + valueMatch.toString() + "}");
        		}
        		match.append(valueMatch.toString());
        	}
        }
        debug("  --> hit?: " + hit);
        return hit;
    }

    // Empty list to wrap null result
    private static <T> Iterable<T> emptyIfNull(Iterable<T> iterable) {
        return iterable == null ? Collections.<T>emptyList() : iterable;
    }

    /*
     * Property is an array, match each primitive array element by value
     */
    private boolean matchArray(Object[] propVal, StringBuilder valueMatch, ResourcePattern pattern) {
        boolean hit = false;
        boolean first = true;
        if (propVal != null && propVal.length > 0) {
            trace("Array property value, length: " + propVal.length);
            valueMatch.append("[");
            for (Object prop : propVal) {
                if (!first) {
                    valueMatch.append(",");
                }
                if (matchPrimitive(prop, valueMatch, pattern)) {
                    hit = true;
                }
                first = false;
            }
            valueMatch.append("]");
        }
        return hit;
    }
    
    /*
     * Match a property primitive value. The value is either a primitive property value or
     * for an array property, the primitive value of a single array element.
     */
    private boolean matchPrimitive(Object propVal, StringBuilder primValueMatch, ResourcePattern pattern) {
        boolean hit = false;
        if ((propVal instanceof String)
            || (propVal instanceof Character)) {
            if (matchString(propVal.getClass().getName(), true, String.valueOf(propVal), primValueMatch, pattern)) {
                hit = true;
            }
        }
        else if ((propVal instanceof Boolean)
            || (propVal instanceof Short)
            || (propVal instanceof Integer)
            || (propVal instanceof Long)
            || (propVal instanceof Float
            || (propVal instanceof Double))) {
            if (matchString(propVal.getClass().getName(), false, String.valueOf(propVal), primValueMatch, pattern)) {
                hit = true;
            }
        } else if (propVal instanceof Byte) {
            if (allContent) {
                if (matchString("byte", false, String.valueOf((Byte)propVal), primValueMatch, pattern)) {
                    hit = true;
                }
            }
        }
        return hit;
    }

    /*
     * Match property primitive value after conversion to String value.
     */
    private boolean matchString(String propType, boolean quoteValue, String propVal, StringBuilder stringMatch, ResourcePattern pattern) {
        if (propVal == null || propVal.isEmpty()) {
            //match.append(":\"\"");
        } else {
            GrepMatcher pm = GrepMatcher.getMatcher(propVal, pattern, graphGrep.getSwitches(), ignoreCase);
            if (pm.matches()) {
                if (quoteValue) {
                    stringMatch.append("\"");
                }
                stringMatch.append(pm.toString());
                if (quoteValue) {
                    stringMatch.append("\"");
                }
                return true;
            }
        }
        return false;
    }

    /*
     * Ensures clean db shutdown.
     */
    private void registerShutdownHook(final GraphDatabaseService graphDb) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }    
    
    public String getUri() {
        return uri;
    }
    
    public String getUser() {
        return user;
    }
   
    public String getPassword() {
        return password;
    }

    public String getDefaultUser() {
        return NEO_DEF_USER;
    }

    public String getDefaultPassword() {
        return NEO_DEF_PW;
    }

    public String getLogPrefix() {
        return LOG_PREFIX;
    }

    public ResourceList<String> getResourceList() {
        return resourceList;
    }

    public void setResourceList(ResourceList<String> resourceList) {
        this.resourceList = resourceList;
    }
    
    public void cleanup() throws GrepException {
        try {
            shutdownGraph();
        } catch (Exception e) {
            throw new GrepException("Neo4J database grep: failed to shutdown graph database: " + e.getLocalizedMessage());
        } 
    }
    
    public void trace(String msg) {
        if (display != null) {
            display.trace(msg);
        }
    }

    public void debug(String msg) {
        if (display != null) {
            display.debug(msg);
        }
    }

    public boolean isDebugOn() {
        if (display != null) {
            return display.isDebugOn();
        }
        return false;
    }

    public boolean isTraceOn() {
        if (display != null) {
            return display.isTraceOn();
        }
        return false;
    }

    public void warning(String msg) {
        if (display != null) {
            display.warn(msg);
        }
    }

    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public Display getDisplay() {
        return display;
    }

    public void setDisplay(Display display) {
        this.display = display;
        if (display != null) {
            display.setLogPrefix(getLogPrefix());
        }
    }
    
    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }

    public boolean isIgnoreCase() {
        return this.ignoreCase;
    }

    public void setEngine(RestCypherQueryEngine restCypherQueryEngine) {
        engine = restCypherQueryEngine;
    }

    public void setGraphDatabase(GraphDatabaseService restGraphDatabase) {
        graphdb = restGraphDatabase;
    }

    public void shutdownGraph() {
        try {
            if ( graphdb != null ) {
                graphdb.shutdown();
            }
        } finally {
            graphdb = null;
        }
    }

    public void setAllContent(boolean allContent) {
        this.allContent = allContent;
    }
}
