/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.ryanfuse.crgrep.ResourceList;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.env.EnvResult;
import com.ryanfuse.crgrep.env.EnvStatus;
import com.ryanfuse.crgrep.util.Display;

/**
 * Base class to be extended by all Database table parsers.
 *
 * See http://wiki.ispirer.com/sqlways/sybase/identifiers for 
 * quote and case rules for various RDBs.
 * 
 * An outline of MySQL case rules is here
 *      http://dev.mysql.com/doc/refman/5.0/en/identifier-case-sensitivity.html
 *      
 * Useful list of JDBC driver info:
 *      http://www.java2s.com/Tutorial/Java/0340__Database/AListofJDBCDriversconnectionstringdrivername.htm
 * 
 * @author Craig Ryan
 */
public abstract class DatabaseParser {

	// Character that may be used by DBs to quote column, table names
	public static final String IDENTIFIER_DOUBLE_QUOTE = "\"";   // default
	public static final String IDENTIFIER_SINGLE_QUOTE = "'";
	public static final String IDENTIFIER_BACK_QUOTE = "`";
	public static final String IDENTIFIER_OPEN_BRACKET = "[";
	public static final String IDENTIFIER_CLOSE_BRACKET = "]";
	
	protected String uri;
	protected String user;
	protected String password;
	private boolean verbose;
	private Display display;
	private DatabaseDriverManager databaseDriverManager;
	private Driver dbDriver;
	private String dbDriverClass;
	
	private ResourceList<String> resourceList;
	private Statement stmt;
	private boolean ignoreCase;
	
	public DatabaseParser(String uri, String user, String password) {
		this.uri = uri;
		if (user == null) {
			user = getDefaultUser();
		}
		if (password == null) {
			password = getDefaultPassword();
		}
		this.user = user;
		this.password = password;
		this.databaseDriverManager = new DatabaseDriverManager();
	}

	/*
	 * Get the JDBC driver class by system property lookup.
     * If not already set, use the defaultClass provided.
	 */
	public String getDriverClass(String key, String defaultClass) {
	    if (dbDriverClass != null) {
            return dbDriverClass;
	    }
        dbDriverClass = System.getProperty(key);
        if (dbDriverClass == null) {
            dbDriverClass = defaultClass;
        }
        return dbDriverClass;
	}

	   // Capture error condition for a missing driver lookup error.
    protected void missingDriver(EnvResult res, String driverClass, String driverClassKey, String driverDefaultClassValue, String driverJarName) {
        StringBuilder sb = new StringBuilder();
        sb.append("  The JDBC driver '").append(driverClass).append("' was not found on the java classpath.\n");
        sb.append("    Suggestion: ensure the JAR file '").append(driverJarName).append("' exists in the CRGREP_HOME/lib directory\n");
        res.getErrors().add(sb.toString());
        if (driverClass.equals(driverDefaultClassValue)) {
            res.setStatus(EnvStatus.ERROR);
        } else {
            res.setStatus(EnvStatus.ERROR_WARN);
            sb = new StringBuilder();
            sb.append("  The driver does not match the default driver class name ('").append(driverDefaultClassValue).append("')\n");
            sb.append("    Hint: was the driver class wrongly specified with '-D").append(driverClassKey);
            sb.append("=").append(driverClass).append("'?\n");
            res.getWarnings().add(sb.toString());
        }
    }


    /**
     * Open a connection to the database.
     * 
     * @return a Connection
     * @throws SQLException failed to connect the db server.
     */
    public Connection openConnection() throws SQLException {
        return databaseDriverManager.getConnection(uri, user, password);
    }

    /**
     * Open a connection to the database.
     * 
     * @return a Connection
     * @param props list of properties passed to the db driver (user and password are added if not
     * already specified).
     * @throws SQLException failed to connect the db server.
     */
    public Connection openConnection(Properties props) throws SQLException {
        if (!props.contains("user")) {
            props.put("user", user);
        }
        if (!props.contains("password")) {
            props.put("password", password);
        }
        return databaseDriverManager.getConnection(uri, props);
    }

	public ResultSet queryDb(Connection conn, String query) throws Exception {
		if (stmt != null) {
			stmt.close();
		}
		stmt = conn.createStatement();
		try {
            stmt.setQueryTimeout(30);
        } catch (java.sql.SQLFeatureNotSupportedException e) {
            // any driver doesn't support timeouts.. just ignore
            debug("SQLFeatureNotSupportedException for JDBC setQueryTimeout, ignored.");
        } catch (SQLException se) {
            // any driver doesn't support timeouts.. just ignore
            debug("SQLException for JDBC setQueryTimeout, ignored.");
        }
		return stmt.executeQuery(query);
	}
	
	/**
	 * The JDBC URI for the DB
	 * 
	 * @return
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * DB user specified on command line
	 * @return
	 */
	public String getUser() {
		return user;
	}

	/**
	 * The DB password specified on the command line 
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Default user typically used for this DB vendor if none is specified.
	 * 
	 * @return
	 */
	public abstract String getDefaultUser();
	
	/**
	 * Default password typically used for this DB vendor if none is specified.
	 * @return
	 */
	public abstract String getDefaultPassword();
	
	/**
	 * Logger prefix to identify the parser implementation class 
	 * 
	 * @return
	 */
	public abstract String getLogPrefix();

	/**
	 * Open a connection to this vendors DB
	 * 
	 * @return a connection to the db
	 * @throws Exception unable to connect to the database (server not running?)
	 */
	public abstract Connection dbConnection() throws Exception;

	/**
	 * Close the db
	 * 
	 * @throws Exception
	 */
	public abstract void dbClose() throws Exception;
	
	/**
	 * Return a map of all tables in the specified database. For each, map a list of columns.
	 * @return
	 * @throws Exception
	 */
	public abstract Map<String, List<String>> getFullTableMap() throws Exception;
	
	/**
	 * Generate a map of all columns for the given table which are grep'able (non-binary columns).
	 * 
	 * @param table the table name
	 * @return list of table column names which are grep'able
	 * @throws Exception db error
	 */
	public abstract List<String> getColumnSubset(String table) throws Exception;
	
	/**
	 * Returns the column data matching the pattern
	 *  
	 * @param columnList list of column names to match against the pattern
	 * @param table the table name with the given columns
	 * @param pattern the pattern used to match columns
	 * @return A list representing all matched rows, where each entry is a list of matching column data
	 * @throws Exception
	 */
	public abstract List<List<String>> getMatchingColumns(List<String> columnList, String table, ResourcePattern pattern) throws Exception;
	
	/**
	 * How to represent a column name in a SELECT or WHERE clause considering the current case sensitivity
	 * and DB's identifier quoting convention.
	 * 
	 * If case insensitive search (-i) is active the convention is to return a column identifier
	 * which specifies UPPERCASE data.
	 *
	 * See http://wiki.ispirer.com/sqlways/sybase/identifiers for details on 
	 * how to quote column names for various DB vendors. 
	 *
	 * @param col the column name
	 * @param ignoreCase determines if the identifier should ignore case.
	 * @param isSelect true if the identifier immediately follows SELECT, false if 
	 *        part of FROM or WHERE. Some implementations require different quoted
	 *        depending on identifier position in the query.
	 * @return the column identifier to use in a SELECT or WHERE clause ('LIKE' or '=')
	 */
	public abstract String columnIdentifier(String col, boolean ignoreCase, boolean isSelect);

	/*
	 * Convert ResultSet of column data to a List where each entry is a row of data values. 
	 */
	protected List<List<String>> resultSetToList(ResultSet rs) {
		List<List<String>> l = new ArrayList<List<String>>();
		try {
			int size = rs.getMetaData().getColumnCount();
			while (rs.next()) {
				List<String> ls = new ArrayList<String>(size);
				for (int i = 1; i <= size; i++) {
					ls.add(rs.getString(i));
				}
				l.add(ls);
			}
			rs.close();
		} catch (Exception e) {
			if (display != null) {
				display.error("Failed to read ResultSet data, error: " + e.getLocalizedMessage());
			}
		}
		return l;
	}

    /*
     * Build a SQL query for columns which match the DB pattern.
     * Basic format "select .. from <t> where c1 like '%pat%' or c2 like..."
     * This method generates the 'where..' part of the query.
     */
    protected String buildColumnWhere(String dbPattern, List<String> cols) {
        if (cols == null || cols.isEmpty()) {
            return null;
        }
        boolean first = true;
        boolean re = dbPattern.contains("%") || dbPattern.contains("_");
        StringBuilder q = new StringBuilder();
        for (String col : cols) {
            if (first) {
                q.append(" WHERE ");
            } else {
                q.append(" OR ");
            }
            q.append(columnIdentifier(col, isIgnoreCase(), false));
            if (re) {
                q.append(" LIKE '");
            } else {
                // never likely to get here since parsers should pass in '%pattern%'
                q.append(" = '");
            }
            // Requires implementations to return upper case IDs from the columnIdentifier() call above.
            q.append(isIgnoreCase() ? dbPattern.toUpperCase() : dbPattern);
            q.append("'");
            first = false;
        }
        return q.toString();
    }

    /*
     * Build a SQL SELECT list for the given columns
     */
    protected String buildColumnSelect(List<String> cols) {
        if (cols == null || cols.isEmpty()) {
            return "*";
        }
        boolean first = true;
        StringBuilder q = new StringBuilder();
        for (String col : cols) {
            if (!first) {
                q.append(", ");
            }
            q.append(columnIdentifier(col, false, true)); // don't upper case the resulting data
            first = false;
        }
        return q.toString();
    }

	public void cleanup() throws Exception {
		dbClose(); 
	}

	public ResourceList<String> getResourceList() {
		return resourceList;
	}

	public void setResourceList(ResourceList<String> resourceList) {
		this.resourceList = resourceList;
	}
	
	public void trace(String msg) {
		if (display != null) {
			display.trace(msg);
		}
	}

	public void debug(String msg) {
		if (display != null) {
			display.debug(msg);
		}
	}

	public void warning(String msg) {
		if (display != null) {
			display.warn(msg);
		}
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	public Display getDisplay() {
		return display;
	}

	public void setDisplay(Display display) {
		this.display = display;
		if (display != null) {
			display.setLogPrefix(getLogPrefix());
		}
	}

	public DatabaseDriverManager getDatabaseDriverManager() {
		return databaseDriverManager;
	}

	public void setDatabaseDriverManager(DatabaseDriverManager databaseDriverManager) {
		this.databaseDriverManager = databaseDriverManager;
	}
	
	/**
	 * @return the dbDriver
	 */
	public Driver getDbDriver() {
		return dbDriver;
	}

	/**
	 * @param dbDriver the dbDriver to set
	 */
	public void setDbDriver(Driver dbDriver) {
		this.dbDriver = dbDriver;
	}

	public void setIgnoreCase(boolean ignoreCase) {
		this.ignoreCase = ignoreCase;
	}

	public boolean isIgnoreCase() {
		return this.ignoreCase;
	}
}