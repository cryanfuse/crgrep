/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * This class exists purely to make testing easier by providing a non-static method which 
 * delegates to the standard DriverManager static method.
 * 
 * The issue avoided by doing this is that use of PowerMock (to mock statics) + Jacoco coverage 
 * do not work together. Any tests using powermock are excluded from coverage results so this 
 * is one approach to testing solely using Mockito.
 *
 * @author 'Craig Ryan'
 */
public class DatabaseDriverManager {
    public Connection getConnection(String uri, String user, String password) throws SQLException {
        return DriverManager.getConnection(uri, user, password);
    }

    public Connection getConnection(String uri, Properties props) throws SQLException {
        return DriverManager.getConnection(uri, props);
    }
}
