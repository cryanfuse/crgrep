/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverPropertyInfo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.env.EnvResult;
import com.ryanfuse.crgrep.env.EnvStatus;

/**
 * Parser class to query and parse data from a PostgreSQL database.
 * 
 * Defaults: user/pw: postgres/password
 * JDBC: jdbc:postgresql://localhost:5432/mydb[?searchpath=myschema]
 * JDBC driver: org.postgresql.Driver
 * 
 * @author Craig Ryan
 */
public class PostgresqlParser extends DatabaseParser {
	
    protected static final String POSTGRES_DEFAULT_DRIVER = "org.postgresql.Driver";
	protected static final String POSTGRES_DEF_USER = "postgres";
	protected static final String POSTGRES_DEF_PW = "password";
	private static final String LOG_PREFIX = "PostgreSQL";
	private static final String GREPABLE_TYPES = "('VARCHAR','CHAR','TEXT',"
	        + "'INT','INT2','INT4','INT8','BOOL','DECIMAL','REAL','FLOAT8',"
	        + "'INET','MACADDR','MONEY',"
	        + "'XML','UUID','DATE','TIMESTAMP','TIME')";
	
	private static final String ALL_TABLES = "INFORMATION_SCHEMA.TABLES t, INFORMATION_SCHEMA.COLUMNS c"
            + " WHERE c.TABLE_NAME = t.TABLE_NAME AND c.TABLE_SCHEMA = t.TABLE_SCHEMA "
            + " AND t.TABLE_TYPE = 'BASE TABLE'";
	private static final String SCHEMA_NAME = " AND c.table_schema = '"; 
	
	private Connection conn = null;
	private String schemaName;
	
	public PostgresqlParser(String uri, String user, String password) {
		super(uri, user, password);
	}

	@Override
	public String getDefaultUser() {
		return POSTGRES_DEF_USER;
	}

	@Override
	public String getDefaultPassword() {
		return POSTGRES_DEF_PW;
	}

	public String getLogPrefix() {
		return LOG_PREFIX;
	}

    //	public static void main(String[] args) {
//	    try {
//            (new PostgresqlParser("jdbc:postgresql://localhost:5432/", "postgres", "password")).dbConnection();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//	}
	
	@Override
	public Connection dbConnection() throws Exception {
		if (conn != null && !conn.isClosed()) {
			return conn;
		}
		if (getDbDriver() == null) {
            Class<?> driver = null;
            String driverClassName = getDriverClass(DatabaseFactory.POSTGRESQL_DRIVER_KEY, POSTGRES_DEFAULT_DRIVER);
            try {
                driver = Class.forName(driverClassName);
            } catch (ClassNotFoundException e) {
                EnvResult res = new EnvResult("Postgresql database grep", EnvStatus.ERROR);
                missingDriver(res, driverClassName, DatabaseFactory.POSTGRESQL_DRIVER_KEY, POSTGRES_DEFAULT_DRIVER, "postgresql-<version>.jar");
                throw new GrepException(res);
            }		    
		    setDbDriver((Driver)driver.newInstance());
		}
		setConnection(openConnection());
		schemaName = getConnection().getCatalog();
		if (schemaName != null && schemaName.isEmpty()) {
		    schemaName = null;
		}
		int max = getUri().length();
		if (max > 40) {
			max = 40;
		}
		debug("Postgresql database connection: user=" + getUser() + ",pw=" + getPassword() + ",uri=" + getUri().substring(0,max) + "...");
		return conn;
	}

	public void dbClose() throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}
	
	@Override
	public Map<String, List<String>> getFullTableMap() throws Exception {
		Connection conn = dbConnection();
        StringBuilder allColsQuery = new StringBuilder("select c.TABLE_NAME, c.COLUMN_NAME from ");
        allColsQuery.append(ALL_TABLES);
		if (getSchemaName() != null) {
			allColsQuery.append(SCHEMA_NAME).append(getSchemaName()).append("'");
		}

		/*
		 * Table/column syntax:
		 *    table.col  	exact column
		 *    .col 			all tables with specific column
		 *    table 		all columns from table
		 *    table*.*col? - limited wildcarded table name(s) and column(s)
		 */
		String tabFilter = dbResourceListFilter();
		if (tabFilter != null) {
			allColsQuery.append(" AND ").append(tabFilter);
		}
		trace("DB TABLE COLUMNS QUERY: " + allColsQuery.toString());
		ResultSet rs = queryDb(conn, allColsQuery.toString());
		Map<String, List<String>> tableMap = mapTableColumns(rs);
		return tableMap;
	}

	@Override
	public List<String> getColumnSubset(String table) throws Exception {
		Connection conn = dbConnection();
		StringBuilder colsQuery = new StringBuilder("SELECT c.COLUMN_NAME from ");
		colsQuery.append(ALL_TABLES).append(" AND UPPER(c.udt_name) IN ").append(GREPABLE_TYPES);
		if (getSchemaName() != null) {
			colsQuery.append(SCHEMA_NAME).append(getSchemaName()).append("'");
		}
		colsQuery.append(" AND UPPER(c.TABLE_NAME) = '" + table.toUpperCase() + "'");
		debug("DB CONTENT SUBSET QUERY> " + colsQuery.toString());
		ResultSet rs = queryDb(conn, colsQuery.toString()); // non numeric/blob columns
		return listColumns(rs);
	}

	@Override
	public List<List<String>> getMatchingColumns(List<String> columnList, String table, ResourcePattern pattern) throws Exception {
		String dbPattern = pattern.getDatabaseWildcardExpression();
		String colCondition = buildColumnWhere(dbPattern, columnList);
		if (colCondition == null || colCondition.isEmpty()) {
			// no columns of the required type
			return null;
		}
		StringBuilder dataQuery = new StringBuilder();
		String colSelect = buildColumnSelect(columnList);
		dataQuery.append("SELECT ").append(colSelect).append(" FROM ").append(table);
		dataQuery.append(colCondition);
		debug("DB CONTENT QUERY> " + dataQuery.toString());
		ResultSet rs = queryDb(dbConnection(), dataQuery.toString());
		return resultSetToList(rs);
	}

	/*
	 * Build a map keyed by table name, and all columns in the result set (value=list of column names)
	 */
	private Map<String, List<String>> mapTableColumns(ResultSet rs) throws SQLException {
		Map<String, List<String>> tabs = new HashMap<String, List<String>>();
		while (rs.next()) {
			String tab = rs.getString("TABLE_NAME");
			String col = rs.getString("COLUMN_NAME");
			if (!tabs.containsKey(tab)) {
				debug("Add table [" + tab + "] to map.");
				tabs.put(tab, new ArrayList<String>());
			}
			debug("Map table [" + tab + "] column [" + col + "]");
			tabs.get(tab).add(col);
		}
		rs.close();
		return tabs;
	}
	
	/*
	 * Build a list of all columns in the result set
	 */
	private List<String> listColumns(ResultSet rs) throws SQLException {
		List<String> cols = new ArrayList<String>();
		while (rs.next()) {
			String col = rs.getString("COLUMN_NAME");
			if (!cols.contains(col)) {
				cols.add(col);
			}
		}
		rs.close();
		// Postgres seems to return columns in reverse order!
		Collections.reverse(cols);
		return cols;
	}
	/*
	 * Filter of tables and or columns based on limited RE.
	 *  table.col  		exact column
	 *  .col 			all tables with specific column
	 *  table 			all columns from table
	 *  table*.*col? 	limited wildcarded table name(s) and column(s)
	 */
	private String dbResourceListFilter() {
		String tab = null;
		String col = null;
		if (getResourceList() == null) {
			return null;
		}
		debug("Resource list: " + getResourceList().get(0));
		if (!getResourceList().isWild()) {
			if (!getResourceList().isFirstPartWild()) {
				tab = getResourceList().firstPartPattern();
			}
			if (!getResourceList().isLastPartWild()) { 
				col = getResourceList().lastPartPattern();
			}
		}
		StringBuilder q = new StringBuilder();
		if (tab != null) {
			if (getResourceList().firstPartContainsWild()) {
				q.append("UPPER(c.TABLE_NAME) LIKE '").append(tab.toUpperCase()).append("'");
			} else {
				q.append("UPPER(c.TABLE_NAME) = '").append(tab.toUpperCase()).append("'");
			}
		}
		if (col != null) {
			if (q.length() > 0) {
				q.append(" AND ");
			}
			if (getResourceList().lastPartContainsWild()) {
				q.append("UPPER(c.COLUMN_NAME) LIKE '").append(col.toUpperCase()).append("'");
			} else {
				q.append("UPPER(c.COLUMN_NAME) = '").append(col.toUpperCase()).append("'");
			}
		}
		debug("TABLE LIST FILTER: " + q.toString());
		return q.length() == 0 ? null : q.toString();
	}

	public Connection getConnection() {
		return conn;
	}

	public void setConnection(Connection conn) {
		this.conn = conn;
	}

	public String getSchemaName() {
		return schemaName;
	}

	/**
	 * How to represent a column name in a WHERE clause considering the current case sensitivity
	 * and DB's identifier quoting convention.
	 */
	@Override
	public String columnIdentifier(String col, boolean ignoreCase, boolean isSelect) {
		StringBuilder q = new StringBuilder();
        if (ignoreCase) {
			q.append(" UPPER(");
		}
        // need to use select "name" from name where 'name'...
        if (isSelect) {
            q.append(IDENTIFIER_DOUBLE_QUOTE).append(col).append(IDENTIFIER_DOUBLE_QUOTE);
        } else {
            // co-ersce all columns to string type otherwise UPPER() fails for numeric types
            q.append("quote_ident('' || ").append(col).append(")");
        }
        if (ignoreCase) {
			q.append(") ");
		}
		return q.toString();
	}
}
