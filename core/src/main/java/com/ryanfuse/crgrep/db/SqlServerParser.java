/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.env.EnvResult;
import com.ryanfuse.crgrep.env.EnvStatus;

/**
 * Parser class to query and parse data from a Microsoft SQLServer database.
 * 
 * Default user/password: sa/password
 * Default schema: 'DBO'
 * JDBC url: jdbc:sqlserver://localhost:1433;database=mydb
 * 
 * More info:
 *      http://msdn.microsoft.com/en-us/library/ms378428(SQL.90).aspx
 *      http://msdn.microsoft.com/en-us/library/ms378672(v=sql.90).aspx
 * @author Craig Ryan
 */
public class SqlServerParser extends DatabaseParser {
	
    protected static final String JTDS_DEFAULT_DRIVER = "net.sourceforge.jtds.jdbc.Driver";
    protected static final String MS_DEFAULT_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	protected static final String SQLSERVER_DEF_USER = "sa";
    protected static final String SQLSERVER_DEF_PW = "password";
    protected static final String SQLSERVER_DEF_SCHEMA = "dbo";
	private static final String LOG_PREFIX = "SqlServer";
	private static final String GREPABLE_TYPES = "('VARCHAR','NVARCHAR','CHAR','NCHAR','NVARCHAR',"
	        + "'INT','TINYINT','SMALLINT','BIGINT',"
            + "'DECIMAL','NUMERIC','FLOAT','REAL',"
            + "'MONEY','SMALLMONEY',"
	        + "'UNIQUEIDENTIFIER','XML',"
            + "'DATE','DATETIME','SMALLDATETIME','TIME','DATETIME2'"
	        + ")";
	
	private static final String ALL_TABLES = "INFORMATION_SCHEMA.COLUMNS";
    private static final String JTDS_URI_PREFIX = "jdbc:jtds.sqlserver:";
	
	private Connection conn = null;
	private String schemaName;
	
	public SqlServerParser(String uri, String user, String password) {
		super(uri, user, password);
	}

	@Override
	public String getDefaultUser() {
		return SQLSERVER_DEF_USER;
	}

	@Override
	public String getDefaultPassword() {
		return SQLSERVER_DEF_PW;
	}

	public String getLogPrefix() {
		return LOG_PREFIX;
	}

	@Override
	public Connection dbConnection() throws Exception {
		if (conn != null && !conn.isClosed()) {
			return conn;
		}
		if (getDbDriver() == null) {
		    // SQLServer 2005 driver (old driver/db not supported).
		    if (getUri().startsWith(JTDS_URI_PREFIX)) {
		        // jdbc:jtds:sqlserver://<host>:<port>/<database_name>
                Class<?> driver = null;
                String driverClassName = getDriverClass(DatabaseFactory.SQLSERVER_JTDS_DRIVER_KEY, JTDS_DEFAULT_DRIVER);
                try {
                    driver = Class.forName(driverClassName);
                } catch (ClassNotFoundException e) {
                    EnvResult res = new EnvResult("SQLServer JTDS database grep", EnvStatus.ERROR);
                    missingDriver(res, driverClassName, DatabaseFactory.SQLSERVER_JTDS_DRIVER_KEY, JTDS_DEFAULT_DRIVER, "jtds-<version>.jar");
                    throw new GrepException(res);
                }
                setDbDriver((Driver) driver.newInstance());             
		    } else {
		        // jdbc:sqlserver://<server_name>:<port> 
                Class<?> driver = null;
                String driverClassName = getDriverClass(DatabaseFactory.SQLSERVER_MS_DRIVER_KEY, MS_DEFAULT_DRIVER);
                try {
                    driver = Class.forName(driverClassName);
                } catch (ClassNotFoundException e) {
                    EnvResult res = new EnvResult("SQLServer native database grep", EnvStatus.ERROR);
                    missingDriver(res, driverClassName, DatabaseFactory.SQLSERVER_MS_DRIVER_KEY, MS_DEFAULT_DRIVER, "sqljdbc-<version>.jar");
                    throw new GrepException(res);
                }
                setDbDriver((Driver) driver.newInstance());             
		    }
		}
		setConnection(openConnection());
		ResultSet rs = queryDb(conn, "SELECT SCHEMA_NAME() AS CURRENT_SCHEMA");
		if (rs.next()) {
		    String schema_name = rs.getString("CURRENT_SCHEMA");
		    schemaName = schema_name == null ? SQLSERVER_DEF_SCHEMA : schema_name;
		} else {
		    schemaName = SQLSERVER_DEF_SCHEMA;
		}
		
		int max = getUri().length();
		if (max > 40) {
			max = 40;
		}
		debug("SQLServer database connection: user=" + getUser() + ",pw=" + getPassword() + ",uri=" + getUri().substring(0,max) + "..." + " schema=" + schemaName);
		return conn;
	}

	public void dbClose() throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}
	
	@Override
	public Map<String, List<String>> getFullTableMap() throws Exception {
		Connection conn = dbConnection();
		StringBuilder allColsQuery = new StringBuilder("select TABLE_NAME, COLUMN_NAME from ");
		allColsQuery.append(ALL_TABLES);
		boolean hasWhere = false;
		if (getSchemaName() != null) {
			allColsQuery.append(" WHERE TABLE_SCHEMA = '").append(getSchemaName()).append("'");
			hasWhere = true;
		}

		String tabFilter = dbResourceListFilter();
		if (tabFilter != null) {
			allColsQuery.append(hasWhere ? " AND " : " WHERE ").append(tabFilter);
		}
		trace("DB TABLE COLUMNS QUERY: " + allColsQuery.toString());
		ResultSet rs = queryDb(conn, allColsQuery.toString());
		Map<String, List<String>> tableMap = mapTableColumns(rs);
		return tableMap;
	}

	@Override
	public List<String> getColumnSubset(String table) throws Exception {
		Connection conn = dbConnection();
		StringBuilder colsQuery = new StringBuilder("SELECT COLUMN_NAME from ");
		colsQuery.append(ALL_TABLES).append(" WHERE data_type IN ").append(GREPABLE_TYPES);
		if (getSchemaName() != null) {
			colsQuery.append(" AND TABLE_SCHEMA = '").append(getSchemaName()).append("'");
		}
		colsQuery.append(" AND TABLE_NAME = '" + table + "'");
		debug("DB CONTENT SUBSET QUERY> " + colsQuery.toString());
		ResultSet rs = queryDb(conn, colsQuery.toString()); // non numeric/blob columns
		return listColumns(rs);
	}

	@Override
	public List<List<String>> getMatchingColumns(List<String> columnList, String table, ResourcePattern pattern) throws Exception {
		String dbPattern = pattern.getDatabaseWildcardExpression();
		String colCondition = buildColumnWhere(dbPattern, columnList);
		if (colCondition == null || colCondition.isEmpty()) {
			// no columns of the required type
			return null;
		}
		StringBuilder dataQuery = new StringBuilder();
		String colSelect = buildColumnSelect(columnList);
		dataQuery.append("SELECT ").append(colSelect).append(" from ").append(table);
		dataQuery.append(colCondition);
		debug("DB CONTENT QUERY> " + dataQuery.toString());
		ResultSet rs = queryDb(dbConnection(), dataQuery.toString());
		return resultSetToList(rs);
	}

	/*
	 * Build a map keyed by table name, and all columns in the result set (value=list of column names)
	 */
	private Map<String, List<String>> mapTableColumns(ResultSet rs) throws SQLException {
		Map<String, List<String>> tabs = new HashMap<String, List<String>>();
		while (rs.next()) {
			String tab = rs.getString("TABLE_NAME");
			String col = rs.getString("COLUMN_NAME");
			if (!tabs.containsKey(tab)) {
				debug("Add table [" + tab + "] to map.");
				tabs.put(tab, new ArrayList<String>());
			}
			debug("Map table [" + tab + "] column [" + col + "]");
			tabs.get(tab).add(col);
		}
		rs.close();
		return tabs;
	}
	
	/*
	 * Build a list of all columns in the result set
	 */
	private List<String> listColumns(ResultSet rs) throws SQLException {
		List<String> cols = new ArrayList<String>();
		while (rs.next()) {
			String col = rs.getString("COLUMN_NAME");
			if (!cols.contains(col)) {
				cols.add(col);
			}
		}
		rs.close();
		return cols;
	}
	/*
	 * Filter of tables and or columns based on limited RE.
	 */
	private String dbResourceListFilter() {
		String tab = null;
		String col = null;
		if (getResourceList() == null) {
			return null;
		}
		debug("Resource list: " + getResourceList().get(0));
		if (!getResourceList().isWild()) {
			if (!getResourceList().isFirstPartWild()) {
				tab = getResourceList().firstPartPattern();
			}
			if (!getResourceList().isLastPartWild()) { 
				col = getResourceList().lastPartPattern();
			}
		}
		StringBuilder q = new StringBuilder();
		if (tab != null) {
			if (getResourceList().firstPartContainsWild()) {
				q.append("TABLE_NAME LIKE '").append(tab).append("'");
			} else {
				q.append("TABLE_NAME = '").append(tab).append("'");
			}
		}
		if (col != null) {
			if (q.length() > 0) {
				q.append(" AND ");
			}
			if (getResourceList().lastPartContainsWild()) {
				q.append("COLUMN_NAME LIKE '").append(col).append("'");
			} else {
				q.append("COLUMN_NAME = '").append(col).append("'");
			}
		}
		debug("TABLE LIST FILTER: " + q.toString());
		return q.length() == 0 ? null : q.toString();
	}

	public Connection getConnection() {
		return conn;
	}

	public void setConnection(Connection conn) {
		this.conn = conn;
	}

	public String getSchemaName() {
		return schemaName;
	}

	/**
	 * How to represent a column name in a WHERE clause considering the current case sensitivity
	 * and DB's identifier quoting convention.
	 */
	@Override
	public String columnIdentifier(String col, boolean ignoreCase, boolean isSelect) {
		StringBuilder q = new StringBuilder();
        if (ignoreCase) {
			q.append(" UPPER(");
		}
		q.append(IDENTIFIER_OPEN_BRACKET).append(col).append(IDENTIFIER_CLOSE_BRACKET);
        if (ignoreCase) {
			q.append(") ");
		}
		return q.toString();
	}
}
