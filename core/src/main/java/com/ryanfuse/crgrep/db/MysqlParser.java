/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.env.EnvResult;
import com.ryanfuse.crgrep.env.EnvStatus;

/**
 * Parser class to query and parse data from a MySQL database.
 * 
 * @author Craig Ryan
 */
public class MysqlParser extends DatabaseParser {
    protected static final String MYSQL_DEFAULT_DRIVER = "com.mysql.jdbc.Driver";	
	protected static final String MYSQL_DEF_USER = "root";
	protected static final String MYSQL_DEF_PW = "password";
	private static final String LOG_PREFIX = "MySQL";
	private static final String GREPABLE_TYPES = "('VARCHAR2','VARCHAR','CHAR','NCHAR','NVARCHAR2','MEDIUMTEXT',"
	        + "'INT','INTEGER','TINYINT','SMALLINT','MEDIUMINT','BIGINT','DECIMAL','NUMERIC','FLOAT','DOUBLE','ENUM','SET',"
	        + "'XML','URI','DATE','TIMESTAMP','DATETIME','YEAR')";
	
	private static final String ALL_TABLES = "INFORMATION_SCHEMA.COLUMNS";
	
	private Connection conn = null;
	private String schemaName;
	
	public MysqlParser(String uri, String user, String password) {
		super(uri, user, password);
	}

	@Override
	public String getDefaultUser() {
		return MYSQL_DEF_USER;
	}

	@Override
	public String getDefaultPassword() {
		return MYSQL_DEF_PW;
	}

	public String getLogPrefix() {
		return LOG_PREFIX;
	}

	@Override
	public Connection dbConnection() throws Exception {
		if (conn != null && !conn.isClosed()) {
			return conn;
		}
		if (getDbDriver() == null) {
		    Class<?> driver = null;
            String driverClassName = getDriverClass(DatabaseFactory.MYSQL_DRIVER_KEY, MYSQL_DEFAULT_DRIVER);
            try {
                driver = Class.forName(driverClassName);
            } catch (ClassNotFoundException e) {
                EnvResult res = new EnvResult("MySQL database grep", EnvStatus.ERROR);
                missingDriver(res, driverClassName, DatabaseFactory.MYSQL_DRIVER_KEY, MYSQL_DEFAULT_DRIVER, "mysql-connector-java-<version>.jar");
                throw new GrepException(res);
            }			
            setDbDriver((Driver) driver.newInstance());
	        Properties parts = ((com.mysql.jdbc.Driver)getDbDriver()).parseURL(getUri(), new Properties());
	        schemaName = parts.getProperty(com.mysql.jdbc.Driver.DBNAME_PROPERTY_KEY);
		}
		setConnection(openConnection());
		int max = getUri().length();
		if (max > 40) {
			max = 40;
		}
		debug("MySQL database connection, user=" + getUser() + ",pw=" + getPassword() + ",uri=" + getUri().substring(0,max) + "...");
		return conn;
	}

	public void dbClose() throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}
	
	@Override
	public Map<String, List<String>> getFullTableMap() throws Exception {
		Connection conn = dbConnection();
		StringBuilder allColsQuery = new StringBuilder("select TABLE_NAME, COLUMN_NAME from ");
		allColsQuery.append(ALL_TABLES);
		boolean hasWhere = false;
		if (getSchemaName() != null) {
			allColsQuery.append(" WHERE TABLE_SCHEMA = '").append(getSchemaName()).append("'");
			hasWhere = true;
		}

		/*
		 * Table/column syntax:
		 *    table.col  	exact column
		 *    .col 			all tables with specific column
		 *    table 		all columns from table
		 *    table*.*col? - limited wildcarded table name(s) and column(s)
		 */
		String tabFilter = dbResourceListFilter();
		if (tabFilter != null) {
			allColsQuery.append(hasWhere ? " AND " : " WHERE ").append(tabFilter);
		}
		trace("DB TABLE COLUMNS QUERY: " + allColsQuery.toString());
		ResultSet rs = queryDb(conn, allColsQuery.toString());
		Map<String, List<String>> tableMap = mapTableColumns(rs);
		return tableMap;
	}

	@Override
	public List<String> getColumnSubset(String table) throws Exception {
		Connection conn = dbConnection();
		StringBuilder colsQuery = new StringBuilder("SELECT COLUMN_NAME from ");
		colsQuery.append(ALL_TABLES).append(" WHERE data_type IN ").append(GREPABLE_TYPES);
		if (getSchemaName() != null) {
			colsQuery.append(" AND TABLE_SCHEMA = '").append(getSchemaName()).append("'");
		}
		colsQuery.append(" AND TABLE_NAME = '" + table + "'");
		debug("DB CONTENT SUBSET QUERY> " + colsQuery.toString());
		ResultSet rs = queryDb(conn, colsQuery.toString()); // non numeric/blob columns
		return listColumns(rs);
	}

	@Override
	public List<List<String>> getMatchingColumns(List<String> columnList, String table, ResourcePattern pattern) throws Exception {
		String dbPattern = pattern.getDatabaseWildcardExpression();
		String colCondition = buildColumnWhere(dbPattern, columnList);
		if (colCondition == null || colCondition.isEmpty()) {
			// no columns of the required type
			return null;
		}
		StringBuilder dataQuery = new StringBuilder();
		String colSelect = buildColumnSelect(columnList);
		dataQuery.append("SELECT ").append(colSelect).append(" from ").append(table);
		dataQuery.append(colCondition);
		debug("DB CONTENT QUERY> " + dataQuery.toString());
		ResultSet rs = queryDb(dbConnection(), dataQuery.toString());
		return resultSetToList(rs);
	}

	/*
	 * Build a map keyed by table name, and all columns in the result set (value=list of column names)
	 */
	private Map<String, List<String>> mapTableColumns(ResultSet rs) throws SQLException {
		Map<String, List<String>> tabs = new HashMap<String, List<String>>();
		while (rs.next()) {
			String tab = rs.getString("TABLE_NAME");
			String col = rs.getString("COLUMN_NAME");
			if (!tabs.containsKey(tab)) {
				debug("Add table [" + tab + "] to map.");
				tabs.put(tab, new ArrayList<String>());
			}
			debug("Map table [" + tab + "] column [" + col + "]");
			tabs.get(tab).add(col);
		}
		rs.close();
		return tabs;
	}
	
	/*
	 * Build a list of all columns in the result set
	 */
	private List<String> listColumns(ResultSet rs) throws SQLException {
		List<String> cols = new ArrayList<String>();
		while (rs.next()) {
			String col = rs.getString("COLUMN_NAME");
			if (!cols.contains(col)) {
				cols.add(col);
			}
		}
		rs.close();
		return cols;
	}
	/*
	 * Filter of tables and or columns based on limited RE.
	 *  table.col  		exact column
	 *  .col 			all tables with specific column
	 *  table 			all columns from table
	 *  table*.*col? 	limited wildcarded table name(s) and column(s)
	 */
	private String dbResourceListFilter() {
		String tab = null;
		String col = null;
		if (getResourceList() == null) {
			return null;
		}
		debug("Resource list: " + getResourceList().get(0));
		if (!getResourceList().isWild()) {
			if (!getResourceList().isFirstPartWild()) {
				tab = getResourceList().firstPartPattern();
			}
			if (!getResourceList().isLastPartWild()) { 
				col = getResourceList().lastPartPattern();
			}
		}
		StringBuilder q = new StringBuilder();
		if (tab != null) {
			if (getResourceList().firstPartContainsWild()) {
				q.append("TABLE_NAME LIKE '").append(tab).append("'");
			} else {
				q.append("TABLE_NAME = '").append(tab).append("'");
			}
		}
		if (col != null) {
			if (q.length() > 0) {
				q.append(" AND ");
			}
			if (getResourceList().lastPartContainsWild()) {
				q.append("COLUMN_NAME LIKE '").append(col).append("'");
			} else {
				q.append("COLUMN_NAME = '").append(col).append("'");
			}
		}
		debug("TABLE LIST FILTER: " + q.toString());
		return q.length() == 0 ? null : q.toString();
	}

	public Connection getConnection() {
		return conn;
	}

	public void setConnection(Connection conn) {
		this.conn = conn;
	}

	public String getSchemaName() {
		return schemaName;
	}

	/**
	 * How to represent a column name in a WHERE clause considering the current case sensitivity
	 * and DB's identifier quoting convention.
	 *
	 * MySQL requires backquote (`) to quote a column name, otherwise if the default is
	 * used (double quotes, ") it's not considered an identifier and is interpreted as
	 * a string value in the default SQL mode. For example:
	 *     ... WHERE "created" LIKE '%CREATED%'
	 * will match string 'created' against the LIKE expression, and 
	 *     ... WHERE `created` LIKE '%CREATED%'
	 * will match the 'created' column data against the LIKE expression (which is what we want).
	 */
	@Override
	public String columnIdentifier(String col, boolean ignoreCase, boolean isSelect) {
		StringBuilder q = new StringBuilder();
        if (ignoreCase) {
			q.append(" UPPER(");
		}
		q.append(IDENTIFIER_BACK_QUOTE).append(col).append(IDENTIFIER_BACK_QUOTE);
        if (ignoreCase) {
			q.append(") ");
		}
		return q.toString();
	}
}
