/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.file.DirectoryStream;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.ryanfuse.crgrep.file.FileTypeFactory;
import com.ryanfuse.crgrep.filters.ContentFilter;
import com.ryanfuse.crgrep.filters.nlp.MoodContentFilter;
import com.ryanfuse.crgrep.util.ResourcePath;
import com.ryanfuse.crgrep.util.ResourcePaths;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Grep files and directories.
 * 
 * A FileTypeFactory is called to supply a FileType implementation for each file resource
 * discovered. The FileType is then called to grep the resource, be it a plain file,
 * archive, image or other supported type of file resource.
 * 
 * @author Craig Ryan
 */
public class FileGrep extends AbstractGrep {

	private static final String BINARY_FILE_EXTENTS = ".*\\.(7z|exe|bzip2|bz2|tgz|class|o|obj|bin|rar)";
	
	private FileTypeFactory fileTypeFactory;
	private Map<String, ContentFilter> contentFilters = new HashMap<String, ContentFilter>();
	
	public FileGrep(Switches switches) {
		super(switches);
		initFilters();
		fileTypeFactory = new FileTypeFactory(this);
	}

	public String displayName() {
	    return "File grep";
	}

	@Override
	public void execute() throws GrepException {
		try {
			grepResources();
        } catch (GrepException ge) {
            throw ge;
        } catch (Exception e) {
			throw new GrepException("crgrep: file grep failed.", e);
		}
	}

    /**
     * Main entry point for FileType implementations to pass back processing of a 
     * file resource they do not directly support.
     * 
     * The various FileType implementations do not communicate directly with each other
     * so, for example, the ArchiveFileType may discover an Image file within an archive
     * and by passing the file back through here the ImageFileType implementation will
     * have a chance to process the resource. 
     * 
     * Through this mechanism we're able to support any combination of arbitrary nesting
     * (eg text within an image nested in a jar which is nested in an EAR file which was
     * discovered via a POM artifact... and so on).
     * 
     * @param file
     * @throws GrepException 
     */
    public void grepByContents(File file) throws GrepException {
        fileTypeFactory.getFileType(file).grepEntry(file);
    }
    
    /**
     * Alternative to {@link #grepByContents(File)} for entries which are read as streams
     * such as a resource within an archive.
     * 
     * @param entry the entry stream
     * @param entryName the name of the entry excluding any parent path
     * @param entryPath entry path including entry name
     * @throws GrepException 
     */
    public void grepByContentsStream(InputStream entry, String entryName, String entryPath) throws GrepException {
        fileTypeFactory.getFileTypeByName(entryName).grepEntryStream(entry, entryName, entryPath);
    }

    public boolean hasContentFilter(String name) {
    	return contentFilters.containsKey(name);
    }

    public ContentFilter getContentFilter(String name) {
    	return contentFilters.get(name);
    }

	private void initFilters() {
		if (getSwitches().isMood()) {
			// Only makes sense to apply sentiment analysis to content searches
			if (getSwitches().isContent()) {
				MoodContentFilter mcf = new MoodContentFilter(this, getSwitches().getSentiment());
				contentFilters.put(ContentFilter.MOOD_FILTER, mcf);
			} else {
				getDisplay().warn("crgrep: --mood ignored since -l was specified.");
			}
		}
	}

    /*
     * Main entry point, grep all resources matching the resource pattern.
     * Handles file wilcards and path wildcards based on Java 7 Paths 
     * including Ant style '**' and other glob patterns.
     */
    private void grepResources() throws GrepException {
        ResourceList<String> resList = getResourceList();
        if (resList.size() == 1 && resList.get(0).equals("-")) {
            // Read from STDIN
            grepByContentsStream(System.in, "-", null);
            return;
        }
        ResourcePaths paths = new ResourcePaths(getSwitches(), getDisplay());
        
        // Iterate over the list of command line resource paths specified. 
        // Can be used to control search such as follow symlinks.
        Set<FileVisitOption> searchOptions = Collections.EMPTY_SET;
        
        // For each individual resource path given on the command line.
        while (resList.hasNext()) {
            String nextResourcePattern = resList.next();
            ResourcePath path = paths.get(nextResourcePattern);
            debug("FileGrep processed resource argument '" + nextResourcePattern + "', resulted in parent '" + path.getParentPath() + "' and wild '" + path.getWildPath() + "' paths.");
            if (!path.isValidPath()) {
                getDisplay().error("crgrep: " + path.toString() + ": No such file or directory");
                continue;
            }
            int maxDepth = getSwitches().isRecurse() ? Integer.MAX_VALUE : path.getMaxPathDepth();
            Path startDir = path.getStartDirectory();
            DirectoryStream.Filter<Path> filters = getResourceFilters(startDir, path);
            
            /* Using visitor + walkFileTree */
            ResourceVisitor finder = new ResourceVisitor(path, filters);
            try {
                getDisplay().debug("Walk tree from startDir: '" + startDir + "' to maxDepth: " + maxDepth); 
                Files.walkFileTree(startDir, searchOptions, maxDepth, finder);
                if (finder.getMatches() == 0) {
                    getDisplay().error("crgrep: " + path.toString() + ": No such file or directory"); 
                }
            } catch (IOException e) {
            	getDisplay().error("crgrep: " + path.toString() + ": Unable to read file or directory");
            }
            
        }
    }

    /*
     * Filters control which files/dirs to include and which to ignore. This depends
     * on patterns found in the resource path specified and also command line include/exclude
     * options (once they are implemented).
     */
    private DirectoryStream.Filter<Path> getResourceFilters(final Path parentPath, ResourcePath path) {
        String globPath = getGlobPath(parentPath.toString(), path.getWildPath());
        final String globPattern = "glob:" + globPath;
        final PathMatcher matcher = FileSystems.getDefault().getPathMatcher(globPattern);
        getDisplay().debug("Path filter created to match glob '" + globPattern + "'");
        
        DirectoryStream.Filter<Path> globMatchFilter = new DirectoryStream.Filter<Path>() {
            @Override
            public boolean accept(Path currpath) throws IOException {
                boolean got = matcher.matches(currpath);
                getDisplay().trace("Filter path '" + currpath + "' matches? " + got);
                return got;
            }
        };
        return globMatchFilter;
    }

    // Build up the path to specify as the pattern in 'glob:<pattern>'.
    // The following logic forces forward slashes in the glob pattern. Otherwise patterns
    // like 'a\b' are interpreted as '\' being an escape character.
    private String getGlobPath(String globParent, String globWild) {
        globParent = globParent.replaceAll("\\\\", "/");
        String globJoined = null;
        if (globParent.isEmpty()) {
            if (globWild.isEmpty()) {
                globJoined = "**";
            } else {
                globJoined = globWild;
            }
        } else {
            if (globWild == null || globWild.isEmpty()) {
                globJoined = "**";
            } else {
                globJoined = globParent + "/" + globWild;
            }
        }
        return globJoined;
    }

    // Visitor to process each real file/directory visited
    private class ResourceVisitor extends SimpleFileVisitor<Path> {
    	private final Filter<Path> filters;
        private GrepException exception;
		private ResourcePath resourcePath;
		private int matches = 0;
        
        ResourceVisitor(ResourcePath path, Filter<Path> filters) {
			this.resourcePath = path;
			this.filters = filters;
        }
        
        // Compares the glob pattern against the file or directory name.
        // Also accept the parent directory itself.
        void matchPath(Path currpath) {
        	boolean matched = false;
        	try {
				matched = filters.accept(currpath);
				//getDisplay().debug("Visit path: " + currpath + ", matched? " + matched);
			} catch (IOException e1) {
	            getDisplay().error("crgrep: " + currpath + ": Unable to read file or directory");
			}
        	if (matched) {
        		try {
        			matches++;
        			fileTypeFactory.getFileType(currpath).grepEntry(currpath);
        		} catch (GrepException e) {
        			exception = e;
        		}
        	}
        }

        public boolean hasErrors() {
            return exception != null;
        }
        
        public GrepException getError() {
            return exception;
        }
        
        public int getMatches() {
        	return matches;
        }

        // Invoke the pattern matching method on each file.
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
            matchPath(file);
            return FileVisitResult.CONTINUE;
        }

        // Invoke the pattern matching method on each directory.
        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
            matchPath(dir);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path path, IOException exc) {
            getDisplay().error("crgrep: " + path.toFile().getPath() + ": Unable to read file or directory");
            return FileVisitResult.CONTINUE;
        }
    }

	public void closeResource(Reader r) {
		if (r == null) {
			return;
		}
		try {
			r.close();
		} catch (IOException e) {
		}
	}

	public void closeResource(InputStream is) {
		if (is == null) {
			return;
		}
		try {
			is.close();
		} catch (IOException e) {
		}
	}

	// Guess if its binary or not
	public boolean isBinaryFile(File f) throws FileNotFoundException, IOException {
		Pattern archiveExtent = Pattern.compile(BINARY_FILE_EXTENTS, Pattern.CASE_INSENSITIVE);
		if (archiveExtent.matcher(f.getName()).matches()) {
		    trace("FileGrep file '" + f.getName() + "' is binary (by file extent)");
			return true;
		} else {
		    trace("FileGrep file '" + f.getName() + "' is not binary (by file extent)");
		}
		// still not sure - check the content for too many non-ascii chars
	    FileInputStream in = new FileInputStream(f);
	    byte[] data = new byte[512/*size*/];
	    int len = in.read(data);
	    in.close();
	    boolean isBin = isBinaryData(data, len);
		trace("FileGrep file '" + f.getPath() + "' is " + (isBin ? "" : "not") + " binary");
		return isBin;
	}

	public boolean isBinaryData(byte[] data, int length) {
	    int ascii = 0;
	    int other = 0;
	    for (int i = 0; i < length; i++) {
	        byte b = data[i];
	        if (b < 0x09) {
	        	return true;
	        }
	        if (b == 0x09 || b == 0x0A || b == 0x0C || b == 0x0D) {
	        	ascii++;
	        }
	        else if (b >= 0x20  &&  b <= 0x7E) {
	        	ascii++;
	        }
	        else {
	        	other++;
	        }
	    }
	    if (other == 0) {
	    	return false;
	    }
	    return (ascii + other) * 100 / other > 95;
	}
}
