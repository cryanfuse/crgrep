package com.ryanfuse.crgrep.filters;

/* 
 * Base class for resource filters 
 */
public abstract class ResourceFilter {

	public abstract boolean filterEnabled();
	
	public abstract boolean acceptResource(String resource);
}
