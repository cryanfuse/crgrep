/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.filters.nlp;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.filters.ContentFilter;
import com.ryanfuse.crgrep.nlp.Sentiment;
import com.ryanfuse.crgrep.util.PathUtils;

import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations.SentimentAnnotatedTree;
import edu.stanford.nlp.util.CoreMap;

/*
 * A content filter which determines if the content is either positive, neutral or
 * negative and thus included in results based on the --mood selection.   
 */
public class MoodContentFilter extends ContentFilter {

    private static final int SAMPLE_TEXT_LENGTH = 25;

	private static final String DEFAULT_SENTIMENT_MODEL = "edu/stanford/nlp/models/sentiment/sentiment.ser.gz";

    private StanfordCoreNLP pipeline;
	private Sentiment expectedSentiment;
	private FileGrep fileGrep;
	private boolean enabled = true;
	
	public MoodContentFilter(FileGrep fileGrep, Sentiment sentiment) {
        this.fileGrep = fileGrep;
		this.expectedSentiment = sentiment;
		Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
        //props.setProperty("parse.maxlen", "10");
        // Disable logging
        System.setProperty("org.slf4j.simpleLogger.log.edu.stanford.nlp", "error");
    	//System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "error");

        // Attempt to locate the MODEL data on the classpath. If not found,
        // assume the MODEL jar has not been installed and therefore disable --mood.
        //
        if (IOUtils.existsInClasspathOrFileSystem(DEFAULT_SENTIMENT_MODEL)) {
            pipeline = new StanfordCoreNLP(props);
            fileGrep.getDisplay().trace("Mood processing pipeline for NLP sentiment filtering is initialised."); 
        } else {
        	if (fileGrep.getSwitches().isVerbose()) {
            	String msg = missingInstallationWarning();
            	this.fileGrep.getDisplay().warn(msg);
        	} else {
        		fileGrep.getDisplay().error("--mood ignored. Re-run with --warn for details.");
        	}
        	enabled = false;
        }
	}

	@Override
	public boolean filterEnabled() {
		return enabled;
	}

	@Override
	public boolean acceptContent(String text) {
		fileGrep.getDisplay().trace("Mood acceptContent filter called for text " + text);
		if ((expectedSentiment == Sentiment.IGNORE) || (pipeline == null)) {
			fileGrep.getDisplay().trace("--mood ignored or no NLP pipeline to process.");
			return true;
		}
		Annotation document = new Annotation(text);
        pipeline.annotate(document);
        int pos = 0, neg = 0, neut = 0;
        
        for (CoreMap sentence : document.get(CoreAnnotations.SentencesAnnotation.class)) {
            if (fileGrep.getSwitches().isTrace()) {
            	fileGrep.getDisplay().trace("Sentence tree: " + sentence.get(SentimentAnnotatedTree.class));
            }
            int words = wordCount(sentence);
            int maxWords = fileGrep.getSwitches().getMaxSentenceWords();
            if (words > maxWords) {
            	String sample = sentenceSample(sentence);
            	fileGrep.getDisplay().debug("Sentence [" + sample + "] not included in mood analysis, contains too many words ('nlp.maxSentenceWords' setting is " + maxWords + ")");
            	continue;
            }
            String sentiment = sentence.get(SentimentCoreAnnotations.SentimentClass.class);
            if (fileGrep.getSwitches().isDebug()) {
            	fileGrep.getDisplay().debug("Sentence [" + sentence.toString() + "]\n  has sentiment (mood): " + sentiment);
            }
            switch (determineSentiment(sentiment)) {
            case NEUTRAL:
            	neut++; break;
            case POSITIVE:
            	pos++; break;
            case NEGATIVE:
            	neg++; break;
            default: break;
            }
        }
		return majority(pos, neg, neut);
	}

	private String sentenceSample(CoreMap sentence) {
		if (sentence == null) {
			return "";
		}
		String s = sentence.toString();
		if (s.length() <= SAMPLE_TEXT_LENGTH) {
			return s;
		}
		return sentence.toString().substring(0, SAMPLE_TEXT_LENGTH) + "...";
	}

	private int wordCount(CoreMap sentence) {
		if (sentence == null) {
			return 0;
		}
		List<CoreLabel> count = sentence.get(CoreAnnotations.TokensAnnotation.class);
		return count == null ? 0 : count.size();
	}

	private boolean majority(int pos, int neg, int neut) {
		if (pos == 0 && neg == 0 && neut == 0) {
			fileGrep.getDisplay().debug("No majority sentiment in mood analysis, matching content will be accepted unconditionally.");
			return true; // no majority, accept unconditionally
		}
		fileGrep.getDisplay().trace("Mood analysis: count of positives: " + pos + ", negatives: " + neg + ", neutrals: " + neut);
		switch (expectedSentiment) {
		case POSITIVE:
			return (pos > neg) && (pos > neut);
		case NEGATIVE:
			return (neg > pos) && (neg > neut);
		case NEUTRAL:
			return (neut >= pos) && (neut >= neg);
		default:
			return true;
		}
	}

	private Sentiment determineSentiment(String s) {
		if (s == null || s.isEmpty()) {
			return Sentiment.NEUTRAL;
		}
		String sen = s.toLowerCase();
		if (sen.contains("negative")) {
			return Sentiment.NEGATIVE;
		} else if (sen.contains("positive")) {
			return Sentiment.POSITIVE;
		} 
		return Sentiment.NEUTRAL;
	}

	String missingInstallationWarning() {
		String home = System.getProperty("crgrep.home");
		if (home != null) {
			// strip out any '..' etc in paths to make it more readable in warning output 
			try {
				Path p = Paths.get(home);
				home = p.normalize().toString();
			} catch (Exception e) {
				// abort attempt to normalise, use home as-is.
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("Mood filtering (--mood) cannot load the required model data contained in 'stanford-corenlp-<version>-models.jar'\n");
		sb.append("Refer to the crgrep document 'INSTALL.txt' for installation details.\n");
		sb.append("   (located in the crgrep home directory '");
		sb.append(PathUtils.normalisedHomeDir()).append("')\n");
		sb.append("Without this data, the --mood option is being ignored.");
		return sb.toString();
	}

	public Sentiment getExpectedSentiment() {
		return expectedSentiment;
	}

	public void setExpectedSentiment(Sentiment expectedSentiment) {
		this.expectedSentiment = expectedSentiment;
	}
}
