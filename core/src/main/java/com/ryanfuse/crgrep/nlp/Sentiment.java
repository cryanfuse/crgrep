/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.nlp;

// Sentiment (mood) detected in matched text using Natural Language Processing (NLP).
// Used with --mood option
//
public enum Sentiment {

	// Combines typical NLP WEAK_POSITIVE and STRONG_POSITIVE values into a simple POSITIVE
	POSITIVE,
	
	// Combines NLP WEAK_NEGATIVE and STRONG_NEGATIVE values into a simple NEGATIVE
	NEGATIVE,
	
	// Neither POSITIVE nor NEGATIVE
	NEUTRAL,
	
	IGNORE  // no mood option specified
}
