/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.nlp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.StringUtils;

/*
 * Examine lines of text before and after the current line to extract the minimum number of
 * complete sentences that include the current line. This will aid the accuracy of subsequent
 * NLP analysis.
 * 
 * The current line may itself consist of multiple sentences in which case that number of sentences 
 * are returned. 
 */
public class SentenceBoundary {

	private String currentLine;
	private List<String> linesBefore;
	private List<String> linesAfter;
	private StanfordCoreNLP pipeline;

	public SentenceBoundary(String currentLine, List<String> linesBefore, List<String> linesAfter) {
		this.currentLine = currentLine;
		this.linesBefore = linesBefore == null ? Collections.emptyList() : linesBefore;
		this.linesAfter = linesAfter == null ? Collections.emptyList() : linesAfter;
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit");
		pipeline = new StanfordCoreNLP(props);
	}

	/*
	 * Attempt to extract the minimum number of complete sentences 'around' the current line.
	 */
	public String getSurroundingSentences() {
		// Trivial - only have the current line
		if (linesBefore.isEmpty() && linesAfter.isEmpty()) {
			return currentLine;
		}
		// current sentence(s)
		List<String> currents = sentences(currentLine);
		if (currents.isEmpty()) {
			// No sentence detected, return the current line unchanged.
			return currentLine;
		}
		String line = currentLine;
		String before = StringUtils.join(linesBefore);
		List<String> befores = sentences(before);
		String after = StringUtils.join(linesAfter);
		List<String> afters = sentences(after);
		// Test with the last sentence before
		if (!befores.isEmpty()) {
			String pre = befores.get(befores.size() - 1) + " " + currentLine;
			List<String> pres = sentences(pre);
			if (pres.size() <= currents.size()) {
				// no increase in sentence count, expand the text to 
				// include the pre text. 
				line = pre;
			}
		}
		// Test with the first sentence after
		if (!afters.isEmpty()) {
			String post = line + " " + afters.get(0);
			List<String> posts = sentences(post);
			if (posts.size() <= currents.size()) {
				// no increase in sentence count, expand the text to
				// include the post text.
				line = post;
			}
		}
		// The most complete sentence(s) inclusive of the current line text
		return line;
	}

	private List<String> sentences(String text) {
		Annotation document = new Annotation(text);
		pipeline.annotate(document);
		List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
		List<String> allSentences = new ArrayList<String>();
	    for (CoreMap sentence : sentences) {
        	//int sBeg = sentence.get(CoreAnnotations.CharacterOffsetBeginAnnotation.class);
        	//int sEnd = sentence.get(CoreAnnotations.CharacterOffsetEndAnnotation.class);
            String sText = sentence.get(CoreAnnotations.TextAnnotation.class);
            allSentences.add(sText);
        }
		return allSentences;
	}
}
