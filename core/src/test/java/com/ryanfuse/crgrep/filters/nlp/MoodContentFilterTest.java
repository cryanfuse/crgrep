/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.filters.nlp;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.nlp.Sentiment;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class MoodContentFilterTest extends TestCommon {

	private static final String POS = "Today is a wonderful sunny summers day under my tree";
	private static final String NEG = "I do not like anything about my tree.";
	private static final String NEUTRAL = "A yard.";
	private static final String TOO_LONG = "Today is a wonderful sunny summers day under my tree "
			+ "and there is an owl up there which I depise as I don't like owls so "
			+ "I think I might call my friends and see if they would like to come over and "
			+ "take the owl home for free so I can enjoy my tree with no owls in it.";
	private MoodContentFilter f;
	private FileGrep fileGrep;
	
	@Before
	public void setUp() throws Exception {
		fileGrep = new FileGrep(new Switches());
		f = new MoodContentFilter(fileGrep, Sentiment.NEUTRAL);
	}

	// This is for testing results from StanfordCoreNLP - used only during development.
	//@Test
	public void testSentence() throws GrepException {
		//edu.stanford.nlp.Document d = new Document();
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		String text = "I had a dream. One day I'll see that dream come true. I'll tell everyone the dream was mine";
		Annotation document = new Annotation(text);
		pipeline.annotate(document);
		printSentences(text, document);
        
		text = "I had\na dream. One day I'll s\nee that dream come true. I'll \ntell everyone the dream was mine\n\n";
        document = new Annotation(text);
        pipeline.annotate(document);
        printSentences(text, document);

		text = "day I'll s\nee that dream of \"1.0 stuff\" come true. I'll tell";
        document = new Annotation(text);
        pipeline.annotate(document);
        printSentences(text, document);

		text = "10 that dream come true. Can";
        document = new Annotation(text);
        pipeline.annotate(document);
        printSentences(text, document);

		text = "a\n bee. Can do \negg. Fair go \"h.i\" jim";
        document = new Annotation(text);
        pipeline.annotate(document);
        printSentences(text, document);
	}

	// This is for testing results from StanfordCoreNLP - used only during development.
	private void printSentences(String text, Annotation document) {
		List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
		System.out.println("---> text [" + text + "]");
		System.out.println("text size: " + text.length() + ", sentence count: " + sentences.size());
        for (CoreMap sentence : sentences) {
        	int sBeg = sentence.get(CoreAnnotations.CharacterOffsetBeginAnnotation.class);
        	int sEnd = sentence.get(CoreAnnotations.CharacterOffsetEndAnnotation.class);
            String sText = sentence.get(CoreAnnotations.TextAnnotation.class);
            System.out.println("   [" + sBeg + ", " + sEnd + "]: " + sText);
        }
	}


	@Test
	public void testMoodFilterPositive() throws GrepException {
		f.setExpectedSentiment(Sentiment.POSITIVE);
		assertTrue("expected text to be positive", f.acceptContent(POS));
		//System.out.println("---> text [" + POS + "] is " + Sentiment.POSITIVE);
	}

	@Test
	public void testMoodFilterNegative() throws GrepException {
		f.setExpectedSentiment(Sentiment.NEGATIVE);
		assertTrue("expected text to be negative", f.acceptContent(NEG));
		//System.out.println("---> text [" + NEG + "] is " + Sentiment.NEGATIVE);
	}

	@Test
	public void testMoodFilterNeutral() throws GrepException {
		f.setExpectedSentiment(Sentiment.NEUTRAL);
		assertTrue("expected text to be neutral", f.acceptContent(NEUTRAL));
		//System.out.println("---> text [" + NEUTRAL + "] is " + Sentiment.NEUTRAL);
	}

	@Test
	public void testMoodFilterIgnore() throws GrepException {
		f.setExpectedSentiment(Sentiment.IGNORE);
		assertTrue("expected text to be accepted (mood is IGNORE)", f.acceptContent("anything"));
	}

	@Test
	public void testMissingDataFiles() throws GrepException {
		System.setProperty("crgrep.home", "c:/");
		String warn = f.missingInstallationWarning();
		assertNotNull(warn);System.out.println(warn);
	}

	@Test
	public void testAcceptContent() throws GrepException {
        title("mood", "tree", "-");
        List<String> expected = Arrays.asList(
                "<stdin>:1:Today is a wonderful sunny summers day under my tree"
                );
        String[] args = new String[] { 
                "-X", "debug,trace",
        		"--mood", "positive",
                "tree",
                "-" };
        InputStream sis = null;
        try {
            sis = IOUtils.toInputStream(POS, "UTF-8");
        } catch (IOException e) {
            fail("Failed to convert POS to InputStream: " + e.getLocalizedMessage());
		}
        // Re-assign stdin to our text
        System.setIn(sis);
        
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testTooManyWordsBypassesAcceptContentFilter() throws GrepException {
        title("mood", "tree", "-");
        List<String> expected = Arrays.asList(
        		"<stdin>:1:Today is a wonderful sunny summers day under my tree and there is an owl up there which I depise as I don't like owls so I think I might call my friends and see if they would like to come over and take the owl home for free so I can enjoy my tree with no owls in it."
        		);
        String[] args = new String[] { 
                //"-X", "debug,trace",
        		"--warn",
        		"--mood", "negative",
                "tree",
                "-" };
        InputStream sis = null;
        try {
            sis = IOUtils.toInputStream(TOO_LONG, "UTF-8");
        } catch (IOException e) {
            fail("Failed to convert TOO_LONG to InputStream: " + e.getLocalizedMessage());
		}
        // Re-assign stdin to our text
        System.setIn(sis);
        
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
        		actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
	}
}
