/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

/**
 * Display wrapper which records results displayed, to aid test result comparisons.
 * 
 * Path conversion to Unix separators combined with sorting of paths (rather 
 * than storing them in chronological order) makes for platform independent
 * testing.
 * 
 * @author Craig Ryan
 */
public class DisplayRecorder extends Display {

	private List<String> msgs = new ArrayList<String>();
	private Display display;
	// By default all logged results are converted to Unix path separators
    private boolean unixPaths = true;
    // Whether to sort the list of paths
    private boolean sortedPaths = true;
    // Whether to string out unreadable chars
    private boolean stripControls = false;
	
    private boolean errorCalled = false;
    private String errorText;
    private boolean warnCalled = false;
    private String warnText;
    private boolean traceCalled = false;
    private String traceText;
    private String filterRegEx;
    private String filterNewText;
    
	public DisplayRecorder(Display display) {
		this.display = display;
	}

	@Override
	public Display result(String msg) {
	    if (stripControls) {
	        msg = msg.replaceAll("[^\\p{ASCII}]", "");
	    }
	    if (filterRegEx != null) {
	        // filter out unknown text, e.g. generated primary keys from result.
	        msg = msg.replaceAll(filterRegEx, filterNewText);
	    }
		String logMsg = msg;
	    if (unixPaths) {
	    	if (display.getPrefix() != null) {
	    		logMsg = display.getPrefix() + msg;
	    	}
	        msg = FilenameUtils.separatorsToUnix(msg);
	        logMsg = FilenameUtils.separatorsToUnix(logMsg);
	    }
		msgs.add(logMsg);
		return display.result(msg);
	}
	
    public List<String> messages() {
	    if (sortedPaths) {
	        Collections.sort(msgs);
	    }
		return msgs;
	}

    /**
     * Determines if results are converted to 
     * @param unixPaths
     */
    public void setUnixPaths(boolean unixPaths) {
        this.unixPaths = unixPaths;
    }

    public void setSortedPaths(boolean sortedPaths) {
        this.sortedPaths = sortedPaths;
    }

    public void setStripControls(boolean strip) {
        this.stripControls = strip;
    }

	public Display warn(String msg) {
	    warnCalled = true;
        warnText = msg; // just keep the latest warning text
		return display.warn(msg);
	}

	public Display error(String msg) {
	    errorCalled = true;
        errorText = msg; // just keep the latest error text
		return display.error(msg);
	}

	public Display trace(String msg) {
	    traceCalled = true;
	    traceText = msg;
		return display.trace(msg);
	}

	public Display debug(String msg) {
		return display.debug(msg);
	}

	public String getPrefix() {
        return display.getPrefix();
    }

    public void unsetPrefix() {
        display.unsetPrefix();
    }

    public void setPrefix(String prefix) {
        display.setPrefix(prefix);
    }

    public String getLogPrefix() {
		return display.getLogPrefix();
	}

	public void setLogPrefix(String prefix) {
		display.setLogPrefix(prefix);
	}

	public boolean isDebugOn() {
		return display.isDebugOn();
	}

	public void setDebugOn(boolean debugOn) {
		display.setDebugOn(debugOn);
	}

	public boolean isTraceOn() {
		return display.isTraceOn();
	}

	public void setTraceOn(boolean traceOn) {
		display.setTraceOn(traceOn);
	}

    public boolean isErrorCalled() {
        return errorCalled;
    }

    public boolean isWarnCalled() {
        return warnCalled;
    }
    
    public boolean isTraceCalled() {
        return traceCalled;
    }
    
    public String getErrorText() {
        return errorText == null ? "" : errorText;
    }
    public String getWarnText() {
        return warnText == null ? "" : warnText;
    }
    public String getTraceText() {
        return traceText == null ? "" : traceText;
    }

    public void filter(String re, String replaceText) {
        this.filterRegEx = re;
        this.filterNewText = replaceText;
    }
}
