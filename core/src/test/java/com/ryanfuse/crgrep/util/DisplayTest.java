/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.reset;

import java.io.PrintStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DisplayTest {

    @Mock private PrintStream psOut;
    @Mock private PrintStream psErr;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testDisplayStreams() {
        Display d = new Display();
        assertEquals(System.out, d.getOutStream());
        assertEquals(System.err, d.getErrStream());
        d.setOutStream(psOut);
        d.setErrStream(psErr);
        assertEquals(psOut, d.getOutStream());
        assertEquals(psErr, d.getErrStream());
    }

    @Test
    public void testDisplayResult() {
        Display d = new Display();
        d.setOutStream(psOut);
        d.setErrStream(psErr);
        d.setLogPrefix(">>");
        assertEquals(d, d.result("oops"));
        verify(psOut).println("oops");
        verify(psErr, times(0)).println(any(String.class));
    }

    @Test
    public void testDisplayError() {
        Display d = new Display();
        d.setOutStream(psOut);
        d.setErrStream(psErr);
        d.setLogPrefix(">>");
        d.setPrefix(">>");
        assertEquals(d, d.error("oops"));
        verify(psErr).println("oops");
        verify(psOut, times(0)).print(any(String.class));
        verify(psOut, times(0)).println(any(String.class));
    }
    
    @Test
    public void testDisplayWarn() {
        Display d = new Display();
        d.setOutStream(psOut);
        d.setErrStream(psErr);
        
        d.setWarnOn(true);
        d.setPrefix(">>");
        d.setLogPrefix(">>");
        assertEquals(d, d.warn("oops"));
        verify(psOut, times(0)).print(any(String.class));
        verify(psOut).println("oops");
        verify(psErr, times(0)).println(any(String.class));        

        reset(psOut);
        d.setWarnOn(false);
        d.setLogPrefix(">>");
        d.setPrefix(">>");
        d.warn("noout");
        verify(psOut, times(0)).print(any(String.class));
        verify(psOut, times(0)).println(any(String.class));
        verify(psErr, times(0)).println(any(String.class));        
    }
    
    @Test
    public void testDisplayDebug() {
        Display d = new Display();
        d.setOutStream(psOut);
        d.setErrStream(psErr);

        d.setDebugOn(true);
        d.setLogPrefix(">>");
        assertEquals(d, d.debug("oops"));
        verify(psOut).print("(>>) ");
        verify(psOut).println("oops");
        verify(psErr, times(0)).println(any(String.class));
        
        reset(psOut);
        d.setDebugOn(true);
        d.setLogPrefix(null);
        d.debug("stuff");
        verify(psOut, times(0)).print(any(String.class));
        verify(psOut).println("stuff");
        verify(psErr, times(0)).println(any(String.class));

        reset(psOut);
        d.setDebugOn(false);
        d.setLogPrefix("log>");
        d.debug("yikes");
        verify(psOut, times(0)).print(any(String.class));
        verify(psOut, times(0)).println(any(String.class));
        verify(psErr, times(0)).println(any(String.class));
    }

    @Test
    public void testDisplayTrace() {
        Display d = new Display();
        d.setOutStream(psOut);
        d.setErrStream(psErr);
        
        d.setTraceOn(true);
        d.setLogPrefix(">>");
        assertEquals(d, d.trace("oops"));
        verify(psOut).print("(>>) ");
        verify(psOut).println("oops");
        verify(psErr, times(0)).println(any(String.class));

        reset(psOut);
        d.setTraceOn(true);
        d.setLogPrefix(null);
        d.trace("yikes");
        verify(psOut, times(0)).print(any(String.class));
        verify(psOut).println("yikes");
        verify(psErr, times(0)).println(any(String.class));

        reset(psOut);
        d.setTraceOn(false);
        d.setLogPrefix(">>");
        d.trace("noout");
        verify(psOut, times(0)).print(any(String.class));
        verify(psOut, times(0)).println(any(String.class));
        verify(psErr, times(0)).println(any(String.class));
    }

    @Test
    public void testDisplaySettings() {
        Display d = new Display();
        d.setDebugOn(true);
        d.setTraceOn(true);
        d.setWarnOn(true);
        assertTrue(d.isDebugOn());
        assertTrue(d.isTraceOn());
        assertTrue(d.isWarnOn());
        d.setPrefix("-->");
        assertEquals("-->", d.getPrefix());
        d.setLogPrefix("log>");
        assertEquals("log>", d.getLogPrefix());
        d.unsetPrefix();
        assertNull(d.getPrefix());
    }
}
