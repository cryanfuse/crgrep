/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * A java application to re-create an Oracle test database on localhost named 'mydb' 
 * and required test tables, user, grants and seeding data.
 * 
 * Assumes Oracle XE is running and root login is "system" / "password". If need
 * be, change DB_ROOT_USER and DB_ROOT_PW accordingly. The JDBC URL sets 
 * SYSTEM_NAME to 'XE' as this is the default database created for an XE installation.
 * Change the URL values to match other, non-XE, Oracle installations as required. 
 *
 * NOTE: you need to add oracle JDBC driver jar (eg ojdbc6.jar which is found under
 * oracle install, for example oracle/app/oracle/product/11.2.0/server/jdbc/lib/ojdbc6.jar
 * OR download from the oracle website.
 *
 * @author 'Craig Ryan'
 */
public class OracleUtils {

    private static final String DB_URL = "jdbc:oracle:thin:@(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521))(ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521))(ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521))(ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521))(LOAD_BALANCE = yes)(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = XE))(FAILOVER_MODE = (TYPE = SELECT)(METHOD = BASIC)(RETRIES = 180)(DELAY = 5)))";
	private static final String DB_USER = "mydb";
	private static final String DB_PW = "password";
	private static final String DB_ROOT_USER = "system";
	private static final String DB_ROOT_PW = "password";

	public static void main(String[] args) {
		// re-create the entire test db
		try {
			dropTestData();
            dropUser();
            createUser();
            createGrants();
			createTestData();
		} catch (SQLException e) {
			System.out.println("Failed to re-create db, msg: " + e.getLocalizedMessage());
		}
		System.out.println("Done.");
	}

	private static void createGrants() throws SQLException {
		System.out.println("Creating grants in database:");
		Connection c = connection(true);
		Statement stmt = c.createStatement();
		String sql = "GRANT CONNECT,RESOURCE,DBA TO mydb";
        stmt.executeUpdate(sql);
		System.out.println("Grants created successfully...");
		c.close();
	}

	private static void createUser() {
		System.out.println("Creating user:");
		try {
			Connection c = connection(true);
			Statement stmt = c.createStatement();
			String sql = "CREATE USER mydb IDENTIFIED BY password";
			stmt.executeUpdate(sql);
			System.out.println("Database user created successfully...");
			c.close();
		} catch (SQLException e) {
			System.out.println("(warn) create user failed: " + e.getLocalizedMessage());
		}
	}

	private static void dropUser() {
		System.out.println("Dropping user:");
		try {
			Connection c = connection(true);
			Statement stmt = c.createStatement();
			String sql = "DROP USER mydb cascade";
			stmt.executeUpdate(sql);
			System.out.println("User dropped successfully...");
			c.close();
		} catch (SQLException e) {
			System.out.println("(warn) drop user failed: " + e.getLocalizedMessage());
		}
	}

	private static void dropTestData() {
		System.out.println("Dropping test data:");
		Connection c = null;
		try {
			c = connection(false);
			Statement stmt = c.createStatement();
			String sql = "DELETE from ALL_BINARY_COLUMNS";
			stmt.executeUpdate(sql);
			sql = "DROP TABLE ALL_BINARY_COLUMNS";
			stmt.executeUpdate(sql);
            sql = "DELETE from CAUSES";
			stmt.executeUpdate(sql);
			sql = "DROP TABLE CAUSES";
            stmt.executeUpdate(sql);
            sql = "DELETE from CATALOG";
            stmt.executeUpdate(sql);
            sql = "DROP TABLE CATALOG";
            stmt.executeUpdate(sql);
            sql = "DELETE from HISTORY";
            stmt.executeUpdate(sql);
            sql = "DROP TABLE HISTORY";
            stmt.executeUpdate(sql);
			System.out.println("Dropped seed data plus tables successfully...");
			c.close();
		} catch (SQLException e) {
			System.out.println("(warn) drop tables failed: " + e.getLocalizedMessage());
		}
	}

	/* 
	 * Database consists of two tables [: columns]
	 * 		ALL_BINARY_COLUMNS: NUM1 (CLOB), NUM2 (CLOB)
	 *      -> no columns types of interest
	 *              1, 2
	 *              12345, 9876
	 * 		CAUSES: ID (INT), NAME (VC50), CAUSE_STUFF (VC20)
	 *      -> table/col names and data both match
	 *              0,cause 1,the cause is mine          
	 *              1,cause 2,also mine          
	 *              2,no match,nada
	 *      HISTORY: EVENT (VC100), author (VC20)
	 *      -> table/col names don't match data
	 *      -> match in one column of a row with diff column in another row and vice versa eg 'ted' and 'cra'
	 *              completed all outstanding tasks!, craig
	 *              crafting new ideas, ted
	 *              More Crazy ideas, Stan
	 *      CATALOG: NAME (VC30), CHUNK (BLOB)
	 *      -> same column name as CAUSES table
	 *              
	 */
	private static void createTestData() throws SQLException {
		System.out.println("Creating test data:");
		Connection c = connection(false);
		Statement stmt = c.createStatement();
		// create tables
		String sql = "CREATE TABLE ALL_BINARY_COLUMNS (NUM1 blob, NUM2 blob)";
		stmt.executeUpdate(sql);
        sql = "CREATE TABLE CAUSES (ID integer, NAME VARCHAR(50), CAUSE_STUFF VARCHAR(20))";
        stmt.executeUpdate(sql);
        // mixed case col names, should create column names as upper case regardless
        sql = "CREATE TABLE HISTORY (EVENT VARCHAR(100), author VARCHAR(20))";
        stmt.executeUpdate(sql);
        sql = "CREATE TABLE CATALOG (NAME VARCHAR(30), CHUNK BLOB)";
        stmt.executeUpdate(sql);
		System.out.println("Database tables created successfully...");
		
		// seed tables
		sql = "INSERT INTO ALL_BINARY_COLUMNS (NUM1, NUM2) VALUES ('1', '2')";
		stmt.executeUpdate(sql);
		sql = "INSERT INTO ALL_BINARY_COLUMNS (NUM1, NUM2) VALUES ('12345', '9876')";
		stmt.executeUpdate(sql);
		sql = "INSERT INTO CAUSES(ID, NAME, CAUSE_STUFF) VALUES (0, 'cause 1', 'the cause is mine')";
		stmt.executeUpdate(sql);
		sql = "INSERT INTO CAUSES(ID, NAME, CAUSE_STUFF) VALUES (1, 'cause 2', 'also mine')";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO CAUSES(ID, NAME, CAUSE_STUFF) VALUES (2, 'no match', 'nada')";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO HISTORY(EVENT, author) VALUES ('completed all outstanding tasks!', 'craig')";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO HISTORY(EVENT, author) VALUES ('crafting new ideas', 'ted')";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO HISTORY(EVENT, author) VALUES ('More Crazy ideas', 'Stan')";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO CATALOG(NAME) VALUES ('cat')";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO CATALOG(NAME, CHUNK) VALUES ('dan', utl_raw.cast_to_raw('a chunk of data'))";
        stmt.executeUpdate(sql);

        System.out.println("Database seed data created successfully...");
		c.close();
	}

	private static Connection connection(boolean asRoot) throws SQLException {
		try {
		    Class.forName ("oracle.jdbc.driver.OracleDriver");
		    if (asRoot) {
		        return DriverManager.getConnection(DB_URL, DB_ROOT_USER, DB_ROOT_PW);
		    }
			return DriverManager.getConnection(DB_URL, DB_USER, DB_PW);
		} catch (ClassNotFoundException e) {
			System.out.println("Failed Oracle connection not found, msg: " + e.getLocalizedMessage());
		} catch (SQLException e) {
			System.out.println("Failed Oracle connection sql, msg: " + e.getLocalizedMessage());
		}
		throw new SQLException("No conneciton");
	}
}
