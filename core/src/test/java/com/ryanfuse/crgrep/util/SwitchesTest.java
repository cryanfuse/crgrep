/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ryanfuse.crgrep.nlp.Sentiment;

@RunWith(MockitoJUnitRunner.class)
public class SwitchesTest {

	private Switches sw;
	
	@Mock private CommandLine cl;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testProcessExtensions() {
		sw = new Switches(cl);
		sw.processExtensions("debug");
		assertTrue(sw.isDebug());
		assertFalse(sw.isTrace());

		sw = new Switches(cl);
		sw.processExtensions("trace");
		assertFalse(sw.isDebug());
		assertTrue(sw.isTrace());

		sw = new Switches(cl);
		sw.processExtensions("trace,debug");
		assertTrue(sw.isDebug());
		assertTrue(sw.isTrace());

		sw = new Switches(cl);
		sw.processExtensions("a,trace,debug,rubbish");
		assertTrue(sw.isDebug());
		assertTrue(sw.isTrace());
	}

	@Test
	public void testGetUserProperties() {
		sw = new Switches(cl);
		Properties p = new Properties();
		sw.setUserProperties(p);
		assertEquals(p, sw.getUserProperties());
		// TODO refactor property processing out of ResourceGrep and
		// add tests here to mock processing of ~/.crgrep settings.
	}

    @Test
    public void testGetMaxResultLength() {
        //when(cl.hasOption("i")).thenReturn(true);
        sw = new Switches(cl);
        sw.setMaxResultLength(null);
        assertNull(sw.getMaxResultLength());
        sw.setMaxResultLength(-10);
        String max = String.valueOf(Integer.MAX_VALUE);
        assertEquals(max, String.valueOf(sw.getMaxResultLength()));
        sw.setMaxResultLength(-1);
        assertEquals(max, String.valueOf(sw.getMaxResultLength()));
        sw.setMaxResultLength(0);
        assertEquals(max, String.valueOf(sw.getMaxResultLength()));
        sw.setMaxResultLength(1);
        assertEquals("1", String.valueOf(sw.getMaxResultLength()));
        sw.setMaxResultLength(123456);
        assertEquals("123456", String.valueOf(sw.getMaxResultLength()));
    }

    @Test
    public void testRemoveLineBreaks() {
        sw = new Switches(cl);
        assertFalse(sw.isRemoveLineBreaks());
        sw.setRemoveLineBreaks(true);
        assertTrue(sw.isRemoveLineBreaks());
    }

    @Test
    public void testOptionsWithoutArgs() {
        sw = new Switches(cl);
		assertFalse(sw.isIgnoreCase());
		assertFalse(sw.isVerbose());
		assertTrue(sw.isListing());
		assertTrue(sw.isContent());
		assertFalse(sw.isOcr());
		assertFalse(sw.isDatabase());
		assertFalse(sw.isMood());
		assertEquals(Sentiment.IGNORE, sw.getSentiment());
		
		when(cl.hasOption('l')).thenReturn(Boolean.TRUE);
		when(cl.hasOption('a')).thenReturn(Boolean.FALSE);
		when(cl.hasOption('r')).thenReturn(Boolean.TRUE);
		sw = new Switches(cl);
		assertTrue(sw.isListing());
		assertFalse(sw.isContent());

		when(cl.hasOption('i')).thenReturn(true);
		when(cl.hasOption("warn")).thenReturn(true);
		when(cl.hasOption('r')).thenReturn(true);
		when(cl.hasOption('l')).thenReturn(true);
		when(cl.hasOption('m')).thenReturn(true);
		when(cl.hasOption('a')).thenReturn(true);
		when(cl.hasOption('d')).thenReturn(true);
		when(cl.hasOption("ocr")).thenReturn(true);
		when(cl.hasOption("mood")).thenReturn(true);
		when(cl.getOptionValue("mood")).thenReturn("positive");
		sw = new Switches(cl);
		assertTrue(sw.isIgnoreCase());
		assertTrue(sw.isVerbose());
		assertTrue(sw.isListing());
		assertTrue(sw.isContent());
		assertTrue(sw.isOcr());
		assertTrue(sw.isDatabase());
		assertTrue(sw.isMood());
	}

    @Test
    public void testSetOptions() {
        new Switches();
        sw = new Switches(cl);
        sw.setAllcontent(true);
        sw.setContent(true);
        sw.setDatabase(true);
        sw.setDebug(true);
        sw.setIgnoreCase(true);
        sw.setListing(true);
        sw.setMaven(true);
        sw.setOcr(true);
        sw.setRecurse(true);
        sw.setTrace(true);
        sw.setVerbose(true);
        sw.setRemoveLineBreaks(true);
        sw.setSentiment(Sentiment.POSITIVE);
    }
}
