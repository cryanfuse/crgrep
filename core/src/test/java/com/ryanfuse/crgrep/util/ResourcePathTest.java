/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

/*
 * Tests the Path handling logic to process resource paths passed as arguments.
 */
public class ResourcePathTest {

    private Display debugDisplay = null;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        debugDisplay = new Display();
        debugDisplay.setDebugOn(true);
        debugDisplay.setTraceOn(true);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testNullPaths() {
        ResourcePath p = new ResourcePath(null, debugDisplay);
        p.setDisplay(null);
        p.setDisplay(debugDisplay);
    }

    @Test
    public void testParentNameCount() {
        ResourcePath p = new ResourcePath("target/resources/monkey-pics.txt", debugDisplay);
        assertEquals(2, p.getParentNameCount());
        p = new ResourcePath("target/resources/does_not_exist", debugDisplay);
        assertEquals(2, p.getParentNameCount());
        p = new ResourcePath("does_not_exist", debugDisplay);
        assertEquals(0, p.getParentNameCount());
    }

    @Test
    public void testExistingFileDoesNotContainsAntGlob() {
        ResourcePath p = new ResourcePath("target/resources/monkey-pics.txt", debugDisplay);
        assertFalse(p.wildPathContainsAntglob());
    }

    @Test
    public void testQuotedPath() {
        ResourcePath p = new ResourcePath("'target/resources/monkey-pics.txt'", debugDisplay);
        assertEquals("target/resources/monkey-pics.txt", p.getFullPath());
        assertEquals("target"+File.separatorChar+"resources", p.getParentPath());
        p = new ResourcePath("\"target/resources/monkey-pics.txt\"", debugDisplay);
        assertEquals("target/resources/monkey-pics.txt", p.getFullPath());
        assertEquals("target"+File.separatorChar+"resources", p.getParentPath());
    }

    @Test
    public void testTextPaths() {
        ResourcePath p = new ResourcePath("C:/root/foo.txt", debugDisplay);
        assertTrue("valid parent", p.hasValidParent());
        assertEquals("getFullPath", "C:/root/foo.txt", p.getFullPath());
        assertEquals("getParentPath", "C:/", p.getParentPath());
        assertEquals("getParentPathCount", 0, p.getParentNameCount());
        assertEquals("getWildPath", "root/foo.txt", p.getWildPath());
        
        p = new ResourcePath("dir/subdir/foo.txt", new Display());
        assertEquals("getFullPath", "dir/subdir/foo.txt", p.getFullPath());
        assertEquals("getParentPath", "", p.getParentPath());
        assertEquals("getParentPathCount", 0, p.getParentNameCount());
        assertEquals("getWildPath", "dir/subdir/foo.txt", p.getWildPath());
    }

    @Test
    public void testDotDotPaths() {
        ResourcePath p = null;
        if (File.separatorChar == '\\') {
            p = new ResourcePath("C:/../foo.txt", debugDisplay);
            assertTrue("valid parent", p.hasValidParent());
            assertEquals("getFullPath", "C:/../foo.txt", p.getFullPath());
            assertEquals("getParentPath", "C:/..", p.getParentPath());
            assertEquals("getParentPathCount", 1, p.getParentNameCount());
            assertEquals("getWildPath", "foo.txt", p.getWildPath());
        } else {        
            p = new ResourcePath("../../.././subdir2/foo.txt", debugDisplay);
            assertEquals("getFullPath", "../../.././subdir2/foo.txt", p.getFullPath());
            assertEquals("getParentPath", "../../../.", p.getParentPath());
            assertEquals("getParentPathCount", 4, p.getParentNameCount());
            assertEquals("getWildPath", "subdir2/foo.txt", p.getWildPath());
        }
    }
    
    @Test
    public void testWildPaths() {
        ResourcePath p = new ResourcePath("target/resources/**/nested_monkey.txt", debugDisplay);
        assertTrue("valid parent", p.hasValidParent());
        assertEquals("getFullPath", "target/resources/**/nested_monkey.txt", p.getFullPath());
        // this path actually exists
        assertEquals("getParentPath", "target"+File.separatorChar+"resources", p.getParentPath());
        assertEquals("getParentPathCount", 2, p.getParentNameCount());
        assertEquals("getWildPath", "**/nested_monkey.txt", p.getWildPath());
    }

    @Test
    public void testTildePaths() {
        ResourcePath p = new ResourcePath("~/foo.txt", debugDisplay);
        if (File.separatorChar == '\\') {
            // tilde is not recognised on Windoze
            assertFalse("valid parent", p.hasValidParent());
            assertTrue("valid wild part", p.hasValidWildPath());
            assertEquals("getFullPath", "~/foo.txt", p.getFullPath());
            assertEquals("getParentPath", "", p.getParentPath());
            assertEquals("getParentPathCount", 0, p.getParentNameCount());
            assertEquals("getWildPath", "~/foo.txt", p.getWildPath());
            
            p = new ResourcePath("~\\../subdir2/foo.txt", debugDisplay);
            assertEquals("getFullPath", "~\\../subdir2/foo.txt", p.getFullPath());
            assertEquals("getParentPath", "", p.getParentPath());
            assertEquals("getParentPathCount", 0, p.getParentNameCount());
            assertEquals("getWildPath", "~/../subdir2/foo.txt", p.getWildPath());
        } else {
        	// tilde requires shell expansion and isn't handled by java 
            assertFalse("valid parent", p.hasValidParent());
            assertTrue("valid wild part", p.hasValidWildPath());
        }
    }
}
