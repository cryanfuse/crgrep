/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PathUtilsTest {

	@Test
	public void normaliseHomeDir() {
		System.setProperty("crgrep.home", "../.");
		String p = PathUtils.normalisedHomeDir();
		assertEquals("..", p);
	}

	@Test
	public void validFilePath() {
		assertFalse(PathUtils.validFilePath(null));
		assertFalse(PathUtils.validFilePath("."));
	}

	@Test
	public void validDirPath() {
		assertFalse(PathUtils.validDirPath(null));
		assertTrue(PathUtils.validDirPath("."));
	}
}
