/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class HighlightTest {

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testStartEnd() {
        Highlight h = new Highlight(null, null);
        String start = String.format(Highlight.HIGHLIGHT_START, h.getColour());
        assertEquals(start, h.getStart());
        assertEquals(Highlight.HIGHLIGHT_END, h.getEnd());
    }

    @Test
    public void testSetColour() {
        Highlight h = new Highlight(null, null);
        String defaultCol = Highlight.colourCode(Highlight.ATTR_BRIGHT, Highlight.FG_RED);
        assertEquals(defaultCol, h.getColour());
        // valid words
        h = new Highlight(null, "cyan");
        assertEquals(Integer.toString(Highlight.FG_CYAN), h.getColour());
        h = new Highlight(null, "MAGENTA");
        assertEquals(Integer.toString(Highlight.FG_MAGENTA), h.getColour());
        h = new Highlight(null, "dim grEEN");
        assertEquals(Highlight.colourCode(Highlight.ATTR_DIM, Highlight.FG_GREEN), h.getColour());
        h = new Highlight(null, "RESET white");
        assertEquals(Highlight.colourCode(Highlight.ATTR_RESET, Highlight.FG_WHITE), h.getColour());
        h = new Highlight(null, "bold black");
        assertEquals(Highlight.colourCode(Highlight.ATTR_BRIGHT, Highlight.FG_BLACK), h.getColour());
        h = new Highlight(null, "underline black");
        assertEquals(Highlight.colourCode(Highlight.ATTR_UNDERLINE, Highlight.FG_BLACK), h.getColour());
        h = new Highlight(null, "blink blue");
        assertEquals(Highlight.colourCode(Highlight.ATTR_BLINK, Highlight.FG_BLUE), h.getColour());
        h = new Highlight(null, "reverse yellow");
        assertEquals(Highlight.colourCode(Highlight.ATTR_REVERSE, Highlight.FG_YELLOW), h.getColour());
        h = new Highlight(null, "hidden red");
        assertEquals(Highlight.colourCode(Highlight.ATTR_HIDDEN, Highlight.FG_RED), h.getColour());
        // invalid words
        h = new Highlight(null, "hot pinkish blue");
        assertEquals(defaultCol, h.getColour());
        h = new Highlight(null, "red bold");
        assertEquals(defaultCol, h.getColour());
        h = new Highlight(null, "brown");
        assertEquals(defaultCol, h.getColour());
        h = new Highlight(null, "");
        assertEquals(defaultCol, h.getColour());

        // valid numerics (not checked, accepted as-is).
        h = new Highlight(null, "01;31");
        assertEquals("01;31", h.getColour());
        h = new Highlight(null, "2;34");
        assertEquals("2;34", h.getColour());
        h = new Highlight(null, "01;31;44");
        assertEquals("01;31;44", h.getColour());
        h = new Highlight(null, "35");
        assertEquals(Integer.toString(Highlight.FG_MAGENTA), h.getColour());
        // invalid numerics (not checked, accepted as-is).
        h = new Highlight(null, "77;88");
        assertEquals("77;88", h.getColour());
        h = new Highlight(null, "1-31");
        assertEquals(defaultCol, h.getColour());
        h = new Highlight(null, ";1");
        assertEquals(defaultCol, h.getColour());
    }

    @Test
    public void testIsHighlighting() {
        Highlight h = new Highlight("always", null);
        assertTrue(h.isHighlighting());
        h = new Highlight("auto", null);
        String t = System.getenv("TERM");
        // test is sensitive to TERM, highlighting is disabled for dumb.
        if (t != null && t.equals("dumb")) {
        	assertFalse(h.isHighlighting());
        } else {
        	assertTrue(h.isHighlighting());
        }
        h = new Highlight("never", null);
        assertFalse(h.isHighlighting());
        // invalid cases
        h = new Highlight(null, null);
        assertFalse(h.isHighlighting());
        h = new Highlight("", null);
        assertFalse(h.isHighlighting());
        h = new Highlight("tuedays", null);
        assertFalse(h.isHighlighting());
    }

}
