/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import com.ryanfuse.crgrep.ResourcePattern;

public class GrepMatcherTest {

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetMatcherResourcePatternSwitches() {
        ResourcePattern rp = new ResourcePattern("*.txt");
        Switches sw = new Switches();
        GrepMatcher gm = GrepMatcher.getMatcher(rp, sw);
        assertEquals("", gm.toString());
        assertNull(gm.getMatcher());
    }

    @Test
    public void testGetMatcherStringResourcePatternSwitches() {
        ResourcePattern rp = new ResourcePattern("*.txt");
        Switches sw = new Switches();
        GrepMatcher gm = GrepMatcher.getMatcher("text", rp, sw);
        assertNotNull(gm.toString());
        assertNotNull(gm.getMatcher());
        assertEquals("text", gm.toString());
        assertFalse(gm.matches());
    }

    @Test
    public void testMatchingStrings() {
        Switches sw = new Switches();
        assertTrue(GrepMatcher.matchingStrings("text", "text", sw, false));
    }

    @Test
    public void testMatchingPattern() {
        Switches sw = new Switches();
        assertTrue(GrepMatcher.matchingPattern("text", "te.t", sw, false));
    }

    @Test
    public void testMatches() {
        ResourcePattern rp = new ResourcePattern("*.blob");
        Switches sw = new Switches();
        GrepMatcher gm = GrepMatcher.getMatcher("text", rp, sw);
        assertFalse(gm.matches());
        gm.setText("goo.blob");
        assertTrue(gm.matches());
        assertEquals("goo.blob", gm.toString());
    }

    @Test
    public void testToString() {
        ResourcePattern rp = new ResourcePattern("col");
        Switches sw = new Switches();
        GrepMatcher gm = GrepMatcher.getMatcher("tab: col1, col2", rp, sw);
        assertEquals("tab: col1, col2", gm.toString());
        assertEquals("tab: col1, col2", gm.toString(true));
        assertEquals("tab: [cols-col2]", gm.toString("tab: [cols-col2]"));
        assertEquals("tab: [cols-col2]", gm.toString("tab: [cols-col2]", true));
    }
}