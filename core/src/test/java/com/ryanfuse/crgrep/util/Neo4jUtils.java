/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.util.HashMap;
import java.util.Map;

import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.rest.graphdb.RestGraphDatabase;
import org.neo4j.rest.graphdb.query.RestCypherQueryEngine;
import org.neo4j.rest.graphdb.util.QueryResult;

/**
 * A java application to re-create a Neo4J test graph database on localhost named 'mydb' 
 * and required test nodes and seeding data.
 * 
 * Assumes neo4j server is running and root login is "root" / "password". If need
 * be, change DB_ROOT_USER and DB_ROOT_PW accordingly.
 * 
 * Uses this useful REST wrapper: https://github.com/neo4j-contrib/java-rest-binding
 *
 * @author 'Craig Ryan'
 */
public class Neo4jUtils {

	private static final String DB_ROOT_URL = "http://localhost:7474/db/data";
	private static final String DB_ROOT_USER = "neo4j";
	private static final String DB_ROOT_PW = "password";

    private static GraphDatabaseService graphdb;
    private static RestCypherQueryEngine engine; // ExecutionEngine does NOT work with rest wrapper
    
	public static void main(String[] args) {
		//System.setProperty("org.neo4j.rest.logging_filter", "true");
	    graphdb = new RestGraphDatabase(DB_ROOT_URL, DB_ROOT_USER, DB_ROOT_PW);
	    engine = new RestCypherQueryEngine(((RestGraphDatabase)graphdb).getRestAPI());
	    
	    
		// re-create the entire test graph
		try {
			dropTestData();
			dropTestDatabase();
			createTestDatabase();
			createUser();
			createTestData();
			createRelationships();
			allRelationships();
		} catch (Exception e) {
			System.out.println("Failed to re-create graph db, msg: " + e.getLocalizedMessage());
		} finally {
	          graphdb.shutdown();
		}
		System.out.println("Done.");
	}

	private static void createUser() {
		System.out.println("Creating user:");
		try {
			System.out.println("Database user created successfully...");
		} catch (Exception e) {
			System.out.println("(warn) create user failed: " + e.getLocalizedMessage());
		}
	}

	private static void dropTestDatabase() {
		System.out.println("Dropping test database:");
		try {
			System.out.println("Database dropped successfully...");
		} catch (Exception e) {
			System.out.println("(warn) drop database failed: " + e.getLocalizedMessage());
		}
	}

	private static void createTestDatabase() throws Exception {
		System.out.println("Creating test database:");
		System.out.println("Database created successfully...");
	}

	private static void dropTestData() {
		System.out.println("Dropping test data:");
        Transaction tx = graphdb.beginTx();
		try {
            engine.query("MATCH (n:CAUSES) DELETE n", null);
            engine.query("MATCH (n:HISTORY) DELETE n", null);

            // DETACH is Neo4j 2.3+ syntax
            engine.query("MATCH (n:CATALOG) DETACH DELETE n", null);
            // Pre Neo4j 2.3 syntax:
            //engine.query("MATCH (n:CATALOG) optional match n-[r]-() DELETE n,r", null);

            engine.query("MATCH (n:MIXED) DELETE n", null);
			System.out.println("Dropped seed data plus tables successfully...");
			tx.success();
		} catch (Exception e) {
		    tx.close();
			System.out.println("(warn) drop tables failed: " + e.getLocalizedMessage());
		}
	}

	/* 
	 * Database consists of these nodes [:properties]
	 * 		MIXED: BYTE1 (BYTE), BYTEARR1 (BYTE array), INT1 (int), INTARR1 (int array), BOOL1 (bool), BOOLARR1 (bool array)
	 * 		CAUSES: ID (INT), NAME (String), CAUSE_STUFF (String)
	 *      HISTORY: EVENT (String), author (String)
	 *      CATALOG: NAME (String), CHUNK (Byte array)
	 *              
	 */
	private static void createTestData() throws Exception {
		System.out.println("Creating test data:");
        Transaction tx = graphdb.beginTx();
        //BatchTransaction bt;
        try {
	        Label lab = DynamicLabel.label("MIXED");
	        byte byteArr[] = "a random string".getBytes();
	        int[] intArr = {1, 2, 3};
	        char[] charArr = {'a', 'b', 'c'};
	        long[] longArr = {1L, 4L, 8L};
	        double[] dblArr = {1.0, 2.0, 4.0};
	        float[] fltArr = {1, 2, 4};
	        boolean[] boolArr = {true, false, true};
	        String[] strArr = new String[3];
            strArr[0] = "one";
            strArr[1] = "two";
            strArr[2] = "three";
	        Node all = graphdb.createNode(lab);
	        all.setProperty("mxbyte", 156);
            all.setProperty("mxbytearr", byteArr);
            all.setProperty("mxint", 1);
            all.setProperty("mxintArr", intArr);
            all.setProperty("mxlong", 2L);
            all.setProperty("mxlongArr", longArr);
            all.setProperty("mxchar", 'a');
            all.setProperty("mxcharArr", charArr);
            all.setProperty("mxstr", "hello");
            all.setProperty("mxstrArr", strArr);
            all.setProperty("mxflt", 0.1234E02);
            all.setProperty("mxfltArr", fltArr);
            all.setProperty("mxdbl", 1.12345E2);
            all.setProperty("mxdblArr", dblArr);
            all.setProperty("mxbool", true);
            all.setProperty("mxboolArr", boolArr);
            
            lab = DynamicLabel.label("CAUSES");
	        Node cause = graphdb.createNode(lab);
            cause.setProperty("cause_stuff", "the cause is mine");
            cause.setProperty("name", "cause 1");
            cause = graphdb.createNode(lab);
            cause.setProperty("cause_stuff", "also mine");
            cause.setProperty("name", "cause 2");
            cause = graphdb.createNode(lab);
            cause.setProperty("cause_stuff", "nada");
            cause.setProperty("name", "no match");

            lab = DynamicLabel.label("HISTORY");
            Node history = graphdb.createNode(lab);
            history.setProperty("EVENT", "completed all outstanding tasks!");
            history.setProperty("AUTHOR", "craig");
            history = graphdb.createNode(lab);
            history.setProperty("EVENT", "crafting new ideas");
            history.setProperty("Author", "ted");
            history = graphdb.createNode(lab);
            history.setProperty("EVENT", "More Crazy ideas");
            history.setProperty("author", "Stan");

            lab = DynamicLabel.label("CATALOG");
            Node catalog = graphdb.createNode(lab);
            catalog.setProperty("name", "cat");
            catalog.setProperty("chunk", null);
            catalog = graphdb.createNode(lab);
            catalog.setProperty("name", "garfield");
            catalog = graphdb.createNode(lab);
            catalog.setProperty("name", "dan");
            catalog.setProperty("chunk", Byte.MAX_VALUE);
            catalog = graphdb.createNode(lab);
            catalog.setProperty("name", "dog");
	        tx.success();
		} catch (Exception ex) {
		    ex.printStackTrace();
		    System.out.println(ex.getLocalizedMessage());
		}

        System.out.println("Database seed data created successfully...");
	}

	private static void createRelationships() {
		System.out.println("Creating relationships:");
        Transaction tx = graphdb.beginTx();
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("aname", "cat");
			params.put("bname", "dan");
            engine.query(
            		"MATCH (a:CATALOG),(b:CATALOG) WHERE a.name = {aname} AND b.name = {bname} CREATE (a)-[r:CAT_OF { relname : a.name + '<->' + b.name }]->(b)", 
            		params);
			params.clear();
			params.put("aname", "garfield");
			params.put("bname", "dan");
            engine.query(
            		"MATCH (a:CATALOG),(b:CATALOG) WHERE a.name = {aname} AND b.name = {bname} CREATE (a)-[r:CAT_OF { relname : a.name + '<->' + b.name }]->(b)", 
            		params);
			params.clear();
			params.put("aname", "dog");
			params.put("bname", "dan");
            engine.query(
            		"MATCH (a:CATALOG),(b:CATALOG) WHERE a.name = {aname} AND b.name = {bname} CREATE (a)-[r:DOG_OF { relname : a.name + '<->' + b.name }]->(b)", 
            		params);
			System.out.println("Created CATALOG relationships...");
			tx.success();
		} catch (Exception e) {
		    tx.close();
			System.out.println("(warn) creating relationships failed: " + e.getLocalizedMessage());
		}
	}

	private static String allRelationships() {
        Transaction tx = graphdb.beginTx();
        try {
            QueryResult<Map<String, Object>> res = engine.query("MATCH ()-[r]-() RETURN DISTINCT r", null);
            for (Map<String, Object> resm : res) {
            	for (String key : resm.keySet()) { // key is 'r'
            		Relationship rship = (Relationship) resm.get(key);
            		System.out.println("rel key [" + key + "] id: " + rship.getId());
            		String rname = rship.getType().name();
            		System.out.println("  - relTypeName [" + rname + "]");
            		Iterable<String> propKeys = rship.getPropertyKeys();
            		for (String pk : propKeys) {
            			System.out.println("    - prop [" + pk + "]");
            			Object pv = rship.getProperty(pk);
            			System.out.println("       - val [" + pv + "] type " + pv.getClass().getName());
            		}
            	}
            }
            tx.success();
            return res.toString();
        } finally {
        }
    }
	/*
	*/
	
	/*
	MATCH (a:Person),(b:Person)
	WHERE a.name = 'Node A' AND b.name = 'Node B'
	CREATE (a)-[r:RELTYPE { name : a.name + '<->' + b.name }]->(b)
	RETURN r
	
    private String executeCypher( String cypher ) {
        Transaction tx = graphdb.beginTx();
        try {
            QueryResult<Map<String, Object>> executionResult = engine.query(cypher, null);
            tx.success();
            return executionResult.toString();
        } finally {
        }
    }
	*/
}

