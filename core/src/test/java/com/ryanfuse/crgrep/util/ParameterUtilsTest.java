/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class ParameterUtilsTest {

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testStripQuotes() {
        String p = "abc de";
        assertEquals("abc de",ParameterUtils.stripQuotes(p));
        p = "'abc de";
        assertEquals("'abc de",ParameterUtils.stripQuotes(p));
        p = "'ab.cd'";
        assertEquals("ab.cd",ParameterUtils.stripQuotes(p));
        p = "\"ab.cd";
        assertEquals("\"ab.cd",ParameterUtils.stripQuotes(p));
        p = "\"ab.cd\"";
        assertEquals("ab.cd",ParameterUtils.stripQuotes(p));
        p = "\"";
        assertEquals("\"",ParameterUtils.stripQuotes(p));
        p = "'";
        assertEquals("'",ParameterUtils.stripQuotes(p));
        p = "'";
        assertEquals("'",ParameterUtils.stripQuotes(p));
        p = "''";
        assertEquals("",ParameterUtils.stripQuotes(p));
    }

    @Test
    public void testStripListQuotes() {
        List<String> p = Arrays.asList("");
        List<String> ex = Arrays.asList("");
        assertEquals(ex,ParameterUtils.stripQuotes(p));
        p = Arrays.asList("abc de");
        ex = Arrays.asList("abc de");
        assertEquals(ex,ParameterUtils.stripQuotes(p));
        p = Arrays.asList("abc de", "'a", "'aa cc'", "\"bb c", "\"axx\"", "\"", "'", "''", "");
        ex = Arrays.asList("abc de", "'a", "aa cc", "\"bb c", "axx", "\"", "'", "", "");
        assertEquals(ex, ParameterUtils.stripQuotes(p));
    }
}
