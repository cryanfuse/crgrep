/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import static org.junit.Assert.*;

import java.nio.file.Paths;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class ResourcePathsTest {

    private ResourcePath path;
    private ResourcePaths paths;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        Switches sw = new Switches();
        sw.setDebug(true);
        paths = new ResourcePaths(sw, new Display());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testSimple() {
//        path = paths.get(".");
//        assertTrue(path.hasValidParent());
//        path = paths.get("./");
//        assertTrue(path.hasValidParent());
//        path = paths.get("..");
//        assertTrue(path.hasValidParent());
//        path = paths.get("../");
//        assertTrue(path.hasValidParent());
        path = paths.get("~foo");
        //assertTrue(path.hasValidParent());
    }

    @Test
    public void testAbsolute() {
    }

    @Test
    public void testRelative() {
    }
}
