/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.nlp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class SentenceBoundaryTest {

	private SentenceBoundary sb;
	
	@Test
	public void nullSentenceText() {
		sb = new SentenceBoundary(null, null, null);
		assertNull(sb.getSurroundingSentences());
	}

	@Test
	public void noLinesBeforeOrAfter() {
		sb = new SentenceBoundary("a sentence", null, null);
		assertEquals("a sentence", sb.getSurroundingSentences());
		sb = new SentenceBoundary("a sentence......", null, null);
		assertEquals("a sentence......", sb.getSurroundingSentences());
		sb = new SentenceBoundary(".a sentence", null, null);
		assertEquals(".a sentence", sb.getSurroundingSentences());
		sb = new SentenceBoundary("A sentence. Another sentence.", null, null);
		assertEquals("A sentence. Another sentence.", sb.getSurroundingSentences());
	}

	@Test
	public void noLinesAfter() {
		List<String> before = new ArrayList<String>();
		before.add("First sentence.");
		before.add("Second sentence. And another.");
		sb = new SentenceBoundary("A sentence", before, null);
		assertEquals("A sentence", sb.getSurroundingSentences());

		before.add("Third sentence. And a fourth which doesn't runs onto next line");

		before.clear();
		before.add("I can see the sea ");
		sb = new SentenceBoundary("from my bedroom window.", before, null);
		assertEquals("I can see the sea from my bedroom window.", sb.getSurroundingSentences());
	}

	@Test
	public void linesBothBeforeAndAfter() {
		List<String> before = new ArrayList<String>();
		before.add("First sentence.");
		before.add("Second sentence. And another.");
		List<String> after = new ArrayList<String>();
		after.add("First after sentence.");
		after.add("Second after sentence. And another after.");
		
		sb = new SentenceBoundary("A sentence.", before, after);
		assertEquals("A sentence.", sb.getSurroundingSentences());

		before.add("Third sentence. And a fourth which doesn't runs onto next line");

		// After should be excluded.
		before.clear();
		before.add("I can see the sea ");
		after.clear();
		after.add("What a view.");
		sb = new SentenceBoundary("from my bedroom window.", before, after);
		assertEquals("I can see the sea from my bedroom window.", sb.getSurroundingSentences());

		// After should be included.
		after.clear();
		after.add("and what a view.");
		sb = new SentenceBoundary("from my bedroom window", before, after);
		assertEquals("I can see the sea from my bedroom window and what a view.", sb.getSurroundingSentences());
	}
}
