/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.nlp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Test;

public class SentimentTest {

	@Test
	public void valueOfIllegal() {
		try {
			assertNull(Sentiment.valueOf("foo"));
			fail("expected illegal arg exception");
		} catch (IllegalArgumentException e) {
			// expected
		}
	}

	@Test
	public void valueOf() {
		assertEquals(Sentiment.IGNORE, Sentiment.valueOf("IGNORE"));
		assertEquals(Sentiment.POSITIVE, Sentiment.valueOf("POSITIVE"));
		assertEquals(Sentiment.NEGATIVE, Sentiment.valueOf("NEGATIVE"));
		assertEquals(Sentiment.NEUTRAL, Sentiment.valueOf("NEUTRAL"));
	}
}
