/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.Permission;
import java.security.PermissionCollection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;

import com.ryanfuse.crgrep.util.DisplayRecorder;

/**
 * Common test methods to display test output and identify resource list differences.
 * 
 * @author Craig Ryan
 */
public class TestCommon {
	
	protected DisplayRecorder display;
	private static Map<Object, Object> permissionMap = new HashMap<Object, Object>();
	
	protected void title(String test, String pat, String res) {
		System.out.println("------------------- " + test + " pattern: [" + pat + "] resource: [" + res + "]");
	}

	// 'pwd' for crgrep project
	public static String projectDir() {
		return System.getProperty("user.dir").replaceAll("\\\\", "/");
	}
	
	public static String getUserHome() {
		return System.getProperty("user.home").replaceAll("\\\\", "/");
	}
	
	/*
	 * Print out the actual list in a readable format and any differences
	 * between actual/expected lists.
	 */
	public String actualList(List<String> res, List<String> expected) {
		StringBuffer sb = new StringBuffer("Unexpected result, actual list entries:\n");
		for (String l : res) {
			sb.append(l).append("\n");
		}
        List<String> diff;
        List<String> diff2 = null;
		boolean showNonAscii = false;
		sb.append("-----------------------------------------------------------------\n");
		if ((expected != null) && (res.size() != expected.size())) {
			sb.append("Actual size [" + res.size() + "]  expected size [" + expected.size() + "]\n");
			if (res.containsAll(expected)) { 
				// too many actual results
				sb.append("Too many actual results, extra entries are:\n");
				diff = ListUtils.subtract(res, expected);
			} else {
				// too few results
				sb.append("Too few actual results, extra expected entries are:\n");
				diff = ListUtils.subtract(expected, res);
			}
		} else {
		    sb.append("Actual == expected size, contents must be different! Actual entries not matching:\n");
			diff = ListUtils.subtract(res, expected);
			diff2 = ListUtils.subtract(expected, res);
			showNonAscii = true;
		}
		if (diff == null || diff.isEmpty()) {
			sb.append("<empty list> - hint: checking ordering of results\n");
			int sz = res.size();
			// find first order mismatch
			for (int i = 0; i < sz; i++) {
                String actual = res.get(i);
                String expect = expected.get(i);
                if (!StringUtils.equals(actual, expect)) {
                    sb.append("First line difference detected (").append(i+1).append("): Expected\n").append(expect);
                    sb.append("\nActual:\n").append(actual).append("\n");
                    break;
                }
			}
		} else {
            for (String l : diff) {
                if (showNonAscii) {
                    l = l.replaceAll("\n", "\\\\n").replaceAll("\r", "\\\\r").replaceAll("\t", "\\\\t");
                    sb.append("[");
                }
                sb.append(l);
                if (showNonAscii) {
                    sb.append("]");
                }
                sb.append("\n");
            }
			if (diff2 != null) {
			    // Reverse diff of expected list
	            sb.append("Expected entries not matching:\n");
	            for (String l : diff2) {
	                if (showNonAscii) {
	                    l = l.replaceAll("\n", "\\\\n").replaceAll("\r", "\\\\r").replaceAll("\t", "\\\\t");
	                    sb.append("[");
	                }
	                sb.append(l);
	                if (showNonAscii) {
	                    sb.append("]");
	                }
	                sb.append("\n");
	            }
			}
		}
		return sb.toString();
	}

	// Used to update unit tests with ready to insert expected result lists
	public void dumpActualList(String filename, List<String> actual) {
		File f = new File(filename);
		try {
			FileWriter fw = new FileWriter(f, false);
			for (String s: actual) {
				String escQuote = s.replaceAll("\"", "\\\\\"");
				fw.write("        \"" + escQuote + "\",\n");
			}
			fw.close();
		} catch (IOException e) {
		}
	}

    /*
     * Hack to disable JCE if not already disabled.
     */
    public static void removeCryptographyRestrictions() {
        if (!isRestrictedCryptography()) {
            System.out.println("RestrictedCryptography false, removeCryptographyRestrictions() has nothing to do.");
            return;
        }
        try {
            /*
             * Do the following, but with reflection to bypass access checks:
             *
             * JceSecurity.isRestricted = false;
             * JceSecurity.defaultPolicy.perms.clear();
             * JceSecurity.defaultPolicy.add(CryptoAllPermission.INSTANCE);
             */
            final Class<?> jceSecurity = Class.forName("javax.crypto.JceSecurity");
            final Class<?> cryptoPermissions = Class.forName("javax.crypto.CryptoPermissions");
            final Class<?> cryptoAllPermission = Class.forName("javax.crypto.CryptoAllPermission");

            final Field isRestrictedField = jceSecurity.getDeclaredField("isRestricted");
            isRestrictedField.setAccessible(true);
            boolean restricted = isRestrictedField.getBoolean(null);
            if (!restricted) {
                // done, JCE must be installed.
                return;
            }
            isRestrictedField.set(null, false);

            final Field defaultPolicyField = jceSecurity.getDeclaredField("defaultPolicy");
            defaultPolicyField.setAccessible(true);
            final PermissionCollection defaultPolicy = (PermissionCollection) defaultPolicyField.get(null);

            final Field perms = cryptoPermissions.getDeclaredField("perms");
            perms.setAccessible(true);
            //(Map<?, ?>)perms.get(defaultPolicy);
            Map<?, ?> ps = (Map<?, ?>)perms.get(defaultPolicy);
            permissionMap.putAll(ps);
            ps.clear();

            final Field instance = cryptoAllPermission.getDeclaredField("INSTANCE");
            instance.setAccessible(true);
            Permission p = (Permission) instance.get(null);
            defaultPolicy.add(p);
        } catch (final Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    /*
     * Hack to enable JCE if not already disabled.
     */
    public static void enableCryptographyRestrictions() {
        try {
            final Class<?> jceSecurity = Class.forName("javax.crypto.JceSecurity");
            final Class<?> cryptoPermissions = Class.forName("javax.crypto.CryptoPermissions");
            final Class<?> cryptoAllPermission = Class.forName("javax.crypto.CryptoAllPermission");

            final Field isRestrictedField = jceSecurity.getDeclaredField("isRestricted");
            isRestrictedField.setAccessible(true);
            boolean restricted = isRestrictedField.getBoolean(null);
            if (restricted) {
                // done, JCE must not be installed.
                return;
            }
            isRestrictedField.set(null, true);

            final Field defaultPolicyField = jceSecurity.getDeclaredField("defaultPolicy");
            defaultPolicyField.setAccessible(true);
            final PermissionCollection defaultPolicy = (PermissionCollection) defaultPolicyField.get(null);

            final Field perms = cryptoPermissions.getDeclaredField("perms");
            perms.setAccessible(true);
            Map<Object, Object> permsMap = (Map<Object, Object>)perms.get(defaultPolicy);
            permsMap.putAll(permissionMap);

            // There is no remove method on PermissionCollection
            //final Field instance = cryptoAllPermission.getDeclaredField("INSTANCE");
            //instance.setAccessible(true);
            //Permission p = (Permission) instance.get(null);
            //defaultPolicy.remove(p);
        } catch (final Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public static boolean isRestrictedCryptography() {
        // This simply matches the Oracle JRE, but not OpenJDK.
        String rname = System.getProperty("java.runtime.name");
        if (rname == null) {
            return false;
        }
        return "Java(TM) SE Runtime Environment".equals(rname);
    }
}
