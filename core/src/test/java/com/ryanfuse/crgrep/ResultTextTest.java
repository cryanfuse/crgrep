/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ryanfuse.crgrep.util.Highlight;
import com.ryanfuse.crgrep.util.Switches;

public class ResultTextTest {

	private static final String DDD = "...";
	private ResourcePattern patStartWild = new ResourcePattern("*PATTERN");
	private ResourcePattern patEndWild = new ResourcePattern("PATTERN*");
	private ResourcePattern patFixed = new ResourcePattern("PATTERN");

	private String t1 = "x";
	private String t2 = "xy";
	private String t3 = "xyz";
	private String t4 = "xyza";
	private String tshort = "a small result";
	
	private String tpatStart = "PATTERN which starts our string";
	private String tpatEnd = "a result which matches a PATTERN";
	private String tpatMid = "a result which matches a PATTERN in the middle of it";
	
    private String tlong = "xy string with lots of stuff in it which will match more than the expected results " + 
            "will ever be and we want to trim this text to something smaller to display on one line";
    private String tlongHighlightWi = "xy string >>wi<<th lots of stuff in it which >>wi<<ll match more than the expected results " + 
            ">>wi<<ll ever be and we want to trim this text to something smaller to display on one line";
    private String tlongHighlightSx = "xy [st]ring with lot[s ]of [st]uff in it which will match more than the expected re[su]lt[s ]" + 
            "will ever be and we want to trim thi[s ]text to [so]mething [sm]aller to di[sp]lay on one line";
	private String tmultilines = "xy string\nwith lots of stuff\r\nin it which will\rmatch more than the expected results\n"; 
	private String tmultilines_stripped = "xy string\\nwith lots of stuff\\nin it which will\\nmatch more than the expected results\\n"; 

	private Switches switches = new Switches();
	private ResourcePattern rpattern = new ResourcePattern("*");
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testResultStringDefaults() {
	    switches.setMaxResultLength(ResultText.DEFAULT_MAX_DATA_VALUE_LENGTH);
	    ResultText rt = new ResultText(rpattern, switches);
		assertEquals(ResultText.DEFAULT_MIN_DATA_VALUE_PREFIX, rt.getMinPrefixIndex());
        assertEquals(ResultText.DEFAULT_MAX_DATA_VALUE_LENGTH, rt.getMaxResultLength());
		switches.setMaxResultLength(null);
		rt = new ResultText(rpattern, switches);
        assertEquals(ResultText.DEFAULT_MAX_DATA_VALUE_LENGTH, rt.getMaxResultLength());
		assertFalse(rt.isRemoveLineBreaks());
		rt.setRemoveLineBreaks(true);
		assertTrue(rt.isRemoveLineBreaks());
	}

	@Test
	public void testResultStringMinMax() {
		// 0, 0 - max will be set to unlimited
        switches.setMaxResultLength(0);
        ResultText rt = new ResultText(rpattern, switches);
		String r = rt.result("");
		assertEquals("", r);
		r = rt.result(t1);
		assertEquals(t1, r);
		r = rt.result(t2);
		assertEquals(t2, r);
		r = rt.result(t3);
		assertEquals(t3, r);
		r = rt.result(tshort);
		assertEquals(tshort, r);
		r = rt.result(tlong);
		assertEquals(tlong, r);
		
		// 0, 1 - both used as-is
        switches.setMaxResultLength(1);
        rt = new ResultText(rpattern, switches);
		r = rt.result("");
		assertEquals("", r);
		r = rt.result(t1);
		assertEquals(t1, r);
		r = rt.result(t2);
		assertEquals(t1+DDD, r);
		r = rt.result(t3);
		assertEquals(t1+DDD, r);
		r = rt.result(tshort);
		assertEquals(tshort.substring(0, 1)+DDD, r);
		r = rt.result(tlong);
		assertEquals(tlong.substring(0, 1)+DDD, r);
		
		// 1, 1 max will be set to 1
        switches.setMaxResultLength(1);
        rt = new ResultText(rpattern, switches);
        rt.setMinPrefixIndex(1);
		r = rt.result("");
		assertEquals("", r);
		r = rt.result(t1);
		assertEquals(t1, r);
		r = rt.result(t2);
		assertEquals(t1+DDD, r);
		r = rt.result(t3);
		assertEquals(t1+DDD, r);
		r = rt.result(tshort);
		assertEquals(tshort.substring(0, 1)+DDD, r);
		r = rt.result(tlong);
		assertEquals(tlong.substring(0, 1)+DDD, r);

		// 1, -1 max will be set to unlimited
        switches.setMaxResultLength(-1);
        rt = new ResultText(rpattern, switches);
        rt.setMinPrefixIndex(1);
		r = rt.result("");
		assertEquals("", r);
		r = rt.result(t1);
		assertEquals(t1, r);
		r = rt.result(t2);
		assertEquals(t2, r);
		r = rt.result(t3);
		assertEquals(t3, r);
		r = rt.result(tshort);
		assertEquals(tshort, r);
		r = rt.result(tlong);
		assertEquals(tlong, r);
		
		// -1, -1 min will be set to 0, max will be set to unlimited
        switches.setMaxResultLength(-1);
        rt = new ResultText(rpattern, switches);
        rt.setMinPrefixIndex(-1);
		r = rt.result("");
		assertEquals("", r);
		r = rt.result(t1);
		assertEquals(t1, r);
		r = rt.result(t2);
		assertEquals(t2, r);
		r = rt.result(t3);
		assertEquals(t3, r);
		r = rt.result(tshort);
		assertEquals(tshort, r);
		r = rt.result(tlong);
		assertEquals(tlong, r);
		
		// 5, 10 both used as-is.
        switches.setMaxResultLength(10);
        rt = new ResultText(rpattern, switches);
        rt.setMinPrefixIndex(5);
		r = rt.result(tshort);
		assertEquals(tshort.substring(0, 10)+DDD, r);
		r = rt.result(tlong);
		assertEquals(tlong.substring(0, 10)+DDD, r);
	}
	
	@Test
	public void testResultPattern() {
		ResourcePattern rp = new ResourcePattern("PATTERN");
		
		// 0, max < 1 means unlimited
        switches.setMaxResultLength(-1);
        ResultText rt = new ResultText(rp, switches);
        rt.setMinPrefixIndex(0);
		String r = rt.result(t1);
		assertNotNull(r);
		assertEquals(t1, r);
		r = rt.result(tpatStart);
		assertNotNull(r);
		assertEquals(tpatStart, r);
		r = rt.result(tpatMid);
		assertNotNull(r);
		assertEquals(tpatMid, r);
		r = rt.result(tpatEnd);
		assertNotNull(r);
		assertEquals(tpatEnd, r);

		// 0, max 1
        switches.setMaxResultLength(1);
        rt = new ResultText(rp, switches);
		r = rt.result(t1);
		assertNotNull(r);
		assertEquals(t1, r);
		r = rt.result(tpatStart);
		assertNotNull(r);
		assertEquals("P...", r);
		r = rt.result(tpatMid);
		assertNotNull(r);
		assertEquals("...P...", r);
		r = rt.result(tpatEnd);
		assertNotNull(r);
		assertEquals("...P...", r);

        // 0, max >1 (5)
        switches.setMaxResultLength(5);
        rt = new ResultText(rp, switches);
        rt.setMinPrefixIndex(0);
        r = rt.result(t1);
        assertNotNull(r);
        assertEquals(t1, r);
        r = rt.result(tpatStart);
        assertNotNull(r);
        assertEquals("PATTE...", r);
        r = rt.result(tpatMid);
        assertNotNull(r);
        assertEquals("...PATTE...", r);
        r = rt.result(tpatEnd);
        assertNotNull(r);
        assertEquals("...PATTE...", r);

        // 0, max >1 (10)
        switches.setMaxResultLength(10);
        rt = new ResultText(rp, switches);
        rt.setMinPrefixIndex(0);
        r = rt.result(t1);
        assertNotNull(r);
        assertEquals(t1, r);
        r = rt.result(tpatStart);
        assertNotNull(r);
        assertEquals("PATTERN wh...", r);
        r = rt.result(tpatMid);
        assertNotNull(r);
        assertEquals("...PATTERN in...", r);
        r = rt.result(tpatEnd);
        assertNotNull(r);
        assertEquals("...PATTERN", r);


		// 5 to max >1 (20)
		switches.setMaxResultLength(20);
        rt = new ResultText(rp, switches);
        rt.setMinPrefixIndex(5);
		r = rt.result(t1);
		assertNotNull(r);
		assertEquals(t1, r);
		r = rt.result(tpatStart);
		assertNotNull(r);
		assertEquals("PATTERN which starts...", r);
		r = rt.result(tpatMid);
		assertNotNull(r);
		assertEquals("...es a PATTERN in the ...", r);
		r = rt.result(tpatEnd);
		assertNotNull(r);
		assertEquals("...es a PATTERN", r);
		
        // 10, max >1
        switches.setMaxResultLength(10);
        rt = new ResultText(rp, switches);
        rt.setMinPrefixIndex(10);
        r = rt.result(t1);
        assertNotNull(r);
        assertEquals(t1, r);
        r = rt.result(tpatStart);
        assertNotNull(r);
        assertEquals("PATTERN wh...", r);
        r = rt.result(tpatMid);
        assertNotNull(r);
        assertEquals("... a PATTERN...", r);
        r = rt.result(tpatEnd);
        assertNotNull(r);
        assertEquals("... a PATTERN", r);
	}

    @Test
    public void testResultLongUnlimited() {
        ResourcePattern rp = new ResourcePattern("stuff");
        
        // 10, unlimited
        switches.setMaxResultLength(-1);
        ResultText rt = new ResultText(rp, switches);
        rt.setMinPrefixIndex(ResultText.DEFAULT_MIN_DATA_VALUE_PREFIX);
        
        String r = rt.result(tlong);
        assertNotNull(r);
        String expect = tlong;
        assertEquals(expect, r);
        
        switches.setMaxResultLength(10);
        rt = new ResultText(rp, switches);
        rt.setMinPrefixIndex(10);

        r = rt.result(tlong);
        assertNotNull(r);
        expect = DDD+tlong.substring(18, 28)+DDD;
        assertEquals(expect, r);
    }

    @Test
    public void testResultLongHighlightUnlimitedWi() {
        ResourcePattern rp = new ResourcePattern("wi");
        // 0, unlimited
        Highlight hl = new Highlight("always", "red");
        hl.setStart(">>");
        hl.setEnd("<<");
        switches.setHighlight(hl);
        ResultText rt = new ResultText(rp, switches);
        rt.setMinPrefixIndex(10);
        String r = rt.result(tlong);
        assertNotNull(r);
        String expect = tlongHighlightWi;
        assertEquals(expect, r);
    }

    @Test
    public void testResultLongHighlightShortWi() {
        ResourcePattern rp = new ResourcePattern("wi");
        Highlight hl = new Highlight("always", "red");
        hl.setStart(">>");
        hl.setEnd("<<");
        switches.setHighlight(hl);

        // 0, 10
        switches.setMaxResultLength(10);
        ResultText rt = new ResultText(rp, switches);
        rt.setMinPrefixIndex(0);
        String r = rt.result(tlong);
        assertNotNull(r);
        // add 4 to include the '>><<' chars, only need to do this in test. The real result excludes the highlighting chars
        // from the range limits (since they are usually not visible).
        String expect = DDD+tlongHighlightWi.substring(10,20+4)+DDD;
        assertEquals(expect, r);

        // 10, 10
        switches.setMaxResultLength(10);
        rt = new ResultText(rp, switches);
        rt.setMinPrefixIndex(10);
        r = rt.result(tlong);
        assertNotNull(r);
        expect = DDD + tlongHighlightWi.substring(2, 10+6) + DDD;
        assertEquals(expect, r);
    }

    @Test
    public void testResultLongHighlightUnlimitedSx() {
        ResourcePattern rp = new ResourcePattern("s?");
        // 0, unlimited
        Highlight hl = new Highlight("always", "red");
        hl.setStart("[");
        hl.setEnd("]");
        switches.setHighlight(hl);
        ResultText rt = new ResultText(rp, switches);
        String r = rt.result(tlong);
        assertNotNull(r);
        String expect = tlongHighlightSx;
        assertEquals(expect, r);
    }

    @Test
    public void testResultLongHighlightShortSx() {
        ResourcePattern rp = new ResourcePattern("s?");
        // 0, 10
        Highlight hl = new Highlight("always", "red");
        hl.setStart("[");
        hl.setEnd("]");
        switches.setHighlight(hl);
        switches.setMaxResultLength(10);
        ResultText rt = new ResultText(rp, switches);
        rt.setMinPrefixIndex(0);
        String r = rt.result(tlong);
        assertNotNull(r);
        String expect = DDD + tlongHighlightSx.substring(tlong.indexOf('s'), tlong.indexOf('s')+10+2) + DDD;
        assertEquals(expect, r);
        
        // 10, 10
        switches.setMaxResultLength(10);
        rt = new ResultText(rp, switches);
        rt.setMinPrefixIndex(10);
        r = rt.result(tlong);
        assertNotNull(r);
        // + 2 for '[]' chars
        assertEquals(tlongHighlightSx.substring(0, 10+2)+DDD, r);
    }
    
	@Test
	public void testNewLineFiltering() {
		ResourcePattern rp = new ResourcePattern("xy");

		switches.setMaxResultLength(-1);
		ResultText rt = new ResultText(rp, switches);
		rt.setMinPrefixIndex(0);
		rt.setRemoveLineBreaks(true);
		String r = rt.result(tmultilines);
		assertNotNull(r);
		assertEquals(tmultilines_stripped, r);

		rt.setRemoveLineBreaks(false);
		r = rt.result(tmultilines);
		assertNotNull(r);
		assertEquals(tmultilines, r);
	}
}
