/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ryanfuse.crgrep.db.NeoGraphParser;
import com.ryanfuse.crgrep.util.Switches;

@RunWith(MockitoJUnitRunner.class)
public class GraphGrepTest {

    private static final String URI = "http://localhost:7474/db/data";
    private static final String USER = "user";
    private static final String PW = "pw";
    
    private Switches switches;
    @Mock private NeoGraphParser par;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        switches = new Switches();
        // default is no matches
        when(par.getMatchingNodeMap()).thenReturn(new HashMap<String, List<Map.Entry<String, List<String>>>>());
        when(par.getMatchingRelationshipMap()).thenReturn(new HashMap<String, List<Map.Entry<String, List<String>>>>());
        when(par.openConnection()).thenReturn(true);
        when(par.getUser()).thenReturn(USER);
        when(par.getUri()).thenReturn(URI);
    }
    
    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGrepGraphExecuteNoMatchingNodesOrRelationships() throws Exception {
        GraphGrep gg = new GraphGrep(switches);
        // empty
        //when(par.getMatchingNodeMap()).thenReturn(new HashMap<String, List<Map.Entry<String, List<String>>>>());
        //when(par.getMatchingRelationshipMap()).thenReturn(new HashMap<String, List<Map.Entry<String, List<String>>>>());
        gg.setParser(par);
        ResourceList<String> resList = new DbResourceList("*.*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteNoConnection() throws Exception {
        GraphGrep gg = new GraphGrep(switches);
        // no connection
        when(par.openConnection()).thenReturn(false);
        gg.setParser(par);
        ResourceList<String> resList = new DbResourceList("*.*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteListing() throws Exception {
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        createValidNodeMatches();
        createValidRelationshipMatches();
        long nodeIds[] = new long[2];
        nodeIds[0] = -1;
        nodeIds[1] = -1;
        when(par.getRelationshipNodeIds(any(String.class))).thenReturn(nodeIds);
        ResourceList<String> resList = new DbResourceList("*.*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            e.printStackTrace();
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteNodeListingResourceListNotWild() {
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        ResourceList<String> resList = new DbResourceList("*.prop1");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            e.printStackTrace();
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteNodeListingResourceListContainsWild() throws Exception {
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        ResourceList<String> resList = new DbResourceList("*.prop?");
        createValidNodeMatches();
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            e.printStackTrace();
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteRelationshipListingResourceListNotWild() {
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        ResourceList<String> resList = new DbResourceList("*.dated");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            e.printStackTrace();
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteRelationshipListingResourceListContainsWild() throws Exception {
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        createValidRelationshipMatches();
        ResourceList<String> resList = new DbResourceList("*.dat?");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            e.printStackTrace();
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteNodeAndRelationshipContent() throws Exception {
        switches.setAllcontent(true);
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        createValidNodeMatches();
        createValidRelationshipMatches();
        ResourceList<String> resList = new DbResourceList("*.*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            e.printStackTrace();
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteRelationshipContent() throws Exception {
        switches.setAllcontent(true);
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        createValidRelationshipMatches();
        ResourceList<String> resList = new DbResourceList("IS_A.*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            e.printStackTrace();
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteNodeContent() throws Exception {
        switches.setAllcontent(true);
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        createValidNodeMatches();
        ResourceList<String> resList = new DbResourceList("label.*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            e.printStackTrace();
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteCleanupGrepException() throws GrepException {
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        doThrow(new GrepException("Graph DB: yikes")).when(par).cleanup();
        ResourceList<String> resList = new DbResourceList("*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*.*"));
        try {
            gg.execute();
            fail("exception expected");
        } catch (GrepException e) {
            assertEquals("Graph DB: yikes", e.getMessage());
            assertNull(e.getCause());
            assertNull(e.getEnvResult());
        }
    }

    @Test
    public void testGrepGraphExecuteCleanupUnknownException() throws GrepException {
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        doThrow(new RuntimeException("yikes")).when(par).cleanup();
        long nodeIds[] = new long[2];
        nodeIds[0] = -1;
        nodeIds[1] = -1;
        when(par.getRelationshipNodeIds(any(String.class))).thenReturn(nodeIds);
        ResourceList<String> resList = new DbResourceList("*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*.*"));
        try {
            gg.execute();
            fail("exception expected");
        } catch (GrepException e) {
            assertEquals("crgrep: failed graph database grep", e.getMessage());
            assertNotNull(e.getCause());
            assertTrue(e.getCause() instanceof RuntimeException);
            assertEquals("yikes", e.getCause().getMessage());
        }
    }

    private void createValidNodeMatches() throws Exception {
        Map<String, List<Map.Entry<String, List<String>>>> nodeMap = new HashMap<String, List<Map.Entry<String, List<String>>>>();  
        List<Map.Entry<String, List<String>>> nodeList = new ArrayList<Map.Entry<String, List<String>>>();
        List<String> nodeProps = new ArrayList<String>();
        nodeProps.add("prop");
        Map.Entry<String, List<String>> nodeEntry = new AbstractMap.SimpleEntry("1234", nodeProps);
        nodeList.add(nodeEntry);
        nodeMap.put("label", nodeList);
        when(par.getMatchingNodeMap()).thenReturn(nodeMap);
    }
    
    private void createValidRelationshipMatches() throws Exception {
        Map<String, List<Map.Entry<String, List<String>>>> relMap = new HashMap<String, List<Map.Entry<String, List<String>>>>();  
        List<Map.Entry<String, List<String>>> relList = new ArrayList<Map.Entry<String, List<String>>>();
        List<String> relProps = new ArrayList<String>();
        relProps.add("date");
        Map.Entry<String, List<String>> relEntry = new AbstractMap.SimpleEntry("5678", relProps);
        relList.add(relEntry);
        relMap.put("IS_A", relList);
        when(par.getMatchingRelationshipMap()).thenReturn(relMap);
    }

}
