/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ryanfuse.crgrep.env.EnvResult;

public class GrepExceptionTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGrepException() {
        GrepException ge = new GrepException("File grep: oops");
        assertEquals("File grep: oops", ge.getMessage());
    }

    @Test
    public void testGrepExceptionWithCause() {
        GrepException ge = new GrepException("Oracle database grep: oops", new RuntimeException("yikes"));
        assertEquals("Oracle database grep: oops", ge.getMessage());
        assertNotNull(ge.getCause());
        assertTrue(ge.getCause() instanceof RuntimeException);
        assertEquals("yikes", ge.getCause().getMessage());
    }

    @Test
    public void testGrepExceptionEnvResult() {
        EnvResult envResult = new EnvResult("myModule");
        GrepException ge = new GrepException(envResult);
        assertNull(ge.getMessage());
        assertNull(ge.getCause());
        assertNotNull(ge.getEnvResult());
        assertEquals("myModule", ge.getEnvResult().getModule());
    }
}
