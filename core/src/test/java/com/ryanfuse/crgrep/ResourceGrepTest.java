/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import com.ryanfuse.crgrep.env.EnvResult;
import com.ryanfuse.crgrep.env.EnvStatus;

public class ResourceGrepTest extends TestCommon {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testArguments() throws GrepException {
        title("file args", "onkey", "foo");
        String[] args = new String[] { "-r", "--warn", "-l", "onkey", "foo" };
        ResourceGrep rg = new ResourceGrep(args);
        FileGrep fg = new FileGrep(rg.getSwitches());
        assertTrue(fg.isListing());
        assertTrue(fg.isVerbose());
        assertTrue(fg.isRecurse());
        ResourceList rl = new FileResourceList("foo");
        ResourcePattern rp = new ResourcePattern("*");
        fg.setResourceList(rl);
        fg.setResourcePattern(rp);
        assertEquals(rl, fg.getResourceList());
        assertEquals(rp, fg.getResourcePattern());
    }

    @Test
    public void testProxy() throws GrepException {
        title("proxy", "foo", "bah");
        String[] args = new String[] { "-P", "foo.com:8011", "foo", "bah" };
        ResourceGrep rg = new ResourceGrep(args);
        assertEquals("foo.com", System.getProperty("http.proxyHost"));
        assertEquals("8011", System.getProperty("http.proxyPort"));
    }

    @Test
    public void testNoArgs() throws GrepException {
        title("no args", "", "");
        String[] args = new String[] {};
        ResourceGrep rg = new ResourceGrep();
        ResourceGrep rgNoExit = spy(rg);
        doNothing().when(rgNoExit).exitWithStatus(1);
        rgNoExit.processCommandLineArgs(args);        
    }

    @Test
    public void testOneArg() throws GrepException {
        title("one arg", "bah", "");
        String[] args = new String[] { "bah" };
        ResourceGrep rg = new ResourceGrep(args);
    }
    
    @Test
    public void testInvalidShortOptionUsage() throws GrepException {
        title("invalid option", "-z", "");
        String[] args = new String[] { "-z" };
        exit.expectSystemExitWithStatus(1);
        ResourceGrep rg = new ResourceGrep(args);
    }

    @Test
    public void testInvalidLongOptionUsage() throws GrepException {
        title("invalid option", "-nosuchoption", "");
        String[] args = new String[] { "-nosuchoption" };
        exit.expectSystemExitWithStatus(1);
        ResourceGrep rg = new ResourceGrep(args);
    }

    @Test
    public void testInvalidOptionNoDbArg() throws GrepException {
        title("invalid option db", "-d", "");
        String[] args = new String[] { "-d" };
        exit.expectSystemExitWithStatus(1);
        ResourceGrep rg = new ResourceGrep(args);
    }

    @Test
    public void testDbTooManyArgs() throws GrepException {
        title("db too many args", "pattern", "tab1.prop tab2.prop");
        String[] args = new String[] { "-d", "pattern", "tab1.prop", "tab2.prop" };
        exit.expectSystemExitWithStatus(1);
        ResourceGrep rg = new ResourceGrep(args);
    }

    @Test
    public void testDbTooManyArgsNoExit() throws GrepException {
        title("db too many args", "pattern", "tab1.prop tab2.prop");
        String[] args = new String[] { "-d", "pattern", "tab1.prop", "tab2.prop" };
        ResourceGrep rg = new ResourceGrep();
        ResourceGrep rgNoExit = spy(rg);
        doNothing().when(rgNoExit).exitWithStatus(1);
        rgNoExit.processCommandLineArgs(args);
    }

    @Test
    public void testDbCreateGraphGrep() throws GrepException {
        title("db graph", "pattern", "http://graph");
        String[] args = new String[] { "-d", "-U", "http://graph", "pattern", "node"};
        ResourceGrep rg = new ResourceGrep(args);
    }

    @Test
    public void testDbCreateJdbcGrep() throws GrepException {
        title("db graph", "pattern", "jdbc:mysql://mydb");
        String[] args = new String[] { "-d", "-U", "jdbc:mysql://mydb", "pattern", "table"};
        ResourceGrep rg = new ResourceGrep(args);
    }

    @Test
    public void testOptions() throws GrepException {
        title("options", "pattern", "url");
        String[] args = new String[] { "-u", "user", "-X", "debug,trace", "--ocr", "http://d.e.f" };
        ResourceGrep rg = new ResourceGrep(args);
    }

    @Test
    public void testHttp() throws GrepException {
        title("http", "pattern", "url");
        String[] args = new String[] { "pattern", "http://d.e.f" };
        ResourceGrep rg = new ResourceGrep(args);
    }

    @Test
    public void testHttpTooManyArgs() throws GrepException {
        title("http too many args", "pattern", "url url");
        String[] args = new String[] { "pattern", "http://a.b.c", "http://d.e.f" };
        exit.expectSystemExitWithStatus(1);
        ResourceGrep rg = new ResourceGrep(args);
    }

    @Test
    public void testHttpTooManyArgsNoExit() throws GrepException {
        title("http too many args", "pattern", "url url");
        String[] args = new String[] { "pattern", "http://a.b.c", "http://d.e.f" };
        ResourceGrep rg = new ResourceGrep();
        ResourceGrep rgNoExit = spy(rg);
        doNothing().when(rgNoExit).exitWithStatus(1);
        rgNoExit.processCommandLineArgs(args);
    }

    @Test
    public void testProxyBadUrlContext() throws GrepException {
        title("proxy bad url", "-P  a#$-b!badurl", "");
        String[] args = new String[] { "-P a#$?-b!badurl", "pat", "."};
        exit.expectSystemExitWithStatus(1);
        ResourceGrep.main(args);
    }

    @Test
    public void testProxyBadUrlProtocol() throws GrepException {
        title("proxy bad url", "-Pmyprot://sneakyproxy", "");
        String[] args = new String[] { "-Pmyprot://sneakyproxy", "pat", "."};
        exit.expectSystemExitWithStatus(1);
        ResourceGrep.main(args);        
    }

    @Test
    public void testProxyBadUrlProtocolNoExit() throws GrepException {
        title("proxy bad url", "-Pmyprot://sneakyproxy", "");
        String[] args = new String[] { "-Pmyprot://sneakyproxy", "pat", "."};
        ResourceGrep rg = new ResourceGrep();
        ResourceGrep rgNoExit = spy(rg);
        doNothing().when(rgNoExit).exitWithStatus(any(Integer.class));
        rgNoExit.processCommandLineArgs(args);
        rgNoExit.execute();        
    }

    @Test
    public void testHelp() throws GrepException {
        title("help", "", "");
        String[] args = new String[] { "-h" };
        exit.expectSystemExitWithStatus(0);
        ResourceGrep rg = new ResourceGrep(args);
    }

    @Test
    public void testHelpNoExit() throws GrepException {
        title("help", "-h", "");
        String[] args = new String[] { "-h" };
        ResourceGrep rg = new ResourceGrep();
        ResourceGrep rgNoExit = spy(rg);
        doNothing().when(rgNoExit).exitWithStatus(0);
        rgNoExit.processCommandLineArgs(args);
    }

    @Test
    public void testVersion() throws GrepException {
        title("version", "", "");
        String[] args = new String[] { "--version" };
        exit.expectSystemExitWithStatus(0);
        ResourceGrep rg = new ResourceGrep(args);
    }
    
    /*
     * Variation of the previous test which prevents System.exit()
     * being called so that this test will be included in code 
     * coverage results.
     */
    @Test
    public void testVersionNoExit() throws GrepException {
        title("version without exit", "", "");
        String[] args = new String[] { "--version" };
        ResourceGrep rg = new ResourceGrep();
        ResourceGrep rgNoExit = spy(rg);
        doNothing().when(rgNoExit).exitWithStatus(0);
        rgNoExit.processCommandLineArgs(args);
    }

    @Test
    public void testEnvResult() {
        GrepException ex = new GrepException("no prob");
        ResourceGrep.reportGrepFailure(ex);
        
        EnvResult er = new EnvResult("module", EnvStatus.OK);
        assertNotNull(er.toString());
        ex = new GrepException(er);
        ResourceGrep.reportGrepFailure(ex);
        
        er = new EnvResult("module", EnvStatus.WARN);
        ResourceGrep.reportGrepFailure(ex);
        er.getWarnings().add("a warning");
        ex = new GrepException(er);
        ResourceGrep.reportGrepFailure(ex);
        
        er = new EnvResult("module", EnvStatus.ERROR);
        ex = new GrepException(er);
        ResourceGrep.reportGrepFailure(ex);
        er.getErrors().add("error 1\n");
        er.getErrors().add("error 2\n");
        er.getErrors().add("error 3\n");
        ResourceGrep.reportGrepFailure(ex);
        
        er = new EnvResult("module", EnvStatus.ERROR_WARN);
        ex = new GrepException(er);
        ResourceGrep.reportGrepFailure(ex);
        er.getWarnings().add("warning 1");
        er.getWarnings().add("warning 2");
        er.getWarnings().add("warning 3");
        er.getErrors().add("error 1");
        ResourceGrep.reportGrepFailure(ex);

        assertFalse(er.isOK());
        er.getErrors();
        er.getWarnings();
        er.getStatus();
        assertEquals("module", er.getModule());
        assertNotNull(er.toString());
        
        EnvStatus.valueOf("WARN");
        assertNotNull(EnvStatus.values());
    }
}
