/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ResourceListTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDbPath() {
		DbResourceList rl = new DbResourceList("");
		assertEquals(1, rl.size());
		rl.remove(); // no-op
		assertNull(rl.pathPart());
	}

	@Test
	public void testDbListQuoted() {
	    DbResourceList rl = new DbResourceList("'a.b'");
        assertTrue(rl.hasNext());
        String n = rl.next();
        assertEquals("a.b", n);
        assertEquals("a.b", rl.get(0));
        rl = new DbResourceList("\"a.b\"");
        assertTrue(rl.hasNext());
        n = rl.next();
        assertEquals("a.b", n);
        assertEquals("a.b", rl.get(0));
        assertEquals("a", rl.firstPart());
        assertEquals("b", rl.lastPart());
        rl = new DbResourceList("\"a.b");
        assertTrue(rl.hasNext());
        n = rl.next();
        assertEquals("\"a.b", n);
        rl = new DbResourceList("'a.b");
        assertTrue(rl.hasNext());
        n = rl.next();
        assertEquals("'a.b", n);
        rl = new DbResourceList("a.b'");
        assertTrue(rl.hasNext());
        n = rl.next();
        assertEquals("a.b'", n);
        rl = new DbResourceList("a.b\"");
        assertTrue(rl.hasNext());
        n = rl.next();
        assertEquals("a.b\"", n);
	}

    @Test
    public void testDbIterate() {
        DbResourceList rl = new DbResourceList("a.b");
        assertTrue(rl.hasNext());
        String n = rl.next();
        assertEquals("a.b", n);
        assertFalse(rl.hasNext());
        assertEquals("a.b", rl.get(0));
        try {
            rl.get(1);
            fail("expected exception (db list may only contain 1 item)");
        } catch (IndexOutOfBoundsException ie) {
            // expected
        }
    }

	@Test
	public void testDbCheckForWild() {
		DbResourceList rl = new DbResourceList("*");
		assertTrue(rl.isWild());
		assertTrue(rl.isFirstPartWild());
		assertTrue(rl.isLastPartWild());
		assertTrue(rl.firstPartContainsWild());
		assertTrue(rl.lastPartContainsWild());
		// no path for db, always true
		assertTrue(rl.isPathWild());
		assertTrue(rl.pathContainsWild());
		assertNull(rl.pathPart());
		assertEquals("*", rl.next());
		assertEquals("*", rl.firstPart());
		assertEquals("%", rl.firstPartPattern());
		assertEquals("*", rl.lastPart());
		assertEquals("%", rl.lastPartPattern());
		
		rl = new DbResourceList("*.*");
		assertTrue(rl.isWild());
		assertTrue(rl.isFirstPartWild());
		assertTrue(rl.isLastPartWild());
		assertTrue(rl.firstPartContainsWild());
		assertTrue(rl.lastPartContainsWild());
		assertEquals("*.*", rl.next());
		assertEquals("*", rl.firstPart());
		assertEquals("%", rl.firstPartPattern());
		assertEquals("*", rl.lastPart());
		assertEquals("%", rl.lastPartPattern());

		rl = new DbResourceList(".*");
		assertTrue(rl.isWild());
		assertTrue(rl.isFirstPartWild());
		assertTrue(rl.isLastPartWild());
		assertTrue(rl.firstPartContainsWild());
		assertTrue(rl.lastPartContainsWild());
		assertEquals(".*", rl.next());
		assertEquals("*", rl.firstPart());
		assertEquals("%", rl.firstPartPattern());
		assertEquals("*", rl.lastPart());
		assertEquals("%", rl.lastPartPattern());

		rl = new DbResourceList("a.*");
		assertFalse(rl.isWild());
		assertFalse(rl.isFirstPartWild());
		assertTrue(rl.isLastPartWild());
		assertFalse(rl.firstPartContainsWild());
		assertTrue(rl.lastPartContainsWild());
		assertEquals("a.*", rl.next());
		assertEquals("a", rl.firstPart());
		assertEquals("a", rl.firstPartPattern());
		assertEquals("*", rl.lastPart());
		assertEquals("%", rl.lastPartPattern());

		rl = new DbResourceList("*.b");
		assertFalse(rl.isWild());
		assertTrue(rl.isFirstPartWild());
		assertFalse(rl.isLastPartWild());
		assertTrue(rl.firstPartContainsWild());
		assertFalse(rl.lastPartContainsWild());
		assertEquals("*.b", rl.next());
		assertEquals("*", rl.firstPart());
		assertEquals("%", rl.firstPartPattern());
		assertEquals("b", rl.lastPart());
		assertEquals("b", rl.lastPartPattern());

		rl = new DbResourceList("a.b");
		assertFalse(rl.isWild());
		assertFalse(rl.isFirstPartWild());
		assertFalse(rl.isLastPartWild());
		assertFalse(rl.firstPartContainsWild());
		assertFalse(rl.lastPartContainsWild());
		assertEquals("a.b", rl.next());
		assertEquals("a", rl.firstPart());
		assertEquals("a", rl.firstPartPattern());
		assertEquals("b", rl.lastPart());
		assertEquals("b", rl.lastPartPattern());
		
		// still true
		assertTrue(rl.isPathWild());
		assertTrue(rl.pathContainsWild());
		assertNull(rl.pathPart());
	}

	@Test
	public void testDbPaths() {
		DbResourceList rl = new DbResourceList("*");
		assertNotNull(rl.firstPart());
		assertNotNull(rl.lastPart());
		
		rl = new DbResourceList("*.*");
		assertNotNull(rl.firstPart());
		assertNotNull(rl.firstPartPattern());
		assertNotNull(rl.lastPart());
		assertNotNull(rl.lastPartPattern());
		
		rl = new DbResourceList(".*");
		assertNotNull(rl.firstPart());
		assertNotNull(rl.firstPartPattern());
		assertNotNull(rl.lastPart());
		assertNotNull(rl.lastPartPattern());

		rl = new DbResourceList("a.*");
		assertNotNull(rl.firstPart());
		assertNotNull(rl.firstPartPattern());
		assertNotNull(rl.lastPart());
		assertNotNull(rl.lastPartPattern());

		rl = new DbResourceList("*.b");
		assertNotNull(rl.firstPart());
		assertNotNull(rl.firstPartPattern());
		assertNotNull(rl.lastPart());
		assertNotNull(rl.lastPartPattern());
		
		rl = new DbResourceList("a.b");
		assertNotNull(rl.firstPart());
		assertNotNull(rl.firstPartPattern());
		assertNotNull(rl.lastPart());
		assertNotNull(rl.lastPartPattern());

		rl = new DbResourceList(".b");
		assertNotNull(rl.firstPart());
		assertNotNull(rl.firstPartPattern());
		assertNotNull(rl.lastPart());
		assertNotNull(rl.lastPartPattern());
	}
	
    @Test
    public void testFileListQuoted() {
        FileResourceList rl = new FileResourceList("'a'");
        assertTrue(rl.hasNext());
        String n = rl.next();
        assertNotNull(n);
        assertEquals("a", n);
        
        String p = rl.firstPart();
        assertNotNull(p);
        assertEquals("a", p);
        
        rl = new FileResourceList("\"a\"");
        assertTrue(rl.hasNext());
        n = rl.next();
        assertNotNull(n);
        assertEquals("a", n);

        rl = new FileResourceList("\"a");
        assertTrue(rl.hasNext());
        n = rl.next();
        assertNotNull(n);
        assertEquals("\"a", n);

        rl = new FileResourceList("a\"");
        assertTrue(rl.hasNext());
        n = rl.next();
        assertNotNull(n);
        assertEquals("a\"", n);
    }

    @Test
    public void testFilePartsSinglePath() {
        FileResourceList rl = new FileResourceList("a");
        // not yet called rl.next(), nothing initialised
        assertNull(rl.firstPart());
        assertNull(rl.lastPart());
        assertNull(rl.pathPart());
        
        assertTrue(rl.hasNext());
        String n = rl.next();
        assertNotNull(n);
        assertEquals("a", n);
        
        String p = rl.firstPart();
        assertNotNull(p);
        assertEquals("a", p);
        p = rl.lastPart();
        assertNotNull(p);
        assertEquals("", p);
        p = rl.pathPart();
        assertNotNull(p);
        assertEquals("", p);
        
		rl = new FileResourceList("/a/file");
		assertTrue(rl.hasNext());
		n = rl.next();
		assertNotNull(n);
		assertEquals("/a/file", n);
		p = rl.firstPart();
		assertNotNull(p);
		assertEquals("file", p);
		p = rl.lastPart();
		assertNotNull(p);
		assertEquals("", p);
		p = rl.pathPart();
		assertNotNull(p);
		assertEquals("/a", p);
		
		rl = new FileResourceList("a?");
		assertTrue(rl.hasNext());
		n = rl.next();
		assertNotNull(n);
		assertEquals("a?", n);
		p = rl.firstPart();
		assertNotNull(p);
		assertEquals("a?", p);
		p = rl.lastPart();
		assertNotNull(p);
		assertEquals("", p);
		p = rl.pathPart();
		assertNotNull(p);
		assertEquals("", p);

		rl = new FileResourceList("a*");
		assertTrue(rl.hasNext());
		n = rl.next();
		assertNotNull(n);
		assertEquals("a*", n);
		p = rl.firstPart();
		assertNotNull(p);
		assertEquals("a*", p);
		p = rl.lastPart();
		assertNotNull(p);
		assertEquals("", p);
		p = rl.pathPart();
		assertNotNull(p);
		assertEquals("", p);

		rl = new FileResourceList("a/b?");
		assertTrue(rl.hasNext());
		n = rl.next();
		assertNotNull(n);
		assertEquals("a/b?", n);
		p = rl.firstPart();
		assertNotNull(p);
		assertEquals("b?", p);
		p = rl.lastPart();
		assertNotNull(p);
		assertEquals("", p);
		p = rl.pathPart();
		assertNotNull(p);
		assertEquals("a", p);

		rl = new FileResourceList("a/b/c*");
		assertTrue(rl.hasNext());
		n = rl.next();
		assertNotNull(n);
		assertEquals("a/b/c*", n);
		p = rl.firstPart();
		assertNotNull(p);
		assertEquals("c*", p);
		p = rl.lastPart();
		assertNotNull(p);
		assertEquals("", p);
		p = rl.pathPart();
		assertNotNull(p);
		assertEquals("a/b", p);

		rl = new FileResourceList("/a/file.txt");
		assertTrue(rl.hasNext());
		n = rl.next();
		assertNotNull(n);
		assertEquals("/a/file.txt", n);
		p = rl.firstPart();
		assertNotNull(p);
		assertEquals("file", p);
		p = rl.lastPart();
		assertNotNull(p);
		assertEquals("txt", p);
		p = rl.pathPart();
		assertNotNull(p);
		assertEquals("/a", p);

		rl = new FileResourceList("/a/file.");
		assertTrue(rl.hasNext());
		n = rl.next();
		assertNotNull(n);
		assertEquals("/a/file.", n);
		p = rl.firstPart();
		assertNotNull(p);
		assertEquals("file", p);
		p = rl.lastPart();
		assertNotNull(p);
		assertEquals("", p);
		p = rl.pathPart();
		assertNotNull(p);
		assertEquals("/a", p);
	}
	
	@Test
	public void testFilePartsMultiplePaths() {
		List<String> fileList = Arrays.asList("*", "foo", "file*.txt");
		FileResourceList rl = new FileResourceList(fileList);
		
		assertTrue(rl.hasNext());
		// list item 1
		String n = rl.next();
		assertNotNull(n);
		assertEquals("*", n);
		String p = rl.firstPart();
		assertNotNull(p);
		assertEquals("*", p);
		assertEquals(".*", rl.firstPartPattern());
		p = rl.lastPart();
		assertNotNull(p);
		assertEquals("", p);
		p = rl.lastPartPattern();
		assertNotNull(p);
		assertEquals("", p);
		p = rl.pathPart();
		assertNotNull(p);
		assertEquals("", p);

		// list item 2
		n = rl.next();
		assertNotNull(n);
		assertEquals("foo", n);
		p = rl.firstPart();
		assertNotNull(p);
		assertEquals("foo", p);
		p = rl.firstPartPattern();
		assertNotNull(p);
		assertEquals("foo", p);
		p = rl.lastPart();
		assertNotNull(p);
		assertEquals("", p);
		p = rl.lastPartPattern();
		assertNotNull(p);
		assertEquals("", p);
		p = rl.pathPart();
		assertNotNull(p);
		assertEquals("", p);

		// list item 3
		n = rl.next();
		assertNotNull(n);
		assertEquals("file*.txt", n);
		p = rl.firstPart();
		assertNotNull(p);
		assertEquals("file*", p);
		p = rl.firstPartPattern();
		assertNotNull(p);
		assertEquals("file.*", p);
		p = rl.lastPart();
		assertNotNull(p);
		assertEquals("txt", p);
		p = rl.lastPartPattern();
		assertNotNull(p);
		assertEquals("txt", p);
		p = rl.pathPart();
		assertNotNull(p);
		assertEquals("", p);
	}

	@Test
	public void testFileCheckWild() {
		List<String> fileList = Arrays.asList("*", "foo", "foo.txt", "file*.txt", "a/*.txt", "foo.*", "/a/b*.*", "*.*", "*/*");
		FileResourceList rl = new FileResourceList(fileList);
		assertTrue(rl.hasNext());
		// list item 1, "*"
		String n = rl.next();
		assertTrue(rl.firstPartContainsWild());
		assertTrue(rl.lastPartContainsWild());
		assertTrue(rl.isFirstPartWild());
		assertTrue(rl.isLastPartWild());
		assertTrue(rl.isPathWild());
		assertTrue(rl.pathContainsWild());
		assertTrue(rl.isWild());
		
		// list item 2, "foo"
		assertTrue(rl.hasNext());
		n = rl.next();
		// all 'missing' parts make wild 'is' check true and 'contains' check false.
		assertFalse(rl.firstPartContainsWild());
		assertFalse(rl.lastPartContainsWild());
		assertFalse(rl.isFirstPartWild());
		assertTrue(rl.isLastPartWild());
		assertTrue(rl.isPathWild());
		assertFalse(rl.pathContainsWild());
		assertFalse(rl.isWild());

		// list item 3, "foo.txt
		assertTrue(rl.hasNext());
		n = rl.next();
		assertFalse(rl.firstPartContainsWild());
		assertFalse(rl.lastPartContainsWild());
		assertFalse(rl.isFirstPartWild());
		assertFalse(rl.isLastPartWild());
		assertTrue(rl.isPathWild());
		assertFalse(rl.pathContainsWild());
		assertFalse(rl.isWild());

		// list item 4, "file*.txt"
		assertTrue(rl.hasNext());
		n = rl.next();
		assertTrue(rl.firstPartContainsWild());
		assertFalse(rl.lastPartContainsWild());
		assertFalse(rl.isFirstPartWild());
		assertFalse(rl.isLastPartWild());
		assertTrue(rl.isPathWild());
		assertFalse(rl.pathContainsWild());
		assertFalse(rl.isWild());

		// list item 5, "a/*.txt"
		assertTrue(rl.hasNext());
		n = rl.next();
		assertTrue(rl.firstPartContainsWild());
		assertFalse(rl.lastPartContainsWild());
		assertTrue(rl.isFirstPartWild());
		assertFalse(rl.isLastPartWild());
		assertFalse(rl.isPathWild());
		assertFalse(rl.pathContainsWild());
		assertFalse(rl.isWild());

		// list item 6, "foo.*"
		assertTrue(rl.hasNext());
		n = rl.next();
		assertFalse(rl.firstPartContainsWild());
		assertTrue(rl.lastPartContainsWild());
		assertFalse(rl.isFirstPartWild());
		assertTrue(rl.isLastPartWild());
		assertTrue(rl.isPathWild());
		assertFalse(rl.pathContainsWild());
		assertFalse(rl.isWild());

		// list item 7, "/a/b*.*"
		assertTrue(rl.hasNext());
		n = rl.next();
		assertTrue(rl.firstPartContainsWild());
		assertTrue(rl.lastPartContainsWild());
		assertFalse(rl.isFirstPartWild());
		assertTrue(rl.isLastPartWild());
		assertFalse(rl.isPathWild());
		assertFalse(rl.pathContainsWild());
		assertFalse(rl.isWild());

		// list item 8, "*.*"
		assertTrue(rl.hasNext());
		n = rl.next();
		assertTrue(rl.firstPartContainsWild());
		assertTrue(rl.lastPartContainsWild());
		assertTrue(rl.isFirstPartWild());
		assertTrue(rl.isLastPartWild());
		assertTrue(rl.isPathWild());
		assertTrue(rl.pathContainsWild());
		assertTrue(rl.isWild());

		// list item 9, "*/*" 
		assertTrue(rl.hasNext());
		n = rl.next();
		assertTrue(rl.firstPartContainsWild());
		assertTrue(rl.lastPartContainsWild());
		assertTrue(rl.isFirstPartWild());
		assertTrue(rl.isLastPartWild());
		assertTrue(rl.isPathWild());
		assertTrue(rl.pathContainsWild());
		assertTrue(rl.isWild());
	}
}
