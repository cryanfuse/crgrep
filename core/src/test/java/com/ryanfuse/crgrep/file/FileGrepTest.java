/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.filters.ContentFilter;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

/**
 * All File type grep tests
 * 
 * @author Craig Ryan
 */
public class FileGrepTest extends TestCommon {

	private DisplayRecorder display;

	/*
	 * Debug test case to verify results of java Paths/Path behaviour. Can be excluded from normal unit test runs.
	 */
	//@Test
	public void testPaths() {
        Path p = Paths.get("/root/mid/end/foo.txt");
        System.out.println("Full Path ='" + p + "' Root='" + p.getRoot() + "' par='" + p.getParent() + "' fname='" + p.getFileName() + "' name count=" + p.getNameCount());
        System.out.println("   normalise ='" + p.normalize() + "' absolute= " + p.isAbsolute());

        p = Paths.get("c:\\root/mid/foo.txt");
        System.out.println("Full Path ='" + p + "' Root='" + p.getRoot() + "' par='" + p.getParent() + "' fname='" + p.getFileName() + "' name count=" + p.getNameCount());
        System.out.println("   normalise ='" + p.normalize() + "' absolute= " + p.isAbsolute());
        for (int i = 0; i < p.getNameCount(); i++) {
            System.out.println("    path part[" + i + "] is '" + p.getName(i) + "'");
        }

        p = Paths.get("c:/../foo.txt");
        System.out.println("Full Path ='" + p + "' Root='" + p.getRoot() + "' par='" + p.getParent() + "' fname='" + p.getFileName() + "' name count=" + p.getNameCount());
        System.out.println("   normalise ='" + p.normalize() + "' absolute= " + p.isAbsolute());
        for (int i = 0; i < p.getNameCount(); i++) {
            System.out.println("    path part[" + i + "] is '" + p.getName(i) + "'");
        }

        p = Paths.get("a/b/foo.txt");
        System.out.println("Full Path ='" + p + "' Root='" + p.getRoot() + "' par='" + p.getParent() + "' fname='" + p.getFileName() + "' name count=" + p.getNameCount());
        System.out.println("   normalise ='" + p.normalize() + "' absolute= " + p.isAbsolute());

        p = Paths.get("/root/mid");
        Path c = Paths.get("last/foo.txt");
        System.out.println("Par/Child. Par   path ='" + p + "' Root='" + p.getRoot() + "' par='" + p.getParent() + "' fname='" + p.getFileName() + "' name count=" + p.getNameCount());
        System.out.println("           Child path ='" + c + "' Root='" + c.getRoot() + "' par='" + c.getParent() + "' fname='" + c.getFileName() + "' name count=" + c.getNameCount());
        System.out.println(" Par.compareTo(child)= " + p.compareTo(c) + " relative= '" + p.relativize(Paths.get("/root")) + "'");

        p = Paths.get("~\\foo.txt");
        System.out.println("Par/Child. Par   path ='" + p + "' Root='" + p.getRoot() + "' par='" + p.getParent() + "' fname='" + p.getFileName() + "' name count=" + p.getNameCount());

        p = Paths.get("~/a/b");
        System.out.println("Par/Child. Par   path ='" + p + "' Root='" + p.getRoot() + "' par='" + p.getParent() + "' fname='" + p.getFileName() + "' name count=" + p.getNameCount());

        p = Paths.get("~craig/");
        System.out.println("Par/Child. Par   path ='" + p + "' Root='" + p.getRoot() + "' par='" + p.getParent() + "' fname='" + p.getFileName() + "' name count=" + p.getNameCount());

        p = Paths.get("\\\\share\\path\\goo.txt");
        System.out.println("Par/Child. Par   path ='" + p + "' Root='" + p.getRoot() + "' par='" + p.getParent() + "' fname='" + p.getFileName() + "' name count=" + p.getNameCount());
        
        try {
            p = Paths.get("root/**/foo.*");
        } catch (Exception e) {
            System.out.println("Paths.get exception, expected..");
        }
        File f = new File("root/**/foo.*");
        System.out.println("File isFile? " + f.isFile());

        try {
            p = Paths.get("target/resources/misc.zip");
            System.out.println("Paths.get " + p);
        } catch (Exception e) {
        }
        f = new File("target/resources/misc.zip");
        System.out.println("File isFile? " + f.isFile());

        try {
            p = Paths.get("~");
            System.out.println("Paths.get ~ is " + p);
        } catch (Exception e) {
        }
        f = new File("~");
        System.out.println("File ~ isFile? " + f.isFile() + " isDir? " + f.isDirectory());
        f = new File("~/");
        System.out.println("File ~/ isFile? " + f.isFile() + " isDir? " + f.isDirectory());
        f = new File("~craig");
        System.out.println("File ~ isFile? " + f.isFile() + " isDir? " + f.isDirectory());
        f = new File("~craig/");
        System.out.println("File ~craig/ isFile? " + f.isFile() + " isDir? " + f.isDirectory());

        try {
            p = Paths.get("~/");
            System.out.println("Paths.get ~/ is " + p);
        } catch (Exception e) {
        }
        
        // PathMatcher checks
        String glob = "glob:target\\**\\*monkey.txt";
        PathMatcher m = FileSystems.getDefault().getPathMatcher(glob);
        match(glob, m, "target");
        match(glob, m, "target\\resources");
        match(glob, m, "target\\resources\\misc");
        match(glob, m, "target\\resources\\misc\\test_monkey.txt");
        glob = "glob:**/*monkey.txt";
        m = FileSystems.getDefault().getPathMatcher(glob);
        match(glob, m, "target\\resources\\misc");
        match(glob, m, "target\\resources\\misc\\test_monkey.txt");
        match(glob, m, "target/resources/misc/test_monkey.txt");
	}

    private void match(String glob, PathMatcher m, String p) {
        System.out.println("PathMatch glob '" + glob + "' match '" + p + "? " + m.matches(Paths.get(p)));
    }

    @Test
    public void testFilesEncryptedZip() throws GrepException {
        title("file password protected", "nested", "target/encrypted/misc-password-is-monkey.zip");
        List<String> expected = Arrays.asList(
                "target/encrypted/misc-password-is-monkey.zip[misc/nested_monkey.txt]"
                );
        String[] args = new String[] { 
                //"-r", 
                //"-X", "debug,trace",
                "-p", "monkey",
                "nested",
                "target/encrypted/misc-password-is-monkey.zip" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testFilesEncryptedZipWrongPassword() throws GrepException {
        title("file password protected", "nested", "target/encrypted/misc-password-is-monkey.zip");
        List<String> expected = Arrays.asList(
            "target/encrypted/misc-password-is-monkey.zip[misc/nested_monkey.txt]"
        );
        String[] args = new String[] { 
                //"-r", 
                //"-X", "debug,trace",
                "-p", "wrongPassword",
                "nested",
                "target/encrypted/misc-password-is-monkey.zip" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testFilesNotEncryptedPasswordGiven() throws GrepException {
        title("file password given but file not password protected", "nested", "target/resources/misc.zip");
        List<String> expected = Arrays.asList(
                "target/resources/misc.zip[misc/nested_monkey.txt]",
                "target/resources/misc.zip[misc/nested_monkey.txt]:1:A file nested under misc which mentions a monkey"
                );
        String[] args = new String[] { 
                //"-r", 
                //"-X", "debug,trace",
                "-p", "whocares",
                "nested",
                "target/resources/misc.zip" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testFilesWildDotText() throws GrepException {
        title("file recurse listing, by name", "nested", "target/resources");
        List<String> expected = Arrays.asList(
                "target/resources/misc/nested_monkey.txt"
                );
        String[] args = new String[] { 
                //"-r", 
                "-l", 
                //"-X", "debug,trace", 
                "nested", 
                "target/resources/*.txt", "target/resources/misc/*.txt" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testFilesRecursiveWildDotText() throws GrepException {
        title("file recurse listing, by name", "nested", "target/resources");
        List<String> expected = Arrays.asList(
                "target/resources/misc/nested_monkey.txt"
                );
        String[] args = new String[] { 
                "-r", 
                "-l", 
                //"-X", "debug,trace", 
                "nested", 
                "target/resources/*.txt", "target/resources/misc/*.txt" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testFilesFilenameWild() throws GrepException {
        title("file listing, by name", "nested", "target/r*/*/*_monkey.txt");
        List<String> expected = Arrays.asList(
                "target/resources/misc/nested_monkey.txt"
                );
        String[] args = new String[] { 
                //"-r", 
                "-l", 
                //"-X", "debug,trace",
                "nested", 
                "target/r*/*/*_monkey.txt" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testFilesRecursiveFilenameWildBothParamsQuoted() throws GrepException {
        title("file listing, by name", "nested", "target/r*/*/*_monkey.txt");
        List<String> expected = Arrays.asList(
                "target/resources/misc/nested_monkey.txt"
                );
        String[] args = new String[] { 
                "-r", 
                "-l", 
                //"-X", "debug,trace",
                "'nested'", 
                "'target/r*/*/*_monkey.txt'" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testFileStartsWithAntGlobDoubleQuotedResPath() throws GrepException {
        title("file recurse listing, by name", "nested", "**/*ces/misc/*_monkey.txt");
        List<String> expected = Arrays.asList(
                "./src/test/resources/misc/nested_monkey.txt",
                "./target/resources/misc/nested_monkey.txt"
                );
        String[] args = new String[] { 
                //"-r", 
                "-l", 
                //"-X", "debug",
                "nested", 
                "\"**/*ces/misc/*_monkey.txt\"" };
        try {
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
            rg.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testFileRecursiveStartsWithAntGlobDoubleQuotedSearchPattern() throws GrepException {
        title("file recurse listing, by name", "nested", "**/*ces/misc/*_monkey.txt");
        List<String> expected = Arrays.asList(
                "./src/test/resources/misc/nested_monkey.txt",
                "./target/resources/misc/nested_monkey.txt"
                );
        String[] args = new String[] { 
                "-r", 
                "-l", 
                //"-X", "debug",
                "\"nested\"", 
                "**/*ces/misc/*_monkey.txt" };
        try {
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
            rg.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
    
    @Test
    public void testFileEndsWithAntGlob() throws GrepException {
        title("file recurse listing, by name", "nested", "target/resources/misc/**");
        List<String> expected = Arrays.asList(
                "target/resources/misc/nested_monkey.txt"
                );
        String[] args = new String[] { 
                //"-r", 
                "-l", 
                //"-X", "debug",
                "nested", 
                "target/resources/misc/**" };
        try {
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
            rg.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testFileContainsAntGlob() throws GrepException {
        title("file recurse listing, by name", "nested", "target/resources/**/*_monkey.txt");
        List<String> expected = Arrays.asList(
                "target/resources/misc/nested_monkey.txt"
                );
        String[] args = new String[] { 
                //"-r", 
                "-l", 
                //"-X", "debug",
                "nested", 
                "target/resources/**/*_monkey.txt" };
        try {
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
            rg.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testFileRecursiveContainsAntGlob() throws GrepException {
        title("file recurse listing, by name", "nested", "target/resources/**/*_monkey.txt");
        List<String> expected = Arrays.asList(
                "target/resources/misc/nested_monkey.txt"
                );
        String[] args = new String[] { 
                "-r", 
                "-l", 
                //"-X", "debug",
                "nested", 
                "target/resources/**/*_monkey.txt" };
        try {
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
            rg.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testFileRecursiveAntGlobAndGrouping() throws GrepException {
        title("file recurse listing, by name", "nested", "target/resources/**/*_[m-n]onkey.{txt,foo}");
        List<String> expected = Arrays.asList(
                "target/resources/misc/nested_monkey.txt"
                );
        String[] args = new String[] { 
                "-r", 
                "-l", 
                //"-X", "debug",
                "nested", 
                "target/resources/**/*_[m-n]onkey.{txt,foo}" };
        try {
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
            rg.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testTarGzip() throws GrepException {
        title("tar gzip", "m", "target/resources/r*.tar.gz");
        List<String> expected = Arrays.asList(
            "target/resources/resources.tar.gz[monkey-pics.txt]",
            "target/resources/resources.tar.gz[monkey-pics.txt]:1:A file about happy monkeys.",
            "target/resources/resources.tar.gz[monkey-pics.txt]:3:A monkey was once here.",
            "target/resources/resources.tar.gz[monkey-pics.txt]:5:End of my story.",
            "target/resources/resources.tar.gz[some-other-monkey.txt]",
            "target/resources/resources.tar.gz[some-other-monkey.txt]:1:Another monkey here. ",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/]",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/]",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/]",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.properties]",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.properties]:4:groupId=com.ryanfuse",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  \t<name>Test Jar in Monkey hands</name>",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:19:\t\t  <groupId>org.apache.maven.plugins</groupId>",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:1:<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:20:\t\t  <artifactId>maven-jar-plugin</artifactId>",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:2:<project xmlns=\"http://maven.apache.org/POM/4.0.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\">",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:34:\t  \t  <scope>runtime</scope>",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:3:  \t<modelVersion>4.0.0</modelVersion>",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:6:        <groupId>com.ryanfuse</groupId>",
            "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:9:        <relativePath>../pom.xml</relativePath>"
        );
        String[] args = new String[] { "-r", /*"-X", "debug",*/ "m", "target/resources/r*.tar.gz" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

	@Test
	public void testFilesRecursiveNoContent() throws GrepException {
        title("file recurse, no content", "monkey", "target/resources");
		List<String> expected = Arrays.asList(
		        "target/resources/misc.zip[misc/nested_monkey.txt]",
		        "target/resources/misc/nested_monkey.txt",
		        "target/resources/monkey-pics.txt",
		        "target/resources/monkey-resources.7z",
		        "target/resources/monkey-resources.7z[monkey-pics.txt]",
		        "target/resources/monkey-resources.7z[some-other-monkey.txt]",
		        "target/resources/ms/pp.ppt:1:Notes about monkeys",
		        "target/resources/ms/pp4-all.ppt:1:Some notes to self: monkeys are funny",
		        "target/resources/ms/pp4-all.ppt:C:A comment about your monkey presentation.",
		        "target/resources/ms/word3-all.doc:18: HYPERLINK  /l \"monkey\" monkey",
		        "target/resources/ms/word3-all.doc:19: HYPERLINK \"mailto:monkey@monkeystuff.combo?subject=Monkey%20address\" mailto:monkey@monkeystuff.combo?subject=Monkey address",
		        "target/resources/ms/word3-all.doc:C:A comment about monkeys",
		        "target/resources/ms/word3-all.doc:TB:A text box to quote a famous monkey",
		        "target/resources/ms/word3-all.docx:C:	Comment by Ryan, Craig: A comment about monkeysAnother comment",
		        "target/resources/ms/word3-all.docx:P:mailto:monkey@monkeystuff.combo?subject=Monkey address",
		        "target/resources/ms/word3-all.docx:P:monkey",
		        "target/resources/ms/word3-all.docx:TB:A text box to quote a famous monkey",
		        "target/resources/ms/word3-all.docx:URL:mailto:monkey@monkeystuff.combo?subject=Monkey%20address",
		        "target/resources/resources.tar.gz[monkey-pics.txt]",
		        "target/resources/resources.tar.gz[some-other-monkey.txt]",
		        "target/resources/resources.tar[monkey-pics.txt]",
		        "target/resources/resources.tar[some-other-monkey.txt]",
		        "target/resources/resources.zip[monkey-pics.txt]",
		        "target/resources/resources.zip[some-other-monkey.txt]",
		        "target/resources/some-other-monkey.txt",
		        "target/resources/test-war-0.1.war[WEB-INF/classes/images/capture_monkey.PNG]",
		        "target/resources/test-war-0.1.war[monkey_play.html]"
			);
		String[] args = new String[] { "-r", "-l", /*"-X", "debug",*/ "monkey", "target/resources" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
        //display.setSortedPaths(false);
		rg.setDisplay(display);
		rg.getSwitches().setOcr(false);
		rg.execute();
		List<String> actual = display.messages();
		//dumpActualList("target/frnc.txt", actual);
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

    @Test
    public void testFilesRecursiveListingOnlyNameMatch() throws GrepException {
        title("file recurse listing only, by name", "nested", "target/resources");
        List<String> expected = Arrays.asList(
            "target/resources/misc.zip[misc/nested_monkey.txt]",
            "target/resources/misc/nested_monkey.txt"
            );
        String[] args = new String[] { 
            "-r", 
            "-l", 
            /*"-X", "debug",*/ 
            "nested", 
            "target/resources" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

	@Test
	public void testFilesRecursiveIncludeContentNameOnlyMatch() throws GrepException {
        title("file recurse content, by name", "nested_m", "target/resources");
		List<String> expected = Arrays.asList(
			"target/resources/misc.zip[misc/nested_monkey.txt]",
            "target/resources/misc/nested_monkey.txt"
		);
		String[] args = new String[] { 
			"-r", 
			//"-i",
			//"-X", "debug,trace", 
			"nested_m", 
			"target/resources" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.getSwitches().setOcr(false);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testFilesRecursiveIncludeContentNameOnlyMatchIgnoreCase() throws GrepException {
        title("file recurse, by name, ignore case", "nested_m", "target/resources");
		List<String> expected = Arrays.asList(
			"target/resources/misc.zip[misc/nested_monkey.txt]",
            "target/resources/misc/nested_monkey.txt"
		);
		String[] args = new String[] { 
			"-r", 
			"-i",
			/*"-X", "debug",*/ 
			"nested_m", 
			"target/resources" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.getSwitches().setOcr(false);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testFilesRecursiveIncludeContentWildNameOnlyMatch() throws GrepException {
        title("file recurse, contents wild, by name", "n??ted_m", "target/resources");
		List<String> expected = Arrays.asList(
			"target/resources/misc.zip[misc/nested_monkey.txt]",
            "target/resources/misc/nested_monkey.txt"
		);
		String[] args = new String[] { 
			"-r", 
			//"-X", "debug",
			"n??ted_m", 
			"target/resources" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.getSwitches().setOcr(false);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	public void testFilesRecursiveIncludeContentWildNameOnlyMatchIgnoreCase() throws GrepException {
        title("file recurse, content wild, by name, ignore case", "n??tEd_m", "target/resources");
		List<String> expected = Arrays.asList(
			"target/resources/misc.zip[misc/nested_monkey.txt]",
            "target/resources/misc/nested_monkey.txt"
		);
		String[] args = new String[] { 
			"-r", 
			"-i",
			/*"-X", "debug",*/ 
			"n??tEd_m", 
			"target/resources" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.getSwitches().setOcr(false);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testEarFilesRecursiveContent() throws GrepException {
        title("ear file recursive content", "onkey", "target/resources/*.ear");
		List<String> expected = Arrays.asList(
			"target/resources/test-ear-0.1.ear[META-INF/MANIFEST.MF]:2:WebLogic-Application-Version: Monkey1.0",
			"target/resources/test-ear-0.1.ear[META-INF/MANIFEST.MF]:5:Created-By: Apache monkey",
			"target/resources/test-ear-0.1.ear[META-INF/maven/com.ryanfuse/test-ear/pom.xml]:14:  	<name>Test Ear of the Monkey</name>",
			"target/resources/test-ear-0.1.ear[META-INF/maven/com.ryanfuse/test-ear/pom.xml]:28:				<WebLogic-Application-Version>Monkey1.0</WebLogic-Application-Version>",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][META-INF/maven/com.ryanfuse/test-war/pom.xml]:14:  	<name>Test War of the Monkeys</name>",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/classes/images/capture_monkey.PNG]",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/lib/test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/web.xml]:6:  <display-name>HelloMonkey Application</display-name>",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][index.html]:2:Hi monkey",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][monkey_play.html]",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][monkey_play.html]:2:\t<head>stuff of monkeys</head>"
		);
		String[] args = new String[] { 
		        "-r", 
		        //"--warn", 
		        //"-X", "debug",
		        "onkey", "target/resources/*.ear" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}
	
	@Test
	public void testEarFilesRecursiveContentIgnoreCase() throws GrepException {
        title("ear file content recursive (ignore case)", "?nKey", "target/resources/*.ear");
		List<String> expected = Arrays.asList(
		   "target/resources/test-ear-0.1.ear[META-INF/MANIFEST.MF]:2:WebLogic-Application-Version: Monkey1.0",
		   "target/resources/test-ear-0.1.ear[META-INF/MANIFEST.MF]:5:Created-By: Apache monkey",
		   "target/resources/test-ear-0.1.ear[META-INF/maven/com.ryanfuse/test-ear/pom.xml]:14:  	<name>Test Ear of the Monkey</name>",
		   "target/resources/test-ear-0.1.ear[META-INF/maven/com.ryanfuse/test-ear/pom.xml]:28:				<WebLogic-Application-Version>Monkey1.0</WebLogic-Application-Version>",
		   "target/resources/test-ear-0.1.ear[test-war-0.1.war][META-INF/maven/com.ryanfuse/test-war/pom.xml]:14:  	<name>Test War of the Monkeys</name>",
		   "target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/classes/images/capture_monkey.PNG]",
		   "target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
		   "target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/lib/test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
		   "target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/web.xml]:6:  <display-name>HelloMonkey Application</display-name>",
		   "target/resources/test-ear-0.1.ear[test-war-0.1.war][index.html]:2:Hi monkey",
		   "target/resources/test-ear-0.1.ear[test-war-0.1.war][monkey_play.html]",
		   "target/resources/test-ear-0.1.ear[test-war-0.1.war][monkey_play.html]:2:	<head>stuff of monkeys</head>"
		);
		String[] args = new String[] { 
		        "-r", 
		        //"--warn", 
		        //"-X", "debug", 
		        "-i",
		        "?nKey", "target/resources/*.ear" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
		        actualList(actual, expected),
		        ListUtils.isEqualList(expected, actual));
	}

	//@Test
	public void testPpt() throws GrepException {
	        title("file recursive content", "onkey", "target/resources/ms/pp4-all.pptx");
	        List<String> expected = Arrays.asList(
	              );
	        String[] args = new String[] { "-r", 
	                //"-X", "debug", 
	                "--warn",
	                "*", "target/resources/ms/pp4-all.pptx" };
	            ResourceGrep rg = new ResourceGrep(args);
	            display = new DisplayRecorder(rg.getDisplay());
	            //display.setSortedPaths(false);
	            rg.setDisplay(display);
	            rg.getSwitches().setOcr(false);
	            rg.execute();
	            List<String> actual = display.messages();
	            //dumpActualList("target/frc.txt", actual);
	            assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	}

    @Test
    public void testFileRecursiveContent() throws GrepException {
        title("file recursive content", "onkey", "target/resources/");
        List<String> expected = Arrays.asList(
                "target/resources/junit-pom.xml:10:		<monkey-junit.version>4.8.2</monkey-junit.version>",
                "target/resources/junit-pom.xml:20:			<version>${monkey-junit.version}</version>",
                "target/resources/misc.zip[misc/nested_monkey.txt]",
                "target/resources/misc.zip[misc/nested_monkey.txt]:1:A file nested under misc which mentions a monkey",
                "target/resources/misc.zip[misc/nested_monkey.txt]:4:but a monkey indeed",
                "target/resources/misc/nested_monkey.txt",
                "target/resources/misc/nested_monkey.txt:1:A file nested under misc which mentions a monkey",
                "target/resources/misc/nested_monkey.txt:4:but a monkey indeed",
                "target/resources/monkey-pics.txt",
                "target/resources/monkey-pics.txt:1:A file about happy monkeys.",
                "target/resources/monkey-pics.txt:3:A monkey was once here.",
                "target/resources/monkey-resources.7z",
                "target/resources/monkey-resources.7z[monkey-pics.txt]",
                "target/resources/monkey-resources.7z[monkey-pics.txt]:1:A file about happy monkeys.",
                "target/resources/monkey-resources.7z[monkey-pics.txt]:3:A monkey was once here.",
                "target/resources/monkey-resources.7z[some-other-monkey.txt]",
                "target/resources/monkey-resources.7z[some-other-monkey.txt]:1:Another monkey here. ",
                "target/resources/ms/excel2-all.xls:0:0:1:Monkey ",
                "target/resources/ms/excel2-all.xls:H:0:Centre Monkey",
                "target/resources/ms/excel2-all.xls:H:0:Left Monkey",
                "target/resources/ms/excel2-all.xls:H:0:Right Monkey",
                "target/resources/ms/excel2-all.xlsx:0:0:1:Monkey ",
                "target/resources/ms/excel2-all.xlsx:H:0:Centre Monkey",
                "target/resources/ms/excel2-all.xlsx:H:0:Left Monkey",
                "target/resources/ms/excel2-all.xlsx:H:0:Right Monkey",
                "target/resources/ms/pp.ppt:1:Notes about monkeys",
                "target/resources/ms/pp4-all.ppt:1:Monkey Business",
                "target/resources/ms/pp4-all.ppt:1:Some notes to self: monkeys are funny",
                "target/resources/ms/pp4-all.ppt:C:A comment about your monkey presentation.",
                "target/resources/ms/pp4-all.pptx:1:Monkey Business",
                "target/resources/ms/word3-all.doc:14:Monkey cell 1",
                "target/resources/ms/word3-all.doc:18: HYPERLINK  /l \"monkey\" monkey",
                "target/resources/ms/word3-all.doc:19: HYPERLINK \"mailto:monkey@monkeystuff.combo?subject=Monkey%20address\" mailto:monkey@monkeystuff.combo?subject=Monkey address",
                "target/resources/ms/word3-all.doc:3:A paper about Monkeys who play lawn bowls",
                "target/resources/ms/word3-all.doc:C:A comment about monkeys",
                "target/resources/ms/word3-all.doc:TB:A text box to quote a famous monkey",
                "target/resources/ms/word3-all.docx:C:	Comment by Ryan, Craig: A comment about monkeysAnother comment",
                "target/resources/ms/word3-all.docx:P:A paper about Monkeys who play lawn bowls",
                "target/resources/ms/word3-all.docx:P:mailto:monkey@monkeystuff.combo?subject=Monkey address",
                "target/resources/ms/word3-all.docx:P:monkey",
                "target/resources/ms/word3-all.docx:T:Monkey cell 1",
                "target/resources/ms/word3-all.docx:TB:A text box to quote a famous monkey",
                "target/resources/ms/word3-all.docx:URL:mailto:monkey@monkeystuff.combo?subject=Monkey%20address",
                "target/resources/resources.tar.gz[monkey-pics.txt]",
                "target/resources/resources.tar.gz[monkey-pics.txt]:1:A file about happy monkeys.",
                "target/resources/resources.tar.gz[monkey-pics.txt]:3:A monkey was once here.",
                "target/resources/resources.tar.gz[some-other-monkey.txt]",
                "target/resources/resources.tar.gz[some-other-monkey.txt]:1:Another monkey here. ",
                "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
                "target/resources/resources.tar.gz[test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
                "target/resources/resources.tar[monkey-pics.txt]",
                "target/resources/resources.tar[monkey-pics.txt]:1:A file about happy monkeys.",
                "target/resources/resources.tar[monkey-pics.txt]:3:A monkey was once here.",
                "target/resources/resources.tar[some-other-monkey.txt]",
                "target/resources/resources.tar[some-other-monkey.txt]:1:Another monkey here. ",
                "target/resources/resources.tar[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
                "target/resources/resources.tar[test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
                "target/resources/resources.zip[monkey-pics.txt]",
                "target/resources/resources.zip[monkey-pics.txt]:1:A file about happy monkeys.",
                "target/resources/resources.zip[monkey-pics.txt]:3:A monkey was once here.",
                "target/resources/resources.zip[some-other-monkey.txt]",
                "target/resources/resources.zip[some-other-monkey.txt]:1:Another monkey here. ",
                "target/resources/some-other-monkey.txt",
                "target/resources/some-other-monkey.txt:1:Another monkey here. ",
                "target/resources/test-ear-0.1.ear[META-INF/MANIFEST.MF]:2:WebLogic-Application-Version: Monkey1.0",
                "target/resources/test-ear-0.1.ear[META-INF/MANIFEST.MF]:5:Created-By: Apache monkey",
                "target/resources/test-ear-0.1.ear[META-INF/maven/com.ryanfuse/test-ear/pom.xml]:14:  	<name>Test Ear of the Monkey</name>",
                "target/resources/test-ear-0.1.ear[META-INF/maven/com.ryanfuse/test-ear/pom.xml]:28:				<WebLogic-Application-Version>Monkey1.0</WebLogic-Application-Version>",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][META-INF/maven/com.ryanfuse/test-war/pom.xml]:14:  	<name>Test War of the Monkeys</name>",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/classes/images/capture_monkey.PNG]",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/lib/test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/web.xml]:6:  <display-name>HelloMonkey Application</display-name>",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][index.html]:2:Hi monkey",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][monkey_play.html]",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][monkey_play.html]:2:	<head>stuff of monkeys</head>",
                "target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
                "target/resources/test-jar-0.1.jar[org/crgrep/TestMonkey.class]",
                "target/resources/test-war-0.1.war[META-INF/maven/com.ryanfuse/test-war/pom.xml]:14:  	<name>Test War of the Monkeys</name>",
                "target/resources/test-war-0.1.war[WEB-INF/classes/images/capture_monkey.PNG]",
                "target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
                "target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
                "target/resources/test-war-0.1.war[WEB-INF/web.xml]:6:  <display-name>HelloMonkey Application</display-name>",
                "target/resources/test-war-0.1.war[index.html]:2:Hi monkey",
                "target/resources/test-war-0.1.war[monkey_play.html]",
                "target/resources/test-war-0.1.war[monkey_play.html]:2:	<head>stuff of monkeys</head>"
                );
        String[] args = new String[] { "-r", 
            //"-X", "debug", 
            "onkey", "target/resources/" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        //display.setSortedPaths(false);
        //display.setStripControls(true);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        //dumpActualList("target/frc2.txt", actual);
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testFileTrimContentOnly() throws GrepException {
        title("file trim content only (not name)", "onkey", "target/resources/");
        List<String> expected = Arrays.asList(
                "target/resources/junit-pom.xml:10:...onkey...",
                "target/resources/junit-pom.xml:20:...onkey...",
                "target/resources/misc.zip[misc/nested_monkey.txt]",
                "target/resources/misc.zip[misc/nested_monkey.txt]:1:...onkey",
                "target/resources/misc.zip[misc/nested_monkey.txt]:4:...onkey...",
                "target/resources/misc/nested_monkey.txt",
                "target/resources/misc/nested_monkey.txt:1:...onkey",
                "target/resources/misc/nested_monkey.txt:4:...onkey...",
                "target/resources/monkey-pics.txt",
                "target/resources/monkey-pics.txt:1:...onkey...",
                "target/resources/monkey-pics.txt:3:...onkey...",
                "target/resources/monkey-resources.7z",
                "target/resources/monkey-resources.7z[monkey-pics.txt]",
                "target/resources/monkey-resources.7z[monkey-pics.txt]:1:...onkey...",
                "target/resources/monkey-resources.7z[monkey-pics.txt]:3:...onkey...",
                "target/resources/monkey-resources.7z[some-other-monkey.txt]",
                "target/resources/monkey-resources.7z[some-other-monkey.txt]:1:...onkey...",
                "target/resources/ms/excel2-all.xls:0:0:1:...onkey...",
                "target/resources/ms/excel2-all.xls:H:0:...onkey",
                "target/resources/ms/excel2-all.xls:H:0:...onkey",
                "target/resources/ms/excel2-all.xls:H:0:...onkey",
                "target/resources/ms/excel2-all.xlsx:0:0:1:...onkey...",
                "target/resources/ms/excel2-all.xlsx:H:0:...onkey",
                "target/resources/ms/excel2-all.xlsx:H:0:...onkey",
                "target/resources/ms/excel2-all.xlsx:H:0:...onkey",
                "target/resources/ms/pp.ppt:1:...onkey...",
                "target/resources/ms/pp4-all.ppt:1:...onkey...",
                "target/resources/ms/pp4-all.ppt:1:...onkey...",
                "target/resources/ms/pp4-all.ppt:C:...onkey...",
                "target/resources/ms/pp4-all.pptx:1:...onkey...",
                "target/resources/ms/word3-all.doc:14:...onkey...",
                "target/resources/ms/word3-all.doc:18:...onkey...",
                "target/resources/ms/word3-all.doc:19:...onkey...",
                "target/resources/ms/word3-all.doc:3:...onkey...",
                "target/resources/ms/word3-all.doc:C:...onkey...",
                "target/resources/ms/word3-all.doc:TB:...onkey",
                "target/resources/ms/word3-all.docx:C:...onkey...",
                "target/resources/ms/word3-all.docx:P:...onkey",
                "target/resources/ms/word3-all.docx:P:...onkey...",
                "target/resources/ms/word3-all.docx:P:...onkey...",
                "target/resources/ms/word3-all.docx:T:...onkey...",
                "target/resources/ms/word3-all.docx:TB:...onkey",
                "target/resources/ms/word3-all.docx:URL:...onkey...",
                "target/resources/resources.tar.gz[monkey-pics.txt]",
                "target/resources/resources.tar.gz[monkey-pics.txt]:1:...onkey...",
                "target/resources/resources.tar.gz[monkey-pics.txt]:3:...onkey...",
                "target/resources/resources.tar.gz[some-other-monkey.txt]",
                "target/resources/resources.tar.gz[some-other-monkey.txt]:1:...onkey...",
                "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:...onkey...",
                "target/resources/resources.tar.gz[test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
                "target/resources/resources.tar[monkey-pics.txt]",
                "target/resources/resources.tar[monkey-pics.txt]:1:...onkey...",
                "target/resources/resources.tar[monkey-pics.txt]:3:...onkey...",
                "target/resources/resources.tar[some-other-monkey.txt]",
                "target/resources/resources.tar[some-other-monkey.txt]:1:...onkey...",
                "target/resources/resources.tar[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:...onkey...",
                "target/resources/resources.tar[test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
                "target/resources/resources.zip[monkey-pics.txt]",
                "target/resources/resources.zip[monkey-pics.txt]:1:...onkey...",
                "target/resources/resources.zip[monkey-pics.txt]:3:...onkey...",
                "target/resources/resources.zip[some-other-monkey.txt]",
                "target/resources/resources.zip[some-other-monkey.txt]:1:...onkey...",
                "target/resources/some-other-monkey.txt",
                "target/resources/some-other-monkey.txt:1:...onkey...",
                "target/resources/test-ear-0.1.ear[META-INF/MANIFEST.MF]:2:...onkey...",
                "target/resources/test-ear-0.1.ear[META-INF/MANIFEST.MF]:5:...onkey",
                "target/resources/test-ear-0.1.ear[META-INF/maven/com.ryanfuse/test-ear/pom.xml]:14:...onkey...",
                "target/resources/test-ear-0.1.ear[META-INF/maven/com.ryanfuse/test-ear/pom.xml]:28:...onkey...",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][META-INF/maven/com.ryanfuse/test-war/pom.xml]:14:...onkey...",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/classes/images/capture_monkey.PNG]",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:...onkey...",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/lib/test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/web.xml]:6:...onkey...",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][index.html]:2:...onkey",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][monkey_play.html]",
                "target/resources/test-ear-0.1.ear[test-war-0.1.war][monkey_play.html]:2:...onkey...",
                "target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:...onkey...",
                "target/resources/test-jar-0.1.jar[org/crgrep/TestMonkey.class]",
                "target/resources/test-war-0.1.war[META-INF/maven/com.ryanfuse/test-war/pom.xml]:14:...onkey...",
                "target/resources/test-war-0.1.war[WEB-INF/classes/images/capture_monkey.PNG]",
                "target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:...onkey...",
                "target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
                "target/resources/test-war-0.1.war[WEB-INF/web.xml]:6:...onkey...",
                "target/resources/test-war-0.1.war[index.html]:2:...onkey",
                "target/resources/test-war-0.1.war[monkey_play.html]",
                "target/resources/test-war-0.1.war[monkey_play.html]:2:...onkey..."
        );
        String[] args = new String[] { "-r", 
            //"-X", "debug", 
            "onkey", "target/resources/" };
        ResourceGrep rg = new ResourceGrep(args);
        rg.getSwitches().setMaxResultLength(5);
        display = new DisplayRecorder(rg.getDisplay());
        //display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        //dumpActualList("target/trim.txt", actual);
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }
	
    @Test
    public void testStandardInput() throws GrepException {
        title("stdin content", "monkey", "-");
		List<String> expected = Arrays.asList(
				"<stdin>:1:A file about happy monkeys.",
				"<stdin>:3:A monkey was once here."
		);
		String[] args = new String[] { 
		    // "--warn", 
		    // "-X", "debug",
		    "monkey", "-" };
		File infile = new File("target/resources/monkey-pics.txt");
		FileInputStream fis = null;
		try {
		    fis = new FileInputStream(infile);
		} catch (FileNotFoundException e) {
		    fail("Failed to read file target/resources/monkey-pics.txt");
		}
		// Re-assign stdin to our test data file
		System.setIn(fis);

		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
		    actualList(actual, expected),
		    ListUtils.isEqualList(expected, actual));
	}
    
    @Test
    public void testStandardInputIgnoreCase() throws GrepException {
        title("stdin content (ignore case)", "monKEY", "-");
        List<String> expected = Arrays.asList(
            "<stdin>:1:A file about happy monkeys.",
            "<stdin>:3:A monkey was once here."
        );
        String[] args = new String[] { 
            /*"--warn", "-X", "debug",*/ 
            "-i", 
            "monKEY", "-" };
        File infile = new File("target/resources/monkey-pics.txt");
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(infile);
        } catch (FileNotFoundException e) {
            fail("Failed to read file target/resources/monkey-pics.txt");
        }
        // Re-assign stdin to our test data file
        System.setIn(fis);

        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }
    
    @Test
    public void testNotSuchFile() throws GrepException {
        title("no such file", "pat", "target/no/such/file.txt");
        String[] args = new String[] {
            //"-X", "debug",
            "pat", "target/no/such/file.txt" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
        assertTrue(display.isErrorCalled());
        assertTrue(display.getErrorText().contains("No such file"));
    }
    
    @Test
    public void testNotSuchFilePattern() throws GrepException {
        title("no such file pattern", "pat", "target/no/such/*.txt");
        String[] args = new String[] {
            //"-X", "debug",
            "pat", "target/no/such/*.txt" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
        assertTrue(display.isErrorCalled());
        assertTrue(display.getErrorText().contains("No such file"));
    }

    @Test
    public void testMoodFilterIgnoredWhenListOptionSpecified() throws GrepException {
        title("file listing, by name. Mood ignored", "nested", "target/resources");
        List<String> expected = Arrays.asList(
                "target/resources/misc/nested_monkey.txt"
                );
        String[] args = new String[] { 
                "--mood", "positive",
                "--warn",
                "-l", 
                //"-X", "debug,trace", 
                "nested", 
                "target/resources/misc/*.txt" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        assertFalse(((FileGrep)rg.getGrep()).hasContentFilter(ContentFilter.MOOD_FILTER));
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testClose() throws GrepException {
        Switches sw = new Switches();
        FileGrep fg = new FileGrep(sw);
        fg.closeResource((Reader)null);
        fg.closeResource((InputStream)null);
    }

    @Test
    public void testUnsupportedBinary() throws GrepException {
        String[] args = new String[] {
           "pat", "target/resources/Test.exe" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
        assertFalse(display.isErrorCalled());
        assertFalse(display.isWarnCalled());
        assertTrue(display.isTraceCalled());
        System.out.println(display.getTraceText());
        assertTrue(display.getTraceText().contains("unsupported binary file type"));
    }

    @Test
    public void testDirectory() throws GrepException {
        Switches sw = new Switches();
        FileGrep fg = new FileGrep(sw);
        ResourcePattern rp = new ResourcePattern("nomatch");
        fg.setResourcePattern(rp);
        DirectoryFileType dir = new DirectoryFileType(fg);
        dir.grepEntry(new File("target"));
    }
}
