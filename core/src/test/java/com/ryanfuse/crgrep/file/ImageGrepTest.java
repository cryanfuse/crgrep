/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import org.apache.commons.collections.ListUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.Display;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Test for matches against specific meta data stored within an image files 
 * and mocked OCR text extraction tests. 
 * 
 * @author Craig Ryan
 */
@RunWith(MockitoJUnitRunner.class)
public class ImageGrepTest extends TestCommon {

    @Mock private Tesseract ocrReader;
    @Mock private FileGrep fileGrep;
    @Mock private ResourcePattern resourcePattern;
    @Mock private BufferedImage bImage;
    
    private Pattern pattern;
    private DisplayRecorder display;
    
    @Before
    public void setUp() throws Exception {
        pattern = Pattern.compile(".*");
    }
    
    @Test
    public void testImageEasyShooting() throws GrepException {
		List<String> expected = Arrays.asList(
				"src/test/resources/images/Canon IXUS v3.jpg: @{ExposureMode=Easy shooting}",
				"src/test/resources/images/Canon PowerShot S300.jpg: @{ExposureMode=Easy shooting}"
				);
		title("images easy shooting", "hoo", "src/test/resources/images/Can*");
        String[] args = new String[] { "-r", 
                //"-X", "debug,trace", 
                "hoo", "src/test/resources/images/Can*" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

    @Test
    public void testImageDPI() throws GrepException {
        List<String> expected = Arrays.asList(
            "src/test/resources/images/Canon EOS D60.jpg: @{Resolution Info=300.0x300.0 DPI}",
            "src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{Resolution Info=300.0x300.0 DPI}",
            "src/test/resources/images/Sony Cybershot (3).jpg: @{Resolution Info=72.0x72.0 DPI}",
            "src/test/resources/images/Sony DigitalMavica.jpg: @{Resolution Info=72.0x72.0 DPI}"
            );
        title("images DPI", "DPI", "src/test/resources/images");
        String[] args = new String[] { "-r", 
                //"-X", "debug",
                "DPI", "src/test/resources/images" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testImageShortOutput() throws GrepException {
        List<String> expected = Arrays.asList(
                "src/test/resources/images/Canon EOS D60.jpg: @{Resolution Info=...0 DPI}",
                "src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{Resolution Info=...0 DPI}",
                "src/test/resources/images/Sony Cybershot (3).jpg: @{Resolution Info=...0 DPI}",
                "src/test/resources/images/Sony DigitalMavica.jpg: @{Resolution Info=...0 DPI}"
                );
        title("images short output", "DPI", "src/test/resources/images");
        String[] args = new String[] { "-r", 
                //"-X", "debug",
                "DPI", "src/test/resources/images" };
        ResourceGrep rg = new ResourceGrep(args);
        rg.getSwitches().setMaxResultLength(5);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        //dumpActualList("target/img.txt", actual);
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testImageDPIIgnoreCase() throws GrepException {
        List<String> expected = Arrays.asList(
           "src/test/resources/images/Canon EOS D60.jpg: @{Resolution Info=300.0x300.0 DPI}",
           "src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{Resolution Info=300.0x300.0 DPI}",
           "src/test/resources/images/Sony Cybershot (3).jpg: @{Resolution Info=72.0x72.0 DPI}",
           "src/test/resources/images/Sony DigitalMavica.jpg: @{Resolution Info=72.0x72.0 DPI}"
        );
        title("images DPI", "DpI", "src/test/resources/images");
        String[] args = new String[] { "-r", 
                //"-X", "debug",
                "-i",
                "DpI", "src/test/resources/images" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testImageDPIWild() throws GrepException {
        List<String> expected = Arrays.asList(
            "src/test/resources/images/Canon EOS D60.jpg: @{Resolution Info=300.0x300.0 DPI}",
            "src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{Resolution Info=300.0x300.0 DPI}",
            "src/test/resources/images/Sony Cybershot (3).jpg: @{Resolution Info=72.0x72.0 DPI}",
            "src/test/resources/images/Sony DigitalMavica.jpg: @{Resolution Info=72.0x72.0 DPI}"
            );
        title("images DPI", "D?I", "src/test/resources/images");
        String[] args = new String[] { "-r", 
                //"-X", "debug",
                "D?I", "src/test/resources/images" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testImageDPIWildIgnoreCase() throws GrepException {
        List<String> expected = Arrays.asList(
            "src/test/resources/images/Canon EOS D60.jpg: @{Resolution Info=300.0x300.0 DPI}",
            "src/test/resources/images/Epson PhotoPC 3000Z.jpg: @{Scene Type=Directly photographed image}",
            "src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{Resolution Info=300.0x300.0 DPI}",
            "src/test/resources/images/Minolta DiMAGE X.jpg: @{Scene Type=Directly photographed image}",
            "src/test/resources/images/Nikon D1X.jpg: @{Scene Type=Directly photographed image}",
            "src/test/resources/images/Nikon E5000.jpg: @{Scene Type=Directly photographed image}",
            "src/test/resources/images/Olympus C4040Z.jpg: @{Scene Type=Directly photographed image}",
            "src/test/resources/images/Ricoh DC-3Z (low res).jpg: @{Copyright=(C)Hendrik Wrdehoff     }",
            "src/test/resources/images/Sony Cybershot (3).jpg: @{Resolution Info=72.0x72.0 DPI}",
            "src/test/resources/images/Sony Cybershot (3).jpg: @{Scene Type=Directly photographed image}",
            "src/test/resources/images/Sony DigitalMavica.jpg: @{Resolution Info=72.0x72.0 DPI}",
            "src/test/resources/images/Sony DigitalMavica.jpg: @{Scene Type=Directly photographed image}"
        );
        title("images DPI", "D?i", "src/test/resources/images");
        String[] args = new String[] { "-r", 
                //"-X", "debug",
                "-i", "D?i", "src/test/resources/images" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testImageLongOutput() throws GrepException {
        List<String> expected = Arrays.asList(
                "src/test/resources/images/Apple iPhone 4.jpg: @{Exposure Time=1/15 sec, F-Number=f/2.8, Exposure Program=Program normal, ISO Speed Ratings=500, Exif Version=2.21, Date/Time Original=2011:01:13 14:33:39, Date/Time Digitized=2011:01:13 14:33:39, Shutter Speed Value=1/15 sec, Aperture Value=f/2.8, Metering Mode=Average, Flash=Flash did not fire, ..., Focal Length=3.85 mm, Subject Location=0, FlashPix Version=1.00, Color Space=sRGB, Exif Image Width=1296 pixels, Exif Image Height=968 pixels, Sensing Method=One-chip color area ..., Exposure Mode=Auto exposure, White Balance Mode=Auto white balance, Scene Capture Type=Standard, Sharpness=Hard}",
                "src/test/resources/images/Apple iPhone 4.jpg: @{GPS Latitude Ref=N, GPS Latitude=41 51' 10.8\", GPS Longitude Ref=E, GPS Longitude=12 29' 19.8\", GPS Time-Stamp=14:33:35.62 UTC, GPS Img Direction Ref=True direction, GPS Img Direction=177.56 degrees}",
                "src/test/resources/images/Apple iPhone 4.jpg: @{Make=Apple, Model=iPhone 4, Orientation=Top, left side (Hori..., X Resolution=72 dots per inch, Y Resolution=72 dots per inch, Resolution Unit=Inch, Software=4.1, Date/Time=2011:01:13 14:33:39}",
                "src/test/resources/images/Apple iPhone 4.jpg: @{Profile Size=3144, CMM Type=Lino, Version=2.1.0, Class=Display Device, Color space=RGB , Profile Connection Space=XYZ , Profile Date/Time=Mon Mar 09 17:49:00 ..., Signature=acsp, Primary Platform=Microsoft Corporatio..., Device manufacturer=IEC , Device model=sRGB, Rendering Intent=Media-Relative Color..., XYZ values=0.9642029 1.0 0.8249..., Tag Count=17, Copyright=Copyright (c) 1998 H..., Profile Description=sRGB IEC61966-2.1, Media White Point=(0.9504547, 1.0, 1.0..., Media Black Point=(0.0, 0.0, 0.0), Red Colorant=(0.43606567, 0.22248..., Green Colorant=(0.3851471, 0.716873..., Blue Colorant=(0.1430664, 0.060607..., Device Mfg Description=IEC http://www.iec.c..., Device Model Description=IEC 61966-2.1 Defaul..., Viewing Conditions Description=Reference Viewing Co..., Viewing Conditions=view(0x76696577): 36..., Luminance=(76.03647, 80.0, 87...., Measurement=1931 2 Observer, Ba..., Technology=CRT , Red TRC=0.0, 0.0000763, 0.00..., Green TRC=0.0, 0.0000763, 0.00..., Blue TRC=0.0, 0.0000763, 0.00...}",
                "src/test/resources/images/Apple iPhone 4.jpg: @{Version=1.1, Resolution Units=inch, X Resolution=72 dots, Y Resolution=72 dots}"                
                );
        title("images long metadata output", "*", "src/test/resources/images/Apple iPhone 4.jpg");
        String[] args = new String[] { 
                /*"-X", "debug,trace",*/ 
                "*", 
                "src/test/resources/images/Apple iPhone 4.jpg" };
        ResourceGrep rg = new ResourceGrep(args);
        rg.getSwitches().setMaxResultLength(20);
        display = new DisplayRecorder(rg.getDisplay());
        display.setSortedPaths(true);
        display.setStripControls(true);
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        //dumpActualList("target/img.txt", actual);
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testImageDotsPerInch() throws GrepException {
        List<String> expected = Arrays.asList(
                "src/test/resources/images/Apple iPhone 4.jpg: @{X Resolution=72 dots per inch, Y Resolution=72 dots per inch}",
                "src/test/resources/images/Canon EOS D60.jpg: @{X Resolution=300 dots per inch, Y Resolution=300 dots per inch}",
                "src/test/resources/images/Canon EOS D60.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
                "src/test/resources/images/Canon IXUS v3.jpg: @{X Resolution=180 dots per inch, Y Resolution=180 dots per inch}",
                "src/test/resources/images/Canon IXUS v3.jpg: @{XResolution=180 dots per inch, YResolution=180 dots per inch}",
                "src/test/resources/images/Canon PowerShot S300.jpg: @{X Resolution=180 dots per inch, Y Resolution=180 dots per inch}",
                "src/test/resources/images/Canon PowerShot S300.jpg: @{XResolution=180 dots per inch, YResolution=180 dots per inch}",
                "src/test/resources/images/Epson PhotoPC 3000Z.jpg: @{X Resolution=480 dots per inch, Y Resolution=480 dots per inch}",
                "src/test/resources/images/Epson PhotoPC 3000Z.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
                "src/test/resources/images/FujiFilm DS-7 (2).jpg: @{X Resolution=72 dots per inch, Y Resolution=72 dots per inch}",
                "src/test/resources/images/FujiFilm DS-7 (2).jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
                "src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{X Resolution=300 dots per inch, Y Resolution=300 dots per inch}",
                "src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
                "src/test/resources/images/HP PhotoSmart 735.jpg: @{X Resolution=72 dots per inch, Y Resolution=72 dots per inch}",
                "src/test/resources/images/HP PhotoSmart 735.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
                "src/test/resources/images/Kodak DC210.jpg: @{X Resolution=216 dots per inch, Y Resolution=216 dots per inch}",
                "src/test/resources/images/Kodak DC210.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
                "src/test/resources/images/Minolta DiMAGE X.jpg: @{X Resolution=72 dots per inch, Y Resolution=72 dots per inch}",
                "src/test/resources/images/Minolta DiMAGE X.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
                "src/test/resources/images/Nikon D1X.jpg: @{X Resolution=72 dots per inch, Y Resolution=72 dots per inch}",
                "src/test/resources/images/Nikon E5000.jpg: @{X Resolution=300 dots per inch, Y Resolution=300 dots per inch}",
                "src/test/resources/images/Nikon E5000.jpg: @{XResolution=300 dots per inch, YResolution=300 dots per inch}",
                "src/test/resources/images/Olympus C4040Z.jpg: @{X Resolution=72 dots per inch, Y Resolution=72 dots per inch}",
                "src/test/resources/images/Olympus C4040Z.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
                "src/test/resources/images/Pentax Optio S4.jpg: @{X Resolution=72 dots per inch, Y Resolution=72 dots per inch}",
                "src/test/resources/images/Ricoh DC-3Z (low res).jpg: @{X Resolution=72 dots per inch, Y Resolution=72 dots per inch}",
                "src/test/resources/images/Sanyo SR6.jpg: @{X Resolution=72 dots per inch, Y Resolution=72 dots per inch}",
                "src/test/resources/images/Sanyo SR6.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
                "src/test/resources/images/Sony Cybershot (3).jpg: @{X Resolution=72 dots per inch, Y Resolution=72 dots per inch}",
                "src/test/resources/images/Sony Cybershot (3).jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
                "src/test/resources/images/Sony DigitalMavica.jpg: @{X Resolution=72 dots per inch, Y Resolution=72 dots per inch}",
                "src/test/resources/images/Sony DigitalMavica.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}"
                );
        title("images dots per inch", "dots per inch", "src/test/resources/images");
        String[] args = new String[] { "-r", /*"-X", "debug,trace",*/ "dots per inch", "src/test/resources/images" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

	@Test
	public void testImageDotsPerInchNoContent() throws GrepException {
		List<String> expected = Arrays.asList(
			"src/test/resources/images/Apple iPhone 4.jpg",
			"src/test/resources/images/Canon EOS D60.jpg",
			"src/test/resources/images/Canon IXUS v3.jpg",
			"src/test/resources/images/Canon PowerShot S300.jpg",
			"src/test/resources/images/Epson PhotoPC 3000Z.jpg",
			"src/test/resources/images/FujiFilm DS-7 (2).jpg",
			"src/test/resources/images/FujiFilm FinePixS2Pro.jpg",
			"src/test/resources/images/HP PhotoSmart 735.jpg",
			"src/test/resources/images/Kodak DC210.jpg",
			"src/test/resources/images/Minolta DiMAGE X.jpg",
			"src/test/resources/images/Nikon D1X.jpg",
			"src/test/resources/images/Nikon E5000.jpg",
			"src/test/resources/images/Olympus C4040Z.jpg",
			"src/test/resources/images/Pentax Optio S4.jpg",
			"src/test/resources/images/Ricoh DC-3Z (low res).jpg",
			"src/test/resources/images/Sanyo SR6.jpg",
			"src/test/resources/images/Sony Cybershot (3).jpg",
			"src/test/resources/images/Sony DigitalMavica.jpg"
			);
		title("images dots per inch no content", "dots", "src/test/resources/images");
		String[] args = new String[] { "-r", "-l", /*"-X", "debug",*/ "dots", "src/test/resources/images" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testImageUserComment() throws GrepException {
		List<String> expected = Arrays.asList(
				"src/test/resources/images/Kodak DC210.jpg: @{JPEG Comment=Lovely smoke effect here, of which I'm secretly very proud.}"
				);
        title("images user comment", "smoke", "src/test/resources/images");
		String[] args = new String[] { "-r", /*"-X", "debug",*/ "smoke", "src/test/resources/images" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}
	
    @Test
    public void testOCRTesseractException() throws GrepException, TesseractException {
        title("image OCR", "*", "src/test/resources/images/Apple iPhone 4.jpg");
        initFileGrep(true, null);
        ImageFileType image = new ImageFileType(fileGrep);
        image.setTesseractReader(ocrReader);
        when(ocrReader.doOCR(bImage)).thenThrow(new TesseractException("oops"));
        image.grepOCRText(bImage, "entry");
    }

    @Test
    public void testOCRLinkError() {
        title("image OCR", "*", "src/test/resources/images/Apple iPhone 4.jpg");
        initFileGrep(true, null);
        ImageFileType image = new ImageFileType(fileGrep);
        image.setTesseractReader(ocrReader);
        try {
            when(ocrReader.doOCR(bImage)).thenThrow(new UnsatisfiedLinkError("oops"));
            System.setProperty("crgrep.home", "C:/monkeys");
            image.grepOCRText(bImage, "entry");
        } catch (UnsatisfiedLinkError e) {
            fail("link error unexpected");
        } catch (TesseractException e) {
            fail("tesseract error unexpected");
        } catch (GrepException e) {
            assertNotNull(e.getEnvResult());
            assertTrue(e.getEnvResult().getErrors().size() > 0);
            assertTrue(e.getEnvResult().getErrors().get(0).contains("Optical Character Recognition"));
        }
    }

    @Test
    public void testOCR() throws GrepException, TesseractException {
        title("image OCR", "*", "src/test/resources/images/Apple iPhone 4.jpg");
        initFileGrep(true, null);
        ImageFileType image = new ImageFileType(fileGrep);
        image.setTesseractReader(ocrReader);
        when(ocrReader.doOCR(any(BufferedImage.class))).thenReturn("text");
        image.grepEntry(new File("src/test/resources/images/Apple iPhone 4.jpg"));
        verify(ocrReader).doOCR(any(BufferedImage.class));
    }

    @Test
    public void testOCRStream() throws GrepException, TesseractException, FileNotFoundException {
        title("image OCR Stream", "*", "src/test/resources/images/Apple iPhone 4.jpg");
        initFileGrep(true, null);
        ImageFileType image = new ImageFileType(fileGrep);
        image.setTesseractReader(ocrReader);
        when(ocrReader.doOCR(any(BufferedImage.class))).thenReturn("text");
        image.grepEntryStream(new FileInputStream("src/test/resources/images/Apple iPhone 4.jpg"), "entryName", "entryPath");
        verify(ocrReader).doOCR(any(BufferedImage.class));
    }

    private void initFileGrep(boolean isOcr, Properties p) {
        Switches sw = new Switches();
        sw.setOcr(isOcr);
        sw.setUserProperties(p);
        display = new DisplayRecorder(new Display());
        when(fileGrep.getSwitches()).thenReturn(sw);
        when(fileGrep.getResourcePattern()).thenReturn(resourcePattern);
        when(fileGrep.getDisplay()).thenReturn(display);
        when(resourcePattern.getPattern()).thenReturn(pattern);
        when(resourcePattern.getPattern(false)).thenReturn(pattern);
    }
}
