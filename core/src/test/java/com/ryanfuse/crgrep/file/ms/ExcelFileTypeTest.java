/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file.ms;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.security.Permission;
import java.security.PermissionCollection;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ListUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

@RunWith(MockitoJUnitRunner.class)
public class ExcelFileTypeTest extends TestCommon {

    @Mock private InputStream stream;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testExcelIOException() throws GrepException {
        ExcelFileType eft = new ExcelFileType(new FileGrep(new Switches()));
        try {
            when(stream.markSupported()).thenReturn(true);
            when(stream.read()).thenThrow(new IOException("yikes"));
            when(stream.read(any(byte[].class), Matchers.anyInt(), Matchers.anyInt())).thenThrow(new IOException("yikes"));
            eft.grepEntryStream(stream, "name", "label");
        } catch (IOException e) {
            // expected
        }
    }

    @Test
    public void testXlsExcel() throws GrepException {
        title("ms excel (xls)", "Oranges", "target/resources/ms/excel.xls");
        List<String> expected = Arrays.asList(
                "target/resources/ms/excel.xls:10:4:20:Oranges",
                "target/resources/ms/excel.xls:10:6:20:Oranges",
                "target/resources/ms/excel.xls:10:11:20:Oranges",
                "target/resources/ms/excel.xls:10:16:20:Oranges",
                "target/resources/ms/excel.xls:10:17:20:Oranges",
                "target/resources/ms/excel.xls:10:22:20:Oranges",
                "target/resources/ms/excel.xls:10:26:20:Oranges"
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "Oranges", 
                "target/resources/ms/excel.xls" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    // Assumes JCE (cryptography extensions) are NOT installed so that decryption will
    // fail with Export Restriction failures by Java.
    //
    @Test
    public void testXlsExcelPasswordProtectedJceRestricted() throws GrepException {
        title("ms excel (xls) password, JCE restricted", "onkey", "target/encrypted/ms/encrypted/pw-protected.xls");
        String[] args = new String[] { 
                //"-X", "debug", //,trace",
                "-p", "password",
                "--warn",
                "onkey",
                "target/encrypted/ms/encrypted/pw-protected.xls" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        // error is 'HSSF does not currently support CryptoAPI encryption' which is shortcoming of 
        // apache POI, saving a .xls old format using new Excel uses unsupported encryption.
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("this format of password protected document"));
    }

    @Test
    public void testXlsxExcelPasswordProtectedJceRestricted() throws GrepException {
        title("ms excel (xlsx) password, JCE restricted", "onkey", "target/encrypted/ms/encrypted/pw-protected.xlsx");
        String[] args = new String[] { 
                //"-X", "debug", //,trace",
                "--warn",
                "-p", "password",
                "onkey", 
                "target/encrypted/ms/encrypted/pw-protected.xlsx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Refer to the 'Java Cryptography Extensions'"));
    }

    @Test
    public void testXlsExcelPasswordProtectedNoPasswordGivenJceRestricted() throws GrepException {
        title("ms excel (xlsx) no password given, JCE restricted", "onkey", "target/encrypted/ms/encrypted/pw-protected.xlsx");
        String[] args = new String[] { 
                //"-X", "debug", //,trace",
                "--warn",
                "onkey", 
                "target/encrypted/ms/encrypted/pw-protected.xlsx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Refer to the 'Java Cryptography Extensions'"));
    }

    @Test
    public void testXlsExcelPasswordProtectedWrongPassword() throws GrepException {
        title("ms excel (xls) wrong password, JCE restricted", "onkey", "target/encrypted/ms/encrypted/pw-protected.xlsx");
        String[] args = new String[] { 
                //"-X", "debug", //,trace",
                "--warn",
                "-p", "wrongPassword",
                "onkey", 
                "target/encrypted/ms/encrypted/pw-protected.xlsx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Refer to the 'Java Cryptography Extensions'"));
    }

    @Test
    public void testXlsExcelNotPasswordProtectedPasswordGiven() throws GrepException {
        title("ms excel (xlsx) not protected but password given", "Demonstrate", "target/resources/ms/excel.xlsx");
        List<String> expected = Arrays.asList(
                "target/resources/ms/excel.xlsx:0:7:1:Demonstrates how to change the formatting (I.e. font, cell color) applied to a cell depending on the current value of the cell."
                );
        String[] args = new String[] { 
                //"-X", "debug", //,trace",
                "-p", "password",
                "Demonstrate", 
                "target/resources/ms/excel.xlsx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
        assertTrue(display.getWarnText() == null || display.getWarnText().isEmpty());
    }

    @Test
    public void testXlsExcelIgnoreCase() throws GrepException {
        title("ms excel (xls) ignore case", "ORANGES", "target/resources/ms/excel.xls");
        List<String> expected = Arrays.asList(
                "target/resources/ms/excel.xls:10:4:20:Oranges",
                "target/resources/ms/excel.xls:10:6:20:Oranges",
                "target/resources/ms/excel.xls:10:11:20:Oranges",
                "target/resources/ms/excel.xls:10:16:20:Oranges",
                "target/resources/ms/excel.xls:10:17:20:Oranges",
                "target/resources/ms/excel.xls:10:22:20:Oranges",
                "target/resources/ms/excel.xls:10:26:20:Oranges"
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "-i",
                "ORANGES",
                "target/resources/ms/excel.xls" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testXlsxExcel() throws GrepException {
        title("ms excel (xlsx)", "value", "target/resources/ms/excel.xlsx");
        List<String> expected = Arrays.asList(
                "target/resources/ms/excel.xlsx:0:7:1:Demonstrates how to change the formatting (I.e. font, cell color) applied to a cell depending on the current value of the cell.",
                "target/resources/ms/excel.xlsx:0:9:1:Shows how to set up restrictions for the values that can be entered into a cell.",
                "target/resources/ms/excel.xlsx:1:4:0:the cell to display the comment. Or, press CTRL+` to switch between displaying values and ",
                "target/resources/ms/excel.xlsx:1:9:0:It is common for worksheet formulas to return an error value (#DIV/0!, #N/A,",
                "target/resources/ms/excel.xlsx:1:10:0:#VALUE!,#REF!, and #NUM!) if they are based on an unexpected value.  An example is a",
                "target/resources/ms/excel.xlsx:1:19:0:where <formula> is the formula for which you want to suppress the error value. If",
                "target/resources/ms/excel.xlsx:1:23:0:Another way to suppress the display of error values is to use the Conditional Formatting",
                "target/resources/ms/excel.xlsx:1:25:0:display different formats depending on the contents of the cell.  For error values, you need to",
                "target/resources/ms/excel.xlsx:1:35:0:The result is that for error values, the font color is the same as the background, and nothing",
                "target/resources/ms/excel.xlsx:1:43:0:One of the most common List Management tasks is to find the value at the intersection of a",
                "target/resources/ms/excel.xlsx:1:54:0:Another common task in Excel is to calculate the sum of the values meeting the conditions",
                "target/resources/ms/excel.xlsx:2:2:0:Conditional Formatting allows you to change the formatting applied to cell depending on the current value of the cell.  This can make auditing large worksheets much faster by automatically highlighting exceptions.  Conditional Formatting allows you to apply up to three separate conditions to a cell.",
                "target/resources/ms/excel.xlsx:2:9:0:Cells H10:H14 contain three Conditional Formatting rules that will change the formatting of the cells depending on the values entered into the cells.",
                "target/resources/ms/excel.xlsx:2:18:0:This example illustrates how to use Conditional Formatting to hide error values that are returned by formulas.  In this example, cells H21 and H23 would normally display the #DIV/0! error code.  The Conditional Formatting that has been applied sets the font color of the cells containing an error value to match the background of the worksheet.",
                "target/resources/ms/excel.xlsx:3:2:0:Data Validation allows you to set up restrictions for the values that can be entered into a cell.  The following examples present several common scenarios for using Data Validation.",
                "target/resources/ms/excel.xlsx:3:8:0:Cells H9:H13 have been formatted with a validation rule that restricts cell entries to numeric values.  This example utilizes the Stop style for the Error alert, which prevents you from making an invalid entry into the selected cell. ",
                "target/resources/ms/excel.xlsx:3:18:0:Cells H19:H23 have been formatted with a validation rule that restricts the length of text entries to seven characters or less.  This example utilizes the Warning style for the Error alert, which gives you the option to cancel the current entry, or enter the invalid value into the selected cell.",
                "target/resources/ms/excel.xlsx:3:29:0:Cells H30:H34 have been formatted with a validation rule that will alert you if you do not enter a whole number between 1 and 10.  This example utilizes the Information style for the Error alert, which informs you of an invalid entry, but allows you to keep the current value. ",
                "target/resources/ms/excel.xlsx:3:40:0:Cells H41:H45 have been formatted with a validation rule that restricts the entries to a list of values.  The list of valid entries is contained in cells M41:M49.  When you select a cell within the range H41:H45, a dropdown arrow appears on the cell.  When you click the arrow, the list of valid entries is displayed.  You can simply click the entry that you wish to make.  This example utilizes the Stop style for the Error alert, which prevents you from making an invalid entry in the cell.",
                "target/resources/ms/excel.xlsx:5:5:0:The samples below focus on a scenario where the programmer has a range of cells in column A containing numbers, and depending on the value of the cell, wishes to change the color of the corresponding cell in column B.",
                "target/resources/ms/excel.xlsx:6:3:0: For j = 1 To 10           'Calculates sample values",
                "target/resources/ms/excel.xlsx:8:8:0:This event will fire whenever the user double-clicks a cell in the worksheet. The parameter Target is passed into the macro so the programmer will know what cell has been double clicked. The Cancel argument has a default value of False but can be changed to True within the code. Setting Cancel to True will cancel the default action for the event.  In this case, the default action for a double click on a cell is to switch to edit mode on the cell.  Since we set Cancel to True, this will not occur. If you want to still have the default action occur, then the Cancel=True line can be removed.",
                "target/resources/ms/excel.xlsx:8:11:0:Another very useful event is the Change event for a worksheet. Any time the user enters a new value in any cell this event fires."
                );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "value", 
                "target/resources/ms/excel.xlsx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
    
    @Test
    public void testXlsxExcelShortOutput() throws GrepException {
        title("ms excel (xlsx) short output", "value", "target/resources/ms/excel.xlsx");
        List<String> expected = Arrays.asList(
                "target/resources/ms/excel.xlsx:0:7:1:...value...",
                "target/resources/ms/excel.xlsx:0:9:1:...value...",
                "target/resources/ms/excel.xlsx:1:4:0:...value...",
                "target/resources/ms/excel.xlsx:1:9:0:...value...",
                "target/resources/ms/excel.xlsx:1:10:0:...value...",
                "target/resources/ms/excel.xlsx:1:19:0:...value...",
                "target/resources/ms/excel.xlsx:1:23:0:...value...",
                "target/resources/ms/excel.xlsx:1:25:0:...value...",
                "target/resources/ms/excel.xlsx:1:35:0:...value...",
                "target/resources/ms/excel.xlsx:1:43:0:...value...",
                "target/resources/ms/excel.xlsx:1:54:0:...value...",
                "target/resources/ms/excel.xlsx:2:2:0:...value...",
                "target/resources/ms/excel.xlsx:2:9:0:...value...",
                "target/resources/ms/excel.xlsx:2:18:0:...value...",
                "target/resources/ms/excel.xlsx:3:2:0:...value...",
                "target/resources/ms/excel.xlsx:3:8:0:...value...",
                "target/resources/ms/excel.xlsx:3:18:0:...value...",
                "target/resources/ms/excel.xlsx:3:29:0:...value...",
                "target/resources/ms/excel.xlsx:3:40:0:...value...",
                "target/resources/ms/excel.xlsx:5:5:0:...value...",
                "target/resources/ms/excel.xlsx:6:3:0:...value...",
                "target/resources/ms/excel.xlsx:8:8:0:...value...",
                "target/resources/ms/excel.xlsx:8:11:0:...value..."
                );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "value", 
                "target/resources/ms/excel.xlsx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.getSwitches().setMaxResultLength(5);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        //dumpActualList("target/xls.txt", actual);
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
}
