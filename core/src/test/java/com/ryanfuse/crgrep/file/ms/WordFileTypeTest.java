/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file.ms;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

/*
 * Tests the MS Word document file type
 */
@RunWith(MockitoJUnitRunner.class)
public class WordFileTypeTest extends TestCommon {

    @Mock private InputStream stream;

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testIOException() throws GrepException {
        WordFileType wft = new WordFileType(new FileGrep(new Switches()));
        try {
            when(stream.markSupported()).thenReturn(true);
            when(stream.read()).thenThrow(new IOException("yikes"));
            when(stream.read(any(byte[].class), Matchers.anyInt(), Matchers.anyInt())).thenThrow(new IOException("yikes"));
            wft.grepEntryStream(stream, "name", "label");
        } catch (IOException e) {
            // expected
        }
    }

    @Test
    public void testXIOException() throws GrepException {
        WordFileType wft = new WordFileType(new FileGrep(new Switches()));
        try {
            when(stream.markSupported()).thenReturn(true);
            when(stream.read()).thenThrow(new IOException("yikes"));
            when(stream.read(any(byte[].class), Matchers.anyInt(), Matchers.anyInt())).thenThrow(new IOException("yikes"));
            wft.grepEntryStream(stream, "name", "label");
        } catch (IOException e) {
            // expected
        }
    }

    @Test
    public void testDocWord2IgnoreCase() throws GrepException {
        title("ms word97", "Pur?ose", "target/resources/ms/word2.doc");
        List<String> expected = Arrays.asList(
                "target/resources/ms/word2.doc:17:[Supply your own summary of the technical purpose of the document.] This document provides a working MS Word 2000 sample from which you can start editing your own OASIS-published document. Instructions are provided as italic text in brackets, which should be deleted before publication. Full instructions are provided in the body of the document.",
                "target/resources/ms/word2.doc:147:This document and translations of it may be copied and furnished to others, and derivative works that comment on or otherwise explain it or assist in its implementation may be prepared, copied, published and distributed, in whole or in part, without restriction of any kind, provided that the above copyright notice and this paragraph are included on all such copies and derivative works. However, this document itself does not be modified in any way, such as by removing the copyright notice or references to OASIS, except as needed for the purpose of developing OASIS specifications, in which case the procedures for copyrights defined in the OASIS Intellectual Property Rights document must be followed, or as required to translate it into languages other than English.",
                "target/resources/ms/word2.doc:149:This document and the information contained herein is provided on an AS IS basis and OASIS DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION HEREIN WILL NOT INFRINGE ANY RIGHTS OR ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE."
        );
        String[] args = new String[] { 
                //"-r",
                //"-l",
                //"-X", "debug", //,trace", 
                "-i",
                "Pur?ose", 
                "target/resources/ms/word2.doc" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testDocWord2IgnoreCaseShortOutput() throws GrepException {
        title("ms word97 short output", "Pur?ose", "target/resources/ms/word2.doc");
        List<String> expected = Arrays.asList(
                "target/resources/ms/word2.doc:17:...purpo...",
                "target/resources/ms/word2.doc:147:...purpo...",
                "target/resources/ms/word2.doc:149:...PURPO..."
                );
        String[] args = new String[] { 
                //"-r",
                //"-l",
                //"-X", "debug", //,trace", 
                "-i",
                "Pur?ose", 
                "target/resources/ms/word2.doc" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.getSwitches().setMaxResultLength(5);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testDocWord() throws GrepException {
        title("ms word97", "sentence", "target/resources/ms/word.doc");
        List<String> expected = Arrays.asList(
                "target/resources/ms/word.doc:59:Instructions about final paper and figure submissions in this document are for IEEE journals; please use this document as a template to prepare your manuscript. For submission guidelines, follow instructions on paper submission system as well as the Conference website. Because IEEE will do the final formatting of your paper, you do not need to position figures and tables at the top and bottom of each column. In fact, all figures, figure captions, and tables can be at the end of the paper. Large figures and tables may span both columns. Place figure captions below the figures; place table titles above the tables. If your figure has two parts, include the labels (a) and (b) as part of the artwork. Please verify that the figures and tables you mention in the text actually exist. Please do not include captions as part of the figures. Do not put captions in text boxes linked to the figures. Do not put borders around the outside of your figures. Use the abbreviation Fig. even at the beginning of a sentence. Do not abbreviate Table. Tables are numbered with Roman numerals. ", 
                "target/resources/ms/word.doc:64:Number citations consecutively in square brackets [1]. The sentence punctuation follows the brackets [2]. Multiple references [2], [3] are each numbered with separate brackets [1][3]. When citing a section in a book, please give the relevant page numbers [2]. In sentences, refer simply to the reference number, as in [3]. Do not use Ref. [3] or reference [3] except at the beginning of a sentence: Reference [3] shows ... . Unfortunately the IEEE document translator cannot handle automatic endnotes in Word; therefore, type the reference list at the end of the paper using the References style.",
                "target/resources/ms/word.doc:71:Number equations consecutively with equation numbers in parentheses flush with the right margin, as in (1). First use the equation editor to create the equation. Then select the Equation markup style. Press the tab key and write the equation number in parentheses. To make your equations more compact, you may use the solidus ( / ), the exp function, or appropriate exponents. Use parentheses to avoid ambiguities in denominators. Punctuate equations when they are part of a sentence, as in",
                "target/resources/ms/word.doc:75:Be sure that the symbols in your equation have been defined before the equation appears or immediately following. Italicize symbols (T might refer to temperature, but T is the unit tesla). Refer to (1), not Eq. (1) or equation (1), except at the beginning of a sentence: Equation (1) is ... .",
                "target/resources/ms/word.doc:79:A parenthetical statement at the end of a sentence is punctuated outside of the closing parenthesis (like this). (A parenthetical sentence is punctuated within the parentheses.) In American English, periods and commas are within quotation marks, like this period. Other punctuation is outside! Avoid contractions; for example, write do not instead of dont. The serial comma is preferred: A, B, and C instead of A, B and C."
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "sentence", 
                "target/resources/ms/word.doc" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testDocWordHighlighted() throws GrepException {
        title("ms word97", "sentence", "target/resources/ms/word.doc");
        List<String> expected = Arrays.asList(
                "target/resources/ms/word.doc:59:Instructions about final paper and figure submissions in this document are for IEEE journals; please use this document as a template to prepare your manuscript. For submission guidelines, follow instructions on paper submission system as well as the Conference website. Because IEEE will do the final formatting of your paper, you do not need to position figures and tables at the top and bottom of each column. In fact, all figures, figure captions, and tables can be at the end of the paper. Large figures and tables may span both columns. Place figure captions below the figures; place table titles above the tables. If your figure has two parts, include the labels (a) and (b) as part of the artwork. Please verify that the figures and tables you mention in the text actually exist. Please do not include captions as part of the figures. Do not put captions in text boxes linked to the figures. Do not put borders around the outside of your figures. Use the abbreviation Fig. even at the beginning of a [1;31m[Ksentence[m[K. Do not abbreviate Table. Tables are numbered with Roman numerals. ",
                "target/resources/ms/word.doc:64:Number citations consecutively in square brackets [1]. The [1;31m[Ksentence[m[K punctuation follows the brackets [2]. Multiple references [2], [3] are each numbered with separate brackets [1][3]. When citing a section in a book, please give the relevant page numbers [2]. In [1;31m[Ksentence[m[Ks, refer simply to the reference number, as in [3]. Do not use Ref. [3] or reference [3] except at the beginning of a [1;31m[Ksentence[m[K: Reference [3] shows ... . Unfortunately the IEEE document translator cannot handle automatic endnotes in Word; therefore, type the reference list at the end of the paper using the References style.",
                "target/resources/ms/word.doc:71:Number equations consecutively with equation numbers in parentheses flush with the right margin, as in (1). First use the equation editor to create the equation. Then select the Equation markup style. Press the tab key and write the equation number in parentheses. To make your equations more compact, you may use the solidus ( / ), the exp function, or appropriate exponents. Use parentheses to avoid ambiguities in denominators. Punctuate equations when they are part of a [1;31m[Ksentence[m[K, as in",
                "target/resources/ms/word.doc:75:Be sure that the symbols in your equation have been defined before the equation appears or immediately following. Italicize symbols (T might refer to temperature, but T is the unit tesla). Refer to (1), not Eq. (1) or equation (1), except at the beginning of a [1;31m[Ksentence[m[K: Equation (1) is ... .",
                "target/resources/ms/word.doc:79:A parenthetical statement at the end of a [1;31m[Ksentence[m[K is punctuated outside of the closing parenthesis (like this). (A parenthetical [1;31m[Ksentence[m[K is punctuated within the parentheses.) In American English, periods and commas are within quotation marks, like this period. Other punctuation is outside! Avoid contractions; for example, write do not instead of dont. The serial comma is preferred: A, B, and C instead of A, B and C."
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "--color", "always",
                "sentence", 
                "target/resources/ms/word.doc" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        //dumpActualList("target/doc.txt", actual);
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testDocxWord() throws GrepException {
        title("ms word2007", "Publish", "target/resources/ms/word.docx");
        List<String> expected = Arrays.asList(
                "target/resources/ms/word.docx:P:S. P. Bingulac, On the compatibility of adaptive controllers (Published Conference Proceedings style), in Proc. 4th Annu. Allerton Conf. Circuits and Systems Theory, New York, 1994, pp. 816."
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "Publish", 
                "target/resources/ms/word.docx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testDocxWord2IgnoreCase() throws GrepException {
        title("ms word97", "template*", "target/resources/ms/word2.docx");
        List<String> expected = Arrays.asList(
                "target/resources/ms/word2.docx:P:[This section is provided to explain and demonstrate the styles available in the Word template attached to this sample document. It is important to use the styles provided in the template consistently and to avoid defining new styles or using raw formatting.",
                "target/resources/ms/word2.docx:P:This template sets Heading 1 and AppendixHeading to start on a new page. You may set the Heading 1 style not to start on a new page if you wish. Major headings have a horizontal rule above them.",
                "target/resources/ms/word2.docx:P:The Definition term and Definition paragraph styles are defined specially for this template. They produce a definition list with a hanging appearance. Pressing Return after one inserts the other directly after.",
                "target/resources/ms/word2.docx:P:This template defines several character styles for general text use:"
        );
        String[] args = new String[] { 
                //"-r",
                //"-l",
                //"-X", "debug", //,trace", 
                "-i",
                "template*", 
                "target/resources/ms/word2.docx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testDocWordAll() throws GrepException {
        title("ms word2007", "*", "target/resources/ms/word3-all.doc");
        List<String> expected = Arrays.asList(
                "target/resources/ms/word3-all.doc:H:odd:Left header\tCentre Header\tRight Header\t",    
                "target/resources/ms/word3-all.doc:1:Introduction text",
                "target/resources/ms/word3-all.doc:3:A paper about Monkeys who play lawn bowls",
                "target/resources/ms/word3-all.doc:7:Page two with three headers",
                "target/resources/ms/word3-all.doc:11:Table column 1",
                "target/resources/ms/word3-all.doc:12:Table column 2",
                "target/resources/ms/word3-all.doc:14:Monkey cell 1",
                "target/resources/ms/word3-all.doc:18: HYPERLINK  /l \"monkey\" monkey",
                "target/resources/ms/word3-all.doc:19: HYPERLINK \"mailto:monkey@monkeystuff.combo?subject=Monkey%20address\" mailto:monkey@monkeystuff.combo?subject=Monkey address",
                "target/resources/ms/word3-all.doc:C:A comment about monkeys",
                "target/resources/ms/word3-all.doc:C:Another comment",
                "target/resources/ms/word3-all.doc:TB:A text box to quote a famous monkey",
                "target/resources/ms/word3-all.doc:TB:I want a banana.",
                "target/resources/ms/word3-all.doc:F:odd:Left Footer\tCentre Footer Table",
                "target/resources/ms/word3-all.doc:F:odd:Centre Footer Table col1",
                "target/resources/ms/word3-all.doc:F:odd:Centre Footer Table col2",
                "target/resources/ms/word3-all.doc:F:odd:\tRight Footer"
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "*", 
                "target/resources/ms/word3-all.doc" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testDocXWordAll() throws GrepException {
        title("ms word2007", "*", "target/resources/ms/word3-all.docx");
        List<String> expected = Arrays.asList(
                "target/resources/ms/word3-all.docx:H:Left header\tCentre Header\tRight Header\t",
                "target/resources/ms/word3-all.docx:P:Introduction text",
                "target/resources/ms/word3-all.docx:P:A paper about Monkeys who play lawn bowls",
                "target/resources/ms/word3-all.docx:C:\tComment by Ryan, Craig: A comment about monkeysAnother comment",
                "target/resources/ms/word3-all.docx:TB:A text box to quote a famous monkey",
                "target/resources/ms/word3-all.docx:TB:I want a banana.",
                "target/resources/ms/word3-all.docx:P:Page two with three headers",
                "target/resources/ms/word3-all.docx:PIC:image1.png",
                "target/resources/ms/word3-all.docx:T:Table column 1",
                "target/resources/ms/word3-all.docx:T:Table column 2",
                "target/resources/ms/word3-all.docx:T:Monkey cell 1",
                "target/resources/ms/word3-all.docx:P:monkey",
                "target/resources/ms/word3-all.docx:P:mailto:monkey@monkeystuff.combo?subject=Monkey address",
                "target/resources/ms/word3-all.docx:URL:mailto:monkey@monkeystuff.combo?subject=Monkey%20address",
                "target/resources/ms/word3-all.docx:BM:_GoBack",
                "target/resources/ms/word3-all.docx:F:Left Footer\tCentre Footer Table",
                "target/resources/ms/word3-all.docx:F:T:Centre Footer Table col1",
                "target/resources/ms/word3-all.docx:F:T:Centre Footer Table col2",
                "target/resources/ms/word3-all.docx:F:\tRight Footer"
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "*", 
                "target/resources/ms/word3-all.docx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    // TODO: POI doesn't yet support full decryption
    @Test
    public void testDocEncryptedWithPassword() throws GrepException {
        title("ms Word (.doc) encrypted", "*", "target/encrypted/ms/encrypted/pw-protected.doc");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "--warn",
                "-p", "password",
                "*",
                "target/encrypted/ms/encrypted/pw-protected.doc" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Cannot process encrypted"));
    }

    @Test
    public void testDocxEncryptedWithPassword() throws GrepException {
        title("ms Word (.docx) encrypted", "*", "target/encrypted/ms/encrypted/pw-protected.docx");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "-p", "password",
                "--warn",
                "*",
                "target/encrypted/ms/encrypted/pw-protected.docx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Refer to the 'Java Cryptography Extensions'"));
    }

    // TODO: POI doesn't yet support full decryption
    @Test
    public void testDocEncryptedNoPasswordGiven() throws GrepException {
        title("ms Word (.doc) encrypted", "*", "target/encrypted/ms/encrypted/pw-protected.doc");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "--warn",
                "*",
                "target/encrypted/ms/encrypted/pw-protected.doc" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Cannot process encrypted"));
    }

    @Test
    public void testDocEncryptedWrongPassword() throws GrepException {
        title("ms Word (.doc) encrypted", "*", "target/encrypted/ms/encrypted/pw-protected.doc");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "-p", "goaway",
                "*",
                "target/encrypted/ms/encrypted/pw-protected.doc" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("encrypted word file"));
    }

    @Test
    public void testDocxEncryptedWrongPassword() throws GrepException {
        title("ms Word (.docx) encrypted, wrong password", "*", "target/encrypted/ms/encrypted/pw-protected.docx");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "-p", "goaway",
                "--warn",
                "*",
                "target/encrypted/ms/encrypted/pw-protected.docx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        //assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("encrypted word file"));
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Refer to the 'Java Cryptography Extensions'"));
    }

    @Test
    public void testDocxNotEncryptedPasswordGiven() throws GrepException {
        title("ms (.docx) not encrypted, password given", "Publish", "target/resources/ms/word.docx");
        List<String> expected = Arrays.asList(
                "target/resources/ms/word.docx:P:S. P. Bingulac, On the compatibility of adaptive controllers (Published Conference Proceedings style), in Proc. 4th Annu. Allerton Conf. Circuits and Systems Theory, New York, 1994, pp. 816."
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "-p", "password",
                "Publish", 
                "target/resources/ms/word.docx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testDocWord95() throws GrepException {
        title("ms word97", "alteration", "target/resources/ms/word95.doc");
        List<String> expected = Arrays.asList(
                "target/resources/ms/word95.doc:1:This is a sample document written with OpenOffice.org 2.2 and then save as a Word 6.0 document.  When opening this document with Word  95, and then after making an alteration, it crashes Word 95 when trying to save this."
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "alteration", 
                "target/resources/ms/word95.doc" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    // Doesn't yet follow embedded links
    //@Test
    public void testDocxWordEmbeddedPdf() throws GrepException {
        title("ms word (.docx) with embedded PDF", "*", "target/resources/ms/word4-embedded-pdf.docx");
        List<String> expected = Arrays.asList(
              ""
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "*", 
                "target/resources/ms/word4-embedded-pdf.docx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
}
