/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file.ms;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

@RunWith(MockitoJUnitRunner.class)
public class PowerPointFileTypeTest extends TestCommon {

    @Mock private InputStream stream;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testMsPowerPointIOException() throws GrepException {
        PowerPointFileType ppft = new PowerPointFileType(new FileGrep(new Switches()));
        try {
            when(stream.markSupported()).thenReturn(true);
            when(stream.read()).thenThrow(new IOException("yikes"));
            when(stream.read(any(byte[].class), Matchers.anyInt(), Matchers.anyInt())).thenThrow(new IOException("yikes"));
            ppft.grepEntryStream(stream, "name", "label");
        } catch (IOException e) {
            // expected
        }
    }
    
    @Test
    public void testMsPowerPoint() throws GrepException {
        title("ms word97 (.ppt)", "ou can", "target/resources/ms/pp.ppt");
        List<String> expected = Arrays.asList(
                "target/resources/ms/pp.ppt:5:You can Apply a Motif by Opening the Design tab on the right ", 
                "target/resources/ms/pp.ppt:6:You can Use the Font Color drop down menu to change the text color ", 
                "target/resources/ms/pp.ppt:11:Once you Select your Transition, you can Change the Speed or Even Add Sound!",
                "target/resources/ms/pp.ppt:15:By selecting several AutoShapes and right-clicking, you can group them together.",
                "target/resources/ms/pp.ppt:16:Then, you can Position and Size the Video how You Wish",
                "target/resources/ms/pp.ppt:17:You can Add Action Buttons to Move Around non-linearly in Your Presentation",
                "target/resources/ms/pp.ppt:17:Set Up your Properties, then you can Double Click the Button to Change Color"
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                "-X", "debug", //,trace", 
                "ou can", 
                "target/resources/ms/pp.ppt" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testMsPowerPointX() throws GrepException {
        title("ms word2007 (.pptx)", "is", "target/resources/ms/pp3.pptx");
        List<String> expected = Arrays.asList(
                "target/resources/ms/pp3.pptx:2:This is a Sample Slide",
                "target/resources/ms/pp3.pptx:2:Here is an outline of bulleted points"
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "is", 
                "target/resources/ms/pp3.pptx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testMsPowerPointXShortOutput() throws GrepException {
        title("ms word2007 (.pptx) short output", "is", "target/resources/ms/pp3.pptx");
        List<String> expected = Arrays.asList(
                "target/resources/ms/pp3.pptx:2:This ...",
                "target/resources/ms/pp3.pptx:2:...re is..."
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "is", 
                "target/resources/ms/pp3.pptx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.getSwitches().setMaxResultLength(5);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testMsPowerPointNotEncryptedPasswordGiven() throws GrepException {
        title("ms word97 unencrypted pw given", "ou can", "target/resources/ms/pp.ppt");
        List<String> expected = Arrays.asList(
                "target/resources/ms/pp.ppt:5:You can Apply a Motif by Opening the Design tab on the right " 
                );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                "-p", "password",
                "Motif by", 
                "target/resources/ms/pp.ppt" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testMsPowerPointXNotEncryptedPasswordGiven() throws GrepException {
        title("ms word2007 (.pptx)", "is", "target/resources/ms/pp3.pptx");
        List<String> expected = Arrays.asList(
                "target/resources/ms/pp3.pptx:2:Here is an outline of bulleted points"
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "-p", "password",
                "bulleted", 
                "target/resources/ms/pp3.pptx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testMsPowerPointEncryptedPasswordGiven() throws GrepException {
        title("ms password pptx", "is", "target/encrypted/ms/encrypted/testPPT_protected_passtika.ppt");
        List<String> expected = Arrays.asList(
                "target/encrypted/ms/encrypted/testPPT_protected_passtika.ppt:1:This is an encrypted PowerPoint 2007 slide."
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "--warn",
                "-p", "tika",
                "is",
                "target/encrypted/ms/encrypted/testPPT_protected_passtika.ppt" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testMsPowerPointXEncryptedPasswordGiven() throws GrepException {
        title("ms password pptx", "is", "target/encrypted/ms/encrypted/pw-protected.pptx");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "--warn",
                "-p", "password",
                "is", 
                "target/encrypted/ms/encrypted/pw-protected.pptx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Refer to the 'Java Cryptography Extensions'"));
    }

    @Test
    public void testMsPowerPointEncryptedPasswordNotGiven() throws GrepException {
        title("ms password pptx", "is", "target/encrypted/ms/encrypted/testPPT_protected_passtika.ppt");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "--warn",
                "is",
                "target/encrypted/ms/encrypted/testPPT_protected_passtika.ppt" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        //assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Refer to the 'Java Cryptography Extensions'"));
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("a valid password"));
    }

    @Test
    public void testMsPowerPointXEncryptedPasswordNotGiven() throws GrepException {
        title("ms password pptx", "is", "target/encrypted/ms/encrypted/pw-protected.pptx");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "--warn",
                "is", 
                "target/encrypted/ms/encrypted/pw-protected.pptx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Refer to the 'Java Cryptography Extensions'"));
    }

    @Test
    public void testMsPowerPointEncryptedWrongPassword() throws GrepException {
        title("ms password pptx", "is", "target/encrypted/ms/encrypted/testPPT_protected_passtika.ppt");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "--warn",
                "-p", "wrongpw",
                "is",
                "target/encrypted/ms/encrypted/testPPT_protected_passtika.ppt" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        //assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Specify the -p"));
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("a valid password"));
    }

    @Test
    public void testMsPowerPointXEncryptedWrongPassword() throws GrepException {
        title("ms password pptx", "is", "target/encrypted/ms/encrypted/pw-protected.pptx");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "--warn",
                "-p", "wrongone",
                "is", 
                "target/encrypted/ms/encrypted/pw-protected.pptx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Refer to the 'Java Cryptography Extensions'"));
    }
}
