/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;

/**
 * Match tags stored in an audio file (MP3). 
 * 
 * @author Craig Ryan
 */
@RunWith(MockitoJUnitRunner.class)
public class AudioGrepTest extends TestCommon {

    @Mock private FileGrep fileGrep;
    @Mock private ResourcePattern resourcePattern;
    
    private DisplayRecorder display;
    
    @Before
    public void setUp() throws Exception {
    }
    
    @Test
    public void testBellsOneTag() throws GrepException {
        List<String> expected = Arrays.asList(
            "src/test/resources/audio/01.HellsBells.mp3",
            "src/test/resources/audio/01.HellsBells.mp3: @{TrackTitle=Hells Bells}"
        );
        title("audio one tag", "Bells", "src/test/resources/audio/*Bells.mp3");
        String[] args = new String[] { "-r", 
                "-X", "trace", // required for code coverage  
                "Bells", "src/test/resources/audio/*Bells.mp3" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testWildcardStory() throws GrepException {
        List<String> expected = Arrays.asList(
            "src/test/resources/audio/07.the story of rome.mp3",
            "src/test/resources/audio/07.the story of rome.mp3: @{Album=The Metal Opera - Part I, Artist=Avantasia, Year=2000, TrackTitle=The Story Of Rome, LeadArtist=Avantasia, Duration=0, TrackNo=7, Size=10507621, BitRate=256, BitRateType=CBR, Frequency=44100, Rating=0, FrontCoverDescription=Cover (front)}",
            "src/test/resources/audio/07.the story of rome.mp3[FrontCover-image.jpg]: @{GIF Format Version=89a, Image Width=1000, Image Height=1000, Color Table Size=2, Is Color Table Sorted=false, Bits per Pixel=8, Has Global Color Table=false, Transparent Color Index=0}"
        );
        title("audio wildcard", "*", "src/test/resources/audio/07.the story of rome.mp3");
        String[] args = new String[] { "-r", 
                //"-X", "debug,trace", "--warn",
                "*", "src/test/resources/audio/07.the story of rome.mp3" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testWildcardShortOutput() throws GrepException {
        List<String> expected = Arrays.asList(
            "src/test/resources/audio/07.the story of rome.mp3",
            "src/test/resources/audio/07.the story of rome.mp3: @{Album=The M..., Artist=Avant..., Year=2000, TrackTitle=The S..., LeadArtist=Avant..., Duration=0, TrackNo=7, Size=10507..., BitRate=256, BitRateType=CBR, Frequency=44100, Rating=0, FrontCoverDescription=Cover...}",
            "src/test/resources/audio/07.the story of rome.mp3[FrontCover-image.jpg]: @{GIF Format Version=89a, Image Width=1000, Image Height=1000, Color Table Size=2, Is Color Table Sorted=false, Bits per Pixel=8, Has Global Color Table=false, Transparent Color Index=0}"
        );
        title("audio wildcard short output", "*", "src/test/resources/audio/07.the story of rome.mp3");
        String[] args = new String[] { "-r", 
                //"-X", "debug,trace", "--warn",
                "*", "src/test/resources/audio/07.the story of rome.mp3" };
        ResourceGrep rg = new ResourceGrep(args);
        rg.getSwitches().setMaxResultLength(5);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        dumpActualList("target/aud.txt", actual);
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testHighlightByName() throws GrepException {
        List<String> expected = Arrays.asList(
                "src/test/resources/audio/11.i[1;31m[Knside[m[K.mp3"
        );
        title("audio highlight by name", "nside", "src/test/resources/audio/*in?ide*");
        String[] args = new String[] { "-r", 
                //"-X", "debug,trace", "--warn",
                "-l",
                "--colour", "always",
                "nside", "src/test/resources/audio/*in?ide*" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testWildcardNoHighlighting() throws GrepException {
        List<String> expected = Arrays.asList(
                "src/test/resources/audio/4.Walk This Way.mp3",
                "src/test/resources/audio/4.Walk This Way.mp3: @{Album=Toys In The Attic, Artist=Aerosmith, Year=1975, TrackTitle=Walk This Way, LeadArtist=Aerosmith, MusicBy=Joe Perry/Steven Tyler, Duration=0, TrackNo=4, Publisher=Sony/BMG, Size=3540660, BitRate=128, BitRateType=CBR, Frequency=44100, Rating=0, FrontCoverDescription=Cover (front)}",
                "src/test/resources/audio/4.Walk This Way.mp3[FrontCover-image.jpg]: @{Version=1.1, Resolution Units=none, X Resolution=1 dot, Y Resolution=1 dot}"
        );
        title("audio wildcard no highlighting", "*", "src/test/resources/audio/4.Walk*");
        String[] args = new String[] { "-r", 
                //"-X", "debug,trace", "--warn",
                "--colour", "always",
                "*", "src/test/resources/audio/4.Walk*" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testIgnoreCaseHighlight() throws GrepException {
        List<String> expected = Arrays.asList(
                "src/test/resources/audio/11.[1;31m[KOver[m[Kture.mp3",
                "src/test/resources/audio/11.Overture.mp3: @{TrackTitle=[1;31m[KOver[m[Kture, FrontCoverDescription=C[1;31m[Kover[m[K (front)}"

        );
        title("audio highlighting ignore case", "over", "src/test/resources/audio/11.Overture.mp3");
        String[] args = new String[] { "-r", 
                //"-X", "debug,trace", "--warn",
                "-i",
                "--colour", "always",
                "over", "src/test/resources/audio/11.Overture.mp3" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testWildcardV1Mp3AllTags() throws GrepException {
        List<String> expected = Arrays.asList(
                "src/test/resources/audio/v1andv23andcustomtags.mp3",
                "src/test/resources/audio/v1andv23andcustomtags.mp3: @{Album=ALBUM1234567890123456789012345, Year=2001, TrackTitle=TITLE1234567890123456789012345, LeadArtist=ARTIST123456789012345678901234, MusicBy=COMPOSER23456789012345678901234, Duration=0, TrackNo=1, Size=0, BitRate=110, BitRateType=VBR, Frequency=44100, Rating=0}",
                "src/test/resources/audio/v1andv23tags.mp3",
                "src/test/resources/audio/v1andv23tags.mp3: @{Album=ALBUM1234567890123456789012345, Year=2001, TrackTitle=TITLE1234567890123456789012345, LeadArtist=ARTIST123456789012345678901234, MusicBy=COMPOSER23456789012345678901234, Duration=0, TrackNo=1, Size=0, BitRate=110, BitRateType=VBR, Frequency=44100, Rating=0}",
                "src/test/resources/audio/v1andv23tagswithalbumimage-utf16le.mp3",
                "src/test/resources/audio/v1andv23tagswithalbumimage-utf16le.mp3: @{Album=ALBUM1234567890123456789012345, Year=2001, TrackTitle=TITLE1234567890123456789012345, LeadArtist=ARTIST123456789012345678901234, MusicBy=COMPOSER23456789012345678901234, Duration=0, TrackNo=1, Size=0, BitRate=110, BitRateType=VBR, Frequency=44100, Rating=0}",
                "src/test/resources/audio/v1andv23tagswithalbumimage.mp3",
                "src/test/resources/audio/v1andv23tagswithalbumimage.mp3: @{Album=ALBUM1234567890123456789012345, Year=2001, TrackTitle=TITLE1234567890123456789012345, LeadArtist=ARTIST123456789012345678901234, MusicBy=COMPOSER23456789012345678901234, Duration=0, TrackNo=1, Size=0, BitRate=110, BitRateType=VBR, Frequency=44100, Rating=0}",
                "src/test/resources/audio/v1andv24tags.mp3",
                "src/test/resources/audio/v1andv24tags.mp3: @{Album=ALBUM1234567890123456789012345, Year=0, TrackTitle=TITLE1234567890123456789012345, LeadArtist=ARTIST123456789012345678901234, MusicBy=COMPOSER23456789012345678901234, Duration=0, TrackNo=1, Size=2997, BitRate=110, BitRateType=VBR, Frequency=44100, Rating=0}",
                "src/test/resources/audio/v1tag.mp3",
                "src/test/resources/audio/v1tag.mp3: @{Album=ALBUM1234567890123456789012345, Artist=ARTIST123456789012345678901234, Year=2001, TrackTitle=TITLE1234567890123456789012345, Duration=0, TrackNo=1, Size=2997, BitRate=110, BitRateType=VBR, Frequency=44100, Rating=0}",
                "src/test/resources/audio/v1tagwithnotrack.mp3",
                "src/test/resources/audio/v1tagwithnotrack.mp3: @{Album=ALBUM1234567890123456789012345, Artist=ARTIST123456789012345678901234, Year=2001, TrackTitle=TITLE1234567890123456789012345, Duration=0, TrackNo=0, Size=2997, BitRate=110, BitRateType=VBR, Frequency=44100, Rating=0}"
        );
        title("audio mp3 all tags", "*", "src/test/resources/audio/v1*.mp3");
        String[] args = new String[] { "-r", 
                //"-X", "debug,trace", 
                "*", "src/test/resources/audio/v1*.mp3" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testWildcardV2Mp3AllTags() throws GrepException {
        List<String> expected = Arrays.asList(
            "src/test/resources/audio/v23tag.mp3",
            "src/test/resources/audio/v23tag.mp3: @{Album=ALBUM1234567890123456789012345, Year=2001, TrackTitle=TITLE1234567890123456789012345, LeadArtist=ARTIST123456789012345678901234, MusicBy=COMPOSER23456789012345678901234, Duration=0, TrackNo=1, Size=0, BitRate=110, BitRateType=VBR, Frequency=44100, Rating=0}",
            "src/test/resources/audio/v23tagwithchapters.mp3",
            "src/test/resources/audio/v23tagwithchapters.mp3: @{Year=2012, TrackTitle=ID3 chapters example, Duration=0, TrackNo=0, Size=0, BitRate=48, BitRateType=CBR, Frequency=32000, Rating=0}",
            "src/test/resources/audio/v23unicodetags.mp3",
            "src/test/resources/audio/v23unicodetags.mp3: @{Album=, Year=0, TrackTitle=, LeadArtist= , MusicBy=, Duration=0, TrackNo=0, Size=0, BitRate=110, BitRateType=VBR, Frequency=44100, Rating=0}",
            "src/test/resources/audio/v24tagswithalbumimage.mp3",
            "src/test/resources/audio/v24tagswithalbumimage.mp3: @{Album=ALBUM1234567890123456789012345, Year=2014, TrackTitle=TITLE1234567890123456789012345, LeadArtist=ARTIST123456789012345678901234, MusicBy=COMPOSER23456789012345678901234, Duration=0, TrackNo=1, Size=2997, BitRate=110, BitRateType=VBR, Frequency=44100, Rating=0}"
        );
        title("audio wildcard mp3 all tags", "*", "src/test/resources/audio/v2*.mp3");
        String[] args = new String[] { "-r", 
                //"-X", "debug,trace", 
                "*", "src/test/resources/audio/v2*.mp3" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testV2Mp3TagsContaining12() throws GrepException {
        List<String> expected = Arrays.asList(
           "src/test/resources/audio/v23tag.mp3: @{Album=ALBUM1234567890123456789012345, TrackTitle=TITLE1234567890123456789012345, LeadArtist=ARTIST123456789012345678901234, MusicBy=COMPOSER23456789012345678901234}",
           "src/test/resources/audio/v23tagwithchapters.mp3: @{Year=2012}",
           "src/test/resources/audio/v24tagswithalbumimage.mp3: @{Album=ALBUM1234567890123456789012345, TrackTitle=TITLE1234567890123456789012345, LeadArtist=ARTIST123456789012345678901234, MusicBy=COMPOSER23456789012345678901234}"
        );
        title("audio tags containing 12", "12", "src/test/resources/audio/v2*.mp3");
        String[] args = new String[] { "-r", 
                //"-X", "debug,trace", 
                "12", "src/test/resources/audio/v2*.mp3" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testArchivedAudioWildcard() throws GrepException {
        List<String> expected = Arrays.asList(
                "src/test/resources/audio/audio.zip[01.HellsBells.mp3]",
                "src/test/resources/audio/audio.zip[01.HellsBells.mp3]: @{Album=Back In Black, Artist=AC/DC, Year=1980, TrackTitle=Hells Bells, LeadArtist=AC/DC, MusicBy=Angus Young/Brian Johnson/Malcolm Young, Duration=0, TrackNo=1, Publisher=Atco, Size=4997248, BitRate=128, BitRateType=CBR, Frequency=44100, Rating=0, FrontCoverDescription=Cover (front)}",
                "src/test/resources/audio/audio.zip[01.HellsBells.mp3][FrontCover-image.jpg]: @{Version=1.1, Resolution Units=none, X Resolution=1 dot, Y Resolution=1 dot}",
                "src/test/resources/audio/audio.zip[11.Overture.mp3]",
                "src/test/resources/audio/audio.zip[11.Overture.mp3]: @{Album=On through The Night, Year=0, TrackTitle=Overture, LeadArtist=Def Leppard, Duration=0, TrackNo=11, Size=7432696, BitRate=128, BitRateType=CBR, Frequency=44100, Rating=0, FrontCoverDescription=Cover (front)}",
                "src/test/resources/audio/audio.zip[11.Overture.mp3][FrontCover-image.jpg]: @{Version=1.1, Resolution Units=inch, X Resolution=72 dots, Y Resolution=72 dots}"
        );
        title("audio archive wildcard", "*", "src/test/resources/audio/audio.zip");
        String[] args = new String[] { "-r", 
                //"-X", "debug,trace", "--warn",
                "*", 
                "src/test/resources/audio/audio.zip" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

}
