/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;

/**
 * Colour Highlighting File type grep tests
 * 
 * @author Craig Ryan
 */
public class HighlightFileGrepTest extends TestCommon {

	private DisplayRecorder display;

    @Test
    public void testHighlightByNameWord() throws GrepException {
        title("highlight by name", "nested", "target/resources/**/*_[m-n]onkey.{txt,foo}");
        List<String> expected = Arrays.asList(
                "target/resources/misc/[1;31m[Knested[m[K_monkey.txt"
                );
        String[] args = new String[] { 
                "-r", 
                "-l",
                "--colour", "always",
                //"-X", "debug",
                "nested",
                "target/resources/**/*_[m-n]onkey.{txt,foo}" };
        try {
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
            rg.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testHighlightContentEndWildcard() throws GrepException {
        title("file recurse listing, by name", "nested*", "target/resources/**/*_[m-n]onkey.{txt,foo}");
        List<String> expected = Arrays.asList(
                "target/resources/misc/[1;31m[Knested_monkey.txt[m[K",
                "target/resources/misc/nested_monkey.txt:1:A file [1;31m[Knested under misc which mentions a monkey[m[K"
                );
        String[] args = new String[] { 
                "-r", 
                "--colour", "always",
                //"-X", "debug",
                "nested*", 
                "target/resources/**/*_[m-n]onkey.{txt,foo}" };
        try {
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
            rg.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testHighlightContentEndWildcardShorterOutputThanMatchText() throws GrepException {
        title("file recurse listing, by name", "nested*", "target/resources/**/*_[m-n]onkey.{txt,foo}");
        List<String> expected = Arrays.asList(
                "target/resources/misc/[1;31m[Knested_monkey.txt[m[K",
                "target/resources/misc/nested_monkey.txt:1:...neste..."
                );
        String[] args = new String[] { 
                "-r", 
                "--colour", "always",
                //"-X", "debug",
                "nested*", 
                "target/resources/**/*_[m-n]onkey.{txt,foo}" };
        try {
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.getSwitches().setMaxResultLength(5);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
            rg.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testHighlightTarGzip() throws GrepException {
        title("tar gzip", "j", "target/resources/r*.tar.gz");
        List<String> expected = Arrays.asList(
                "target/resources/resources.tar.gz[test-[1;31m[Kj[m[Kar-0.1.[1;31m[Kj[m[Kar]",
                "target/resources/resources.tar.gz[test-[1;31m[Kj[m[Kar-0.1.[1;31m[Kj[m[Kar][META-INF/maven/com.ryanfuse/test-[1;31m[Kj[m[Kar/]",
                "target/resources/resources.tar.gz[test-[1;31m[Kj[m[Kar-0.1.[1;31m[Kj[m[Kar][META-INF/maven/com.ryanfuse/test-[1;31m[Kj[m[Kar/pom.properties]",
                "target/resources/resources.tar.gz[test-[1;31m[Kj[m[Kar-0.1.[1;31m[Kj[m[Kar][META-INF/maven/com.ryanfuse/test-[1;31m[Kj[m[Kar/pom.xml]",
                "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.properties]:5:artifactId=test-[1;31m[Kj[m[Kar",
                "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:12:  	<packaging>[1;31m[Kj[m[Kar</packaging>",
                "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:13:  	<artifactId>test-[1;31m[Kj[m[Kar</artifactId>",
                "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:20:		  <artifactId>maven-[1;31m[Kj[m[Kar-plugin</artifactId>",
                "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:2:<pro[1;31m[Kj[m[Kect xmlns=\"http://maven.apache.org/POM/4.0.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\">",
                "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:38:</pro[1;31m[Kj[m[Kect>"
        );
        String[] args = new String[] { 
                "-r", 
                "--colour", "always",
                /*"-X", "debug",*/ 
                "j", 
                "target/resources/r*.tar.gz" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        display.setSortedPaths(true);
        rg.execute();
        List<String> actual = display.messages();
        //dumpActualList("target/hl2.txt", actual);
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
    
    @Test
    public void testHighlightTarGzipShortOutput() throws GrepException {
        title("tar gzip short output", "kag", "target/resources/r*.tar.gz");
        List<String> expected = Arrays.asList(
                "target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:12:...ac[1;31m[Kkag[m[K..."
        );
        String[] args = new String[] { 
                "-r", 
                "--colour", "always",
                /*"-X", "debug",*/ 
                "kag", 
                "target/resources/r*.tar.gz" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.getSwitches().setMaxResultLength(5);
        rg.setDisplay(display);
        display.setSortedPaths(true);
        rg.execute();
        List<String> actual = display.messages();
        //dumpActualList("target/hl2.txt", actual);
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
}
