/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Default File type tests
 * 
 * @author Craig Ryan
 */
@RunWith(MockitoJUnitRunner.class)
public class DefaultFileTypeTest extends TestCommon {

    @Mock private FileGrep fileGrep;
    @Mock private File file;
    @Mock private ResourcePattern resourcePattern;
    @Mock private DisplayRecorder display;
    @Mock private InputStream inputStream;
    private Switches switches;
    
    private Pattern pattern;

    @Before
    public void setUp() throws Exception {
        pattern = Pattern.compile(".*");
        switches = new Switches();
    }

    @Test
    public void testSupported() {
        DefaultFileType dft = new DefaultFileType(null);
        assertTrue(dft.matchesSupportedFileName(null));
        assertTrue(dft.matchesSupportedType(null));
    }

    @Test
    public void testEntryIOException() {
        DefaultFileType dft = new DefaultFileType(fileGrep);
        when(file.getPath()).thenReturn("/a");
        when(file.getName()).thenReturn("b.txt");
        when(fileGrep.isContent()).thenReturn(true);
        when(fileGrep.getResourcePattern()).thenReturn(resourcePattern);
        when(fileGrep.getDisplay()).thenReturn(display);
        when(fileGrep.getSwitches()).thenReturn(switches);
        when(resourcePattern.getPattern()).thenReturn(pattern);
        when(resourcePattern.getPattern(false)).thenReturn(pattern);
        try {
            when(fileGrep.isBinaryFile(file)).thenThrow(new IOException("no file"));
            dft.grepEntry(file);
        } catch (FileNotFoundException e) {
            fail("expected ioexception");
        } catch (IOException e) {
            // expected
        }
    }

    @Test
    public void testEntryFileNotFoundException() {
        DefaultFileType dft = new DefaultFileType(fileGrep);
        when(file.getPath()).thenReturn("/a");
        when(file.getName()).thenReturn("b.txt");
        when(fileGrep.isContent()).thenReturn(true);
        when(fileGrep.getResourcePattern()).thenReturn(resourcePattern);
        when(fileGrep.getDisplay()).thenReturn(display);
        when(fileGrep.getSwitches()).thenReturn(switches);
        when(resourcePattern.getPattern()).thenReturn(pattern);
        when(resourcePattern.getPattern(false)).thenReturn(pattern);
        try {
            when(fileGrep.isBinaryFile(file)).thenThrow(new FileNotFoundException("no file"));
            dft.grepEntry(file);
        } catch (FileNotFoundException e) {
            // expected
        } catch (IOException e) {
            fail("expected file not found exception");
        }
    }

    @Test
    public void testEntryStreamIOException() {
        DefaultFileType dft = new DefaultFileType(fileGrep);
        when(fileGrep.isContent()).thenReturn(true);
        when(fileGrep.getResourcePattern()).thenReturn(resourcePattern);
        when(fileGrep.getDisplay()).thenReturn(display);
        when(resourcePattern.getPattern()).thenReturn(pattern);
        when(resourcePattern.getPattern(false)).thenReturn(pattern);
        try {
            when(inputStream.read(any(byte[].class))).thenThrow(new IOException("no stream"));
            dft.grepEntryStream(inputStream, "entry", "path");
        } catch (FileNotFoundException e) {
            fail("expected IOexception");
        } catch (IOException e) {
            // expected
        }
    }

    @Test
    public void testEntryStreamLength() {
        DefaultFileType dft = new DefaultFileType(fileGrep);
        when(fileGrep.isContent()).thenReturn(true);
        when(fileGrep.getResourcePattern()).thenReturn(resourcePattern);
        when(fileGrep.getDisplay()).thenReturn(display);
        when(resourcePattern.getPattern()).thenReturn(pattern);
        try {
            when(inputStream.read(any(byte[].class))).thenReturn(-1);
            dft.grepEntryStream(inputStream, "entry", "path");
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
            // expected
        }
    }
}
