/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.rest.graphdb.RestAPI;
import org.neo4j.rest.graphdb.RestGraphDatabase;
import org.neo4j.rest.graphdb.entity.RestNode;
import org.neo4j.rest.graphdb.query.RestCypherQueryEngine;
import org.neo4j.rest.graphdb.util.QueryResult;

import com.ryanfuse.crgrep.DbResourceList;
import com.ryanfuse.crgrep.GraphGrep;
import com.ryanfuse.crgrep.ResourceList;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.util.Display;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Test the NeoGraph parser against a mocked Rest + Cypher interface.
 * 
 * @author 'Craig Ryan'
 */
@RunWith(MockitoJUnitRunner.class)
public class NeoGraphParserTest {

	private static final String URI = "http://localhost:7474/db/data";
	private static final String USER = "user";
	private static final String PW = "pw";
	
	private NeoGraphParser par;
	
	@Mock private ResourceList<String> resourceList;
	@Mock private RestCypherQueryEngine engine;
    @Mock private RestGraphDatabase graphdb;
    @Mock private Transaction tx;
    @Mock private RestAPI restApi;
    @Mock private Iterable<RestNode> iterableNodes;
    @Mock private Iterable<Relationship> iterableRels;
    @Mock private Iterable<String> iterableProps;
    @Mock private RestNode restNode1;
    @Mock private RestNode restNode2;
    @Mock private Relationship relship1;
    @Mock private Relationship relship2;
    @Mock private RelationshipType relshipType;

	@Before
	public void setUp() throws Exception {
        GraphGrep gg = new GraphGrep(new Switches());
		par = new NeoGraphParser(gg, URI, USER, PW);
		par.setEngine(engine);
		par.setGraphDatabase(graphdb);
		when(graphdb.isAvailable(any(Long.class))).thenReturn(true);
		when(graphdb.beginTx()).thenReturn(tx);
		when(graphdb.getRestAPI()).thenReturn(restApi);
        when(restNode1.getId()).thenReturn(1L);
        when(restNode2.getId()).thenReturn(2L);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetDefaults() {
		assertEquals(par.NEO_DEF_USER, par.getDefaultUser());
		assertEquals(par.NEO_DEF_PW, par.getDefaultPassword());
		par.setVerbose(true);
        par.setDisplay(new Display());
		assertTrue(par.isVerbose());
		assertNotNull(par.getDisplay());
        GraphGrep gg = new GraphGrep(new Switches());
		NeoGraphParser defaultPar = new NeoGraphParser(gg, null, null, null);
        assertNull(defaultPar.getUser());
        assertEquals(defaultPar.getDefaultUser(), defaultPar.getUser());
        assertNull(defaultPar.getPassword());
        assertEquals(defaultPar.getDefaultPassword(), defaultPar.getPassword());
	}

	@Test
	public void testGetLogPrefix() {
		assertNotNull(par.getLogPrefix());
	}

	@Test
	public void testDbConnection() throws SQLException {
		try {
			assertTrue(par.openConnection());
		} catch (Exception e) {
			fail("unexpected error: " + e.getLocalizedMessage());
		}
	}

    @Test
    public void testGetMatchingRelationshipMapTwoRelationsTwoProperties() throws Exception {
        // Iterator mocks
        Iterator<String> mockPropIterator = mock(Iterator.class);
        Iterator<Relationship> mockRelIterator = mock(Iterator.class);
        
        when(iterableRels.iterator()).thenReturn(mockRelIterator);
        when(iterableProps.iterator()).thenReturn(mockPropIterator);

        // two rels
        when(mockRelIterator.hasNext()).thenReturn(true, false, true, false);
        when(mockRelIterator.next()).thenReturn(relship1, relship2);

        // rel property iterators
        when(relship1.getPropertyKeys()).thenReturn(iterableProps, iterableProps);
        when(relship2.getPropertyKeys()).thenReturn(iterableProps, iterableProps);
    	
        // two properties for each relationship
        when(mockPropIterator.hasNext()).thenReturn(true, true, false, true, true, false);
        when(mockPropIterator.next()).thenReturn("prop1", "prop2", "prop3", "prop4");
        
        QueryResult<Map<String, Object>> result = mock(QueryResult.class);
        Iterator<Map<String, Object>> mockResultIterator = mock(Iterator.class);
        when(result.iterator()).thenReturn(mockResultIterator);
        when(mockResultIterator.hasNext()).thenReturn(true, true, false);
        Map<String, Object> matchingRow = genRow("r", relship1);
        Map<String, Object> matchingRow2 = genRow("r", relship2);
        when(mockResultIterator.next()).thenReturn(matchingRow, matchingRow2);
        when(relship1.getType()).thenReturn(relshipType);
        when(relship1.getId()).thenReturn(222L);
        when(relship2.getType()).thenReturn(relshipType);
        when(relship2.getId()).thenReturn(223L);
        when(relshipType.name()).thenReturn("IS_A", "HAS_A");
        when(engine.query(any(String.class), any(Map.class))).thenReturn(result);
        
        ResourceList rl = new DbResourceList("*.*");
        par.setDisplay(new Display());
        par.setResourceList(rl);
        Map<String, List<Map.Entry<String, List<String>>>> relMap = par.getMatchingRelationshipMap();
        
        assertNotNull(relMap);
        assertEquals(2, relMap.size()); // Two names
        
        // 1st relationship
        List<Map.Entry<String, List<String>>> relNameMap = relMap.get("IS_A");
        assertNotNull(relNameMap);
        assertEquals(1, relNameMap.size()); // one rel id -> list<property> entry for this label
        
        Map.Entry<String, List<String>> relIdToProps = relNameMap.get(0);
        assertNotNull(relIdToProps);
        String relId = relIdToProps.getKey();
        assertNotNull(relId);
        assertEquals("222", relId);
        List<String> props = relIdToProps.getValue();
        assertNotNull(props);
        assertEquals(2, props.size());
        assertEquals("prop1", props.get(0));
        assertEquals("prop2", props.get(1));
        
        // 2nd node
        relNameMap = relMap.get("HAS_A");
        assertNotNull(relNameMap);
        assertEquals(1, relNameMap.size()); // one node id -> list<property> entry for this label
        
        relIdToProps = relNameMap.get(0);
        assertNotNull(relIdToProps);
        relId = relIdToProps.getKey();
        assertNotNull(relId);
        assertEquals("223", relId);
        props = relIdToProps.getValue();
        assertNotNull(props);
        assertEquals(2, props.size());
        assertEquals("prop3", props.get(0));
        assertEquals("prop4", props.get(1));
    }

    @Test
    public void testGetMatchingNodeMapTwoNodesTwoProperties() throws Exception {
        // Iterator mocks
        Iterator<RestNode> mockNodeIterator = mock(Iterator.class);
        Iterator<String> mockPropIterator = mock(Iterator.class);
        
        when(iterableNodes.iterator()).thenReturn(mockNodeIterator);
        when(iterableProps.iterator()).thenReturn(mockPropIterator);

        // two nodes
        when(mockNodeIterator.hasNext()).thenReturn(true, false, true, false);
        when(mockNodeIterator.next()).thenReturn(restNode1, restNode2);

        // node property iterators
        when(restNode1.getPropertyKeys()).thenReturn(iterableProps, iterableProps);
        when(restNode2.getPropertyKeys()).thenReturn(iterableProps, iterableProps);
        
        // two properties for each node
        when(mockPropIterator.hasNext()).thenReturn(true, true, false, true, true, false);
        when(mockPropIterator.next()).thenReturn("prop1", "prop2", "prop3", "prop4");
        
        when(restApi.getNodesByLabel(any(String.class))).thenReturn(iterableNodes);

        // Label lookup
        Collection<String> labels = new ArrayList<String>();
        labels.add("label1");
        labels.add("label2");
        when(graphdb.getAllLabelNames()).thenReturn(labels);
        
        ResourceList rl = new DbResourceList("*.*");
        par.setDisplay(new Display());
        par.setResourceList(rl);
        Map<String, List<Map.Entry<String, List<String>>>> nodeMap = par.getMatchingNodeMap();
        
        assertNotNull(nodeMap);
        assertEquals(2, nodeMap.size()); // Two labels
        
        // 1st node
        List<Map.Entry<String, List<String>>> labelMap = nodeMap.get("label1");
        assertNotNull(labelMap);
        assertEquals(1, labelMap.size()); // one node id -> list<property> entry for this label
        
        Map.Entry<String, List<String>> nodeIdToProps = labelMap.get(0);
        assertNotNull(nodeIdToProps);
        String nodeId = nodeIdToProps.getKey();
        assertNotNull(nodeId);
        assertEquals("1", nodeId);
        List<String> props = nodeIdToProps.getValue();
        assertNotNull(props);
        assertEquals(2, props.size());
        assertEquals("prop1", props.get(0));
        assertEquals("prop2", props.get(1));
        
        // 2nd node
        labelMap = nodeMap.get("label2");
        assertNotNull(labelMap);
        assertEquals(1, labelMap.size()); // one node id -> list<property> entry for this label
        
        nodeIdToProps = labelMap.get(0);
        assertNotNull(nodeIdToProps);
        nodeId = nodeIdToProps.getKey();
        assertNotNull(nodeId);
        assertEquals("2", nodeId);
        props = nodeIdToProps.getValue();
        assertNotNull(props);
        assertEquals(2, props.size());
        assertEquals("prop3", props.get(0));
        assertEquals("prop4", props.get(1));
    }

    @Test
    public void testGetMatchingRelationshipMapTwoRelationsOneWithProperties() throws Exception {
        // Iterator mocks
        Iterator<String> mockPropIterator = mock(Iterator.class);
        Iterator<Relationship> mockRelIterator = mock(Iterator.class);
        
        when(iterableRels.iterator()).thenReturn(mockRelIterator);
        when(iterableProps.iterator()).thenReturn(mockPropIterator);

        // two rels
        when(mockRelIterator.hasNext()).thenReturn(true, false, true, false);
        when(mockRelIterator.next()).thenReturn(relship1, relship2);

        // rel property iterators
        when(relship1.getPropertyKeys()).thenReturn(iterableProps, iterableProps);
        when(relship2.getPropertyKeys()).thenReturn(iterableProps, iterableProps);
    	
        // two properties for each relationship
        when(mockPropIterator.hasNext()).thenReturn(true, false, false);
        when(mockPropIterator.next()).thenReturn("prop1");
        
        QueryResult<Map<String, Object>> result = mock(QueryResult.class);
        Iterator<Map<String, Object>> mockResultIterator = mock(Iterator.class);
        when(result.iterator()).thenReturn(mockResultIterator);
        when(mockResultIterator.hasNext()).thenReturn(true, true, false);
        Map<String, Object> matchingRow = genRow("r", relship1);
        Map<String, Object> matchingRow2 = genRow("r", relship2);
        when(mockResultIterator.next()).thenReturn(matchingRow, matchingRow2);
        when(relship1.getType()).thenReturn(relshipType);
        when(relship1.getId()).thenReturn(222L);
        when(relship2.getType()).thenReturn(relshipType);
        when(relship2.getId()).thenReturn(223L);
        when(relshipType.name()).thenReturn("IS_A", "HAS_A");
        when(engine.query(any(String.class), any(Map.class))).thenReturn(result);
        
        ResourceList rl = new DbResourceList("*.*");
        par.setDisplay(new Display());
        par.setResourceList(rl);
        Map<String, List<Map.Entry<String, List<String>>>> relMap = par.getMatchingRelationshipMap();
        
        assertNotNull(relMap);
        assertEquals(2, relMap.size()); // Two names
        
        // 1st relationship
        List<Map.Entry<String, List<String>>> relNameMap = relMap.get("IS_A");
        assertNotNull(relNameMap);
        assertEquals(1, relNameMap.size()); // one rel id -> list<property> entry for this label
        
        Map.Entry<String, List<String>> relIdToProps = relNameMap.get(0);
        assertNotNull(relIdToProps);
        String relId = relIdToProps.getKey();
        assertNotNull(relId);
        assertEquals("222", relId);
        List<String> props = relIdToProps.getValue();
        assertNotNull(props);
        assertEquals(1, props.size());
        assertEquals("prop1", props.get(0));
        
        // 2nd node
        relNameMap = relMap.get("HAS_A");
        assertNotNull(relNameMap);
        assertEquals(1, relNameMap.size()); // one node id -> list<property> entry for this label
        
        relIdToProps = relNameMap.get(0);
        assertNotNull(relIdToProps);
        relId = relIdToProps.getKey();
        assertNotNull(relId);
        assertEquals("223", relId);
        props = relIdToProps.getValue();
        assertNotNull(props);
        assertNotNull(props);
        assertEquals(0, props.size());
    }

    @Test
    public void testGetMatchingNodeMapTwoNodesOneWithProperties() throws Exception {
        // Iterator mocks
        Iterator<RestNode> mockNodeIterator = mock(Iterator.class);
        Iterator<String> mockPropIterator = mock(Iterator.class);
        
        when(iterableNodes.iterator()).thenReturn(mockNodeIterator);
        when(iterableProps.iterator()).thenReturn(mockPropIterator);

        // two nodes
        when(mockNodeIterator.hasNext()).thenReturn(true, true, false);
        when(mockNodeIterator.next()).thenReturn(restNode1, restNode2);

        // node property iterators
        when(restNode1.getPropertyKeys()).thenReturn(iterableProps, iterableProps);
        when(restNode2.getPropertyKeys()).thenReturn(iterableProps, iterableProps);
        
        // one property for first node, no properties for next node
        when(mockPropIterator.hasNext()).thenReturn(true, false, false);
        when(mockPropIterator.next()).thenReturn("prop1");
        
        when(restApi.getNodesByLabel(any(String.class))).thenReturn(iterableNodes);

        // Label lookup
        Collection<String> labels = new ArrayList<String>();
        labels.add("label1");
        when(graphdb.getAllLabelNames()).thenReturn(labels);
        
        ResourceList rl = new DbResourceList("*.*");
        par.setDisplay(new Display());
//        par.getDisplay().setDebugOn(true);
        par.setResourceList(rl);
        Map<String, List<Map.Entry<String, List<String>>>> nodeMap = par.getMatchingNodeMap();
        
        assertNotNull(nodeMap);
        assertEquals(1, nodeMap.size()); // Two labels
        
        // 1st node
        List<Map.Entry<String, List<String>>> labelMap = nodeMap.get("label1");
        assertNotNull(labelMap);
        assertEquals(2, labelMap.size());
        
        // 1st node; 1 prop
        Map.Entry<String, List<String>> nodeIdToProps = labelMap.get(0);
        assertNotNull(nodeIdToProps);
        String nodeId = nodeIdToProps.getKey();
        assertNotNull(nodeId);
        assertEquals("1", nodeId);
        List<String> props = nodeIdToProps.getValue();
        assertNotNull(props);
        assertEquals(1, props.size());
        assertEquals("prop1", props.get(0));
        
        // 2nd node, no props
        nodeIdToProps = labelMap.get(1);
        assertNotNull(nodeIdToProps);
        nodeId = nodeIdToProps.getKey();
        assertNotNull(nodeId);
        assertEquals("2", nodeId);
        props = nodeIdToProps.getValue();
        assertNotNull(props);
        assertEquals(0, props.size());
    }

    @Test
    public void testGetMatchingRelationshipMapNoResults() throws Exception {
        // Iterator mocks
        Iterator<Relationship> mockRelIterator = mock(Iterator.class);
        Iterator<String> mockPropIterator = mock(Iterator.class);
        
        when(iterableRels.iterator()).thenReturn(mockRelIterator);
        when(iterableProps.iterator()).thenReturn(mockPropIterator);
        when(mockRelIterator.hasNext()).thenReturn(false);

        //when(restApi.getNodesByLabel(any(String.class))).thenReturn(iterableNodes);
        
        ResourceList rl = new DbResourceList("*.*");
        par.setDisplay(new Display());
        par.getDisplay().setDebugOn(true);
        par.setResourceList(rl);
        Map<String, List<Map.Entry<String, List<String>>>> relMap = par.getMatchingRelationshipMap();
        
        assertNotNull(relMap);
        assertEquals(0, relMap.size());
    }

    @Test
    public void testGetMatchingNodeMapNoResults() throws Exception {
        // Iterator mocks
        Iterator<RestNode> mockNodeIterator = mock(Iterator.class);
        Iterator<String> mockPropIterator = mock(Iterator.class);
        
        when(iterableNodes.iterator()).thenReturn(mockNodeIterator);
        when(iterableProps.iterator()).thenReturn(mockPropIterator);
        when(mockNodeIterator.hasNext()).thenReturn(false);
        when(restApi.getNodesByLabel(any(String.class))).thenReturn(iterableNodes);

        // Label lookup
        Collection<String> labels = new ArrayList<String>();
        labels.add("label1");
        when(graphdb.getAllLabelNames()).thenReturn(labels);
        
        ResourceList rl = new DbResourceList("*.*");
        par.setDisplay(new Display());
        par.getDisplay().setDebugOn(true);
        par.setResourceList(rl);
        Map<String, List<Map.Entry<String, List<String>>>> ftm = par.getMatchingNodeMap();
        
        assertNotNull(ftm);
        assertEquals(0, ftm.size());
    }

    @Test
    public void testMatchesPropertyValueSimpleIsMatched() throws Exception {
        StringBuilder match = new StringBuilder();
        QueryResult<Map<String, Object>> result = mock(QueryResult.class);
        Iterator<Map<String, Object>> mockResultIterator = mock(Iterator.class);
        when(result.iterator()).thenReturn(mockResultIterator);
        when(mockResultIterator.hasNext()).thenReturn(true, false);
        Map<String, Object> matchingRow = genRow("val", new String("string"));
        when(mockResultIterator.next()).thenReturn(matchingRow);
        when(engine.query(any(String.class), any(Map.class))).thenReturn(result);

        ResourcePattern pattern = new ResourcePattern("str");
        for (Map<String, Object> row : result) {
        	assertTrue(par.matchesPropertyValue("123", row, match, pattern));
        }
    }

    @Test
    public void testMatchesPropertyValueSimpleNotMatched() throws Exception {
        StringBuilder match = new StringBuilder();
        QueryResult<Map<String, Object>> emptyResult = mock(QueryResult.class);
        Iterator<Map<String, Object>> mockResultIterator = mock(Iterator.class);
        when(emptyResult.iterator()).thenReturn(mockResultIterator);
        when(mockResultIterator.hasNext()).thenReturn(false);
        when(engine.query(any(String.class), any(Map.class))).thenReturn(emptyResult);

        ResourcePattern pattern = new ResourcePattern("str");
        for (Map<String, Object> row : emptyResult) {
        	assertFalse(par.matchesPropertyValue("123", row, match, pattern));
        }
    }

    @Test
    public void testMatchesPropertyValueArrayIsMatched() throws Exception {
        StringBuilder match = new StringBuilder();
        QueryResult<Map<String, Object>> result = mock(QueryResult.class);
        Iterator<Map<String, Object>> mockResultIterator = mock(Iterator.class);
        when(result.iterator()).thenReturn(mockResultIterator);
        when(mockResultIterator.hasNext()).thenReturn(true, false);
        List<Integer> ints = new ArrayList<Integer>();
        ints.add(1);
        ints.add(10);
        ints.add(100);
        Map<String, Object> matchingRow = genRow("val", ints);
        when(mockResultIterator.next()).thenReturn(matchingRow);
        when(engine.query(any(String.class), any(Map.class))).thenReturn(result);

        ResourcePattern pattern = new ResourcePattern("1");
        for (Map<String, Object> row : result) {
        	assertTrue(par.matchesPropertyValue("123", row, match, pattern));
        }
    }

    @Test
    public void relationshipNodeIdsStart() throws Exception {
        QueryResult<Map<String, Object>> result = mock(QueryResult.class);
        Iterator<Map<String, Object>> mockResultIterator = mock(Iterator.class);
        when(result.iterator()).thenReturn(mockResultIterator);
        when(mockResultIterator.hasNext()).thenReturn(true, false);
        Relationship mockRship = mock(Relationship.class);
        when(mockRship.getStartNode()).thenReturn(restNode1);
        Map<String, Object> matchingRow = genRow("r", mockRship);
        when(mockResultIterator.next()).thenReturn(matchingRow);
    	when(engine.query(any(String.class), any(Map.class))).thenReturn(result);

    	long[] nodeIds = par.getRelationshipNodeIds("1234");
    	assertNotNull(nodeIds);
    	assertEquals(2, nodeIds.length);
    	assertEquals(1L, nodeIds[0]);
    	assertEquals(-1L, nodeIds[1]);
    }

    @Test
    public void relationshipNodeIdsEnd() throws Exception {
        QueryResult<Map<String, Object>> result = mock(QueryResult.class);
        Iterator<Map<String, Object>> mockResultIterator = mock(Iterator.class);
        when(result.iterator()).thenReturn(mockResultIterator);
        when(mockResultIterator.hasNext()).thenReturn(true, false);
        Relationship mockRship = mock(Relationship.class);
        when(mockRship.getEndNode()).thenReturn(restNode2);
        Map<String, Object> matchingRow = genRow("r", mockRship);
        when(mockResultIterator.next()).thenReturn(matchingRow);
    	when(engine.query(any(String.class), any(Map.class))).thenReturn(result);

    	long[] nodeIds = par.getRelationshipNodeIds("1234");
    	assertNotNull(nodeIds);
    	assertEquals(2, nodeIds.length);
    	assertEquals(-1L, nodeIds[0]);
    	assertEquals(2L, nodeIds[1]);
    }

    @Test
    public void relationshipNodeIdsStartAndEnd() throws Exception {
        QueryResult<Map<String, Object>> result = mock(QueryResult.class);
        Iterator<Map<String, Object>> mockResultIterator = mock(Iterator.class);
        when(result.iterator()).thenReturn(mockResultIterator);
        when(mockResultIterator.hasNext()).thenReturn(true, false);
        Relationship mockRship = mock(Relationship.class);
        when(mockRship.getStartNode()).thenReturn(restNode1);
        when(mockRship.getEndNode()).thenReturn(restNode2);
        Map<String, Object> matchingRow = genRow("r", mockRship);
        when(mockResultIterator.next()).thenReturn(matchingRow);
    	when(engine.query(any(String.class), any(Map.class))).thenReturn(result);

    	long[] nodeIds = par.getRelationshipNodeIds("1234");
    	assertNotNull(nodeIds);
    	assertEquals(2, nodeIds.length);
    	assertEquals(1L, nodeIds[0]);
    	assertEquals(2L, nodeIds[1]);
    }

    @Test
    public void relationshipNodeIdsInvalid() {
    	long[] nodeIds = par.getRelationshipNodeIds("abcd");
    	assertNotNull(nodeIds);
    	assertEquals(2, nodeIds.length);
    	assertEquals(-1L, nodeIds[0]);
    	assertEquals(-1L, nodeIds[1]);
    }

    @Test
    public void relationshipPropertyResultNullParams() {
    	QueryResult<Map<String, Object>> result = mock(QueryResult.class);
    	when(engine.query(any(String.class), any(Map.class))).thenReturn(result);
    	assertNull(par.relationshipPropertyResult(null, null));
    	assertNull(par.relationshipPropertyResult("xxx", null));
    }

    @Test
    public void relationshipPropertyResultNullResult() {
    	when(engine.query(any(String.class), any(Map.class))).thenReturn(null);
    	assertNull(par.relationshipPropertyResult("123", "prop"));
    }

    @Test
    public void relationshipPropertyResult() {
    	QueryResult<Map<String, Object>> result = mock(QueryResult.class);
    	when(engine.query(any(String.class), any(Map.class))).thenReturn(result);
    	Iterator<Map<String, Object>> mockResultIterator = mock(Iterator.class);
    	Map<String, Object> mockRow = mock(Map.class);
        when(result.iterator()).thenReturn(mockResultIterator);
        when(mockResultIterator.hasNext()).thenReturn(true, false);
        when(mockResultIterator.next()).thenReturn(mockRow);
        
    	Map<String, Object> row = par.relationshipPropertyResult("123", "prop");
    	assertNotNull(row);
    }

    @Test
    public void nodePropertyResultNullParams() {
    	QueryResult<Map<String, Object>> result = mock(QueryResult.class);
    	when(engine.query(any(String.class), any(Map.class))).thenReturn(result);
    	assertNull(par.nodePropertyResult(null, null));
    	assertNull(par.nodePropertyResult("xxx", null));
    }

    @Test
    public void nodePropertyResultNullResult() {
    	when(engine.query(any(String.class), any(Map.class))).thenReturn(null);
    	assertNull(par.nodePropertyResult("123", "prop"));
    }

    @Test
    public void nodePropertyResult() {
    	QueryResult<Map<String, Object>> result = mock(QueryResult.class);
    	when(engine.query(any(String.class), any(Map.class))).thenReturn(result);
    	Iterator<Map<String, Object>> mockResultIterator = mock(Iterator.class);
    	Map<String, Object> mockRow = mock(Map.class);
        when(result.iterator()).thenReturn(mockResultIterator);
        when(mockResultIterator.hasNext()).thenReturn(true, false);
        when(mockResultIterator.next()).thenReturn(mockRow);
        
    	Map<String, Object> row = par.nodePropertyResult("123", "prop");
    	assertNotNull(row);
    }

    @Test
    public void testResourceList() throws Exception {
        par.setResourceList(resourceList);

        when(resourceList.get(0)).thenReturn("node1.prop1");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.firstPartPattern()).thenReturn("node1");
        when(resourceList.firstPart()).thenReturn("node1");
        when(resourceList.lastPartPattern()).thenReturn("prop1");

        when(resourceList.firstPartContainsWild()).thenReturn(false); // use =
        when(resourceList.lastPartContainsWild()).thenReturn(false);

        Map<String, List<Map.Entry<String, List<String>>>> ftm = par.getMatchingNodeMap();
        
        assertTrue(par.resourceNameMatch("node1"));
        
        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.firstPartPattern()).thenReturn("a");
        when(resourceList.firstPart()).thenReturn("a");
        when(resourceList.lastPartPattern()).thenReturn("b");
        when(resourceList.firstPartContainsWild()).thenReturn(true); // use =
        when(resourceList.lastPartContainsWild()).thenReturn(false);
        ResourcePattern pattern = new ResourcePattern("a");
        when(resourceList.firstPartResourcePattern()).thenReturn(pattern);

        ftm = par.getMatchingNodeMap();
        assertTrue(par.resourceNameMatch("a"));
        
        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.firstPartPattern()).thenReturn("a");
        when(resourceList.firstPart()).thenReturn("a");
        when(resourceList.lastPartPattern()).thenReturn("b");
        when(resourceList.firstPartContainsWild()).thenReturn(false);
        when(resourceList.lastPartContainsWild()).thenReturn(true);

        ftm = par.getMatchingNodeMap();
        assertTrue(par.resourceNameMatch("a"));

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.firstPartPattern()).thenReturn("a");
        when(resourceList.lastPartPattern()).thenReturn("b");
        when(resourceList.firstPartContainsWild()).thenReturn(true);
        when(resourceList.lastPartContainsWild()).thenReturn(true);

        ftm = par.getMatchingNodeMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(true);
        when(resourceList.isLastPartWild()).thenReturn(true);

        ftm = par.getMatchingNodeMap();
        
        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(true);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.lastPartPattern()).thenReturn("b");

        ftm = par.getMatchingNodeMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(true);
        when(resourceList.firstPartPattern()).thenReturn("a");

        ftm = par.getMatchingNodeMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(true);
        ftm = par.getMatchingNodeMap();
    }

    private Map<String, Object> genRow(String key, Object value) {
        Map<String, Object> row = new HashMap<String, Object>();
        row.put(key, value);
        return row;
    }
}
