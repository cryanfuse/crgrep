/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test the factory for creating db parser instances based on URI.
 * 
 * @author 'Craig Ryan'
 */
public class DatabaseFactoryTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testOracleParser() {
        DatabaseParser p = DatabaseFactory.getDatabaseParser("jdbc:ora:mydb", "sa", "shhh");
        assertNotNull(p);
        assertTrue(p instanceof OracleParser);
        assertEquals("sa", p.getUser());
        assertEquals("shhh", p.getPassword());
    }
    @Test
    public void testMysqlParser() {
        DatabaseParser p = DatabaseFactory.getDatabaseParser("jdbc:mysql:mydb", "sa", "shhh");
        assertNotNull(p);
        assertTrue(p instanceof MysqlParser);
        assertEquals("sa", p.getUser());
        assertEquals("shhh", p.getPassword());
    }
    @Test
    public void testSqliteParser() {
        DatabaseParser p = DatabaseFactory.getDatabaseParser("jdbc:sqlite:mydb", "sa", "shhh");
        assertNotNull(p);
        assertTrue(p instanceof SqliteParser);
        assertEquals("sa", p.getUser());
        assertEquals("shhh", p.getPassword());
    }
    @Test
    public void testH2Parser() {
        DatabaseParser p = DatabaseFactory.getDatabaseParser("jdbc:h2:mydb", "sa", "shhh");
        assertNotNull(p);
        assertTrue(p instanceof H2Parser);
        assertEquals("sa", p.getUser());
        assertEquals("shhh", p.getPassword());
    }
    @Test
    public void testSqlServerMsParser() {
        DatabaseParser p = DatabaseFactory.getDatabaseParser("jdbc:sqlserver:mydb", "sa", "shhh");
        assertNotNull(p);
        assertTrue(p instanceof SqlServerParser);
        assertEquals("sa", p.getUser());
        assertEquals("shhh", p.getPassword());
    }
    @Test
    public void testSqlServerJtdsParser() {
        DatabaseParser p = DatabaseFactory.getDatabaseParser("jdbc:jtds:sqlserver", "sa", "shhh");
        assertNotNull(p);
        assertTrue(p instanceof SqlServerParser);
        assertEquals("sa", p.getUser());
        assertEquals("shhh", p.getPassword());
    }
    @Test
    public void testPostgresqlParser() {
        DatabaseParser p = DatabaseFactory.getDatabaseParser("jdbc:postgresql", "sa", "shhh");
        assertNotNull(p);
        assertTrue(p instanceof PostgresqlParser);
        assertEquals("sa", p.getUser());
        assertEquals("shhh", p.getPassword());
    }

    @Test
    public void testUnknownParser() {
        DatabaseParser p = DatabaseFactory.getDatabaseParser("jdbc:bobsdriver:dodgydb", "", "");
        assertNull(p);
    }
}
