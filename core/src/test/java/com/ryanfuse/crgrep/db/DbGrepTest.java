/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.doReturn;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ryanfuse.crgrep.DbGrep;
import com.ryanfuse.crgrep.DbResourceList;
import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceList;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.util.Display;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

/*
 * Tests database grep. 
 */
@RunWith(MockitoJUnitRunner.class)
public class DbGrepTest {

    @Mock private DatabaseParser parser;

    private Pattern pattern;
    private ResourceList resourceList;
    private DisplayRecorder display;
    
    @Before
    public void setUp() throws Exception {
        display = new DisplayRecorder(new Display());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testNoParser() throws GrepException {
        Switches sw = new Switches();
        DbGrep dg = new DbGrep(sw);
        ResourcePattern rp = new ResourcePattern("nomatch");
        dg.setResourcePattern(rp);
        dg.setUri("jdbc:dodgy");
        dg.setDisplay(display);
        dg.execute();
        assertTrue(display.isErrorCalled());
        assertTrue(display.getErrorText().contains("unsupported database"));
    }

    @Test
    public void testNoTables() throws Exception {
        Switches sw = new Switches();
        DbGrep dg = new DbGrep(sw);
        ResourcePattern rp = new ResourcePattern("*");
        dg.setResourcePattern(rp);
        dg.setDisplay(display);
        dg.setParser(parser);
        when(parser.getFullTableMap()).thenReturn(null);
        dg.execute();
        assertTrue(display.isWarnCalled());
        assertTrue(display.getWarnText().contains("Database has no tables"));
    }

    @Test
    public void testListingMatchingColumns() throws Exception {
        Switches sw = new Switches();
        DbGrep dg = new DbGrep(sw);
        ResourcePattern rp = new ResourcePattern("*");
        dg.setResourcePattern(rp);
        dg.setDisplay(display);
        dg.setParser(parser);
        when(parser.getFullTableMap()).thenReturn(genTableMap());
        when(parser.getColumnSubset(any(String.class))).thenReturn(genColumnSubset());
        resourceList = new DbResourceList("*");
        when(parser.getResourceList()).thenReturn(resourceList);
        dg.execute();
    }

    @Test
    public void testListingMatchingColumnsAllContent() throws Exception {
        Switches sw = new Switches();
        sw.setAllcontent(true);
        sw.setDebug(true);
        DbGrep dg = new DbGrep(sw);
        ResourcePattern rp = new ResourcePattern("tab*.col*");
        dg.setResourcePattern(rp);
        dg.setDisplay(display);
        dg.setParser(parser);
        when(parser.getFullTableMap()).thenReturn(genTableMap());
        when(parser.getColumnSubset(any(String.class))).thenReturn(genColumnSubset());
        resourceList = new DbResourceList("*");
        when(parser.getResourceList()).thenReturn(resourceList);
        dg.execute();
    }

    @Test
    public void testListingNoMatchingColumnsContainsWildWild() throws Exception {
        Switches sw = new Switches();
        DbGrep dg = new DbGrep(sw);
        ResourcePattern rp = new ResourcePattern("xxx");
        dg.setResourcePattern(rp);
        dg.setDisplay(display);
        dg.setParser(parser);
        when(parser.getFullTableMap()).thenReturn(genTableMap());
        when(parser.getColumnSubset(any(String.class))).thenReturn(genColumnSubset());
        resourceList = new DbResourceList("tab*.col*");
        when(parser.getResourceList()).thenReturn(resourceList);
        dg.execute();
    }

    @Test
    public void testListingNoMatchingColumnsContainsWildExact() throws Exception {
        Switches sw = new Switches();
        DbGrep dg = new DbGrep(sw);
        ResourcePattern rp = new ResourcePattern("xxx");
        dg.setResourcePattern(rp);
        dg.setDisplay(display);
        dg.setParser(parser);
        when(parser.getFullTableMap()).thenReturn(genTableMap());
        when(parser.getColumnSubset(any(String.class))).thenReturn(genColumnSubset());
        resourceList = new DbResourceList("tab*.col1");
        when(parser.getResourceList()).thenReturn(resourceList);
        dg.execute();
    }

    @Test
    public void testListingNoMatchingColumnsContainsExactWild() throws Exception {
        Switches sw = new Switches();
        DbGrep dg = new DbGrep(sw);
        ResourcePattern rp = new ResourcePattern("xxx");
        dg.setResourcePattern(rp);
        dg.setDisplay(display);
        dg.setParser(parser);
        when(parser.getFullTableMap()).thenReturn(genTableMap());
        when(parser.getColumnSubset(any(String.class))).thenReturn(genColumnSubset());
        resourceList = new DbResourceList("tab1.col?");
        when(parser.getResourceList()).thenReturn(resourceList);
        dg.execute();
    }

    @Test
    public void testListingNoMatchingResources() throws Exception {
        Switches sw = new Switches();
        DbGrep dg = new DbGrep(sw);
        ResourcePattern rp = new ResourcePattern("xxx");
        dg.setResourcePattern(rp);
        dg.setDisplay(display);
        dg.setParser(parser);
        when(parser.getFullTableMap()).thenReturn(genTableMap());
        when(parser.getColumnSubset(any(String.class))).thenReturn(genColumnSubset());
        resourceList = new DbResourceList("notab.nocol");
        when(parser.getResourceList()).thenReturn(resourceList);
        dg.execute();
    }
    
    @Test
    public void testContentMatchingAll() throws Exception {
        Switches sw = new Switches();
        sw.setContent(true);
        DbGrep dg = new DbGrep(sw);
        ResourcePattern rp = new ResourcePattern("*");
        dg.setResourcePattern(rp);
        dg.setDisplay(display);
        dg.setParser(parser);
        when(parser.getFullTableMap()).thenReturn(genTableMap());
        when(parser.getColumnSubset(any(String.class))).thenReturn(genColumnSubset());
        when(parser.getMatchingColumns(any(List.class), any(String.class), eq(rp))).thenReturn(genMatchingColumns());
        resourceList = new DbResourceList("*");
        when(parser.getResourceList()).thenReturn(resourceList);
        dg.execute();
    }

    @Test
    public void testContentMatchingDataOnly() throws Exception {
        Switches sw = new Switches();
        sw.setContent(true);
        DbGrep dg = new DbGrep(sw);
        ResourcePattern rp = new ResourcePattern("data");
        dg.setResourcePattern(rp);
        dg.setDisplay(display);
        dg.setParser(parser);
        when(parser.getFullTableMap()).thenReturn(genTableMap());
        when(parser.getColumnSubset(any(String.class))).thenReturn(genColumnSubset());
        when(parser.getMatchingColumns(any(List.class), any(String.class), eq(rp))).thenReturn(genMatchingColumns());
        resourceList = new DbResourceList("*");
        when(parser.getResourceList()).thenReturn(resourceList);
        dg.execute();
    }

    @Test
    public void testGetFullMapException() {
        Switches sw = new Switches();
        sw.setContent(true);
        DbGrep dg = new DbGrep(sw);
        ResourcePattern rp = new ResourcePattern("*");
        dg.setResourcePattern(rp);
        dg.setDisplay(display);
        dg.setParser(parser);
        try {
            when(parser.getFullTableMap()).thenThrow(new SQLException("no db"));
        } catch (Exception e) {
            fail("fullTableMap error: " + e.getLocalizedMessage());
        }
        try {
            dg.execute();
            fail("expected exception");
        } catch (GrepException e) {
            assertNotNull(e.getCause());
            assertTrue(e.getCause().getMessage().contains("no db"));
        }
    }
    private List<List<String>> genMatchingColumns() {
        List<String> cols = new ArrayList<String>();
        cols.add("data1");
        cols.add(null);
        List<List<String>> matchingCols = new ArrayList<List<String>>();
        matchingCols.add(cols);
        return matchingCols;
    }

    private List<String> genColumnSubset() {
        List<String> cols = new ArrayList<String>();
        cols.add("col1");
        cols.add("col2");
        return cols;
    }

    private Map<String, List<String>> genTableMap() {
        List<String> cols = new ArrayList<String>();
        cols.add("col1");
        cols.add("col2");
        Map<String, List<String>> tableMap = new HashMap<String, List<String>>();
        tableMap.put("tab1", cols);
        return tableMap;
    }
}
