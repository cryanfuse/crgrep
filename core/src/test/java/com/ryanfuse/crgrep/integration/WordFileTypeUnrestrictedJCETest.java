/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.integration;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;

/*
 * Tests the MS Word document file type
 */
@RunWith(MockitoJUnitRunner.class)
public class WordFileTypeUnrestrictedJCETest extends TestCommon {

    @Mock private InputStream stream;

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        // force unrestricted JCE 
        removeCryptographyRestrictions();
    }
    @AfterClass
    public static void setUpAfterClass() throws Exception {
        // force restricted JCE 
        enableCryptographyRestrictions();
    }
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testDocEncryptedWrongPasswordJceUnrestricted() throws GrepException {
        title("ms Word (.doc) encrypted", "*", "target/encrypted/ms/encrypted/pw-protected.doc");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "-p", "goaway",
                "*",
                "target/encrypted/ms/encrypted/pw-protected.doc" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("encrypted word file"));
    }

    // POI can't decrypt this file
    @Test
    public void testDocEncryptedWithPasswordJceUnrestricted() throws GrepException {
        title("ms Word (.doc) encrypted", "*", "target/encrypted/ms/encrypted/pw-protected.doc");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "--warn",
                "-p", "password",
                "*",
                "target/encrypted/ms/encrypted/pw-protected.doc" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Cannot process encrypted"));
    }

    @Test
    public void testDocxEncryptedWrongPasswordJceUnrestricted() throws GrepException {
        title("ms Word (.docx) encrypted", "*", "target/encrypted/ms/encrypted/pw-protected.docx");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "-p", "goaway",
                "--warn",
                "*",
                "target/encrypted/ms/encrypted/pw-protected.docx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Specify a valid password"));
    }

    @Test
    public void testDocxEncryptedWithPasswordJceUnrestricted() throws GrepException {
        title("ms Word (.docx) encrypted", "*", "target/encrypted/ms/encrypted/pw-protected.docx");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "-p", "password",
                "--warn",
                "*",
                "target/encrypted/ms/encrypted/pw-protected.docx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertFalse(actual.isEmpty());
    }

    @Test
    public void testDocxNotEncryptedPasswordGivenJceUnrestricted() throws GrepException {
        title("ms (.docx) not encrypted, password given", "Publish", "target/resources/ms/word.docx");
        List<String> expected = Arrays.asList(
                "target/resources/ms/word.docx:P:S. P. Bingulac, On the compatibility of adaptive controllers (Published Conference Proceedings style), in Proc. 4th Annu. Allerton Conf. Circuits and Systems Theory, New York, 1994, pp. 816."
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace",
                "-p", "password",
                "Publish", 
                "target/resources/ms/word.docx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
}
