/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.integration;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;

/*
 * Forces unrestricted JCE cryptography which removes Java Export Restrictions. 
 * All tests will be able to set and use a password when opening password protected sheets.
 */
@RunWith(MockitoJUnitRunner.class)
public class ExcelFileTypeUnrestrictedJCETest extends TestCommon {

    @Mock private InputStream stream;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        // force unrestricted JCE 
        removeCryptographyRestrictions();
    }
    @AfterClass
    public static void setUpAfterClass() throws Exception {
        // force restricted JCE 
        enableCryptographyRestrictions();
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testXlsExcelPasswordProtectedJceUnrestricted() throws GrepException {
        title("ms excel (xls) password JCE off", "onkey", "target/encrypted/ms/encrypted/pw-protected.xls");
        List<String> expected = Arrays.asList(
                "target/encrypted/ms/encrypted/pw-protected.xls:0:0:0:monkey"
        );
        String[] args = new String[] { 
                //"-X", "debug", //,trace",
                "-p", "password",
                "--warn",
                "onkey", 
                "target/encrypted/ms/encrypted/pw-protected.xls" };

        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("protected document"));
    }

    @Test
    public void testXlsxExcelPasswordProtectedJceUnrestricted() throws GrepException {
        title("ms excel (xlsx) password JCE off", "onkey", "target/encrypted/ms/encrypted/pw-protected.xlsx");
        List<String> expected = Arrays.asList(
                "target/encrypted/ms/encrypted/pw-protected.xlsx:0:0:0:monkey"
        );
        String[] args = new String[] { 
                //"-X", "debug", //,trace",
                "-p", "password",
                "onkey", 
                "target/encrypted/ms/encrypted/pw-protected.xlsx" };

        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        // should be no warnings about ignored because pw is valid
        assertTrue(display.getWarnText() == null || display.getWarnText().isEmpty());
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testXlsExcelPasswordProtectedNoPasswordGivenJceUnrestricted() throws GrepException {
        title("ms excel (xls) no password given, JCE on", "onkey", "target/encrypted/ms/encrypted/pw-protected.xls");
        String[] args = new String[] { 
                //"-X", "debug", //,trace",
                "--warn",
                "onkey", 
                "target/encrypted/ms/encrypted/pw-protected.xls" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.getGrep().setPw(null); // make sure no password is set
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("protected document"));
    }


    @Test
    public void testXlsxExcelPasswordProtectedNoPasswordGivenJceUnrestricted() throws GrepException {
        title("ms excel (xlsx) no password given, JCE on", "onkey", "target/encrypted/ms/encrypted/pw-protected.xlsx");
        String[] args = new String[] { 
                //"-X", "debug", //,trace",
                "--warn",
                "onkey", 
                "target/encrypted/ms/encrypted/pw-protected.xlsx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.getGrep().setPw(null); // make sure no password is set
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("Specify a valid password"));
    }

    @Test
    public void testXlsExcelPasswordProtectedWrongPasswordJceUnrestricted() throws GrepException {
        title("ms excel (xls) wrong password", "onkey", "target/encrypted/ms/encrypted/pw-protected.xls");
        String[] args = new String[] { 
                //"-X", "debug", //,trace",
                "--warn",
                "-p", "wrongPassword",
                "onkey", 
                "target/encrypted/ms/encrypted/pw-protected.xls" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("format of password protected"));
    }

    @Test
    public void testXlsxExcelPasswordProtectedWrongPasswordJceUnrestricted() throws GrepException {
        title("ms excel (xlsx) wrong password", "onkey", "target/encrypted/ms/encrypted/pw-protected.xlsx");
        String[] args = new String[] { 
                //"-X", "debug", //,trace",
                "--warn",
                "-p", "wrongPassword",
                "onkey", 
                "target/encrypted/ms/encrypted/pw-protected.xlsx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("password incorrect"));
    }

    @Test
    public void testXlsExcelNotPasswordProtectedPasswordGivenJceUnrestricted() throws GrepException {
        title("ms excel (xls) not protected but password given", "Demonstrate", "target/resources/ms/excel.xls");
        List<String> expected = Arrays.asList(
                "target/resources/ms/excel.xls:0:7:1:Demonstrates how to change the formatting (I.e. font, cell color) applied to a cell depending on the current value of the cell."
        );
        String[] args = new String[] { 
                //"-X", "debug", //,trace",
                "-p", "password",
                "Demonstrate", 
                "target/resources/ms/excel.xls" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testXlsxExcelNotPasswordProtectedPasswordGivenJceUnrestricted() throws GrepException {
        title("ms excel (xlsx) not protected but password given", "Demonstrate", "target/resources/ms/excel.xlsx");
        List<String> expected = Arrays.asList(
                "target/resources/ms/excel.xlsx:0:7:1:Demonstrates how to change the formatting (I.e. font, cell color) applied to a cell depending on the current value of the cell."
        );
        String[] args = new String[] { 
                //"-X", "debug", //,trace",
                "-p", "password",
                "Demonstrate", 
                "target/resources/ms/excel.xlsx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
}
