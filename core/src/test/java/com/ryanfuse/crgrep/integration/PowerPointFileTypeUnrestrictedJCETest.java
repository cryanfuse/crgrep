/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.integration;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

@RunWith(MockitoJUnitRunner.class)
public class PowerPointFileTypeUnrestrictedJCETest extends TestCommon {

    @Mock private InputStream stream;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        // force unrestricted JCE 
        removeCryptographyRestrictions();
    }
    @AfterClass
    public static void setUpAfterClass() throws Exception {
        // force restricted JCE 
        enableCryptographyRestrictions();
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testMsPowerPointXPasswordProtectedJceUnrestricted() throws GrepException {
        title("ms password pptx JceUnrestricted", "Secret", "target/encrypted/ms/encrypted/pw-protected.pptx");
        List<String> expected = Arrays.asList(
                "target/encrypted/ms/encrypted/pw-protected.pptx:1:Secret"
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "--warn",
                "-p", "password",
                "Secret", 
                "target/encrypted/ms/encrypted/pw-protected.pptx" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testMsPowerPointPasswordProtectedJceUnrestricted() throws GrepException {
        title("ms password pptx JceUnrestricted", "is", "target/encrypted/ms/encrypted/testPPT_protected_passtika.ppt");
        List<String> expected = Arrays.asList(
                "target/encrypted/ms/encrypted/testPPT_protected_passtika.ppt:1:This is an encrypted PowerPoint 2007 slide."
        );
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "--warn",
                "-p", "tika",
                "is",
                "target/encrypted/ms/encrypted/testPPT_protected_passtika.ppt" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testMsPowerPointPasswordProtectedWrongPasswordJceUnrestricted() throws GrepException {
        title("ms password pptx JceUnrestricted", "is", "target/encrypted/ms/encrypted/testPPT_protected_passtika.ppt");
        String[] args = new String[] { 
                //"-r", 
                //"-l", 
                //"-X", "debug", //,trace", 
                "--warn",
                "-p", "wrongpw",
                "is",
                "target/encrypted/ms/encrypted/testPPT_protected_passtika.ppt" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        display.setStripControls(true);
        display.setSortedPaths(false);
        rg.setDisplay(display);
        rg.getSwitches().setOcr(false);
        rg.execute();
        List<String> actual = display.messages();
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
        assertNotNull(display.getWarnText());
        assertTrue("unexpected " + display.getWarnText(), display.getWarnText().contains("a valid password"));
    }
}
