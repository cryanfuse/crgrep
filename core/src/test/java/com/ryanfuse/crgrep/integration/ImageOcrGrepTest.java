/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.integration;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;

/**
 * Test for matching image content search using OCR. 
 *
 * Interacts with an external C++ library and therefore placed in integration test package.
 *
 * @author Craig Ryan
 */
public class ImageOcrGrepTest extends TestCommon {

	@Test
	public void testOCRBmp() throws GrepException {
		List<String> expected = Arrays.asList(
				"src/test/resources/ocr/ocrtext.bmp:0: The (quick) [brown] {fox} jumps!"
				);
		title("images OCR (bmp)", "brown", "src/test/resources/ocr/ocrtext.bmp");
		String[] args = new String[] { "--ocr", 
		        //"-X", "debug,trace", 
		        "brown", "src/test/resources/ocr/ocrtext.bmp" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
			actualList(actual, expected),
			ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testOCRGif() throws GrepException {
		List<String> expected = Arrays.asList(
				"src/test/resources/ocr/ocrtext.gif:0: The (quick) [brown] {fox} jumps!"
				);
		title("images OCR (gif)", "brown", "src/test/resources/ocr/ocrtext.gif");
		String[] args = new String[] { "--ocr", 
		        //"-X", "debug,trace", 
		        "brown", "src/test/resources/ocr/ocrtext.gif" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
			actualList(actual, expected),
			ListUtils.isEqualList(expected, actual));
	}

    @Test
    public void testOCRPng() throws GrepException {
        List<String> expected = Arrays.asList(
                "src/test/resources/ocr/ocrtext.png:0: The (quick) [brown] {fox} jumps!"
                );
        title("images OCR (png)", "brown", "src/test/resources/ocr/ocrtext.png");
        String[] args = new String[] { "--ocr", 
                //"-X", "debug,trace", 
                "brown", "src/test/resources/ocr/ocrtext.png" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testOCRPngShortOutput() throws GrepException {
        List<String> expected = Arrays.asList(
                "src/test/resources/ocr/ocrtext.png:0: ...brown..."
                );
        title("images OCR (png) short output", "brown", "src/test/resources/ocr/ocrtext.png");
        String[] args = new String[] { "--ocr", 
                //"-X", "debug,trace", 
                "brown", "src/test/resources/ocr/ocrtext.png" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.getSwitches().setMaxResultLength(5);
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }
	 
	@Test
	public void testOCRTif() throws GrepException {
		List<String> expected = Arrays.asList(
				"src/test/resources/ocr/ocrtext.tif:0: The (quick) [brown] {fox} jumps!"
				);
		title("images OCR (tif)", "brown", "src/test/resources/ocr/ocrtext.tif");
		String[] args = new String[] { "--ocr", 
		        //"-X", "debug,trace", 
		        "brown", "src/test/resources/ocr/ocrtext.tif" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
			actualList(actual, expected),
			ListUtils.isEqualList(expected, actual));
	}
}
