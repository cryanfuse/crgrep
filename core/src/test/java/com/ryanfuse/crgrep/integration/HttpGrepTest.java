/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.junit.Test;

import com.ryanfuse.crgrep.FileResourceList;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.HttpGrep;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Web page (HTTP GET) tests.
 * 
 * If this test fails due to assert failure, it is most likely google have changed the html around the favicon text.
 * 
 * @author Craig Ryan
 */
public class HttpGrepTest extends TestCommon {
    
    @Test
    public void testHttp() throws GrepException {
        title("http", "googleg_lodp", "http://www.google.com");
        String expectedStartsWith = "http://www.google.com:</style><style>body";
        String[] args = new String[] {
            //"--warn", "-X", "debug",
            "googleg_lodp",
            "http://www.google.com" 
        };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertEquals("expected 1 line result (google home page may have changed!)", 1, actual.size());
        assertTrue("unexpected result string", actual.get(0).startsWith(expectedStartsWith));
    }

    @Test
    public void testHttpShortOutput() throws GrepException {
        title("http", "googleg_lodp", "http://www.google.com");
        String expectedStartsWith = "http://www.google.com:...googleg_lo...";
        String[] args = new String[] {
            //"--warn", "-X", "debug",
            "googleg_lodp",
            "http://www.google.com" 
        };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.getSwitches().setMaxResultLength(10);
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertEquals("expected 1 line result (google home page may have changed!)", 1, actual.size());
        assertTrue("unexpected result string", actual.get(0).startsWith(expectedStartsWith));
    }

    @Test
    public void testHttpShortOutputThanMatchText() throws GrepException {
        title("http", "googleg_lodp", "http://www.google.com");
        String expectedStartsWith = "http://www.google.com:...googl..."; 
        String[] args = new String[] {
            //"--warn", "-X", "debug",
            "googleg_lodp",
            "http://www.google.com" 
        };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.getSwitches().setMaxResultLength(5);
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertEquals("expected 1 line result (google home page may have changed!)", 1, actual.size());
        assertTrue("unexpected result string", actual.get(0).startsWith(expectedStartsWith));
    }

    @Test
    public void testHttpHighlighted() throws GrepException {
        title("http", "googleg_lodp", "http://www.google.com");
        String expectedStartsWith = "http://www.google.com:</style><style>body,td,a,p,.h{font-family:arial,sans-serif}body{margin:0;overflow-y:scroll}#gog{padding:3px 8px 0}td{line-height:.8em}.gac_m td{line-height:17px}form{margin-bottom:20px}.h{color:#36c}.q{color:#00c}.ts td{padding:0}.ts{border-collapse:collapse}em{font-weight:bold;font-style:normal}.lst{height:25px;width:496px}.gsfi,.lst{font:18px arial,sans-serif}.gsfs{font:17px arial,sans-serif}.ds{display:inline-box;display:inline-block;margin:3px 0 4px;margin-left:4px}input{font-family:inherit}a.gb1,a.gb2,a.gb3,a.gb4{color:#11c !important}body{background:#fff;color:black}a{color:#11c;text-decoration:none}a:hover,a:active{text-decoration:underline}.fl a{color:#36c}a:visited{color:#551a8b}a.gb1,a.gb4{text-decoration:underline}a.gb3:hover{text-decoration:none}#ghead a.gb2:hover{color:#fff !important}.sblc{padding-top:5px}.sblc a{display:block;margin:2px 0;margin-left:13px;font-size:11px}.lsbb{background:#eee;border:solid 1px;border-color:#ccc #999 #999 #ccc;height:30px}.lsbb{display:block}.ftl,#fll a{display:inline-block;margin:0 12px}.lsb{background:url(/images/nav_logo229.png) 0 -261px repeat-x;border:none;color:#000;cursor:pointer;height:30px;margin:0;outline:0;font:15px arial,sans-serif;vertical-align:top}.lsb:active{background:#ccc}.lst:focus{outline:none}</style><script></script><link href=\"/images/branding/product/ico/[1;31m[Kgoogleg_lodp[m[K.ico\" rel=\"shortcut icon\"></head><body bgcolor=\"#fff\"><script>(function(){var src='/images/nav_logo229.png';var iesg=false;document.body.onload = function(){window.n && window.n();if (document.images){new Image().src=src;}";
        String[] args = new String[] {
            //"--warn", "-X", "debug",
            "--color", "always",
            "googleg_lodp",
            "http://www.google.com" 
        };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertEquals("expected 1 line result (google home page may have changed!)", 1, actual.size());
        assertTrue("unexpected result string", actual.get(0).startsWith(expectedStartsWith));
    }

    @Test
    public void testHttpInvalidUrl() {
        title("http", "nip", "not-a-url");
        Switches switches = new Switches();
        HttpGrep rg = new HttpGrep(switches);
        FileResourceList rl = new FileResourceList("not-a-url");
        rg.setArguments(null, rl);
        try {
            rg.execute();
            fail("expected malformed url exception");
        } catch (GrepException e) {
            // expected
            assertTrue(e.getCause() instanceof MalformedURLException);
        }
    }

    @Test
    public void testHttpUnreachableUrl() {
        title("http", "nip", "http://nolikelyasite.ryanfuse.com.nob100dyway/");
        String[] args = new String[] {
            //"--warn", "-X", "debug,trace",
            "nip", "http://nolikelyasite.ryanfuse.com.nob100dyway/" 
        };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        try {
            rg.execute();
            fail("expected IO exception");
        } catch (Exception e) {
            // expected
            assertTrue(e.getCause() instanceof IOException);
        }
    }
}
