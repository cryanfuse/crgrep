/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.db.H2Parser;
import com.ryanfuse.crgrep.util.DisplayRecorder;

/**
 * H2 database tests.
 * 
 * H2 specific behaviour:
 *  - execute create tables with mixed case names, they're created as uppercase
 *  - 
 *  
 * The required test database exists in the src/test/databases directory and does not
 * need to be created manually or via the build.
 *
 * Database consists of these tables [: columns]
 * 		ALL_BINARY_COLUMNS: NUM1 (CLOB), NUM2 (CLOB)
 *      -> all binary columns
 *              1, 2
 *              12345, 9876
 * 		CAUSES: ID (INT), NAME (VC50), CAUSE_STUFF (VC20)
 *      -> table/col names and data both match
 *              0,cause 1,this cause is mine          
 *              1,cause 2,also mine          
 *              2,no match,nada
 *      HISTORY: EVENT (VC100), AUTHOR (VC20)
 *      -> table/col names don't match data
 *      -> match in one column of a row with diff column in another row and vice versa eg 'ted' and 'cra'
 *              completed all outstanding tasks!,craig
 *              crafting new ideas,ted
 *              More Crazy ideas,Stan
 *      CATALOG: NAME (VC30), CHUNK(BLOB)
 *      -> same column name as CAUSES table
 *      -> extra binary column
 *              cat,null
 *              dan,null
 *
 * To connect to this DB via DbVis etc, I used this JDBC URL including drive:path to the database file:
 *     jdbc:h2:d:/crgrep/core/src/test/databases/h2db;AUTO_SERVER=TRUE;
 *     
 * There must NOT be any open connections to the DB when this test is executed.
 * 
 * @author Craig Ryan
 */
public class H2DbGrepTest extends TestCommon {

    private static final String DB_USER = "sa";
    private static final String DB_PW = "";
    private static final String DB_URL = "jdbc:h2:/" + projectDir() + "/src/test/databases/h2db";


    @Test
    public void testH2Defaults() throws GrepException {
        H2Parser hp = new H2Parser("jdbc:h2:stuff", "user", "pw");
        assertNotNull(hp.getDefaultPassword());
        assertNotNull(hp.getDefaultUser());
        assertEquals("user", hp.getUser());
        assertEquals("pw", hp.getPassword());
        assertEquals("jdbc:h2:stuff", hp.getUri());
    }
    
    @Test
    public void testPatternUpperExact_TabWildColWild_ByName_Case() throws GrepException {
        title("H2", "CAUSE", "*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "CAUSE", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternUpperExact_TabWildColWild_ByName_Case_ShortOutput() throws GrepException {
        title("H2", "CAUSE", "*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "CAUSE", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        rg.getSwitches().setMaxResultLength(5);
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternMixedExact_TabWildColWild_ByName_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "StufF", "*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "StufF", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWildColWild_NameData_Case() throws GrepException {
        title("H2", "cause", "*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]",
                "CAUSES: 0,cause 1,the cause is mine",
                "CAUSES: 1,cause 2,also mine"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWildColWild_NameData_Case_ShortOuput() throws GrepException {
        title("H2", "cause", "*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]",
                "CAUSES: 0,cau...,...cau...",
                "CAUSES: 1,cau...,als..."
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.getSwitches().setMaxResultLength(3);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWildColWild_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "cause", "*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]",
                "CAUSES: 0,cause 1,the cause is mine",
                "CAUSES: 1,cause 2,also mine"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWildColWild_TwoNameData_Case() throws GrepException {
        title("H2", "ca", "*");
        List<String> expected = Arrays.asList(
                "CATALOG: [NAME]",
                "CATALOG: cat",
                "CAUSES: [ID,NAME,CAUSE_STUFF]",
                "CAUSES: 0,cause 1,the cause is mine",
                "CAUSES: 1,cause 2,also mine"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWildColWild_TwoNameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "cA", "*");
        List<String> expected = Arrays.asList(
        		"CATALOG: [NAME]",
                "CATALOG: cat",
                "CAUSES: [ID,NAME,CAUSE_STUFF]",
                "CAUSES: 0,cause 1,the cause is mine",
                "CAUSES: 1,cause 2,also mine"
        		);
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cA", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternMixedExact_TabWildColWild_NoMatches_Case() throws GrepException {
        title("H2", "MiNe", "*.*");
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "MiNe", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testPatternMixedExact_TabWildColWild_NoMatches_IgnoreCase() throws GrepException {
        title("H2", "SomePattern", "*.*");
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "SomePattern", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testPatternLowerExact_TabColWild_ByName_Case() throws GrepException {
        title("H2", "CAT", "CATALOG.*");
        List<String> expected = Arrays.asList(
                "CATALOG: [NAME]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "CAT", "CATALOG.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabColWild_ByName_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "CATA", "CATALOG.*");
        List<String> expected = Arrays.asList(
                "CATALOG: [NAME]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "CATA", "CATALOG.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabColWild_NameData_Case() throws GrepException {
        title("H2", "cause", "CAUSES.*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]",
                "CAUSES: 0,cause 1,the cause is mine",
                "CAUSES: 1,cause 2,also mine"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "CAUSES.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabColWild_NameData_IgnoreCase() throws GrepException {
        title("H2", "cause", "CAUSES.*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]",
                "CAUSES: 0,cause 1,the cause is mine",
                "CAUSES: 1,cause 2,also mine"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "CAUSES.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWild_ByName_Case() throws GrepException {
        title("H2", "TORY", "*.AUTHOR");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TORY", "*.AUTHOR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWild_ByName_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "TOrY", "*.author");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TOrY", "*.author"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWild_NameData_Case() throws GrepException {
        title("H2", "n", "*.EVENT");
        List<String> expected = Arrays.asList(
            "HISTORY: [EVENT]",
            "HISTORY: completed all outstanding tasks!",
            "HISTORY: crafting new ideas"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "n", "*.EVENT"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternUpperExact_TabWild_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "N", "*.EVENT");
        List<String> expected = Arrays.asList(
                "HISTORY: [EVENT]",
                "HISTORY: completed all outstanding tasks!",
                "HISTORY: crafting new ideas"
                );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "N", "*.EVENT"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }


    @Test
    public void testPatternLowerExact_TabWild_TwoNameData_Case() throws GrepException {
        title("H2", "ca", "*.NAME");
        List<String> expected = Arrays.asList(
                "CATALOG: [NAME]",
                "CATALOG: cat",
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca", "*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWild_TwoNameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "ca", "*.NAME");
        List<String> expected = Arrays.asList(
                "CATALOG: [NAME]",
                "CATALOG: cat",
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca", "*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }
    

    @Test
    public void testPatternLowerExact_TabCol_ByName_Case() throws GrepException {
        title("H2", "TORY", "HISTORY.AUTHOR");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TORY", "HISTORY.AUTHOR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabCol_ByName_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "TOrY", "HISTORY.author");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TOrY", "HISTORY.author"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabCol_NameData_Case() throws GrepException {
        title("H2", "cau", "CAUSES.NAME");
        List<String> expected = Arrays.asList(
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau", "CAUSES.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabCol_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "cau", "CAUSES.NAME");
        List<String> expected = Arrays.asList(
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau", "CAUSES.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_ByName_Case() throws GrepException {
        title("H2", "C?T", "*.*");
        List<String> expected = Arrays.asList(
                "CATALOG: [NAME]" // exclude CHUNK?
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "C?T", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_ByName_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "*UFF", "*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "*UFF", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_NameData_Case() throws GrepException {
        title("H2", "c??s*", "*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]",
                "CAUSES: 0,cause 1,the cause is mine",
                "CAUSES: 1,cause 2,also mine"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "c??s*", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "ca?S?", "*.*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]",
                "CAUSES: 0,cause 1,the cause is mine",
                "CAUSES: 1,cause 2,also mine"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca?S?", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_ManyNameData_Case() throws GrepException {
        title("H2", "?a?*", "*");
        List<String> expected = Arrays.asList(
                "CATALOG: [NAME]",
                "CATALOG: cat",
                "CATALOG: dan",                
                "CAUSES: [ID,NAME,CAUSE_STUFF]",
                "CAUSES: 0,cause 1,the cause is mine",
                "CAUSES: 2,no match,nada",
                "CAUSES: 1,cause 2,also mine",
                "ALL_BINARY_COLUMNS: []",
                "HISTORY: [EVENT,AUTHOR]",
                "HISTORY: completed all outstanding tasks!,craig",
                "HISTORY: crafting new ideas,ted",
                "HISTORY: More Crazy ideas,Stan"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "?a?*", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_TwoNameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "Ca?", "*");
        List<String> expected = Arrays.asList(
                "CATALOG: [NAME]",
                "CATALOG: cat",
                "CAUSES: [ID,NAME,CAUSE_STUFF]",
                "CAUSES: 0,cause 1,the cause is mine",
                "CAUSES: 1,cause 2,also mine"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "Ca?", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_NoMatches_Case() throws GrepException {
        title("H2", "M??E", "*.*");
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "M??E", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testPattern_TabColWild_NoMatches_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "S*Not?ed", "*.*");
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "S*Not?ed", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testPattern_TabWild_ByName_Case() throws GrepException {
        title("H2", "?OR*", "*.AUTHOR");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "?OR*", "*.AUTHOR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabWild_ByName_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "?Ory", "*.author");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "?Ory", "*.author"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabWild_NameData_Case() throws GrepException {
        title("H2", "?ing", "*.EVENT");
        List<String> expected = Arrays.asList(
            "HISTORY: [EVENT]",
            "HISTORY: completed all outstanding tasks!",
            "HISTORY: crafting new ideas"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "?ing", "*.EVENT"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabWild_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "?Ng ", "*.EVENT");
        List<String> expected = Arrays.asList(
                "HISTORY: [EVENT]",
                "HISTORY: completed all outstanding tasks!",
                "HISTORY: crafting new ideas"
                );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "?Ng", "*.EVENT"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }


    @Test
    public void testPattern_TabWild_TwoNameData_Case() throws GrepException {
        title("H2", "ca?", "*.NAME");
        List<String> expected = Arrays.asList(
                "CATALOG: [NAME]",
                "CATALOG: cat",
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca?", "*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabWild_TwoNameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "ca?*", "*.NAME");
        List<String> expected = Arrays.asList(
                "CATALOG: [NAME]",
                "CATALOG: cat",
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
                );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca?*", "*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
     
    @Test
    public void testPattern_TabCol_ByName_Case() throws GrepException {
        title("H2", "TO?Y", "HISTORY.AUTHOR");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TO?Y", "HISTORY.AUTHOR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabCol_ByName_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "T?rY", "HISTORY.author");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "T?rY", "HISTORY.author"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabCol_NameData_Case() throws GrepException {
        title("H2", "cau?e", "CAUSES.NAME");
        List<String> expected = Arrays.asList(
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau?e", "CAUSES.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabCol_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "cau??", "CAUSES.NAME");
        List<String> expected = Arrays.asList(
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau??", "CAUSES.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColPatt_ByName_Case() throws GrepException {
        title("H2", "TO?Y", "HISTORY.AUT??R");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TO?Y", "HISTORY.AUT??R"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColPatt_ByName_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "T?rY", "HISTORY.au*hor");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "T?rY", "HISTORY.au*hor"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColPatt_NameData_Case() throws GrepException {
        title("H2", "cau?e", "CAUSES.N?M*");
        List<String> expected = Arrays.asList(
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau?e", "CAUSES.N?M*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColPatt_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "cau??", "CAUSES.?AME");
        List<String> expected = Arrays.asList(
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau??", "CAUSES.?AME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
    
    @Test
    public void testPattern_TabPattCol_ByName_Case() throws GrepException {
        title("H2", "TO?Y", "H??TORY.AUTHOR");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TO?Y", "H??TORY.AUTHOR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattCol_ByName_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "T?rY", "HIS*RY.author");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "T?rY", "HIS*RY.author"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattCol_NameData_Case() throws GrepException {
        title("H2", "cau?e", "?AUSE?.NAME");
        List<String> expected = Arrays.asList(
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau?e", "?AUSE?.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattCol_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "cau??", "?AUS*S.NAME");
        List<String> expected = Arrays.asList(
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau??", "?AUS*S.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
    
    @Test
    public void testPattern_TabPattColPatt_ByName_Case() throws GrepException {
        title("H2", "TO?Y", "H??TORY.*UT?OR");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TO?Y", "H??TORY.*UT?OR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattColPatt_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "cau??", "?AUS*S.N*E");
        List<String> expected = Arrays.asList(
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau??", "?AUS*S.N*E"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattColWild_ByName_Case() throws GrepException {
        title("H2", "TO?Y", "H??TORY.*");
        List<String> expected = Arrays.asList(
                "HISTORY: [EVENT,AUTHOR]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TO?Y", "H??TORY.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattColWild_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "cau??", "?AUS*S");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]",
                "CAUSES: 0,cause 1,the cause is mine",
                "CAUSES: 1,cause 2,also mine"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau??", "?AUS*S"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabColPatt_NameData_Case() throws GrepException {
        title("H2", "cause", "CAUSES.NA*");
        List<String> expected = Arrays.asList(
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "CAUSES.NA*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWildCol_NameData_Case() throws GrepException {
        title("H2", "cause", "*.NAME");
        List<String> expected = Arrays.asList(
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWildCol_TwoNameData_Case() throws GrepException {
        title("H2", "ca", "*.NAME");
        List<String> expected = Arrays.asList(
                "CATALOG: [NAME]",
                "CATALOG: cat",
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca", "*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabPattCol_NoMatch_Case() throws GrepException {
        title("H2", "cause", "NOTAB*.CAUSE_STUFF");
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "NOTAB*.CAUSE_STUFF"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testPatternLowerExact_TabPattCol_NameData_Case() throws GrepException {
        title("H2", "mine", "CA*.CAUSE_STUFF");
        List<String> expected = Arrays.asList(
                "CAUSES: [CAUSE_STUFF]",
                "CAUSES: the cause is mine",
                "CAUSES: also mine"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "mine", "CA*.CAUSE_STUFF"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabPattCol_TwoNameData_Case() throws GrepException {
        title("H2", "ca", "CA*.NAME");
        List<String> expected = Arrays.asList(
                "CATALOG: [NAME]",
                "CATALOG: cat",
                "CAUSES: [NAME]",
                "CAUSES: cause 1",
                "CAUSES: cause 2"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca", "CA*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabPatt_ByNameOnly_Case() throws GrepException {
        title("H2", "USES", "CA*.NAME");
        List<String> expected = Arrays.asList(
                "CAUSES: [NAME]"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "USES", "CA*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

	@Test
	public void testPatternLowerExact_TabNoExistColNoExist_NoMatches_Case() throws GrepException {
		title("H2", "cause", "NOTAB.NOCOL");
		String[] args = new String[] {
				"-d",
				//"-X", "debug", 
				"--warn",
				"-u", DB_USER, "-p", DB_PW,	"-U", DB_URL,
				"cause", "NOTAB.NOCOL"
		};
		ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(actual.isEmpty());
	}

	@Test
	public void testPatternLowerExact_TabNoExistColWild_NoMatches() throws GrepException {
		title("H2", "cause", "NOTAB.*");
		String[] args = new String[] {
				"-d",
				//"-X", "debug", 
				"--warn",
				"-u", DB_USER, "-p", DB_PW,	"-U", DB_URL,
				"cause", "NOTAB.*"
		};
		ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(actual.isEmpty());
	}

	@Test
	public void testPatternLowerExact_TabWildColNonExist_NoMatches() throws GrepException {
		title("H2", "cause", "*.NOCOL");
		String[] args = new String[] {
				"-d",
				//"-X", "debug", 
				"--warn",
				"-u", DB_USER, "-p", DB_PW,	"-U", DB_URL,
				"cause", "*.NOCOL"
		};
		ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(actual.isEmpty());
	}
	
    @Test
    public void testPatternMixedExact_TabWildColWild_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "cAuSe", "*.*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]",
                "CAUSES: 0,cause 1,the cause is mine",
                "CAUSES: 1,cause 2,also mine"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug,trace", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cAuSe", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternUpperExact_TabColWild_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "TED", "HISTORY.*");
        List<String> expected = Arrays.asList(
                "HISTORY: [EVENT,AUTHOR]",
                "HISTORY: completed all outstanding tasks!,craig",
                "HISTORY: crafting new ideas,ted"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TED", "HISTORY.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternMixedExact_TabColWild_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "crA", "HISTORY.*");
        List<String> expected = Arrays.asList(
                "HISTORY: [EVENT,AUTHOR]",
                "HISTORY: completed all outstanding tasks!,craig",
                "HISTORY: crafting new ideas,ted",
                "HISTORY: More Crazy ideas,Stan"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "crA", "HISTORY.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabColWild2_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "stan", "HISTORY.*");
        List<String> expected = Arrays.asList(
                "HISTORY: [EVENT,AUTHOR]",
                "HISTORY: completed all outstanding tasks!,craig",
                "HISTORY: More Crazy ideas,Stan"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "stan", "HISTORY.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabCol2_NameData_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "stan", "HISTORY.AUTHOR");
        List<String> expected = Arrays.asList(
                "HISTORY: [AUTHOR]",
                "HISTORY: Stan"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "stan", "HISTORY.AUTHOR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_Listing_Case() throws GrepException {
        title("H2 (ignore case)", "cause", "*.*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]"
            );
        String[] args = new String[] {
                "-d",
                "-l",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_Listing_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "cause", "*.*");
        List<String> expected = Arrays.asList(
                "CAUSES: [ID,NAME,CAUSE_STUFF]"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                "-l",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattColWild_TwoListing_IgnoreCase() throws GrepException {
        title("H2 (ignore case)", "n", "CA*.*");
        List<String> expected = Arrays.asList(
                "CATALOG: [NAME]",
                "CAUSES: [ID,NAME,CAUSE_STUFF]"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                "-l",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "n", "CA*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }


    private void initDisplay(ResourceGrep rg) {
        display = new DisplayRecorder(rg.getDisplay());
        display.setSortedPaths(false);
        rg.setDisplay(display);
    }
}
