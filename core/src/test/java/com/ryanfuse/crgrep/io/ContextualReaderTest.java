/*
 * (C) Copyright 2013-2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ContextualReaderTest {

	private ContextualReader r;
	@Mock
	private BufferedReader bReader;
	
	@Before
	public void setUp() throws Exception {
		r = new ContextualReader(bReader);
	}

	@Test
	public void testReadEmpty() {
		assertTrue(r.linesBefore().isEmpty());
		assertTrue(r.linesAfter().isEmpty());
	}

	@Test
	public void testReadLineDefaultSizeLargerThanStream() throws IOException {
		// default context size 10
		when(bReader.readLine()).thenReturn("a", "b", "c", "d", "e", null);

		assertEquals("a", r.readLine());
		assertTrue("not empty lines before", r.linesBefore().isEmpty());
		assertFalse("empty lines after", r.linesAfter().isEmpty());
		assertEquals(4, r.linesAfter().size());

		assertEquals("b", r.readLine());
		assertFalse("empty lines before", r.linesBefore().isEmpty());
		assertEquals(1, r.linesBefore().size());
		assertEquals("a", r.linesBefore().get(0));
		assertEquals(3, r.linesAfter().size());
		
		assertEquals("c", r.readLine());
		assertEquals(2, r.linesBefore().size());
		assertEquals("a", r.linesBefore().get(0));
		assertEquals("b", r.linesBefore().get(1));
		assertEquals(2, r.linesAfter().size());

		assertEquals("d", r.readLine());
		assertEquals(3, r.linesBefore().size());
		assertEquals("a", r.linesBefore().get(0));
		assertEquals("b", r.linesBefore().get(1));
		assertEquals("c", r.linesBefore().get(2));
		assertEquals(1, r.linesAfter().size());

		assertEquals("e", r.readLine());
		assertEquals(4, r.linesBefore().size());
		assertEquals("a", r.linesBefore().get(0));
		assertEquals("b", r.linesBefore().get(1));
		assertEquals("c", r.linesBefore().get(2));
		assertEquals("d", r.linesBefore().get(3));
		assertEquals(0, r.linesAfter().size());
		
		assertNull(r.readLine());
	}

	@Test
	public void testReadLineContextSizeLessThanStreamSize() throws IOException {
		// default context size 10
		when(bReader.readLine()).thenReturn("a", "b", "c", "d", "e", "f", null);
		r.setContextSize(1);
		assertEquals(1, r.getContextSize());

		assertEquals("a", r.readLine());
		assertTrue("not empty lines before", r.linesBefore().isEmpty());
		assertFalse("empty lines after", r.linesAfter().isEmpty());
		assertEquals(1, r.linesAfter().size());

		assertEquals("b", r.readLine());
		assertFalse("empty lines before", r.linesBefore().isEmpty());
		assertEquals(1, r.linesBefore().size());
		assertEquals("a", r.linesBefore().get(0));
		assertEquals(1, r.linesAfter().size());
		
		assertEquals("c", r.readLine());
		assertEquals(1, r.linesBefore().size());
		assertEquals("b", r.linesBefore().get(0));
		assertEquals(1, r.linesAfter().size());

		assertEquals("d", r.readLine());
		assertEquals(1, r.linesBefore().size());
		assertEquals("c", r.linesBefore().get(0));
		assertEquals(1, r.linesAfter().size());
		assertEquals("e", r.linesAfter().get(0));
	}
}
