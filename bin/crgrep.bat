@REM ----------------------------------------------------------------------------
@REM
@REM Crgrep start script.
@REM
@REM Author: Craig Ryan [cryan.dublin@gmail.com]
@REM
@REM Environment Variables
@REM
@REM   CRGREP_HOME       (Optional) Home directory for crgrep
@REM   CRGREP_OPTS       (Optional) Java runtime options used when the command is
@REM                      executed.
@REM
@REM   CRGREP_TMPDIR     (Optional) Directory path location of temporary directory
@REM                      the JVM should use (java.io.tmpdir).  Defaults to
@REM                      $CRGREP_BASE/temp.
@REM
@REM   JAVA_HOME          Must point at your Java Development Kit installation.
@REM
@REM   CRGREP_JVM_OPTS   (Optional) Java runtime options used when the command is
@REM                       executed.
@REM
@REM -----------------------------------------------------------------------------

@echo off

set ERROR_CODE=0

:init

@REM Locate where crgrep is in filesystem
@REM and decide how to startup depending on the version of windows

@REM -- Win98ME
if NOT "%OS%"=="Windows_NT" goto Win9xArg

@REM set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" @setlocal

@REM -- 4NT shell
if "%eval[2+2]" == "4" goto 4NTArgs

@REM -- Regular WinNT shell
set CMD_LINE_ARGS=%*
goto WinNTGetScriptDir

@REM The 4NT Shell from jp software
:4NTArgs
set CMD_LINE_ARGS=%$
goto WinNTGetScriptDir

:Win9xArg
@REM Slurp the command line arguments.  This loop allows for an unlimited number
@REM of arguments (up to the command line limit, anyway).
set CMD_LINE_ARGS=
:Win9xApp
if %1a==a goto Win9xGetScriptDir
set CMD_LINE_ARGS=%CMD_LINE_ARGS% %1
shift
goto Win9xApp

:Win9xGetScriptDir
set SAVEDIR=%CD%
%0\
cd %0\..\.. 
set CRGREP_HOME=%CD%
cd %SAVEDIR%
set SAVE_DIR=
goto initJava

:WinNTGetScriptDir
set CRGREP_HOME=%~dp0\..

:initJava

@REM Determine if JAVA_HOME is set and if so then use it

if not "%JAVA_HOME%"=="" goto found_java_home
echo Please set JAVA_HOME environment variable to your Java Virtual Machine directory and re-try.
goto end

:found_java_home
if exist "%JAVA_HOME%\bin\java.exe" goto found_java
if exist "%JAVA_HOME%\java.exe" goto found_java_bin
echo JAVA_HOME environment variable is not set to a valid directory
echo Please set JAVA_HOME environment variable to your Java Virtual Machine directory and re-try.
goto end

:found_java_bin
echo JAVA_HOME environment variable is set to the Java 'bin' directory
echo Please set JAVA_HOME to your Java Virtual Machine directory, containing 'bin', and then re-try.
goto end

:found_java
set CRGREP_JAVACMD=%JAVA_HOME%\bin\java.exe

if not "%CRGREP_HOME%" == "" goto found_home
echo Error: cannot determine a value for CRGREP_HOME environment variable.
echo Please manually set CRGREP_HOME to the directory in which you installed CRGREP and re-try.
goto end

:found_home
if exist "%CRGREP_HOME%\bin\crgrep.bat" goto found_bin
echo CRGREP_HOME environment variable is set to %CRGREP_HOME% which is not a valid CRGREP installation directory
echo Please manually set CRGREP_HOME to the directory in which you installed CRGREP and re-try.
goto end

:found_bin
if not "%CRGREP_TMPDIR%"=="" goto afterTmpDir
set CRGREP_TMPDIR=%CRGREP_HOME%\temp
if not exist "%CRGREP_TMPDIR%" mkdir "%CRGREP_TMPDIR%"

:afterTmpDir

@REM OCR support
if "%TESSDATA_PREFIX%" == "" (
   set TESSDATA_PREFIX=%CRGREP_HOME%\ocr
)
IF %PROCESSOR_ARCHITECTURE% == x86 IF NOT DEFINED PROCESSOR_ARCHITEW6432 (
   set CRGREP_LIB_PATH="%PATH%;%CRGREP_HOME%\ocr;%CRGREP_HOME%\lib\x86"
) ELSE (
   set CRGREP_LIB_PATH="%PATH%;%CRGREP_HOME%\ocr\x64;%CRGREP_HOME%\lib\x64"
)

@REM echo Using CRGREP_HOME:   %CRGREP_HOME%
@REM echo Using CRGREP_TMPDIR: %CRGREP_TMPDIR%
@REM echo Using CRGREP_LIB_PATH: %CRGREP_LIB_PATH%
@REM echo Using JAVA_HOME:      %JAVA_HOME%

@REM set CRGREP_SM=
@REM if "%CRGREP_SECURE%" == "false" goto postSecure
@REM Make Crgrep run with security Manager enabled
@REM set CRGREP_SM="-Djava.security.manager"
@REM :postSecure

@REM Crgrep system properties
@REM set CRGREP_JVM_OPTS=%CRGREP_JVM_OPTS% -Dblah

@REM uncomment to enable remote debugging 
@REM set DEBUG=-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=y

"%CRGREP_JAVACMD%" %JAVA_OPTS% -classpath "%CRGREP_HOME%\lib\*" %DEBUG% -Dcrgrep.home="%CRGREP_HOME%" -Dcrgrep.tessdata="%TESSDATA_PREFIX%" %CRGREP_JVM_OPTS% -Djna.library.path=%CRGREP_LIB_PATH% com.ryanfuse.crgrep.ResourceGrep %CMD_LINE_ARGS%

if %ERRORLEVEL% NEQ 0 goto error
goto end

:error
if "%OS%"=="Windows_NT" @endlocal
set ERROR_CODE=%ERRORLEVEL%

:end
@REM set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" goto endNT

@REM For old DOS remove the set variables from ENV - we assume they were not set
@REM before we started - at least we don't leave any baggage around
set CMD_LINE_ARGS=
goto postExec

:endNT
@REM If error code is set to 1 then the endlocal was done already in :error.
if %ERROR_CODE% EQU 0 @endlocal


:postExec

if "%FORCE_EXIT_ON_ERROR%" == "on" (
  if %ERROR_CODE% NEQ 0 exit %ERROR_CODE%
)

exit /B %ERROR_CODE%

